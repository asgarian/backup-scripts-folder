from Deploy.DeployUtils.DeployLocation import DeployLocation

__author__ = 'mohammad'

import os
import re
from _weakref import proxy
from Utils.Utils import am_i_newjenkins, make_section, blue, warning, error
from Utils.jfabric import proxy, skip_output
from Utils.jfabric import run, host, local


class DiagnosticsAgent:
    def __init__(self, job_name, location):
        self.job_name = job_name
        self.location_info = DeployLocation(location)

        self.initialize_host()
        self.prepare_host()

    def initialize_host(self):
        if self.job_name == "Tir":
            self.user = "iru"
        else:
            with host("2013"):
                line = run("cat auto/{}/customsnameswithurls | grep {}=".format(self.job_name,self.location_info.code))
                self.user = re.search("\d+=(.+)@(.+)", line).group(1)

        ip = self.location_info.url
        self.host = "{}@{}".format(self.user, ip)

    def prepare_host(self):
        with host("2013"):
            run("ssh-copy-id -i jenkins/id_rsa.pub {}".format(self.host))
            with skip_output():
                password_line = run("python /home/mkt/abz/decrypt.py -s {}".format(self.location_info.code))
            self.password = password_line.replace("\n", "")

        local("proxychains ssh {} -o StrictHostKeyChecking=no uptime".format(self.host))

    def run(self):
        with proxy(not am_i_newjenkins()), host(self.host), skip_output():
            propertice_files = self.find_config_file()
            for file in propertice_files:
                file_name = os.path.basename(file.strip("/"))
                content = run("cat {}".format(file))

                with make_section(file_name):
                    if self.job_name == "Customs":
                        self.check_config_customs(file_name, content)
                    else:
                        print(content)

    def find_config_file(self):
        if self.job_name == "Customs":
            propertice_files = ["/tomcat/webapps/Customs/WEB-INF/classes/asycuda.properties",
                                "/tomcat/webapps/Customs/WEB-INF/classes/Config.properties",
                                "/tomcat/webapps/Customs/WEB-INF/classes/customsdecleration.properties",
                                "/tomcat/webapps/Customs/WEB-INF/classes/zones.properties",
                                "/tomcat/webapps/Customs/WEB-INF/classes/ir/setad/customs/fecade/config/layers.properties",
                                "/tomcat/webapps/Customs/WEB-INF/classes/ir/customs/saloon/config/maliat.properties"]
        else:
            file_string = run(
                "ls /tomcat/webapps/{}/WEB-INF/classes/*.properties".format(self.job_name).replace("Acc", "acc"))
            propertice_files = file_string.split("\n\n")
            for file in propertice_files:
                if file == "private.properties":
                    propertice_files.remove(file)

        return propertice_files

    def check_config_customs(self, file_name, content):
        if file_name == "asycuda.properties":
            self.check_asycuda_properties(content)
        elif file_name == "maliat.properties":
            self.check_maliat_properties(content)
        elif file_name == "customsdecleration.properties":
            self.check_customsdecleration_properties(content)
        else:
            print(content)

    def check_asycuda_properties(self, content):
        gomrok_code = re.search('gomrok=(\d\d\d\d\d+?)', content)
        if gomrok_code:
            gomrok_code = gomrok_code.group(1)
            print("current code is: " + gomrok_code)
            print("original code is: " + self.location_code)
            if gomrok_code == self.location_code:
                print(blue("so current code is true"))
            else:
                print(warning("so current code is false"))
        else:
            print(error("code gomrok not found"))

    def check_maliat_properties(self, content):
        this_year = "1395"
        temp = re.search(this_year, content)
        if temp:
            print(blue("OK {} is exist".format(this_year)))
        else:
            print(warning("Warning 1395 dont exist"))

    def check_customsdecleration_properties(self, content):
        temp = re.search(self.location_code, content)
        if temp:
            print(blue("code gomrok FOUND"))
        else:
            print(warning("code gomrok DONT FOUND"))
