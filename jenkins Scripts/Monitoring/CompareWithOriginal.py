import os

from Monitoring.DiagnosticsAgent import DiagnosticsAgent
from Utils.Utils import make_section, warning, am_i_newjenkins, run
from Utils.jfabric import host, get_content, proxy

__author__ = 'mohammad'


class CompareWithOriginal:
    def __init__(self, job_name, location, original_job_name):
        self.root = os.path.basename(location.strip("/")).split("@")[0]
        if "/" not in location:
            self.root = "ROOT"
        connector = DiagnosticsAgent(job_name, location.replace(":8080/Customs", "").replace(":8080", "").replace(
            "/new2", ""))
        self.host = connector.host
        self.config_files = ["WEB-INF/classes/asycuda.properties",
                             "WEB-INF/classes/Config.properties",
                             "WEB-INF/classes/customsdecleration.properties",
                             "WEB-INF/classes/zones.properties",
                             "WEB-INF/classes/ir/setad/customs/fecade/config/layers.properties",
                             "WEB-INF/classes/ir/customs/saloon/config/maliat.properties"]
        self.original_job_name = original_job_name
        self.job_name = job_name

    def read_from_2013(self):

        with host("2013"):
            return self.read_files(self.original_job_name)

    def read_from_location(self):
        with proxy(not am_i_newjenkins()), host(self.host):
            return self.read_files("/tomcat/webapps/{}".format(self.root))

    def read_files(self, src):
        print(src)
        set_of_contents = {}
        for file in self.config_files:
            filename = os.path.basename(file.strip("/"))
            content_string = get_content(src + "/" + file)
            content = content_string.split("\n")
            for line in content:
                if line.startswith("ProxyChains"):
                    content.remove(line)
            set_of_contents.update({filename: content})

        return set_of_contents

    def run(self):
        original_content = self.read_from_2013()
        location_content = self.read_from_location()
        self.compare(original_content, location_content)
        self.remove_job_from_2013()

    def compare(self, original_content, location_content):
        for key in original_content:
            with make_section(key):
                if len(original_content[key]) != len(location_content[key]):
                    print(warning(
                        "Number of Lines In Server 2013 is {0} But Number of Lines In this Server is {1} ".format(
                            len(original_content[key]), len(location_content[key]))))

                temp = original_content[key]
                if len(original_content[key]) > len(location_content[key]):
                    temp = location_content[key]

                for line in range(len(temp)):
                    if original_content[key][line] != location_content[key][line]:
                        print("Expected: " + original_content[key][line])
                        print("Current: " + location_content[key][line])
                        print(warning("Values Above Is Not Equal"))

    def remove_job_from_2013(self):
        with host("2013"):
            run("rm -r {}".format(self.original_job_name))
