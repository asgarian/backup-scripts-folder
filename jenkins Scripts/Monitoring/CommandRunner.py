from Monitoring.DiagnosticsAgent import DiagnosticsAgent
from Utils.Utils import make_section
from Utils.jfabric import host, run

__author__ = 'mohammad'


class CommandRunner:
    def __init__(self, command, location):
        self.command = command
        self.location = location
        location = location.split("@")
        self.location_name = location[0]
        self.location_code = location[1]
        self.location_ip = location[2]

    def run(self):
        connector = DiagnosticsAgent(job_name="Customs", location=self.location)
        self.host = connector.host

        with host(self.host):
            with make_section(self.command):
                run(self.command)
