__author__ = 'mohammad'

from dateutil.parser import parse

from Deploy.Observable import Observable
import configs
from Utils.Utils import try_get_url, warning, urljoin, HttpProxy
from datetime import datetime


class Inspector(Observable):
    def __init__(self, location, job_name):
        self.location = location
        self.name = job_name

    def get_build_date(self):
        if self.name in configs.build_date_postfix:
            build_date_postfix = configs.build_date_postfix[self.name]
        else:
            build_date_postfix = "/buildDate"
        buil_date_url = urljoin("http://" + self.location.url, build_date_postfix)
        build_date_byte = try_get_url(buil_date_url, try_count=1, timeout=30, sleep_time=30)
        if build_date_byte is None:
            self.set_status(warning("Error: Cannot get buildDate from {}".format(self.location.url)))
            return

        try:
            if build_date_byte.isdigit():
                build_date = datetime.fromtimestamp(int(build_date_byte) / 1000)
            else:
                build_date = parse(build_date_byte)
        except ValueError as ex:
            raise Exception("invalid date from site: {}".format(build_date_byte)) from ex

        build_date = build_date.replace(tzinfo=None)
        build_date_str = str(build_date_byte)
        self.set_status(build_date_str.replace("\n",""))
        return build_date

    def get_name_for_observer(self):
        return self.location.name
