from Utils.Operation import Operation
from Utils.Utils import get_jenkins_build_json

__author__ = 'borna'


class AfterDeployTestInfo(object):
    def __init__(self, sysargs):
        self.sysargs = sysargs
        self.build_id = sysargs[2]
        self.name = sysargs[3]
        self.selenium_path = sysargs[4]
        info = Operation(sysargs[1], sysargs[2])
        json_obj = get_jenkins_build_json(info)
        if json_obj["actions"][1] == {}:  # test was called from customs
            (self.user, self.password, self.loc) = self.get_test_args(json_obj)  # args = [user, pass, loc]
        else:  # test was run from ADT itself
            (self.user, self.password, self.loc) = ("0070408831", "123456Qwe", "forTest@12345@10.64.0.18:8080")

    def Can_Test(self):
        return self.user and self.password

    def get_seleniumAgentArgs(self):
        return self.name, self.build_id, self.selenium_path+self.build_id

    def get_ADT_args(self):
        return self.user, self.password, self.loc, self.build_id

    def get_test_args(self, current_build_json):
        upstream_info = Operation(current_build_json["actions"][2]["causes"][0]["upstreamProject"]
                                  , current_build_json["actions"][2]["causes"][0]["upstreamBuild"])
        json_upstream = get_jenkins_build_json(upstream_info)
        user = json_upstream["actions"][0]["parameters"][6]["value"]
        password = json_upstream["actions"][0]["parameters"][7]["value"]
        loc = json_upstream["actions"][0]["parameters"][5]["value"]
        return user, password, loc
