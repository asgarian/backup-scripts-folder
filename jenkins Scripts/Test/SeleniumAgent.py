import configs
from Utils.Utils import run_windows_command, get_json_from_jenkins

__author__ = 'borna'


class SeleniumAgent:
    def __init__(self, name, build_id, seleniumtest_path):
        self.name = name
        self.build_id = build_id
        self.seleniumtest_path = seleniumtest_path

    def run_corresponding_script(self, name, arg1, arg2):
        return run_windows_command("C:\\\\JenkinsScripts\\\\ADTest\\\\{}.ps1 {} {}".format(name, arg1, arg2))

    def find_idle_node_from_2032(self):
        while True:
            for i in range(1, 4):
                with open("/jenkins/test_nodes/" + str(i), 'r') as node_state:
                    content = node_state.read()
                    if content == "" or content == "\n":
                        return i
                    else:
                        content_parts = content.split('-')
                        node_occupier = get_json_from_jenkins(
                            "http://127.0.0.1:8080/job/{}/{}/api/json".format(content_parts[0],
                                                                              content_parts[1].replace("\n", "")))
                        if not node_occupier["building"]:
                            return i

    def check_idle_node_on_2034(self, idle_node):
        return run_windows_command(
            "Get-Content C:\\\\Test-Nodes\\\\Test-Node-{}\\\\SeleniumAutoRunnerTrigger\\\\State.txt".format(
                idle_node)) == "idle\n\n"

    def find_idle_node(self):
        while True:
            idle_node = self.find_idle_node_from_2032()
            if self.check_idle_node_on_2034(idle_node):
                return idle_node

    def occupy_node(self, idle_node):
        with open("/jenkins/test_nodes/" + str(idle_node), 'w') as node_state:
            node_state.truncate()
            node_state.write(self.name + "-" + self.build_id)

    def free_node(self, idle_node):
        with open("/jenkins/test_nodes/" + str(idle_node), 'w') as node_state:
            node_state.truncate()

    def run_selenium_test(self):
        if not configs.alaki:
            idle_node = self.find_idle_node()
            self.occupy_node(idle_node)
            node_root = "C:\\\\Test-Nodes\\\\Test-Node-" + str(idle_node) + "\\\\SeleniumAutoRunnerTrigger"
            try:
                self.run_corresponding_script("Remove", node_root + "\\\\Snapshot.png", "")
                self.run_corresponding_script("Remove", node_root + "\\\\TestResults\\\\*", "")
                self.run_corresponding_script("Remove", node_root + "\\\\TestResult.txt", "")
                self.run_corresponding_script("Set_Content", node_root + "\\\\TestCases.txt", self.seleniumtest_path)
                self.run_corresponding_script("Set_Content", node_root + "\\\\Id.txt"
                                              , self.build_id + " " + self.name)
                self.run_corresponding_script("Set_Content", node_root + "\\\\RunTest.txt", "1")
                self.run_corresponding_script("FetchResult", node_root, self.build_id)
                print("Test Number -" + self.build_id + "- Passed!")
            except Exception as ex:
                result_path = node_root + "\\\\TestResult.txt"
                result_content = run_windows_command(
                    "'Get-Content " + result_path + " | Select-String 'interact' -quiet'")
                remoter = "C:\\\\Remoter\\\\id.txt"
                if result_content:
                    self.run_corresponding_script("Set_Content", remoter, idle_node)
                raise ex
            finally:
                self.free_node(idle_node)
