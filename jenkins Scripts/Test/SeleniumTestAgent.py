from Build.BuildInfo import BuildInfo
from Test.Package import Package
from Test.UnitTestGenerator import UnitTestGenerator
from Utils.Utils import *


class SeleniumTestAgent(object):
    def __init__(self, test_info):
        self.test_info = test_info
        self.testing_project = get_upstream_build(test_info)
        if self.testing_project:
            self.testing_project = BuildInfo(self.testing_project)

    def run(self):
        if self.testing_project !="Swich_test":
                upstream_json = get_jenkins_build_json(self.testing_project)
                project_branch = [action["lastBuiltRevision"]["branch"][0]["name"] for action in upstream_json["actions"] if
                                  "lastBuiltRevision" in action][0]
                command = "C:\\\\JenkinsScripts\\\\Run-Test.ps1 {} {}".format(self.test_info.to_powershell_arg(),
                                                                              project_branch[project_branch.find(
                                                                                  "origin") + 7:project_branch.__len__()])

        if not configs.alaki:
            run_windows_command(command)

            try:
                package_maker = Package()
                print("Building Package with name: " + package_maker.last_stable_builds_package_name())
                package_maker.build_package()
            except Exception as ex:
                print(error("Error in creating package and sending it to Deploy Jenkins"))
                print(str(ex))

        generator = UnitTestGenerator(self.test_info)
        generator.run_immediate_parts()
