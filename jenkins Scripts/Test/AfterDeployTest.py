from Test.SeleniumAgent import SeleniumAgent
from Utils.Utils import run_windows_command

__author__ = 'borna'


class AfterDeployTest(SeleniumAgent):
    def __init__(self, info):
        name, build_id, seleniumtest_path = info.get_seleniumAgentArgs()
        super().__init__(name, build_id, seleniumtest_path)
        self.user, self.password, self.location, self.build_id = info.get_adt_args()

    def tunnel_to_location(self):
        location_parts = self.location.split('@')
        port = 6050
        tunnel_out = run_windows_command("C:\\\\JenkinsScripts\\\\{}.ps1 {} {} {} {} {}".format("Establish-Tunnel"
                                                                                                , "?"
                                                                                                , port,
                                                                                                location_parts[2],
                                                                                                "0",
                                                                                                "zero"))
        parsed_tunnel_out = tunnel_out.split("\n\n")
        tunnel_port = parsed_tunnel_out[parsed_tunnel_out.__len__() - 4].replace("$port=", "")
        tunnel_id = parsed_tunnel_out[parsed_tunnel_out.__len__() - 5].replace("$id=", "")
        return tunnel_port, tunnel_id

    def destroy_tunnel(self, tunnel_id):
        run_windows_command("C:\\\\JenkinsScripts\\\\{}.ps1 {}".format("Destroy-Tunnel", tunnel_id))

    def make_selenium(self, tunnel_port):
        run_windows_command("C:\\\\JenkinsScripts\\\\ADTest\\\\{}.ps1 {} {} {} {}".format("Make_Sel"
                                                                                          ,
                                                                                          "http://127.0.0.1:{}/".format(
                                                                                              tunnel_port)
                                                                                          , self.user
                                                                                          , self.password
                                                                                          , self.build_id))

    def run(self):
        tunnel_id = 1
        try:
            (tunnel_port, tunnel_id) = self.tunnel_to_location()
            print("TUNNEL ID IS : "+tunnel_id)
            self.make_selenium(tunnel_port)
            self.run_selenium_test()
        finally:
            self.destroy_tunnel(tunnel_id)
            print("Test SnapShot Available at :")
            print("http://172.17.20.32:8080/userContent/Monitoring/SnapShot/Snapshot{}.png".format(self.build_id))
            print("")
            self.run_corresponding_script("Remove", "C:\\\\JenkinsScripts\\\\ADTest\\\\suit" + self.build_id, "")
