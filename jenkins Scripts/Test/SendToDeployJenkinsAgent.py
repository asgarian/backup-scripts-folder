import requests

from Deploy.DeployUtils.ChangesAgent import ChangesAgent
from Utils.Utils import get_last_version
from Utils.jfabric import host, put, local


class SendToDeployJenkinsAgent:
    def __init__(self, testing_project):
        self.testing_project = testing_project

    def send_to_deploy_jenkins(self):
        sb_folder = self.testing_project.get_sb_folder()

        local("du -h {}".format(sb_folder))

        with host("2032"):
            put(sb_folder, sb_folder)

        self.build_in_deploy_jenkins()

        self.set_change_log()

    def build_in_deploy_jenkins(self):
        payload = {'original_build_number': self.testing_project.number}
        r = requests.post(
            "http://newjenkins:da00ed9b7af94a088b637d9c312e68c7@172.17.20.32:8080/job/" +
            self.testing_project.name + "/buildWithParameters",
            data=payload)
        return r.status_code

    def set_change_log(self):
        agent = ChangesAgent(self.testing_project.name)
        agent.set_change_log_in_deploy_jenkins(1, int(get_last_version(self.testing_project.name)))
