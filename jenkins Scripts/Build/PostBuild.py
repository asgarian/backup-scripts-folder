import ntpath
from os.path import join

from Utils.Utils import get_items_path
from Utils.jfabric import lcd, local
import configs


class PostBuild:
    def __init__(self, build_info):
        self.build_info = build_info
        self.name = build_info.name
        self.number = build_info.number
        self.workspace = build_info.get_workspace()

    def run(self):
        if self.name == "Center-customs-iru-tir-epd":
            local("/bin/bash /home/ut/EPD_replace/replace.sh")

        if self.name in configs.endpoints:
            self.copy_artifact_to_sb()

        if self.name in configs.aut_projects:
            src_backup_folder = join(self.build_info.get_temp_folder(), "aut-src-backup")
            with lcd(src_backup_folder):
                local("zip -mr {} *".format(self.build_info.get_artifact_path()))

    def copy_artifact_to_sb(self):
        artifact_folder = self.build_info.get_artifact_folder()
        workspace_folder = self.build_info.get_workspace()

        if self.name in configs.maven_projects:
            target_folder = join(workspace_folder, "target")
            war_path = get_items_path(join(target_folder, "*.war"))
            if war_path:
                war_name = ntpath.basename(war_path[0])
                with lcd(artifact_folder):
                    local("cp {} {}".format(war_path[0], war_name))
            else:
                raise Exception("no war in {}".format(target_folder))

        if self.name in (configs.php_projects + configs.nodejs_projects):
            with lcd(workspace_folder):
                local("jar -cf  {}.war .".format(join(artifact_folder, self.name)))
