from datetime import time
import os

import requests

from Utils.Utils import get_json_from_jenkins, recreate_folder
from Utils.jfabric import host, local
from Utils.jfabric import get
import configs


class post_deploy:
    class testing_project:
        def __init__(self):
            pass

        BUILD_NUMBER = None
        JOB_NAME = None

    def __init__(self, buildinfo):
        print("###creating aut_agent")
        print("updating agent project info")

        ###first get last build url
        j = get_json_from_jenkins("http://172.16.111.8:8080/job/AUT%20Processing/")
        last_buid_url = j["builds"][0]["url"]
        ###get last aut build json and last upstream test url
        j2 = get_json_from_jenkins(last_buid_url)
        job_test_b_number = j2["actions"][0]["causes"][0]["upstreamBuild"]
        job_test_addr = j2["actions"][0]["causes"][0]["upstreamUrl"]
        job_test_url = "http://172.16.111.8:8080/{}{}".format(job_test_addr, job_test_b_number)
        ###get last main project build json
        j3 = get_json_from_jenkins(job_test_url)
        ###uodating information
        self.testing_project.BUILD_NUMBER = str(j3["actions"][1]["causes"][0]["upstreamBuild"])
        self.testing_project.JOB_NAME = j3["actions"][1]["causes"][0]["upstreamProject"]

        print("aut testing project updated : " + self.testing_project.JOB_NAME)
        print("version : " + self.testing_project.BUILD_NUMBER)

    def run(self):
        if self.testing_project.JOB_NAME in configs.aut_projects:
            self.aspect_post_processing()

    def aspect_post_processing(self):

        # if not Configs.alaki:
        self.download_aspect_logs()

        log_files_path_local_uncompressed = self.log_files_path_local + "2/"
        if self.testing_project.JOB_NAME in configs.time_monitor_projects:
            tag = self.testing_project.BUILD_NUMBER + "_" + time.strftime("%y/%m/%d-%H.%M")
            # AUTParser-1.0-SNAPSHOT.jar
            # os.system(
            #    "java -jar /home/ut/AUT/TimeParser/TimeParser-1.0.jar {}/ {} {}".format(self.log_files_path_local,
            #                                                                          log_files_path_local_uncompressed,
            #
            #                                                                        tag))
            local("mkdir -p {}".format(log_files_path_local_uncompressed))
            os.system(
                "java -jar /home/ut/AUT/TimeParser/AUTParser-1.0-SNAPSHOT.jar -unzip {}/ {}".format(
                    self.log_files_path_local,
                    log_files_path_local_uncompressed))
            os.system(
                "java -jar /home/ut/AUT/TimeParser/AUTParser-1.0-SNAPSHOT.jar -time  {} {} {}".format(
                    log_files_path_local_uncompressed, tag, self.testing_project.JOB_NAME))
            ###comparing time
            self.check_time()

        if self.testing_project.JOB_NAME in configs.test_generating_projects:
            ###test generating process
            print("generating new unit tests in : ")
            ##source_folder initializing
            source_folder = "/home/ut/AUT/{}/test/{}".format(self.testing_project.JOB_NAME,
                                                             self.testing_project.BUILD_NUMBER)
            print(source_folder)
            ##create test folder
            local("mkdir -p {}".format(source_folder))
            ##run parser with -update option
            os.system(
                "java -jar /home/ut/AUT/TimeParser/new-AUTParser-1.0-SNAPSHOT.jar -update {} {} {}".format(
                    log_files_path_local_uncompressed, source_folder, self.testing_project.BUILD_NUMBER))
            ##commit

    def download_aspect_logs(self):
        log_files_path_remote = os.path.join('/tomcat/AUT/', self.testing_project.JOB_NAME, '*')
        self.log_files_path_local = os.path.join('/home/ut/AUT/', self.testing_project.JOB_NAME, 'Logs',
                                                 self.testing_project.BUILD_NUMBER, "STU")
        recreate_folder(self.log_files_path_local)

        # if not Configs.alaki:
        local("curl http://172.16.111.113:8080/" + self.testing_project.JOB_NAME + "/AUTController?action=write")
        with host("113"):
            get(log_files_path_remote, self.log_files_path_local)

    def check_time(self):
        print("analyzing runing time ...")
        # Open database connection
        db = MySQLdb.connect("172.16.111.113", "root", "mysql123sjb", "PerformanceRecords")
        # prepare a cursor object
        cursor = db.cursor()
        # make string guery
        project_name = self.testing_project.JOB_NAME
        sql = "SELECT * FROM info WHERE project = ('" + project_name + "') ORDER BY idinfo DESC LIMIT 0, 1"
        # execute query
        cursor.execute(sql)
        ##get results by fetch
        results = cursor.fetchall()
        if len(results) > 1:
            old_time = results[1][3]
            new_time = results[0][3]
            if new_time > old_time:
                self.sms_admin(project_name, self.testing_project.BUILD_NUMBER, old_time, new_time)
        print("Check Time Finished")

    def sms_admin(self, project_name, build_number, old_time, new_time):
        phone1 = '00989376682477'
        phone2 = '00989112353455'
        msg = 'runing time is increased in project' + project_name + ' build : ' + build_number + ' old time:' + old_time + '< new time: ' + new_time
        print("sms")
        url = 'http://172.17.20.10:8080/SMSManager/'
        req1 = "http://172.16.111.63:8080/SMSManager/send?Recipient={}&Message={}&USE_CODING=false".format(phone1, msg)
        requests.post(req1)

        req2 = "http://172.16.111.63:8080/SMSManager/send?Recipient={}&Message={}&USE_CODING=false".format(phone2, msg)
        requests.post(req2)
