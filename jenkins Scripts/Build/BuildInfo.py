import ntpath
from os.path import join

import configs
from Utils.Operation import Operation
from Utils.Utils import get_build_from_deputy_url, get_items_path


class BuildInfo(Operation):
    def __init__(self, value):
        if isinstance(value, list):
            name = value[0]
            number = value[1]
        if isinstance(value, Operation):
            name = value.name
            number = value.number
        elif isinstance(value, str) and len(value) != 0:
            if len(value) != 0:  # sample:http://172.16.111.8:8080/job/Customs/488/
                self.deputy_build_url = value
                name, number = get_build_from_deputy_url(value)
                self.BUILD_URL = "http://newjenkins.iais.ir/job/{}/{}/".format(name, number)

        super().__init__(name, number)

    # get Stable build folder path
    # sample output: /stable-builds/Customs/21
    def get_sb_folder(self):
        return join(configs.sb_folder, self.name, self.number)

    # get artifact build folder path
    # sample output: /stable-builds/Customs/21/artifacts
    def get_artifact_folder(self):
        return join(self.get_sb_folder(), "artifacts")

    # sample output(mavens): /stable-builds/Customs/492/artifacts/Customs-5.5.3-r000.war
    # sample output(phps): /stable-builds/Mis/492/artifacts/Mis.zip
    def get_artifact_path(self):
        artifact_folder = self.get_artifact_folder()
        artifacts = get_items_path(join(artifact_folder, "*.war"))
        if artifacts:
            return artifacts[0]
        else:
            raise Exception("No war in {}".format(artifact_folder))

    # sample output(maven): Customs-5.5.3-r000.war
    # sample output(php): Mis.zip
    def get_artifact_name(self):
        artifact_path = self.get_artifact_path()
        return ntpath.basename(artifact_path)

    def get_build_date(self):
        sb_folder = self.get_sb_folder()
        build_date_path = join(sb_folder, "buildDate.txt")
        with open(build_date_path, "r") as build_date_file:
            return build_date_file.read().replace('\n', '')

    def get_group_name(self):
        if self.name in configs.maven_projects:
            return "Maven"
        if self.name in configs.php_projects:
            return "Php"
        if self.name in configs.asp_projects:
            return "Asp"
        if self.name in configs.nodejs_projects:
            return "Nodejs"

    # sample output: /j/jobs/Customs/workspace
    def get_workspace(self):
        return join(configs.jenkins_home, "jobs/{}/workspace".format(self.name))
