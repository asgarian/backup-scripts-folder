import os

import configs


#
# Every things that is done by a jenkins build
# that has a name(Job name) and number(build number)
# it can be either a Build or a Test or a Deploy.
#
class Operation(object):
    def __init__(self, name, number):
        self.name = name
        self.number = number

    # sample output: /jenkins/temp/Mis_deploy/23
    # Responsible for creating the folder
    def get_temp_folder(self):
        folder_path = os.path.join(configs.temp_folder, self.name.replace(" ", "_"), self.number)
        from Utils import Utils
        Utils.create_folder(folder_path)
        return folder_path
