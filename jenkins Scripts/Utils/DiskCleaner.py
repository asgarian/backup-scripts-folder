import os
import time
from datetime import datetime, timedelta
from os.path import join

import configs
from Utils.Operation import Operation
from Utils.Utils import get_items_path, get_jenkins_build_json, blue, make_section
from Utils.jfabric import lcd, local

__author__ = 'mohammad'


class DiskCleaner():
    def __init__(self):
        self.today = datetime.fromtimestamp(time.time())

    def run(self):
        print(self.today)
        print(blue("Removing content of temp folder"))
        local("sudo rm -r /jenkins/temp/*")
        print(blue("Removing unstable builds"))
        self.delete_unstable_builds()


    def delete_unstable_builds(self):
        for job in configs.endpoints:
            builds = get_items_path(join(configs.sb_folder, job, "*"))
            for build in builds:

                build_folder = os.path.basename(build.strip("/"))
                build_folders = build_folder.split("-")
                build_number = build_folders[0]
                try:
                    build_json = get_jenkins_build_json(Operation(job, build_number))
                    build_date = datetime.fromtimestamp(build_json["timestamp"] / 1000)
                    if "result" in build_json and build_json[
                        "result"] != "SUCCESS" and self.today > build_date + timedelta(days=14):
                        sb_folder = join(configs.sb_folder, job, build_folder)
                        local("sudo rm -r {}".format(sb_folder))
                        print("{} is Removed".format(sb_folder))
                except Exception as ex:
                    print(str(ex))
