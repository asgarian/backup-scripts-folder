import os

from Utils.Utils import get_items_path, blue, green, error
from Utils.jfabric import *
import configs


def add_public_key(public_key_id, servers):
    public_key = get_pubic_key_by_id(public_key_id)

    servers = servers.split(",")
    for server in servers:
        with host(server), cd(".ssh"):
            print(blue("server {}:".format(server)))
            add_to_file_if_not_exist(configs.authorized_keys_filename, public_key)
            print(os.linesep)


def add_to_file_if_not_exist(file_name, new_line):
    if exists(file_name):
        file_content = get_content(file_name)
        lines = file_content.split(os.linesep)
    else:
        lines = []

    print("Before:")
    print(lines)

    if new_line not in lines:
        lines.append(new_line)
        print(green("public key added successfully."))
    else:
        print(blue("public key already exists."))

    lines = list(set(lines))
    put_content(file_name, os.linesep.join(lines))

    print("After:")
    print(get_content(file_name))


def remove_public_key(public_key_id, servers):
    if public_key_id == "ALL":
        public_key = public_key_id
    else:
        public_key = get_pubic_key_by_id(public_key_id)

    servers = servers.split(",")

    for server in servers:
        try:
            with host(server), cd(".ssh"):
                if public_key == "ALL":
                    remove_all_public_keys(configs.authorized_keys_filename)
                else:
                    remove_if_exist(configs.authorized_keys_filename, public_key)
        except Exception as ex:
            print(error(str(ex)))


def remove_if_exist(file_name, public_key):
    if not exists(file_name):
        return

    file_contant = get_content(file_name)
    lines = file_contant.split(os.linesep)

    print("Before:")
    print(lines)

    for key in lines:
        if key.startswith(public_key) or key.endswith(public_key):
            lines.remove(key)
            print("key {} was removed from host {}".format(key, get_current_host()))

    lines.append(get_my_public_key())

    lines = list(set(lines))
    put_content(file_name, os.linesep.join(lines))

    print("After:")
    print(get_content(file_name))


def remove_all_public_keys(file_name):
    my_public_key = get_my_public_key()
    put_content(file_name, my_public_key)


def get_my_public_key():
    with open('/opt/tomcat/.ssh/id_rsa.pub', 'r') as file:
        my_public_key = file.read().replace(os.linesep, '')
        return my_public_key


def get_pubic_key_by_id(id):
    path = "/public_keys/{}".format(id)
    if not get_items_path(path):
        raise Exception("Error: no public key: {}".format(path))

    with open(path) as file:
        lines = file.readlines()

        if len(lines):
            return lines[0].replace("\n", "").replace("\r", "")
        else:
            raise Exception("Error: public key file ({}) is empty".format(path))

    raise Exception("Error: error in reading {}".format(path))
