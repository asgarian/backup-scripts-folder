from os.path import join

jenkins_home = "/j/"
user_contet_folder = join(jenkins_home, "userContent")
deploy_reports_folder = join(user_contet_folder, "DeployReports")
scripts_home = "/scripts/"
temp_folder = "/jenkins/temp"
configs_folder = "/configs"
sb_folder = "/stable-builds"

time_monitor_projects = ['Customs']
test_generating_projects = ['Customs']
aut_projects = time_monitor_projects + test_generating_projects

maven_libraris = ["Hn-utils", "G-utils", "Calender", "Center-customs-iru-tir-epd", "Center-lib", "Connections",
                  "Cryptography",
                  "Customs-shared", "Db-utils", "default-pom", "Lazy-connections", "OCM", "Ramtin",
                  "Realtime-safe-tir", "Rulemanager", "SalonGomrok", "ServiceProvider", "Ui-utils", "Swich-lib",
                  "NezBazCore", "NezBazWeb"]
maven_projects = ["Nezarat", "Customs", "Baskool", "Swich", "Center", "Acc", "PackingList", "Tir",
                  "customs-reporter-server", "Guard", "Nazer", "Center-Swich", "Nazer", "Statistics"]
php_projects = ["Mis", "Bazbini", "Tina", "Coding", "srm-student", "srm-admin", "srm-bank", "srm-mali", "srm-sdr",
                "srm-services", "Coding-services", "Merchant", "SRM"]
nodejs_projects = ["Khoone"]
asp_projects = ["Anbar", "Reporter"]
endpoints = ["Nezarat", "Customs", "Baskool", "Swich", "Center", "Acc", "Mis", "Anbar", "PackingList", "Bazbini",
             "Tir", "Coding", "Merchant", "Center-Swich", "Nazer", "Khoone", "Statistics"]
SRM_childs = {"srm-admin": "admin", "srm-mali": "mali", "srm-sdr": "sdr", "srm-bank": "bank",
              "srm-services": "services", "srm-student": "student"}

#
# Deploy
#
general_method_deploy = ["Tir", "Acc", "Statistics", "Customs"]
non_pilot_customs = ["50100", "45100", "45110", "10300", "30100", "50200", "10103", "40400", "40202"]
confidence_interval = 12  # hour
deploy_superusers = ["mohammad ali tavallaie", "hamidreza ghasemi", "Hamed Hasani", "saeed", "Meysam Mirzaie",
                     "Mostafa Farsi", "Hamed Babaei", "Abouzar Kamaee", "amin",
                     "pouyan momeni", "saeed arash"]
excluded_projects = ["tir", "Center", "Mis", "Coding", "Acc"]

arrangement_projects = ["Customs", "Baskool", "Swich", "Acc"]

build_date_postfix = {
    "Anbar": "UI/buildDate.aspx",
    "Acc": "rest/build/buildDate",
}

deploy_audiences = [
    "09394760654",  # hasani
    "09365007447",  # babaei
    "09361855091",  # farsi
    "09212834421",  # ghasemi
    "09193240483",  # saeedi
    "09376307633",  # hasanKazemi
]

deploy_audiences_for_project = {
    "PackingList": ["09186093821", "09392076485"],  # said, ashkan
    "Anbar": ["09301756825", "09360110145", "09367198181"],  # kamaei, sina, afshar
    "SRM": ["09370102416"],  # morteza
    "Mis": ["09370102416"],  # morteza
    "Bazbini": ["09370102416", "09376682416"]  # morteza, darioush
}

pip = ["termcolor", "python-dateutil", "jenkinsapi", "pysocks", 'jira', 'python-telegram-bot']

authorized_keys_filename = "authorized_keys"

exclude_from_code_analyse = ["Acc", "Center", "Center-lib"]

jenkins_user = "scripts"
jenkins_password = "scripts123scripts"

alaki = False

clusters = {
    "jenkins": {
        "Customs": {
            "113": "113@172.16.111.113:8080/Customs",
            # "15": "15@172.16.111.113:8081/Customs",
            "116": "116@172.16.111.116:8080/Customs",
            # "46": "46@172.16.111.46:8081/Customs",
            "12": "12@172.16.111.12:8080/Customs",
        },

        "Swich": {
            "113": "113@172.16.111.113:8080/Swich",
            # "15": "15@172.16.111.113:8081/Swich",
            "116": "116@172.16.111.116:8080/Swich",
            # "46": "46@172.16.111.46:8081/Swich",
            "12": "12@172.16.111.12:8080/Swich",
        },

        "Baskool": {
            "113": "113@172.16.111.113:8080/Baskool",
            # "15": "15@172.16.111.113:8081/Baskool",
            "116": "116@172.16.111.116:8080/Baskool",
            # "46": "46@172.16.111.46:8081/Baskool",
            "12": "12@172.16.111.12:8080/Baskool",
        },

        "Acc": {
            # "113": "113@test@172.16.111.113:8080/acc",
            "113": "113@dockerTest@172.16.111.113:8080/acc",
            # "116": "116@172.16.111.116:8080/acc/rest/build",
            # "12": "60@172.16.111.60:8080/acc/rest/build",
        },

        "Anbar": {
            "Rajae-Jenkins": "Rajae-Jenkins@172.16.111.51:8091",
            "Bazargan-Jenkins": "Bazargan-Jenkins@172.16.111.51:8093",
            "Tehran-Jenkins": "Tehran-Jenkins@172.16.111.51:8099",
        },

        "Center": {
            "113": "113@172.16.111.113:80/Center",
            # "15": "15@172.16.111.113:8081/Center",
        },

        "Center-Swich": {
            "113": "113@172.16.111.113:8080/Center-Swich",
            # "15": "15@172.16.111.113:8081/Center-Swich",
        },

        "PackingList": {
            "43": "43@172.16.111.43:8080/PackingList",
        },

        "Mis": {
            "jenkins": "114@jenkins@172.16.111.114/mis-test2/site",
        },

        "Bazbini": {
            "jenkins": "114@jenkins@172.16.111.114/bazbini",
        },

        "Nezarat": {
            "36": "36@172.16.111.36:8080/Nezarat",
        },
    },

    "private": {
        "Customs": {
            "13": "13@172.16.111.13:8080/Customs",
            "15": "15@172.16.111.113:80/Customs",
            "34": "34@172.16.111.34:8080/Customs",
            "60": "60@172.16.111.60:8080/Customs",
        },

        "Swich": {
            "13": "13@172.16.111.13:8080/Swich",
            "15": "15@172.16.111.113:80/Swich",
            "34": "34@172.16.111.34:8080/Swich",
            "60": "60@172.16.111.60:8080/Swich",
        },

        "Baskool": {
            "13": "13@172.16.111.13:8080/Baskool",
            "15": "15@172.16.111.113:80/Baskool",
            "34": "34@172.16.111.34:8080/Baskool",
            "60": "60@172.16.111.60:8080/Baskool",
            "46": "46@172.16.111.46:8081/Baskool",
        },

        "Acc": {
            #"13": "13@test@172.16.111.13:8080/acc",
            "34": "34@test@172.16.111.34:8080/acc",
            # "60": "60@172.16.111.60:8080/acc/rest/build",
        },

        "Anbar": {
            "Rajae-Private": "Rajae-Private@172.16.111.51:8095",
            "Bazargan-Private": "Bazargan-Private@172.16.111.51:8096",
            "Tehran-Private": "Tehran-Private@172.16.111.51:8097",
        },

        "Center": {
            "13": "13@172.16.111.13:8080/Center",
            "15": "15@172.16.111.113:80/Center",
        },

        "Center-Swich": {
            "13": "13@172.16.111.13:8080/Center",
            "15": "15@172.16.111.113:80/Center",
        },

        "PackingList": {
            "33": "33@172.16.111.33:8080/PackingList",
        },

        "Mis": {
            "private": "114@private@172.16.111.114/mis-private/site",
        },

        "Bazbini": {
            "private": "114@private@172.16.111.114/bazbini-private",
        },

        "Nezarat": {
        },
    },

    "Khoone": {
        "Khoone": {
            "193138": "193136@172.17.193.138",
        },
    },

    "Statistics": {
        "Statistics": {
            "113": "113@172.16.111.113:8080/Statistics",
        },
    },
}
