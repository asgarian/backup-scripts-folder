import subprocess
import sys

process = subprocess.Popen("ls -l ", stdout=subprocess.PIPE, shell=True,
                               universal_newlines=True)
stdout = ""

for line in process.stdout:
    if line != "ProxyChains-3.1 (http://proxychains.sf.net)\n":
        sys.stdout.write(line)
        stdout += line + "\n"
