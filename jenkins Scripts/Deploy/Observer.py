#
# An Agent that observe some other agents (eg. deploy agent) and report their status
# other agents must first be subscribded to the observer and then report their status to it
# by calling set_status and we ask observer to give us information about its
# childs by calling report
#
# see Observable
#

from Deploy.Observable import Observable
from Utils.Utils import synchronized, split_by_size, remove_color, get_color, add_color


class Observer(object):
    def __init__(self, task_name):
        self.agents = {}
        self.task_name = task_name

    def subscribe(self, new_agent):
        if not isinstance(new_agent, Observable):
            raise ValueError("agent must implements Observable")

    def set_status(self, agent, status):
        self.agents[agent] = status
        self.report()

    @synchronized
    def report(self):
        print_list = sorted(self.agents.items(), key=lambda agent_state: agent_state[0].get_name_for_observer())

        print("")
        print("")
        print("+{:=<87}+".format(""))
        print("| {:<35}{:<50} |".format(self.task_name, "Status"))
        print("+{:-<87}+".format(""))

        i = 0
        for (agent, state) in print_list:
            color_size = len(state) - len(remove_color(state))
            colors = get_color(state)
            splits = split_by_size(remove_color(state), 50)
            align_size = str(50 + color_size)
            print(("| {:-<35}{:<" + align_size + "} |").format(agent.get_name_for_observer(),
                                                               add_color(splits[0], colors)))
            for part in splits[1:]:
                print(("| {:<35}{:<" + align_size + "} |").format("", add_color(part, colors)))
            if i != len(print_list) - 1:
                print("| {:<35}{:<50} |".format("", ""))
            i += 1

        print("+{:=<87}+".format(""))
        print("")
        print("")

    @synchronized
    def inform(self, agent, message):
        print("({}):{}".format(agent.get_name_for_observer(), message))
