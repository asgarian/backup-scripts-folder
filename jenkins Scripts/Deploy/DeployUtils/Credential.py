class Credential:
    # sample input: ut@password
    def __init__(self, input):
        parts = input.split('@')
        if len(parts) != 2:
            raise Exception("invalid format for Credential: {}".format(input))

        self.user = parts[0]
        self.password = parts[1]
