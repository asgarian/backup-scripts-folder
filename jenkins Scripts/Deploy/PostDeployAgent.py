from Deploy.DeployUtils.Credential import Credential
from Deploy.DeployUtils.DatabaseInfo import DatabaseInfo
from Utils.Utils import get_items_path
from Utils.jfabric import host, run, put, lcd, get


class PostdeployAgent:
    def __init__(self, build, location):
        self.build = build
        self.location = location

    def perform(self):
        postdeploy_file = self.get_postdeploy_file()

        if postdeploy_file:
            credentional = self.get_server_credential()
            db_info = self.get_databse_info()
            postdeploy_folder_server = "/home/{}/jenkins/postdeploy".format(credentional.user)

            with host(self.location.ip):
                run("mkdir -p {}".format(postdeploy_folder_server))
                put(postdeploy_file, postdeploy_folder_server)
                run("mysql -h {} -u {} -p{} {} < {}/{}".format(db_info.url, db_info.user,
                                                               db_info.password, db_info.name, postdeploy_folder_server,
                                                               postdeploy_file))

    def get_postdeploy_file(self):
        temp_folder = self.build.get_temp_folder()

        with lcd(temp_folder):
            run("cp {} .".format(self.build.get_artifact_path()))
            run("jar xvf {}".format(self.build.get_artifact_name()))

            result = get_items_path("{}/WEB-INF/classes/posdeploy.sql".format(temp_folder))
            if not result:
                result = None

            return result

    def get_server_credential(self):
        with host("2013"):
            raw_output = run("python /home/mkt/abz/decrypt.py -m dec -s {}".format(self.location.code))

            return Credential(raw_output)

    def get_databse_info(self):
        temp_folder = self.build.get_temp_folder()

        with lcd(temp_folder):
            with host("2013"):
                file_name = self.location.code + ".sh"
                get("/home/mkt/auto/{}/configurations/{}".format(self.build.name, file_name), temp_folder)

                with open(file_name) as f:
                    content = f.readlines()
                    return DatabaseInfo(content)
