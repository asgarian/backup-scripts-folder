#!/bin/bash
#create ssh key
echo "enter mysql user"
read user
echo "enter mysql password"
read pass
echo "enter network (e.g 10.32.0.0 for rajaei)"
read net
echo "enter ip (e.g 19 for bazbini)"
read ip
read -p "Is there local hard ? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
	read -p "Enter local mount point press enter for default  " localpath
	localpath=${localpath:-/mnt/external/$net/$ip}

	read -p "Enter user@ip backup user  press enter for default " userip
	userip=${localhard:-back@192.168.0.17}
	localhard=$userip:$localpath
	#echo $localhard;
	#echo $localpath;
exit;
fi 

ssh-keygen
#copy my ssh key to center 
ssh-copy-id -i ~/.ssh/id_rsa.pub back@172.17.20.13
#enable binary log in mysql
sed -i "/log_bin/ s/# *//" /etc/mysql/my.cnf
service mysql restart 
#move scripts to script folderi
echo "create scripts folder"
mkdir -p /home/scripts/
echo "get scripts from center"
scp back@172.17.20.13:/home/back/scripts/incremental.sh /home/scripts/
scp back@172.17.20.13:/home/back/scripts/full-back-sync.sh /home/scripts/
scp back@172.17.20.13:/home/back/scripts/time.txt /home/scripts/
scp back@172.17.20.13:/home/back/scripts/config.bash /home/scripts/
sed -i 's/netaddress/'$net'/g' /home/scripts/config.bash
sed -i 's/ipaddress/'$ip'/g' /home/scripts/config.bash

sed -i 's/mysqlusername/'$user'/g' /home/scripts/config.bash
sed -i 's/mysqlpassword/'$pass'/g' /home/scripts/config.bash

if [[ -n "$localhard" ]]; then
	ssh-copy-id -i ~/.ssh/id_rsa.pub $userip
	echo "localhard="$localhard >> /home/scripts/config.bash
	echo "localpath="$localpath >> /home/scripts/config.bash
fi

while read line; do
     n=${line%%;*};
    if [[ $n == "$net" ]]; then

        i=${line#*;}
        i2=${i%%;*}
        if [ "$i2" == "$ip" ]; then
                time1=${i#*;}
                miniute=${time1#*:}
                hour=${time1%:*}
        fi
    fi
done < /home/scripts/time.txt

#write out current crontab
crontab -l > mycron
sed -i '/ *full-back-sync.sh*/d' mycron
#echo new cron into cron file
echo "$miniute $hour * * * bash /home/scripts/full-back-sync.sh" >> mycron
echo "0 * * * * bash /home/scripts/incremental.sh" >> mycron

echo "0 * * * * scp back@172.17.20.13:/home/back/scripts/incremental.sh /home/scripts/" >> mycron
echo "0 * * * * scp back@172.17.20.13:/home/back/scripts/full-back-sync.sh  /home/scripts/" >> mycron
#install new cron file
crontab mycron
rm mycron
