from fabric.api import local, run, env, put, sudo
import os, time
from fabric.operations import run, put, sudo
from fabric.contrib.project import rsync_project
import re
import sys
import subprocess
import MySQLdb
import os
from subprocess import call
from fabric.contrib.files import sed, exists, append, upload_template
import itertools
#from fabric.contrib.project import fabric.contrib.project.upload_project(local_dir=None, remote_dir='', use_sudo=True)


#get local ip
def CatchIp():
        sudo("ifconfig | perl -nle'/dr:(\S+)/ && print $1' >iptemp1")
        sudo("sed -n 2p iptemp1 >iptemp2")
        ip=sudo("cat iptemp2")
        sudo("rm iptemp*")
        print "                      ip is: " + ip


#print triple enter
def TriplePrint():
        print
        print
        print


def JoinSplit():
        sudo('/etc/init.d/apparmor stop')
        sudo('update-rc.d -f apparmor remove')
        set_server_state('apparmor-disabled')
        append("/home/%s/.profile"% env.user, "export WORKON_HOME=$HOME/env")
        append("/home/%s/.profile"% env.user, "source /usr/local/bin/virtualenvwrapper.sh")
        sudo("easy_install virtualenv")
        sudo("easy_install pip")
        sudo("easy_install virtualenvwrapper")
        set_server_state('packages_installed', packages)
        env.installed_packages[env.host] = packages
        sudo('/etc/init.d/apparmor stop')
        sudo('update-rc.d -f apparmor remove')
        set_server_state('apparmor-disabled')
        p = run("dpkg -l | awk '/ii/ {print $2}'").split('\n')
        if not exists(apache_name):
                sudo('ln -s %s %s' % (apache_config, apache_name))
                sudo('nxensite %s' % instance_name)
                sudo('chgrp -R www-data %s%s/staticmedia' % (env.site_root, instance_name))
                sudo('chmod -R g+w %s%s/staticmedia' % (env.site_root, instance_name))


#set hosts ip and credentials
def CredHosts():
#list of all hosts
        env.hosts = [ '10.48.0.18','10.56.0.18','10.33.0.18','10.19.0.18','10.104.0.18','10.18.0.18','10.128.0.18','10.126.0.18','10.134.0.18','10.44.0.18','192.1.52.18','192.1.52.114']
#ssh username
        env.user = 'ut'
#ssh password for user
        env.password = 'mkt123sjb'



#'''Install a package / apt-get'''
def InstallPackage():
        sudo("apt-get -y install python2.7" )
	#sudo("apt-get -y install freetds-bin" )
        #sudo("apt-get -y update" )
        #sudo("apt-get -y install samba" )
        #sudo("apt-get -y install mysql-server-5.6" )
        #sudo("apt-get -y install postgresql postgresql-contrib")
        #sudo("apt-get -y install mysql-client-core-5.6")
        #sudo("apt-get -y install mysql-client-5.6")
        #sudo("apt-get -y install mysql-server-core-5.6")
        #sudo("apt-get -y -f install")
        #sudo("apt-get -y install apache2")
#        sudo("apt-get -y autoremove")
        #sudo("apt-get -y dist-upgrade")
        #sudo("apt-get -y upgrade")


#change file mode
def ChmodFile():
        sudo('chmod 777 /home/scripts/*.sql')
        sudo('mkdir -p /home/backups/anbar/inc')
        sudo('mkdir -p /home/backups/anbar')
        sudo('chmod 777 /home/backups/anbar')
        sudo('chmod 777 /home/backups/anbar/inc')



#set crontab with default ip
def CronSetDef():
#write out current crontab
        sudo('crontab -l > mycron')
#delete previous jobs
        sudo("sed -i '/TDSVER\|find/d' mycron")
#echo new cron into cron file
        sudo('echo "06 06 * * * TDSVER=8.0 tsql -H 192.168.0.21 -p 1433 -D Warehouse -U JavadWarehouseCreator -P GharehWarehouseCreator < /home/scripts/full.sql" >> mycron')
        sudo('echo "06 * * * * TDSVER=8.0 tsql -H 192.168.0.21 -p 1433 -D Warehouse -U JavadWarehouseCreator -P GharehWarehouseCreator < /home/scripts/diff.sql" >> mycron')
        sudo('echo "06 06 * * * find /home/backups/anbar/* -mtime +3 -delete" >> mycron')
        sudo('echo "06 06 * * * find /home/backups/anbar/inc/* -mmin +1080 -delete" >> mycron')
    	sudo('echo "06 06 06 * * find /home/backups/2016/* -mtime +60 -delete" >> mycron')
    	sudo('echo "07 * * * * scp back@172.17.20.13:/home/back/scripts/center.py /home/scripts/" >> mycron')
    	sudo('echo "07 07 * * * python /home/scripts/center.py" >> mycron')
#install new cron file
        sudo('crontab mycron')
#remove tempcron
        sudo('rm mycron')


#update transfered files with local ip
def FileLocalVal():
#        sed('/home/scripts/full.sql',before='192.168.0.18',after='10.48.0.116',use_sudo=True)
#        sed('/home/scripts/diff.sql',before='192.168.0.18',after='10.48.0.116',use_sudo=True)
#        sed('/home/ut/testsed',before='ENABLED="true"',after='ENABLED="false"',use_sudo=True)
        sudo("ifconfig | perl -nle'/dr:(\S+)/ && print $1' >iptemp1")
        sudo("sed -n 2p iptemp1 >iptemp2")
        ip=sudo("cat iptemp2")
        sed('/home/scripts/full.sql',before='192.168.0.18',after=ip,use_sudo=True)
        sed('/home/scripts/diff.sql',before='192.168.0.18',after=ip,use_sudo=True)
        sudo("rm iptemp*")
        sudo("rm /home/scripts/*.sql.bak")


#send your files to hosts
def TransFile():
        put("/home/scripts/*.sql", "/home/scripts/", use_sudo=True)


#set samba setting
def SmbSet():
        sudo('cat /etc/samba/smb.conf > smbtemp')
        sudo("sed -i '/backups\|available = yes\|valid users = ut\|valid users = ut\|read only = no\|browsable = yes\|public = yes\|writable = yes\|guest ok = yes/d' smbtemp")
        sudo('echo "[backups]" >> smbtemp')
        sudo('echo "path = /home/backups" >> smbtemp')
        sudo('echo "available = yes" >> smbtemp')
        sudo('echo "valid users = ut" >> smbtemp')
        sudo('echo "read only = no" >> smbtemp')
        sudo('echo "browsable = yes" >> smbtemp')
        sudo('echo "public = yes" >> smbtemp')
        sudo('echo "writable = yes" >> smbtemp')
        sudo('echo "guest ok = yes" >> smbtemp')
        sudo("cat smbtemp > /etc/samba/smb.conf")
        sudo('rm smbtemp')
        sudo('smbpasswd -a ut')
        sudo('service smbd restart')


#print datacenter information
def EnvDetail():
#       print('env.hosts inside : %s' % env.hosts)
        TriplePrint()
        TriplePrint()
        print "                 DataCenter Contain " + str(len(env.hosts)) + " Hosts"
#       CountBox=range(1, len (env.hosts)+1)
        j=1
        for i in env.hosts:
                print str (j) + "th" + " host is: " + i
                j +=1


#update sourcelist with local update server and package
def SourceList():
        sudo('echo "deb http://172.16.130.30:8888/ubuntu trusty main restricted" > /etc/apt/sources.list')
        sudo('echo "deb http://172.16.130.30:8888/ubuntu trusty-updates main restricted" >> /etc/apt/sources.list')
        sudo('echo "deb http://172.16.130.30:8888/ubuntu trusty universe" >> /etc/apt/sources.list')
        sudo('echo "deb http://172.16.130.30:8888/ubuntu trusty-updates universe" >> /etc/apt/sources.list')
        sudo('echo "deb http://172.16.130.30:8888/ubuntu trusty multiverse" >> /etc/apt/sources.list')
        sudo('echo "deb http://172.16.130.30:8888/ubuntu trusty-updates multiverse" >> /etc/apt/sources.list')
        sudo('echo "deb http://172.16.130.30:8888/ubuntu trusty-backports main restricted universe multiverse" >> /etc/apt/sources.list')
        sudo('echo "deb http://172.16.130.30:8888/security trusty-security main restricted" >> /etc/apt/sources.list')
        sudo('echo "deb http://172.16.130.30:8888/security trusty-security universe" >> /etc/apt/sources.list')
        sudo('echo "deb http://172.16.130.30:8888/security trusty-security multiverse" >> /etc/apt/sources.list')
        sudo('echo "deb http://172.16.130.30:8888/canonical trusty partner" >> /etc/apt/sources.list')
        sudo('echo "deb http://172.16.130.30:8888/apt VERSION main" >> /etc/apt/sources.list')
        sudo('echo "deb-src http://172.16.130.30:8888/apt VERSION main" >> /etc/apt/sources.list')


#update crontab with local ip
def CronSetLocal():
        sudo("ifconfig | perl -nle'/dr:(\S+)/ && print $1' >iptemp1")
        sudo("sed -n 2p iptemp1 >iptemp2")
        ip=sudo("cat iptemp2")
        sudo("cut -d '.' -f 4 iptemp2 >t1")
        field4=sudo("cat t1")
        sudo('crontab -l > mycron')
        sudo("cut -d '.' --complement -f 4 iptemp2 > t2")

        if int(field4)==18:
                sudo("sed -i 's/$/.21/' t2")
                localip=sudo("cat t2")
                sed('mycron',before='192.168.0.21',after=localip,use_sudo=True)

        if int(field4)==114:
                sudo("sed -i 's/$/.115/' t2")
                localip=sudo("cat t2")
                sed('mycron',before='192.168.0.21',after=localip,use_sudo=True)

        if int(field4)==116:
                sudo("sed -i 's/$/.117/' t2")
                localip=sudo("cat t2")
                sed('mycron',before='192.168.0.21',after=localip,use_sudo=True)

        sudo('crontab mycron')
        sudo('rm mycron')
#	sudo('rm mycron.bak')
        sudo('rm t1')
        sudo('rm t2')
        sudo("rm iptemp*")


#update postgres setting
def PostSet():
#        sudo("apt-get -y install postgresql postgresql-contrib")
#        sudo("sudo -u postgres createdb acc")
        sudo("sudo -u postgres psql --command '\password postgres'")
#       sudo("sudo -u postgres psql -f pgt")
#       sudo("sudo -u postgres psql -c "ALTER USER postgres WITH PASSWORD 'bahmanz';"")


#update grub setting
def GrubSet():
        sudo('echo "GRUB_DISABLE_OS_PROBER=true" >> /etc/default/grub')
        sudo('echo "GRUB_RECORDFAIL_TIMEOUT=0" >> /etc/default/grub')
        sudo("update-grub")


#update logrotate setting
def LogrotateSet():
        sudo('echo "/tomcat/logs/*.log /tomcat/logs/*.txt /tomcat/logs/catalina.out{" >> /etc/logrotate.conf')
        sudo('echo "    hourly" >> /etc/logrotate.conf')
        sudo('echo "    copytruncate" >> /etc/logrotate.conf')
        sudo('echo "    size 100M" >> /etc/logrotate.conf')
        sudo('echo "    rotate 10" >> /etc/logrotate.conf')
        sudo('echo "    compress" >> /etc/logrotate.conf')
        sudo('echo "}" >> /etc/logrotate.conf')


#set tomcat setting
def TomcatDefault():
        sudo('echo "JAVA_HOME=/opt/jdk" > /etc/etc/default/tomcat')
        sudo('echo "JAVA_OPTS="$JAVA_OPTS -Djava.security.egd=file:/dev/./urandom"" >> /etc/etc/default/tomcat')
        sudo('echo "CATALINA_OPTS="-XX:PermSize=3000m -Xmx14000m -Xms8000m -XX:-OmitStackTraceInFastThrow"" >> /etc/etc/default/tomcat')


#upgrade all
def Upgrade():
        sed('/etc/update-manager/meta-release',before='changelogs.ubuntu.com',after='172.17.20.13',use_sudo=True)
#        sudo("apt-get -y update" )
#        sudo("apt-get -y dist-upgrade")
#        sudo("apt-get -y upgrade")


#backup sqlserver on windows servers
def BackupWarehouse():
        TransFile()
        ChmodFile()
        FileLocalVal()
        CronSetDef()
        CronSetLocal()


#pass arg from bash
def PassArg():
        x=raw_input('Enter your :')


#deploy some project to tomcat
def DeployProject():
	argcode=raw_input('Enter your customs code:')
	depscri = "python dep1.py {}".format(argcode)
	local(depscri, capture=True)
        cmd1 = "awk /{}/ /home/back/configuration > t11".format(argcode)
        local(cmd1)
        local("cat t11")
        local("cut -d '=' -f 2 t11 > t12")
        local("cut -d '@' -f 2 t12 > t13")
        dest=local('cat t13', capture=True)
        local("rm t11")
        local("rm t12")
        local("rm t13")
#       cmd2 = "rsync -avrz /home/ut/box/Customs ut@{}:testd:".format(dest)
        cmd2=str('echo "mkt123sjb\n" | sudo -S rsync -avrz /home/back/box/Customs ut@{}:testd').format(dest)
#       print cmd2
        local(cmd2, capture=True)
        sudo("/etc/init.d/tomcat stop")
        sudo("mv /home/ut/testd/Customs /home/ut/testd/tCustoms")
	sudo("rsync /home/ut/testd/tCustoms  /tomcat/webapps/ -r -avz --delete")
	sudo("chown tomcat:tomcat /tomcat/webapps/* -R")
#	sudo("rm /home/ut/testd/tCustoms")
	sudo("/etc/init.d/tomcat start")
	local('echo "       deployed successfully"')

def CustomDeploy():
        import re
        import sys
        import subprocess
        import MySQLdb
        import os
        from subprocess import call

        argcode = 25500
        cmd1 = "awk /{}/ /home/ut/configuration > t11".format(argcode)
        local(cmd1)
        local("cat t11")
        local("cut -d '=' -f 2 t11 > t12")
        local("cut -d '@' -f 2 t12 > t13")
        dest=local('cat t13', capture=True)
        local("rm t11")
        local("rm t12")
        local("rm t13")
#       cmd2 = "rsync -avrz /home/ut/box/Customs ut@{}:testd:".format(dest)
        cmd2=str('echo "mkt123sjb\n" | sudo -S rsync -avrz /home/back/box/Customs ut@{}:testd').format(dest)
#       print cmd2
        local(cmd2, capture=True)





#fab -H 127.0.0.1 list_files
#fab CredHosts CatchIp --hide=status,aborts,warnings,running,stdout,stderr,user

