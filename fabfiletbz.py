from fabric.api import local, run, env, put, sudo
import os, time
from fabric.operations import run, put, sudo
from fabric.contrib.project import rsync_project
import re
import sys
import subprocess
import MySQLdb
import os
from subprocess import call
from fabric.contrib.files import sed


def mint():
        sudo('/etc/init.d/apparmor stop')
        sudo('update-rc.d -f apparmor remove')
        set_server_state('apparmor-disabled')
        append("/home/%s/.profile"% env.user, "export WORKON_HOME=$HOME/env")
        append("/home/%s/.profile"% env.user, "source /usr/local/bin/virtualenvwrapper.sh")
        sudo("easy_install virtualenv")
        sudo("easy_install pip")
        sudo("easy_install virtualenvwrapper")
        set_server_state('packages_installed', packages)
        env.installed_packages[env.host] = packages
        sudo('/etc/init.d/apparmor stop')
        sudo('update-rc.d -f apparmor remove')
        set_server_state('apparmor-disabled')
        p = run("dpkg -l | awk '/ii/ {print $2}'").split('\n')
        if not exists(apache_name):
                sudo('ln -s %s %s' % (apache_config, apache_name))
                sudo('nxensite %s' % instance_name)
                sudo('chgrp -R www-data %s%s/staticmedia' % (env.site_root, instance_name))
                sudo('chmod -R g+w %s%s/staticmedia' % (env.site_root, instance_name))


def deploy(loc):
        rsync_project(local_dir=loc, remote_dir='/var/www', exclude='.git')
        splitName = string.split(fileName, "_")
        endName = splitName[2]
        splitTwo = string.split(endName, ".")
        userFolder = splitTwo[0]
        parser = argparse.ArgumentParser()
        parser.add_argument( 'command', nargs=1 )
        parser.add_argument( 'fileName', nargs='+' )
        args= parser.parse_args()
        function = function_map[args.command]
        function( args.fileName )


def cred():                                                 #set hosts ip and credentials
        env.hosts = [ '10.122.0.114' ,'10.47.0.18' ,'10.46.0.18', '10.87.0.18', '10.49.0.18' ]                        #list of all hosts
        env.user = 'ut'                                     #ssh username
        env.password = 'mkt123sjb'              #ssh password for user


def install():                                              #'''Install a package / apt-get'''
        sudo("apt-get -y install freetds-bin" )             # install package freetds-bin
        sudo("apt-get -y update" )                          # update repository
        sudo("apt-get -y install samba" )                   #install package samba


def chmod():                                                #change file mode
        sudo('chmod 777 /home/scripts/*.sql')               #r/w/x to user-group-other
        sudo('mkdir -p /home/backups/anbar/inc')            #create necessary folders
        sudo('mkdir -p /home/backups/anbar')                #create necessary folders
        sudo('chmod 777 /home/backups/anbar')               #r/w/x to user-group-other
        sudo('chmod 777 /home/backups/anbar/inc')           #r/w/x to user-group-other


def cronjob():                                              #crontab settings
        sudo('crontab -l > mycron')                         #write out current crontab
        sudo("sed -i '/TDSVER\|find/d' mycron")             #delete previous jobs
        sudo('echo "06 06 * * * TDSVER=8.0 tsql -H 192.168.0.21 -p 1433 -D Warehouse -U JavadWarehouseCreator -P GharehWarehouseCreator < /home/scripts/full.sql" >> mycron')           #echo new cron into cron file
        sudo('echo "06 * * * * TDSVER=8.0 tsql -H 192.168.0.21 -p 1433 -D Warehouse -U JavadWarehouseCreator -P GharehWarehouseCreator < /home/scripts/diff.sql" >> mycron')            #echo new cron into cron file
        sudo('echo "06 06 * * * find /home/backups/anbar/* -mtime +3 -delete" >> mycron')               #echo new cron into cron file
        sudo('echo "06 06 * * * find /home/backups/anbar/inc/* -mmin +1080 -delete" >> mycron')         #echo new cron into cron file
        sudo('crontab mycron')  #install new cron file
        sudo('rm mycron')       #remove tempcron


def edit():
#        sed('/home/scripts/full.sql',before='10.48.0.18',after='10.48.0.116',use_sudo=True)
#        sed('/home/scripts/diff.sql',before='10.48.0.18',after='10.48.0.116',use_sudo=True)
        sed('/home/ut/testsed',before='ENABLED="true"',after='ENABLED="false"',use_sudo=True)

def trans():                                                #put your local files to remote
        put("/home/scripts/*.sql", "/home/scripts/", use_sudo=True)


#def corr():    #replace default value with local
def smbset():
        sudo('echo "[backups]" >> /etc/samba/smb.conf')
        sudo('echo "path = /home/backups" >> /etc/samba/smb.conf')
        sudo('echo "available = yes" >> /etc/samba/smb.conf')
        sudo('echo "valid users = ut" >> /etc/samba/smb.conf')
        sudo('echo "read only = no" >> /etc/samba/smb.conf')
        sudo('echo "browsable = yes" >> /etc/samba/smb.conf')
        sudo('echo "public = yes" >> /etc/samba/smb.conf')
        sudo('echo "writable = yes" >> /etc/samba/smb.conf')
        sudo('echo "guest ok = yes" >> /etc/samba/smb.conf')
