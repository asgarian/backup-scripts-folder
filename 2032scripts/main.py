#
# gateway of jenkins: every things starts from here
#
import sys
import time
import requests

from Build import AutAgent
from Build.Build import Build
from Build.BuildInfo import *
from Build.PostBuild import PostBuild
from Build.PreBuild import PreBuild
from Deploy.DeployASP2 import DeployAsp2
from Deploy.DeployAsp import DeployAsp
from Deploy.DeployInfo import DeployInfo
from Deploy.DeployMasterAgent import DeployMasterAgent
from Deploy.DeployMaven import DeployMaven
from Deploy.DeployPackage import DeployPackage
from Deploy.DeployProduction import DeployProduction
from Deploy.DeployTest import DeployTest
from Deploy.MultiDeployTrigger import MultiDeployTrigger
from Monitoring.CommandRunner import CommandRunner
from Monitoring.CompareWithOriginal import CompareWithOriginal
from Monitoring.DiagnosticsAgent import DiagnosticsAgent
from Monitoring.MasterInspector import MasterInspector
from Test.AfterDeployTest import AfterDeployTest
from Test.AfterDeployTestInfo import AfterDeployTestInfo
from Test.SeleniumTestAgent import SeleniumTestAgent
from Test.SendToDeployJenkinsAgent import SendToDeployJenkinsAgent
from Test.TestInfo import TestInfo
from Test.UnitTestGenerator import UnitTestGenerator
from Utils.Backup import Backup
from Utils.DiskCleaner import DiskCleaner
from Utils.PKM import add_public_key
from Utils.PKM import remove_public_key
from Utils.Utils import get_upstream_build, print_exception, am_i_newjenkins, HttpProxy, run_windows_command, \
    get_json_from_jenkins, send_sms
from Utils.jfabric import local

if __name__ == "__main__":
    if len(sys.argv) == 1:
        sys.argv = ['/scripts/main.py', 'Deploy2', 'Customs Deploy', '2532', 'http://172.17.20.32:8080/job/Customs%20Deploy/2532/', 'mohammad hassan kazemi', 'http://172.17.20.32:8080/job/Customs/205/', 'Test@99999@172.17.0.243:8080/Customs@09361855091', 'default', '', '', '', 'false', 'true', 'false', 'false']
        configs.alaki = True

    print("")
    print("sys.argv =", sys.argv)
    print("")

    sys.excepthook = print_exception

    if sys.argv[1] == "PackageDeploy":
        package_deployer = DeployPackage(sys.argv)
        package_deployer.build_jobs()
        exit(0)

    if sys.argv[1] == "AfterDeployTest":
        info = AfterDeployTestInfo(sys.argv)
        if info.can_test():
            tester = AfterDeployTest(info)
            tester.run()
        sys.exit(0)

    if sys.argv[1] == "SeleniumTestAgent":
        info = TestInfo(sys.argv)

        tester = SeleniumTestAgent(info)
        tester.run()
        sys.exit(0)

    if sys.argv[1] == "Deploy" or sys.argv[1] == "Deploy2":

        info = DeployInfo(sys.argv)
        name = info.get_deploy_name()
        if info.DEPLOY_REPORTER == "true":
            payload = {'VERSION': "{}".format(info.VERSION)
            , 'LOCATION': info.REPORTER_LOCATION, 'DPATH': info.DPATH, 'COMMENT': info.COMMENT
            , 'FAKE': info.FAKE}
            requests.post("http://newjenkins:da00ed9b7af94a088b637d9c312e68c7@172.17.20.32:8080/job/customs-reporter-server%20Deploy/buildWithParameters",
        data=payload)

        if info.BUILD_USER == "New Jenkins":
            if DeployPackage.must_change_user(info):
                info.BUILD_USER = DeployPackage.get_last_builds_user()

        if name in configs.general_method_deploy:
            deploy_agent = DeployProduction(info)
        elif name in configs.maven_projects:
            deploy_agent = DeployMaven(info)
        elif name in configs.php_projects:
            deploy_agent = DeployProduction(info)
        elif name in configs.asp_projects:
            if am_i_newjenkins():
                deploy_agent = DeployAsp(info)
            else:
                deploy_agent = DeployAsp2(info)
        else:
            raise Exception("Deploy not supported yet for {}!".format(name))
        exit_code = deploy_agent.deploy()
        sys.exit(exit_code)

    if sys.argv[1] == "PrivateDeploy":
        project_name = sys.argv[2].replace(" Private Deploy", "")
        if project_name in ["Khoone", "Statistics"]:
            sys.argv[7] = configs.clusters[project_name][project_name][sys.argv[7]]
        elif sys.argv[7] in configs.clusters["private"][project_name]:
            sys.argv[7] = configs.clusters["private"][project_name][sys.argv[7]]
        else:
            sys.argv[7] = configs.clusters["jenkins"][project_name][sys.argv[7]]

        info = DeployInfo(sys.argv)
        name = info.DEPLOYING_BUILD.name

        if name in configs.asp_projects:
            deploy_agent = DeployAsp2(info)
        else:
            deploy_agent = DeployTest(info)

        exit_code = deploy_agent.deploy()
        sys.exit(exit_code)

    if sys.argv[1] == "ClusterDeploy":
        info = DeployInfo(sys.argv)
        deploy_agent = DeployMasterAgent(info, info.LOCATION.name)

        exit_code = deploy_agent.deploy()
        sys.exit(exit_code)

    if sys.argv[1] == "PrepareForTest":
        build_json = get_json_from_jenkins("http://newjenkins.iais.ir/job/Prepare_for_test/api/json?pretty=true")
        if build_json["description"].startswith("disable by"):
            disabler = build_json["description"].replace("disable by", "")
            print("Prepare For Test is Disabled by " + disabler)
            while True: pass

        info = DeployInfo(sys.argv)

        upstream_build = get_upstream_build(info)
        if upstream_build and upstream_build.name in ["Khoone", "Statistics"]:
            cluster_name = upstream_build.name
        else:
            cluster_name = info.LOCATION.name

        deploy_agent = DeployMasterAgent(info, cluster_name)

        if upstream_build:
            deploy_agent.force_version(upstream_build)

        exit_code = deploy_agent.deploy()
        sys.exit(exit_code)

    if sys.argv[1] == "MultiDeployTrigger":
        info = BuildInfo(sys.argv)
        exit_code = MultiDeployTrigger(info)
        sys.exit(exit_code)

    if sys.argv[1] == "Backup":
        if am_i_newjenkins():
            folders = ["/home/ut/Scripts/", "/home/ut/DeployConfigs/", "/home/ut/jenkins/public_keys/"]
            repository = "https://newjenkins@git.iais.co/scm/tdr/newjenkins.git"
            run_windows_command("C:\\\\JenkinsScripts\\\\Save-Backup.ps1")
            server = "newjenkins"
        else:
            folders = ["/home/ut/jenkins/scripts/", "/home/ut/jenkins/configs/", "/j/userContent/DeployReports/"]
            repository = "https://newjenkins@git.iais.co/scm/tdr/deployjenkins.git"
            server = "deployJenkins"
        backup_agent = Backup(folders,repository)

        sys.exit(0)

    if sys.argv[1] == "SendToDeployJenkins":
        info = Operation(sys.argv[2], sys.argv[3])
        if sys.argv[4]:
            if sys.argv[5]:
                target_build = BuildInfo([sys.argv[4], sys.argv[5]])
            else:
                raise Exception("UP_STREAM_NUMBER field should not be empty")
        else:

            target_build = get_upstream_build(info)
        sender = SendToDeployJenkinsAgent(target_build)
        sender.send_to_deploy_jenkins()

        sys.exit(0)

    if sys.argv[1] == "Monitoring":
        with HttpProxy():
            agent = MasterInspector(sys.argv[2], sys.argv[3])
            agent.check_build_dates()
        sys.exit(0)

    if sys.argv[1] == "Aut":
        info = BuildInfo(sys.argv)
        agent = AutAgent.post_deploy(info)
        agent.run()
        exit_code = 1
        sys.exit(exit_code)

    if sys.argv[1] == "AddPublicKey":
        add_public_key(sys.argv[2], sys.argv[3])

        local("cp /opt/tomcat/.ssh/config /j/userContent/config -f")

        print("\n\n===================================================================\n")
        print("download this file and put it in your ~/.ssh/ and then just type 'ssh 113' :")
        print("http://newjenkins.iais.ir/userContent/config")
        print("\n===================================================================\n\n")
        sys.exit(0)

    if sys.argv[1] == "RemovePublicKey":
        remove_public_key(sys.argv[2], sys.argv[3])
        sys.exit(0)

    if sys.argv[1] == "PreBuild":
        buildInfo = BuildInfo(sys.argv[2:])
        agent1 = PreBuild(buildInfo)
        agent1.run()

        agent2 = Build(buildInfo)
        agent2.run()
        exit(0)

    if sys.argv[1] == "PostBuild":
        buildInfo = BuildInfo(sys.argv[2:])
        agent = PostBuild(buildInfo)
        agent.run()
        exit(0)

    if sys.argv[1] == "GenerateUnitTest":
        info = Operation(sys.argv[2], sys.argv[3])

        generator = UnitTestGenerator(info)
        generator.run_non_immediate_parts()
        exit(0)
    if sys.argv[1] == "diskCleanUp":
        agent = DiskCleaner()
        agent.run()
        exit(0)
    if sys.argv[1] == "customsDiagnostics":
        if sys.argv[2] or sys.argv[3]:
            agent = DiagnosticsAgent(sys.argv[2], sys.argv[3])

            agent.run()
        else:
            raise Exception("Fill All Items")
        exit(0)
    if sys.argv[1] == "runCommand":
        if sys.argv[2] or sys.argv[3]:
            agent = CommandRunner(sys.argv[2], sys.argv[3])
            agent.run()
        else:
            raise Exception("Fill All Items")
        exit(0)

    if sys.argv[1] == "checkConfigs":
        time_of_now = time.time()
        original_job_name = sys.argv[2] + "-" + str(time_of_now)

        info = DeployInfo(sys.argv)
        name = info.DEPLOYING_BUILD.name
        deploy_agent = DeployMaven(info)
        deploy_agent.run_fake_deploy(time_of_now)
        compare_agent = CompareWithOriginal(sys.argv[2], sys.argv[7], original_job_name)
        compare_agent.run()

        exit(0)

    raise Exception("Error: should not reach here( maybe wrong command )")
