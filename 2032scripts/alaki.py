import sys
import requests
from Deploy import DeployPackage
from Deploy.DeployASP2 import DeployAsp2
from Deploy.DeployAsp import DeployAsp
from Deploy.DeployInfo import DeployInfo
from Deploy.DeployMaven import DeployMaven
from Deploy.DeployProduction import DeployProduction
from Utils.Utils import am_i_newjenkins
from Utils.jfabric import local
import configs

sys.argv = ['/scripts/main.py', 'Deploy2', 'Customs Deploy', '2066', 'http://172.17.20.32:8080/job/Customs%20Deploy/2066/', 'hamid asgarian', 'http://172.17.20.32:8080/job/Customs/182/', 'Test@99999@172.17.0.243:8080/Customs@09365007447-09376307633-09361855091-09195159060-091', 'default', '', '', '', 'false', 'true', 'false', 'true']
info = DeployInfo(sys.argv)
name = info.get_deploy_name()
if sys.argv[1] == "Deploy" or sys.argv[1] == "Deploy2":

    info = DeployInfo(sys.argv)
    name = info.get_deploy_name()

    if info.BUILD_USER == "New Jenkins":
        if DeployPackage.must_change_user(info):
            info.BUILD_USER = DeployPackage.get_last_builds_user()

    if name in configs.general_method_deploy:
        deploy_agent = DeployProduction(info)
    elif name in configs.maven_projects:
        deploy_agent = DeployMaven(info)
    elif name in configs.php_projects:
        deploy_agent = DeployProduction(info)
    elif name in configs.asp_projects:
        if am_i_newjenkins():
            deploy_agent = DeployAsp(info)
        else:
            deploy_agent = DeployAsp2(info)
    else:
        raise Exception("Deploy not supported yet for {}!".format(name))
    if info.DEPLOY_REPORTER:
        payload = {'VERSION': "{}".format(info.VERSION)
        , 'LOCATION': info.REPORTER_LOCATION, 'DPATH': info.DPATH, 'COMMENT': info.COMMENT
        , 'FAKE': info.FAKE}
        requests.post("http://newjenkins:da00ed9b7af94a088b637d9c312e68c7@172.17.20.32:8080/job/customs-reporter-server%20Deploy/buildWithParameters",
    data=payload)