from os.path import join
import urllib

from Build.BuildInfo import BuildInfo
from Utils.Utils import recreate_folder, get_root_info, get_last_stable_version, get_items_path, create_folder, section, \
    blue
from Utils.jfabric import host, get, lcd, local
import configs


class UnitTestGenerator:
    def __init__(self, info):
        self.info = info
        self.target = get_root_info(info)

        if self.target.name not in configs.endpoints:
            self.target = BuildInfo(["Customs", get_last_stable_version("Customs")])

        if not isinstance(self.target, BuildInfo):
            self.target = BuildInfo(self.target)

        self.id = "{}-{}".format(self.target.number, self.info.number)
        self.aut_folder = join("/aut", self.target.name, "builds", self.id)
        self.log_folder = join(self.aut_folder, "logs")
        self.should_run = "Customs" in self.target.name or "Generate Unit Test" == self.target.name

    #
    # Because of performance we split generating into two phase:
    # 1) immediate pahse contains parts that must be run immediately after running tests
    # 2) non-immediate phase contains parts that can be run after a while
    #
    def run_immediate_parts(self):
        if not self.should_run:
            return

        self.send_write_request()
        self.download_logs()

    @section
    def download_logs(self):
        recreate_folder(self.log_folder)
        with host("113"), lcd(self.log_folder):
            get("/tomcat/AUT/Customs/*", ".")
            local("ls -l")
            local("du -h")

    @section
    def send_write_request(self):
        self.send_request_to_aut("getSize")
        self.send_request_to_aut("write")

    def send_request_to_aut(self, action):
        url = "http://172.16.111.113:8080/Customs/AUTPanel?action={}".format(action)
        print("sending {}".format(url))

        response = urllib.request.urlopen(
            url).read().decode()
        print("Response from aut {} request:".format(action))
        print(response)
        print("")

    def run_non_immediate_parts(self):
        if not self.should_run:
            return

        if not get_items_path(self.log_folder):
            self.run_immediate_parts()

        self.generate_unit_tests()
        self.commit_new_tests()

    @section
    def generate_unit_tests(self):
        self.valid_tests_folder = join(self.aut_folder, "valid_tests")
        self.invalid_tests_folder = join(self.aut_folder, "invalid_tests")
        aut_temp_folder = join(self.aut_folder, "temp")
        temp_folder = self.info.get_temp_folder()

        local("unzip -qo {} -d {}".format(self.target.get_artifact_path(), temp_folder))
        classes_folder = join(temp_folder, "WEB-INF/classes")
        lib_folder = join(temp_folder, "WEB-INF/lib")
        local("cp /aut/Customs/TestUtils/* {} -r".format(classes_folder))

        create_folder(self.valid_tests_folder)
        create_folder(self.invalid_tests_folder)
        create_folder(aut_temp_folder)

        print(blue("Valid Tests: {}".format(self.valid_tests_folder)))
        print(blue("Invalid Tests: {}".format(self.invalid_tests_folder)))

        generator_path = join(configs.jenkins_home, "jobs/AUTGenerator/workspace/target")
        with lcd(generator_path):
            local(
                "java -cp AUTGenerator-1.0-SNAPSHOT.jar ir.iais.aut.generator.UnitTestGenerator  '{}' '{}' '{}' '{}' '{}' '{}'".format(
                    self.log_folder, self.valid_tests_folder, self.invalid_tests_folder, aut_temp_folder,
                    classes_folder,
                    lib_folder))

    @section
    def commit_new_tests(self):
        temp_folder = self.info.get_temp_folder()
        checkout_folder = join(temp_folder, "source")
        local("mkdir -p {}".format(checkout_folder))

        with lcd(checkout_folder):
            local("svn checkout http://svn.iais.ir/customs-main/Customs")
            test_folders = "Customs/src/test/java"
            local("svn rm {}".format(test_folders))
            local("cp {} {} -r".format(self.valid_tests_folder, test_folders))
            local("svn add {}".format(test_folders))
            commit_message = "auto commit by AUT, id={}".format(self.id)
            local("svn ci Customs -m '{}'".format(commit_message))
