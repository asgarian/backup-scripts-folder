from marionette import Marionette
from marionette_driver import By
import sys

client = Marionette('{}'.format(sys.argv[2]), port=2828)
client.start_session()
print "session Started"
client.set_context('chrome')
client.find_element(By.ID, 'selenium-ide-button').click()
print "Selenium Opened"
selenium_window = client.window_handles[1]  # needs editing
client.switch_to_window(selenium_window)
print "Opening Test Suit in {}".format(sys.argv[1])
client.execute_script("window.editor.app.loadTestSuite('{}')".format(sys.argv[1]))
client.execute_script("goDoCommand('cmd_selenium_play_suite')")
