from Utils.Operation import Operation


class TestInfo(Operation):
    def __init__(self, value):
        super().__init__(value[5], value[2])

        self.BUILD_ID = value[3]
        self.BUILD_DISPLAY_NAME = value[4]
        self.BUILD_TAG = value[6]
        self.EXECUTOR_NUMBER = value[7]
        self.NODE_NAME = value[8]
        self.NODE_LABELS = value[9]
        self.WORKSPACE = value[10]
        self.JENKINS_HOME = value[11]
        self.JENKINS_URL = value[12]
        self.BUILD_URL = value[13]
        self.JOB_URL = value[14]
        self.SVN_REVISION = value[15]
        self.SVN_URL = value[16]
        self.BUILD_USER = value[17]
        self.GIT_COMMIT = value[18]
        self.GIT_BRANCH = value[19]
        self.GIT_PREVIOUS_COMMIT = value[20]
        self.GIT_URL = value[21]
        self.GIT_URL_N = value[22]
        self.GIT_AUTHOR_EMAIL = value[23]
        self.GIT_COMMITTER_EMAIL = value[24]

    def to_powershell_arg(self):
        return '{"' + '"} {"'.join([self.number, self.BUILD_ID, '\\`' + self.BUILD_DISPLAY_NAME, self.name,
                                    self.BUILD_TAG, self.EXECUTOR_NUMBER, self.NODE_NAME, self.NODE_LABELS,
                                    self.WORKSPACE, self.JENKINS_HOME, self.JENKINS_URL, self.BUILD_URL, self.JOB_URL,
                                    self.SVN_REVISION, self.SVN_URL, self.BUILD_USER, self.GIT_COMMIT, self.GIT_BRANCH,
                                    self.GIT_PREVIOUS_COMMIT, self.GIT_URL, self.GIT_URL_N, self.GIT_AUTHOR_EMAIL,
                                    self.GIT_COMMITTER_EMAIL]) + '"}'
