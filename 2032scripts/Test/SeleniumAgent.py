import time

import configs
from Test.FirefoxAutomator import FirefoxAutomator
from Utils.Utils import run_windows_command, get_json_from_jenkins

__author__ = 'borna'


class SeleniumAgent:
    def __init__(self, name, build_id, selenium_test_path):
        self.name = name
        self.build_id = build_id
        self.selenium_test_path = selenium_test_path
        self.first_time = True

    def run_corresponding_script(self, name, arg1, arg2):
        return run_windows_command("C:\\\\JenkinsScripts\\\\ADTest\\\\{}.ps1 {} {}".format(name, arg1, arg2))

    def find_idle_node_from_2032(self):
        while True:
            for i in range(1, 4):
                with open("/jenkins/test_nodes/" + str(i), 'r') as node_state:
                    content = node_state.read()
                    if content == "" or content == "\n":
                        return i
                    else:
                        content_parts = content.split('-')
                        node_occupier = get_json_from_jenkins(
                            "http://127.0.0.1:8080/job/{}/{}/api/json".format(content_parts[0],
                                                                              content_parts[1].replace("\n", "")))
                        if not node_occupier["building"]:
                            return i

    def check_idle_node_on_2034(self, idle_node):
        return run_windows_command(
            "Get-Content C:\\\\Test-Nodes\\\\Test-Node-{}\\\\SeleniumAutoRunnerTrigger\\\\State.txt".format(
                idle_node)) == "idle\n\n"

    def find_idle_node(self):
        while True:
            idle_node = self.find_idle_node_from_2032()
            if self.check_idle_node_on_2034(idle_node):
                return idle_node

    def occupy_node(self, idle_node):
        with open("/jenkins/test_nodes/" + str(idle_node), 'w') as node_state:
            node_state.truncate()
            node_state.write(self.name + "-" + self.build_id)

    def free_node(self, idle_node):
        with open("/jenkins/test_nodes/" + str(idle_node), 'w') as node_state:
            node_state.truncate()

    def run_selenium_test(self):
        if not configs.alaki:
            try:
                log_file_path = "C:\\\\Test-Nodes\\\\Test-Node-1\\\\SeleniumAutoRunnerTrigger\\\\selenium-log.txt"
                self.run_corresponding_script("Remove",
                                              log_file_path,
                                              "")
                print("adding suite {}".format(self.selenium_test_path))
                automator = FirefoxAutomator(self.selenium_test_path)
                automator.start_test()
                time.sleep(2)
                while not run_windows_command(
                        "'Get-Content '{}' | Select-String -Pattern \"completed:\"'".format(log_file_path)):
                    time.sleep(1.5)
                if (run_windows_command(
                        "'Get-Content '{}' | Select-String -Pattern \"passed!\"'".format(log_file_path))):
                    print("passed!")
                else:
                    raise Exception("Test Failed")
            finally:
                time.sleep(1.5)
                run_windows_command("taskkill /im firefox.exe")
                run_windows_command("taskkill /im firefox.exe")

                # if not configs.alaki:
                #     idle_node = self.find_idle_node()
                #     self.occupy_node(idle_node)
                #     node_root = "C:\\\\Test-Nodes\\\\Test-Node-" + str(idle_node) + "\\\\SeleniumAutoRunnerTrigger"
                #     try:
                #         self.run_corresponding_script("Remove", node_root + "\\\\Snapshot.png", "")
                #         self.run_corresponding_script("Remove", node_root + "\\\\TestResults\\\\*", "")
                #         self.run_corresponding_script("Remove", node_root + "\\\\TestResult.txt", "")
                #         self.run_corresponding_script("Set_Content", node_root + "\\\\TestCases.txt", self.seleniumtest_path)
                #         self.run_corresponding_script("Set_Content", node_root + "\\\\Id.txt"
                #                                       , self.build_id + " " + self.name)
                #         self.run_corresponding_script("Set_Content", node_root + "\\\\RunTest.txt", "1")
                #         self.run_corresponding_script("FetchResult", node_root, self.build_id)
                #         print("Test Number -" + self.build_id + "- Passed!")
                #     except Exception as ex:
                #         if self.first_time:
                #             self.first_time = False
                #             result_path = node_root + "\\\\TestResult.txt"
                #             result_content = run_windows_command(
                #                 "'Get-Content " + result_path + " | Select-String -Pattern 'interact' -quiet'")
                #             remoter = "C:\\\\Remoter\\\\id.txt"
                #             if result_content:
                #                 print("Node "+idle_node+" is down! running test again!")
                #                 self.run_corresponding_script("Set_Content", remoter, idle_node)
                #                 self.run_selenium_test()
                #             else:
                #                 raise ex
                #         else:
                #             raise ex
                #     finally:
                #         self.free_node(idle_node)
