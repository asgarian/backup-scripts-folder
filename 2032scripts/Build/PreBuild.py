from os.path import join

import configs
from Utils.Utils import getstatusoutput, recreate_folder, get_items_path, replace_in_file, get_last_stable_version
from Utils.jfabric import lcd, local
from Build.BuildInfo import BuildInfo


class PreBuild:
    def __init__(self, build_info):
        self.build_info = build_info
        self.name = build_info.name
        self.number = build_info.number
        self.workspace = build_info.get_workspace()
        self.branch = build_info.branch

    def run(self):
        if self.name in configs.aut_projects:
            self.aspect_prebuild()

        if self.name not in configs.exclude_from_code_analyse:
            self.run_code_analyse()

        # Remove target folder for mavens
        if self.name in (configs.maven_projects + configs.maven_libraris):
            target_folder = join(self.workspace, "target")
            if get_items_path(target_folder):
                local("rm -r {}".format(target_folder))

        if self.name in configs.endpoints:
            for library_name in configs.maven_libraris:
                library_buildNumber = get_last_stable_version(library_name)
                library_parameters = [(library_name), (library_buildNumber)]
                library_info = BuildInfo(library_parameters)
#                xc = PreBuild(library_parameters)
                print (xc.branch)
                print (library_info.branch)
                print (self.branch)
#                if library_info.branch != self.branch:
#                    print("warning: branch {} don't match with branch {} ".format(library_info.branch, self.branch))

    def run_code_analyse(self):
        with lcd("/j/jobs/SessionCheck/workspace/"):
            return_code, stdout = getstatusoutput(
                "find /j/jobs/{}/workspace/src/main/java -type f -name *.java -printf '%p ' ".format(self.name))
            files = stdout

            if return_code == 0:
                local(
                    'mvn exec:java -Dexec.mainClass="ir.iais.ghalatgir.CorrectSessionClosingInsurance" -Dexec.args="{}"'.format(
                        files))

    def aspect_prebuild(self):
        #
        # Backup hibernate.cfg
        #
        temp_folder = join(self.build_info.get_temp_folder(), "aut-src-backup")
        recreate_folder(temp_folder)

        hibernate_path = join(self.workspace, "src/main/java/hibernate.cfg.xml")
        log_4j_path = join(self.workspace, "src/main/java/log4j.properties")
        if not get_items_path(hibernate_path):
            hibernate_path = join(self.workspace, "src/main/resources/hibernate.cfg.xml")

        if get_items_path(hibernate_path):
            classes_path = join(temp_folder, "WEB-INF/classes/")
            local("mkdir -p {}".format(classes_path))
            local("cp {} {}".format(hibernate_path, classes_path))

            if get_items_path(log_4j_path):
                local("cp {} {}".format(log_4j_path, classes_path))
        else:
            raise Exception("Could not found hibernate.cfg.xml")

        #
        # Add Aspect
        #
        aspects_paths = join("/home/ut/AUT", self.name, "Aspects/*")
        src_folder = join(self.workspace, "src/main/java")
        aspect_files = get_items_path(aspects_paths)
        if aspect_files:
            local("cp -r {} {}".format(aspects_paths, src_folder))
            print("adding {}".format(", ".join(aspect_files)))

        #
        # Change pom to enable aspectj plugin
        #
        pom_path = join(self.workspace, "pom.xml")
        replace_in_file(pom_path, '<!--<goal>compile</goal>-->', '<goal>compile</goal>')
