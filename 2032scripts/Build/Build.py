from os.path import join
from time import strftime, localtime

import configs
from Utils.Utils import get_items_path, run_windows_command
from Utils.jfabric import local, lcd


class Build:
    def __init__(self, build_info):
        self.build_info = build_info
        self.name = build_info.name
        self.number = build_info.number
        self.workspace = build_info.get_workspace()

    def run(self):
        self.save_build_number()

        if self.name in configs.endpoints:
            self.create_artifact_folder()

            self.save_build_date(self.build_info.get_sb_folder())

            if self.name in configs.php_projects:
                self.save_build_date(self.workspace)

            if self.name in configs.asp_projects:
                self.build_asp()

            if self.name == "SRM":
                self.build_srm()

    def create_artifact_folder(self):
        artifact_folder = self.build_info.get_artifact_folder()
        local("mkdir -p {}".format(artifact_folder))

    def save_build_date(self, folder):
        with lcd(folder):
            local("echo '{}' > buildDate.txt".format(self.get_build_date_string()))

    def get_build_date_string(self):
        return strftime("%Y-%m-%d %H:%M:%S %z", localtime())

    def save_build_number(self):
        build_number_folder = join(self.workspace, "src/main/resources")
        if get_items_path(build_number_folder):
            build_number_path = join(build_number_folder, self.name + "-BUILD_NUMBER.properties")
            content = "build_number=" + self.number
        else:
            build_number_path = join(self.workspace, "BUILD_NUMBER.txt")
            content = self.number

        with open(build_number_path, 'w') as f:
            f.write(str(content))
            f.close()

    def build_asp(self):
        # we have to do the following because of special chars
        build_date_parts = self.get_build_date_string().split(' ')
        build_date1 = build_date_parts[0]
        build_date2 = " ".join(build_date_parts[1:])

        run_windows_command('C:\\\\JenkinsScripts\\\\Build-Asp.ps1 {} {} {} {{"{}"}}'.format(self.name, self.number,
                                                                                             build_date1,
                                                                                             build_date2))

    def build_srm(self):
        with lcd(join(configs.jenkins_home, "jobs")):
            for child in configs.SRM_childs:
                if child in ["srm-bank", "srm-sdr", "srm-services"]:
                    local('cp  "{}"/workspace SRM/workspace/"{}" -r'.format(child, configs.SRM_childs[child]))
                else:
                    local(
                        'cp  "{}"/workspace/output SRM/workspace/"{}" -r'.format(child, configs.SRM_childs[child]))
