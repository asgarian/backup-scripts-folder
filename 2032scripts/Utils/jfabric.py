from copy import copy
import subprocess
from os.path import join
import threading
import sys

states = {}


def get_state():
    tid = threading.get_ident()
    if tid not in states:
        states[tid] = {}

    return states[tid]


class StateChanger:
    def __init__(self, new_state):
        self.new_state = new_state

    def __enter__(self):
        state = get_state()
        self.saved_state = copy(state)
        for key in self.new_state:
            state[key] = self.new_state[key]

    def __exit__(self, type, value, traceback):
        states[threading.get_ident()] = self.saved_state


def get_path_key(host):
    return "path-{}".format(host)


def path_changer(host, path):
    state = get_state()
    key = get_path_key(host)

    if key not in state:
        current_path = ""
    else:
        current_path = state[key]

    return StateChanger({key: join(current_path, path)})


def lcd(path):
    return path_changer("localhost", path)


def cd(path):
    return path_changer("remotehost", path)


def host(new_host):
    return StateChanger({"host": new_host, get_path_key("remotehost"): "."})


def proxy(condition):
    return StateChanger({"use_proxy": True} if condition else {})


def sudo(password):
    return StateChanger({"use_sudo": True, "password": password})


def skip_output():
    return StateChanger({"skip_output": True})


def get_current_path(host):
    state = get_state()
    key = get_path_key(host)

    if key not in state:
        return "."
    else:
        return state[key]


def get_current_host():
    state = get_state()
    key = "host"

    if key not in state:
        raise Exception("No host provided")
    else:
        return state[key]


def is_flag_on(key):
    state = get_state()
    return key in state and state[key]


def get_proxy_part():
    if is_flag_on("use_proxy"):
        return "proxychains "
    else:
        return ""


def get_sudo_part():
    if is_flag_on("use_sudo"):
        state = get_state()
        return "echo {} | sudo -S ".format(state["password"])
    else:
        return ""


def add_cd_to_command(path, command):
    if path == ".":
        return command
    else:
        return "cd {} >/dev/null && {}".format(path, command)


def execute(command):
    current_path = get_current_path("localhost")
    final_command = add_cd_to_command(current_path, command)

    process = subprocess.Popen(final_command, stdout=subprocess.PIPE, shell=True,
                               universal_newlines=True)
    stdout = ""
    for line in process.stdout:
        if line != "ProxyChains-3.1 (http://proxychains.sf.net)\n":
            if not is_flag_on("skip_output"):
                sys.stdout.write(line)
            stdout += line + "\n"

    return_code = process.wait()
    if return_code != 0:
        raise Exception("nonzero exit code for {}".format(final_command))

    return stdout


def local(command):
    print("[localhost] local: {}".format(command))
    return execute(command)


def run(command):
    current_host = get_current_host()
    current_path = get_current_path("remotehost")

    print("[{}] run: {}".format(current_host, command))
    sudo_command = get_sudo_part() + command
    return execute("{}ssh -q {} 'source /etc/profile; {}'".format(get_proxy_part(), current_host,
                                                                  add_cd_to_command(current_path, sudo_command)))


def put(src, des):
    current_host = get_current_host()
    current_path = get_current_path("remotehost")
    absolute_des = join(current_path, des)

    print("[{}] put: {} -> {}".format(current_host, src, absolute_des))
    execute("{}scp -r {} {}:{}".format(get_proxy_part(), src, current_host, absolute_des))


def rsync(src, des):
    current_host = get_current_host()
    current_path = get_current_path("remotehost")
    absolute_des = join(current_path, des)

    print("[{}] rsync: {} -> {}".format(current_host, src, absolute_des))
    execute("{}rsync {} {}:{} -az --delete".format(get_proxy_part(), src, current_host, absolute_des))


def get(src, des):
    current_host = get_current_host()
    current_path = get_current_path("remotehost")
    absolute_src = join(current_path, src)

    print("[{}] get: {} -> {}".format(current_host, src, des))
    execute("{}scp -r {}:{} {}".format(get_proxy_part(), current_host, absolute_src, des))


def exists(path):
    try:
        run("ls -d {}".format(path))
    except:
        return False
    return True


def get_content(path):
    current_host = get_current_host()
    current_path = get_current_path("remotehost")
    absolute_path = join(current_path, path)

    return_code, stdout = subprocess.getstatusoutput(
        "{}ssh {} 'cat {}'".format(get_proxy_part(), current_host, absolute_path))

    return stdout


def put_content(path, content):
    run('printf "{}" > {}'.format(content.replace("\n", "\\n"), path))
