from os.path import join

from Utils.Utils import create_folder, send_sms

__author__ = 'Mohammad'

import os
from datetime import datetime

from Utils.jfabric import lcd, local


class Backup:
    backup_folder = "/home/ut/Backup/"

    def __init__(self, folders, repository):
        self.repository = repository
        create_folder(self.backup_folder)
        local("rm -rf /home/ut/Backup/*")
        self.backup_default_folders()
        self.backup(folders)
        self.push_to_repository(self.repository)

    def backup_default_folders(self):
        local(
            "rsync -a --prune-empty-dirs --exclude 'jobs'  --include '*/' --include '*.xml' --exclude '*'  /j/ /home/ut/Backup/jenkins_backUp")
        local(
            "rsync -a --prune-empty-dirs --exclude '*/*/'   --include '*/' --include '*.xml' --exclude '*'  /j/jobs/ /home/ut/Backup/jenkins_backUp/jobs/")

    def backup(self, folders):
        for source in folders:
            dest_dir = os.path.basename(source.strip("/"))
            local(
                "rsync -a --prune-empty-dirs --include '*/' {} {}".format(source, join(self.backup_folder, dest_dir)))
            local("rm -rf {}".format(join(self.backup_folder, dest_dir, ".git")))

    def push_to_repository(self, repasitory):
        with lcd(self.backup_folder):
            local("git init")
            local("git add --all")
            local("git status")
            local("git commit -a -m " + str(datetime.now()).replace(" ", "_"))
            try:
                local("git remote add origin " + repasitory)
            except:
                print("remote origin already exists")
            local('git push -f origin master')
