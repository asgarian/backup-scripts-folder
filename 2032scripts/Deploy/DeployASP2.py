from Deploy.DeployProduction import DeployProduction
from Utils.Utils import section, run_windows_command

__author__ = 'borna'


class DeployAsp2(DeployProduction):
    def __init__(self, deploy_info):
        super().__init__(deploy_info)
        self.warning_exit_code = 1

    def run_corresponding_script(self, name):
        return run_windows_command("C:\\\\JenkinsScripts\\\\Deploys\\\\{}.ps1 {} {} {} {}".format(
            name,
            self.deploy_info.get_deploy_name(),  # job name
            self.deploying_number,  # version
            self.deploy_info.number,  # deploy id
            self.deploy_info.LOCATION.long_name))  # full location

    @section
    def config(self):
        self.run_corresponding_script("ASP_Config")

    def copy(self):
        pass

    def is_allowed(self):
        return True

    @section
    def run(self):
        exit_code = self.run_corresponding_script("ASP_Copy-Run")

        print(exit_code)

    def report(self):
        pass

    def prepare_host(self):
        pass

    def deploy_start_notify(self):
        pass

    def start_report_telegram(self):
        pass