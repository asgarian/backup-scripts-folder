"""
add is_alowed and report logics to deploy
"""
from os.path import join
import time
from copy import copy
from datetime import datetime

import configs
from Deploy.DeployAgent import DeployAgent
from Deploy.DeployUtils.DeployReportCsv import DeployReportCsv
from Utils.TelegramBot import TelegramBot
from Utils.Utils import section, send_sms, remove_color, warning
from Utils.jfabric import host, run, local, skip_output


class DeployProduction(DeployAgent):
    def __init__(self, deploy_info):
        super().__init__(deploy_info)
        self.warning_exit_code = 0
        self.message_content = remove_color("\n".join(
            ["Version : " + self.deploying_number, "Gomrok : " + self.deploy_info.get_nice_location(), ]))
        #self.check_placeip()

    @section
    def is_allowed(self):
        if self.name in configs.excluded_projects:
            print("Deploy allowed for excluded project!")
            return True
        if self.deploy_info.LOCATION.code not in configs.non_pilot_customs:
            print("Deploy allowed for pilot custom!")
            return True
        if self.deploy_info.DPATH != "default":
            print("Deploy allowed for non default path!")
            return True
        if self.deploy_info.BUILD_USER in configs.deploy_superusers:
            print("Deploy allowed for {}!".format(self.deploy_info.BUILD_USER))
            return True

        first_successful_deploy = DeployReportCsv.find_first_successful_deploy(self.deploy_info)
        if first_successful_deploy:
            print("First successful deploy is " + first_successful_deploy.BUILD_URL)
            delta_time = datetime.now() - first_successful_deploy.time
            if delta_time.total_seconds() > (3600 * configs.confidence_interval):
                print("Confidence interval passed!")
                return True
            else:
                print("Confidence interval still remains! you can deploy after", delta_time)
                return False
        else:
            print("First deploy on pilot customs!")
            return False

    @section
    def prepare_host(self):
        with host("2013"):
            run("ssh-copy-id -i jenkins/id_rsa.pub {}".format(self.host))
            with skip_output():
                password_line = run("python /home/mkt/abz/decrypt.py -s {}  | sed -n 1p".format(self.deploy_info.LOCATION.code))
            self.password = password_line.replace("\n", "")

        local("proxychains ssh {} -o StrictHostKeyChecking=no uptime".format(self.host))

        super().prepare_host()

    @section
    def report(self):
        self.add_to_csv()
        self.send_sms()
        self.result_report_telegram()

    def send_sms(self):
        self.sms_content = remove_color("\n".join([
            self.deploy_info.get_nice_deploy_name(),
            self.result_description,
            self.deploy_info.get_nice_location(),
            "V:" + self.deploying_number,
            self.deploy_info.BUILD_USER,
        ]))
        phones = self.get_phone_numbers()

        for phone in phones:
            send_sms(phone, self.sms_content)

    def add_to_csv(self):
        deploy_report = DeployReportCsv(self)
        deploy_report.add_to_file()

    def get_phone_numbers(self):
        if self.deploy_info.FAKE:
            return ["09394760654"]

        result = configs.deploy_audiences
        if self.name in configs.deploy_audiences_for_project:
            result += configs.deploy_audiences_for_project[self.name]

        return result

    @section
    def check_arrangement(self):
        if self.name not in configs.arrangement_projects:
            return

        arrangemets = DeployReportCsv.get_customs_arragments()
        similar_arrangements = []

        if self.deploy_info.LOCATION.name in arrangemets:
            final_arrangement = copy(arrangemets[self.deploy_info.LOCATION.name])
            final_arrangement.set_version(self.name, self.deploying_number, datetime.now())
            self.inform("Current arrangement is: {}".format(final_arrangement))

            for location in arrangemets:
                arrangemet = arrangemets[location]
                if final_arrangement == arrangemet:
                    similar_arrangements.append(arrangemet)

        if similar_arrangements:
            self.inform(
                "found {} similar arrangements: {}".format(len(similar_arrangements),
                                                           ", ".join(str(x) for x in similar_arrangements)))
        else:
            self.inform(warning("No similar arrangment found!"))

    def start_report_telegram(self):
        try:
            self.reporter = TelegramBot()
            self.reporter.send_message_to_bot(
                "\n".join(["{} started ".format(self.deploy_info.name),
                           self.message_content,
                           ]), self.deploy_info.FAKE)
        except Exception as ex:
            print(ex)

    def result_report_telegram(self):
        try:
            finish_message_content = remove_color("\n".join(
                ["Result : " + self.result_description,
                 "Version : " + self.deploying_number, "Gomrok : " + self.deploy_info.get_nice_location(), ]))
            self.reporter.send_message_to_bot("\n".join(["{}  is finished ".format(self.deploy_info.name),
                                                         finish_message_content]), self.deploy_info.FAKE)
        except Exception as ex:
            print(ex)

    def deploy_start_notify(self):
        if self.deploy_info.START_NOTIFY:
            phones = self.deploy_info.LOCATION.phone_number.split("-")

            for phone in phones:
                send_sms(phone,
                         "{} of {} will be updated in 10 min if you dont want this plz call HelpDesk".format(
                             self.deploy_info.name.replace(" Deploy", ""),
                             self.deploy_info.LOCATION.name))

            time.sleep(900)

    def check_placeip(self):
        if self.name == "Acc":
            config_file_name = "{}.conf".format(self.deploy_info.LOCATION.code)
            config_path = join("/acc_ghif", config_file_name)
            cmd1 = "sed -n 10p {} | cut -d '=' -f 2".format(config_path)
            expected_ip = local(cmd1).strip()

            if self.deploy_info.LOCATION.ip != expected_ip:
                raise Exception('Correct ip from configuration menu!')
