#
# Represent arrangement of apps on a customs
# for example: Baskool build 123, Anbar build 234, Customs build 324 and ...
#

class Arrangement:
    def __init__(self, name):
        self.name = name

        self.versions = {}

    def get_version_for_project(self, name):
        if name in self.versions:
            return self.versions[name][0]
        else:
            return None

    def set_version(self, project, version, deploy_time):
        must_update = False
        if project in self.versions:
            (current_version, current_deploy_time) = self.versions[project]

            if current_deploy_time < deploy_time:
                must_update = True
        else:
            must_update = True

        if must_update:
            self.versions[project] = (version, deploy_time)

    def __eq__(self, other):
        for project in self.versions:
            if other.get_version_for_project(project) != self.get_version_for_project(project):
                return False

        for project in other.versions:
            if project not in self.versions:
                return False

        return True

    def __str__(self):
        str = "[{}]: (".format(self.name)
        versions_str = []

        for project in self.versions:
            version = self.get_version_for_project(project)
            versions_str.append("{}: {}".format(project, version))

        str += ", ".join(versions_str) + ")"

        return str
