class DatabaseInfo:
    def __init__(self, database_info_file_content):
        for line in database_info_file_content:
            if line.startswith("database_name="):
                self.name = line.replace("database_name=", "")

            if line.startswith("database_url="):
                self.url = line.replace("database_url=", "")

            if line.startswith("database_user="):
                self.user = line.replace("database_user=", "")

            if line.startswith("database_pass="):
                self.password = line.replace("database_pass=", "")
