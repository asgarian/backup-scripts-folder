from Build.BuildInfo import BuildInfo
from Deploy.DeployUtils.DeployLocation import DeployLocation
from Utils.Operation import Operation


class DeployInfo(Operation):
    def __init__(self, value):
        super().__init__(value[2], value[3])
        self.BUILD_URL = value[4]
        self.BUILD_USER = value[5]
        self.DEPLOY_REPORTER = False
        if value[1] not in ["PrepareForTest", "ClusterDeploy"]:
            self.DEPLOYING_BUILD = BuildInfo(value[6])
        if "," in value[7]:
            self.LOCATION = []
            locations = value[7].split(",")
            for location in locations:
                self.LOCATION.append(DeployLocation(location))
        else:
            self.LOCATION = DeployLocation(value[7])
        self.DPATH = "default" if value[8] == "" else value[8]
        self.COMMENT = value[9]
        self.ONLY_TEST = (value[10] == "true")
        self.FORTI_DOWN = (value[11] == "true")
        self.FAKE = (value[12] == "true")
        if len(value) > 13:
            self.FORCE = (value[13] == "true")
        else:
            self.FORCE = True

        if len(value) > 14:
            self.START_NOTIFY = (value[14] == "true")
        else:
            self.START_NOTIFY = False
        if value[2] in ["Customs Deploy"]:
            self.DEPLOY_REPORTER = value[15]
            self.VERSION = "{}#{}".format(value[6].split("/")[4], value[6].split("/")[5])
            self.REPORTER_LOCATION = self.LOCATION.long_name.replace("Customs", "customs-reporter-server")


    # sample output: Zobahan/test
    def get_nice_location(self):
        dpath_part = "" if self.DPATH == "default" else "/" + self.DPATH
        return self.LOCATION.name + dpath_part

    # sample output: CRS instead of customs-reporter-server
    def get_nice_deploy_name(self):
        return self.get_deploy_name().replace("customs-reporter-server", "CRS")

    # sample output: Customs or customs-reporter-server or ...
    def get_deploy_name(self):
        return self.name.replace(" Deploy", "").replace(" Private", "")
