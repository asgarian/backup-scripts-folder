"""
get a cluster name and deploy to all of its servers the last stable version
by calling deploy agents concurrently and waiting for all of them to finish

you can provide for a project a custom version and master agent will use it
"""
import threading
from copy import copy

import threading
from Deploy.DeployASP2 import DeployAsp2

from Deploy.DeployTest import DeployTest
from Deploy.DeployUtils.DeployLocation import DeployLocation
from Deploy.Observer import Observer
from Build.BuildInfo import BuildInfo
import configs
from Deploy.DeployUtils.DeployLocation import DeployLocation
from Utils.Utils import get_last_stable_version, error
from Utils.jfabric import run, host


class DeployMasterAgent(object):
    def __init__(self, build_info, cluster_name):
        self.build_info = build_info
        self.trigge_jenkins_build = True
        self.observer = Observer("Deploy")
        self.deploy_counter = 1
        self.cluster = configs.clusters[cluster_name]
        self.exit_code = 0

        # for forcing a version for a project
        self.fv_project = None

    def deploy(self):
        deploys = [(project, location) for project in self.cluster for location in self.cluster[project]]
        threads = []

        revert_done = False
        for (project, location) in deploys:
            if ~revert_done and location == '12':
                revert_done = True
                print("Reverting database on 12")
                with host('12'):
                    run("sudo docker stop f3448b4224736c3")
                    run("sudo rm -r /home/ut/docker/mysql")
                    run("sudo cp -r /home/ut/docker/mysql-back-2 /home/ut/docker/mysql")
                    run("sudo docker start f3448b4224736c3")
            t = threading.Thread(target=self.employ_new_agent, args=(project, location))
            t.start()
            threads.append(t)

        # join all threads
        for t in threads:
            t.join()

        return self.exit_code

    def employ_new_agent(self, project, location):
        if project == self.fv_project:
            version = self.fv_version
        else:
            version = get_last_stable_version(project)

        deploy_info = copy(self.build_info)

        deploy_info.name = "{} Private Deploy".format(project)
        deploy_info.DEPLOYING_BUILD = BuildInfo([project, version])
        deploy_info.LOCATION = DeployLocation(self.cluster[project][location])
        deploy_info.FORCE = False

        # for temp folder name to be unique
        deploy_info.number = "{}-{}".format(self.build_info.number, self.deploy_counter)
        self.deploy_counter += 1

        if project in configs.asp_projects:
            agent = DeployAsp2(deploy_info)
        else:
            agent = DeployTest(deploy_info)

        agent.print_section_mark = False
        agent.subscribe_to(self.observer)

        try:
            agent_exit_code = agent.deploy()
        except Exception as ex:
            agent.inform(error(str(ex)))
            agent.set_status(error("Error in {}: {}".format(agent.get_status(), ex)))
            agent_exit_code = 1

        if agent_exit_code != 0:
            self.exit_code = 1

    def force_version(self, build_info):
        self.fv_project = build_info.name
        self.fv_version = build_info.number
