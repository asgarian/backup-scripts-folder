"""
Deploy agent is the parent of all deploys that determine deploy steps
Other deploys only changes the necessary steps by overriding the corresponding methods
see DeployAgent.deploy
"""
from datetime import datetime
from os.path import join
import re

from dateutil.parser import parse

from Deploy.Observable import Observable
from Utils.Utils import section, urljoin, HttpProxy, try_get_url, error, green, warning, get_items_path, \
    recreate_folder, blue, am_i_newjenkins
from Utils.jfabric import run, cd, host, lcd, local, proxy, sudo, rsync
import configs


class DeployAgent(Observable):
    def __init__(self, deploy_info):
        super().__init__()

        self.deploy_info = deploy_info
        self.name = deploy_info.get_deploy_name()
        self.deploying_number = deploy_info.DEPLOYING_BUILD.number
        self.print_section_mark = True
        self.warning_exit_code = 1
        self.initialize_host()
        self.rsync_des = join("/home", self.user, "jenkins", self.name)
        self.password = ""

    def deploy(self):
#        print(error("deploy is not allowed now"))
#        return 1
        self.start_report_telegram()
#        if not self.need_deploy(): return 0
        if not self.is_allowed(): return 1
        self.deploy_start_notify()
        self.check_arrangement()
        self.config()
        self.prepare_host()
        self.copy()
        self.run()
        self.check(5)
        if self.exit_code == 0: self.postdeploy()
        self.report()
        return self.exit_code

    # if force is false will check if desired version is currently running on target server
    # by getting buildDate
    def need_deploy(self):
        if self.deploy_info.FORCE:
            return True

        try:
            self.check(1)
        except Exception as ex:
            self.inform(str(ex))
            return True

        if self.exit_code == 0:
            self.set_status(blue("Already Deployed"))
            self.inform("Version {} is currently on server {} and no need to deploy", self.deploying_number,
                        self.deploy_info.LOCATION.name)
            return False

        return True

    def is_allowed(self):
        return True

    # responsible for setting self.configed_artifact
    @section
    def config(self):
        #
        # comments after each statement stands for sample values
        # please update them after changes
        #

        artifact_path = self.deploy_info.DEPLOYING_BUILD.get_artifact_path()
        # artifact_path = /stable-builds/Customs/492/target/Customs-5.5.3-r000.war

        self.artifact_name = self.deploy_info.DEPLOYING_BUILD.get_artifact_name()
        # artifact_name = Customs-5.5.3-r000.war

        temp_folder = self.deploy_info.get_temp_folder()
        # temp_folder = /jenkins/temp/Customs_private_deploy/23

        self.configed_artifact = join(temp_folder, self.name)
        # configed_artifact = /jenkins/temp/Customs_private_deploy/23/Customs

        config_path = join(self.get_config_path(), "*")
        # config_path = /jenkins/temp/Customs_private_deploy/23/configs/*

        with lcd(temp_folder):
            local("unzip -q {} -d {}".format(artifact_path, self.configed_artifact))
            if get_items_path(config_path):
                local("cp -r {} {}".format(config_path, self.configed_artifact))
            else:
                self.inform("No config to add!")

            self.run_config_sh()

    def run_config_sh(self):
        # check if config.sh exists
        if get_items_path(join(self.configed_artifact, "config.sh")):
            with lcd(self.configed_artifact):
                local("/bin/bash config.sh {}".format(self.deploy_info.LOCATION.code))

    @section
    def copy(self):
        with proxy(not am_i_newjenkins()), host(self.host):
            rsync(self.configed_artifact + "/", self.rsync_des)

    @section
    def run(self):
        with proxy(not am_i_newjenkins()), host(self.host), sudo(self.password), cd(self.rsync_des):
            if self.deploy_info.DPATH == "default":
                path_name = self.deploy_info.LOCATION.path
            else:
                path_name = self.deploy_info.DPATH

            run_command = "/bin/bash deploy.sh {} {}".format(self.name, path_name)

            if not self.deploy_info.FAKE:
                run(run_command)
            else:
                print("instead of '{}' we run 'ls'".format(run_command))
                run("ls")

    def initialize_host(self):
        self.user = "ut"

        if self.name == "Tir":
            self.user = "iru"
        if self.name == "Acc":
            with host("2013"):
                line = run("cat auto/Acc/customsnameswithurls | grep {}=".format(self.deploy_info.LOCATION.code))
                self.user = re.search("\d+=(.+)@(.+)", line).group(1)

        ip = self.deploy_info.LOCATION.ip
        self.host = "{}@{}".format(self.user, ip)

    # create a temp folder containing all configs needed for current deploy
    #
    # we have three sources for configs:
    # 1) per group configs
    # 2) per project configs
    # 3) per location configs
    #
    # this method combine these sources into a folder and return path of that
    #
    def get_config_path(self):
        config_path = join(self.deploy_info.get_temp_folder(), "configs")
        # config_path = /jenkins/temp/Customs_private_deploy/23/configs

        per_group_configs = self.deploy_info.DEPLOYING_BUILD.get_group_name()
        per_project_configs = self.name + "s"
        per_location_configs = join(self.name, self.deploy_info.LOCATION.code)
        config_sources = [per_group_configs, per_project_configs, per_location_configs]

        recreate_folder(config_path)
        with lcd(config_path):
            for config_folder_name in config_sources:
                config_source = join(configs.configs_folder, config_folder_name, ".")
                # '.' at the end of the path is for including hidden files too

                if get_items_path(config_source):
                    local("cp {} . -r".format(config_source))

        return config_path

    # responsible for setting self.exit_code and self.result_description
    @section
    def check(self, try_count):
        try:
            build_date_from_file = self.get_build_date_from_file()
            self.inform("Expected date is {}", build_date_from_file)

            build_date_from_site = self.get_build_date_from_site(try_count)
            if not build_date_from_site:
                self.result_description = warning(
                    "Error: Cannot get buildDate from {}".format(self.get_build_date_url()))
                self.exit_code = self.warning_exit_code
                return

            self.inform("Current date is {}", build_date_from_site)
            self.inform("Expected date is {}", build_date_from_file)

            deployed_successfully = self.compare_build_dates(build_date_from_file, build_date_from_site)

            if deployed_successfully:
                self.result_description = green("Deployed Successfully")
                self.exit_code = 0
            else:
                self.result_description = error("Error in Deploy")
                self.exit_code = 1

        except Exception as ex:
            self.exit_code = 2
            self.result_description = "Could not check build date"
            raise ex
        finally:
            self.set_status(self.result_description)
            self.inform(self.result_description)

    def get_build_date_from_file(self):
        build_date_from_file_str = self.deploy_info.DEPLOYING_BUILD.get_build_date()
        try:
            build_date_from_file = parse(build_date_from_file_str)
        except ValueError:
            raise Exception("invalid date from file: {}", build_date_from_file_str)

        return build_date_from_file

    def get_build_date_from_site(self, try_count):
        build_date_url = self.get_build_date_url()

        with HttpProxy():
            build_date_from_site_str = try_get_url(build_date_url, try_count=try_count, timeout=30, sleep_time=30)
        if not build_date_from_site_str:
            return None

        try:
            if build_date_from_site_str.isdigit():
                build_date_from_site = datetime.fromtimestamp(int(build_date_from_site_str) / 1000)
            else:
                build_date_from_site = parse(build_date_from_site_str)
        except ValueError as ex:
            raise Exception("invalid date from site: {}".format(build_date_from_site_str)) from ex

        return build_date_from_site

    def get_build_date_url(self):
        if self.name in configs.build_date_postfix:
            build_date_postfix = configs.build_date_postfix[self.name]
        else:
            build_date_postfix = "/buildDate"

        build_date_url = urljoin("http://" + self.deploy_info.LOCATION.url,
                                 ("" if self.deploy_info.DPATH == "default" else self.deploy_info.DPATH),
                                 build_date_postfix)
        return build_date_url

    def compare_build_dates(self, build_date_from_file, build_date_from_site):
        try:
            build_date_from_file = build_date_from_file.replace(tzinfo=None)
            build_date_from_site = build_date_from_site.replace(tzinfo=None)
            delta_time = abs((build_date_from_file - build_date_from_site).total_seconds())  # seconds
        except TypeError as ex:
            self.inform(str(ex))
            delta_time = 9999999

        deployed_successfully = delta_time < 4 * 60 or abs(delta_time - 3.5 * 3600) < 180 or abs(
            delta_time - 4.5 * 3600) < 180
        return deployed_successfully

    def postdeploy(self):
        pass

    def report(self):
        pass

    def get_name_for_observer(self):
        return "{} in {}".format(self.name, self.deploy_info.LOCATION.name)

    def prepare_host(self):
        with proxy(not am_i_newjenkins()), host(self.host):
            run("mkdir -p {}".format(self.rsync_des))

    def check_arrangement(self):
        pass

    def deploy_start_notify(self):
        pass

    def start_report_telegram(self):
        pass
