#
# any class that can be observed by Observer
#


class Observable(object):
    def __init__(self):
        self.subscribe_to(None)
        self.status = None

    def get_name_for_observer(self):
        return "Observable"

    def subscribe_to(self, observer):
        self.observer = observer
        if self.observer:
            self.observer.subscribe(self)

    def set_status(self, *args, **kwargs):
        status = str.format(*args, **kwargs)
        self.status = status
        if self.observer:
            self.observer.set_status(self, status)

    def get_status(self):
        return self.status

    def inform(self, *args, **kwargs):
        if len(args) > 1:
            message = str.format(*args, **kwargs)
        else:
            message = args[0]

        if self.observer:
            self.observer.inform(self, message)
        else:
            print(message)
