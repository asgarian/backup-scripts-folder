#!/bin/bash

#     set proxy 172.17.193.220 7006

apt-get update
apt-get -y install apt-transport-https ca-certificates
apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
mkdir -p "/etc/apt/sources.list.d"
echo "deb https://apt.dockerproject.org/repo ubuntu-xenial main" > /etc/apt/sources.list.d/docker.list
apt-get update
apt-get purge lxc-docker
apt-cache policy docker-engine
apt-get -y install docker-engine
curl -L https://github.com/docker/compose/releases/download/1.7.1/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose


mkdir -p "/etc/docker/certs.d/hub.docker.iais.co:5000"
scp ut@172.16.111.200:ca.crt .
cp ca.crt /etc/docker/certs.d/hub.docker.iais.co:5000


#systemctl unmask docker.service
#systemctl unmask docker.socket
#systemctl start docker.service

echo "if ! shopt -oq posix; then
 if [ -f /usr/share/bash-completion/bash_completion ]; then
   . /usr/share/bash-completion/bash_completion
 elif [ -f /etc/bash_completion ]; then
   . /etc/bash_completion
 fi
fi" >> /etc/bash.bashrc



echo -e "\t\t\t\t\t*******all good*******"
docker --version
docker-compose --version
