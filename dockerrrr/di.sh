scp ut@172.16.111.200:docker-compose .
cp docker-compose /usr/local/bin
chmod +x /usr/local/bin/docker-compose
mkdir -p "/etc/docker/certs.d/hub.docker.iais.co:5000"
scp ut@172.16.111.200:ca.crt .
cp ca.crt /etc/docker/certs.d/hub.docker.iais.co:5000

echo "if ! shopt -oq posix; then
 if [ -f /usr/share/bash-completion/bash_completion ]; then
   . /usr/share/bash-completion/bash_completion
 elif [ -f /etc/bash_completion ]; then
   . /etc/bash_completion
 fi
fi" >> /etc/bash.bashrc

scp ut@172.16.111.200:docker .
cp docker /etc/bash_completion.d

echo -e "\t\t\t\t\t*******all good*******"
docker --version
docker-compose --version

