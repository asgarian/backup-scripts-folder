EXEC sp_configure 'show advanced options', 1
GO
RECONFIGURE
GO
EXEC sp_configure 'xp_cmdshell', 1
GO
RECONFIGURE
GO
EXEC xp_cmdshell 'net use * /d /Y';
EXEC xp_cmdshell 'net use  Z: "\\10.56.0.18\backups" /Y /persistent:no /User:ut mkt123sjb'
DECLARE @BackupFileName varchar(100)
SET @BackupFileName = 'z:\anbar\inc/' + REPLACE(REPLACE(REPLACE(CONVERT(varchar,GETDATE(), 20),'-',''),':',''),' ','') + '-diff- Warehouse.BAK'
BACKUP DATABASE Warehouse
TO DISK=@BackupFileName
WITH DIFFERENTIAL
go

