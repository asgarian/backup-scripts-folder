#!/bin/bash

args=("$@")
if [[ $# -le 1 ]] ; then
    echo -e 'follow the example:\nEx:  bash restore.sh [backup-path] [backup-name] '
    exit 0
fi


echo "restore plan beggining..."

############################### Stop cassandra ###############################
function stop {
	cd /home/ut/cassandra/bin
	user="ut"
	pgrep -u $user -f cassandra | xargs kill -9
}
############################### End of stop cassandra ###############################
############################### Start cassandra ###############################
function start {
	cd /home/ut/cassandra/bin
	./cassandra
}
############################### End of start cassandra ###############################

############################### Restore schema ###############################

cd /home/ut/cassandra/bin
ls /var/lib/cassandra/data >t1
sed -i '/system\|system_auth\|system_distributed\|system_schema\|system_traces/d' t1
chnull=$(cat t1)
if [ -n "$chnull" ]; then
	for VAR in $(cat t1)
        do
        	./cqlsh -e "DROP KEYSPACE $VAR"
        	rm -rf /var/lib/cassandra/data/$VAR/
        done
fi
rm t1
rm -rf /var/lib/cassandra/commitlog/*.*
cd /home/ut/cassandra/bin
ls ${args[0]}/${args[1]}/SCHEMA > temp
sed -i '/system\|system_auth\|system_distributed\|system_schema\|system_traces/d' temp
for VAR_KEYSPACE in $(cat temp)
	do
	./cqlsh -f ${args[0]}/${args[1]}/SCHEMA/$VAR_KEYSPACE/*.cql
	rm /var/lib/cassandra/data/$VAR_KEYSPACE/*/*.*
	cp -rf ${args[0]}/${args[1]}/SNAPSHOTS/$VAR_KEYSPACE/*/snapshots/*/* /var/lib/cassandra/data/$VAR_KEYSPACE/*/
	done
stop
############################### End of  Restore schema ###############################
chown ut:ut /var/lib/cassandra/ -R
sleep 5s
start

############################### Restore data ###############################

#sudo rm /var/lib/cassandra/data/*/*/*.*
#cp -rf /backup/*/SNAPSHOTS/$VAR_KEYSPACE/*/snapshots/*/* $dataloc/$VAR_KEYSPACE/*/
#sudo rsync -avz $bakloc/*/SNAPSHOTS/* $dataloc/
#chown ut:ut /var/lib/cassandra/ -R
#rm -rf /var/lib/cassandra/data/$VAR_KEYSPACE



############################### End of  Restore data ###############################

#nodetool -h localhost -p 7199 repair

sleep 5s
rm /home/ut/cassandra/bin/temp
echo " successfully executed"

