#!/bin/bash
args=("$@")
if [[ $# -le 1 ]] ; then
    echo -e 'follow the example:\nEx:  bash incbak.sh [incremental-backup-path] [incremental-backup-name] '
    exit 0
fi

_BACKUP_DIR=${args[0]}
_DATA_DIR=/var/lib/cassandra/data
_TODAY_DATE=${args[1]}
_INCDIR="$_BACKUP_DIR/$_TODAY_DATE/INCDIR"


########################
ls /var/lib/cassandra/data >t1
sed -i '/system\|system_auth\|system_distributed\|system_schema\|system_traces/d' t1
chnull=$(cat t1)
if [ -n "$chnull" ]; then
        for VAR in $(cat t1)
        do
                rm -rf $_DATA_DIR/$VAR/*/backups/*
#		rm $_DATA_DIR/$VAR/*/*.* 
        done
fi
rm t1

###### Necessary Variables #########
cd /home/ut/cassandra/bin
./nodetool flush

###### Create Necessary Directory ####

if [ -d  "$_INCDIR" ]
then
echo "$_INCDIR already exist"
else
mkdir -p "$_INCDIR"
fi

######################################

ls /var/lib/cassandra/data >t1
sed -i '/system\|system_auth\|system_distributed\|system_schema\|system_traces/d' t1
chnull=$(cat t1)
if [ -n "$chnull" ]; then
        for VAR in $(cat t1)
        do
                cp -rf $_DATA_DIR/$VAR/*/backups/* $_INCDIR
        done
fi
rm t1

