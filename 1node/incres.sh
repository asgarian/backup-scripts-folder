#!/bin/bash

args=("$@")
if [[ $# -le 1 ]] ; then
    echo -e 'follow the example:\nEx:  bash incres.sh [incremental-backup-path] [incremental-backup-name] '
    exit 0
fi
echo "restore plan beggining..."

###### Necessary Variables #########
cd /home/ut/cassandra/bin
_BACKUP_DIR=${args[0]}
_DATA_DIR=/var/lib/cassandra/data
_TODAY_DATE=${args[1]}
_INCDIR="$_BACKUP_DIR/$_TODAY_DATE/INCDIR"

############################### Stop cassandra ###############################
function stop {
        cd /home/ut/cassandra/bin
        user="ut"
        pgrep -u $user -f cassandra | xargs kill -9
}
############################### End of stop cassandra ###############################
############################### Start cassandra ###############################
function start {
        cd /home/ut/cassandra/bin
        ./cassandra
}
############################### End of start cassandra ###############################
############################### Restore schema ###############################
stop
rm -rf /var/lib/cassandra/commitlog/*.*
ls /var/lib/cassandra/data >t1
sed -i '/system\|system_auth\|system_distributed\|system_schema\|system_traces/d' t1
chnull=$(cat t1)
if [ -n "$chnull" ]; then
        for VAR in $(cat t1)
        do
                rm -rf $_DATA_DIR/$VAR/*/backups/* 
		cp  $_INCDIR/* $_DATA_DIR/$VAR/*/ 
        done
fi
rm t1
start
