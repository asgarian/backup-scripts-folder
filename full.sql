EXEC xp_cmdshell 'netsh firewall set portopening protocol = TCP port = 1433 name = SQLPort mode = ENABLE scope = SUBNET profile = CURRENT'
GO
DELETE FROM MSDB..SYSMAINTPLAN_LOG WHERE PLAN_ID IN(SELECT ID FROM MSDB..SYSMAINTPLAN_PLANS) 
GO
DELETE FROM MSDB..SYSMAINTPLAN_SUBPLANS WHERE PLAN_ID IN(SELECT ID FROM MSDB..SYSMAINTPLAN_PLANS)
GO
DELETE FROM MSDB..SYSMAINTPLAN_PLANS WHERE ID IN(SELECT ID FROM MSDB..SYSMAINTPLAN_PLANS)
GO
EXEC sp_configure 'show advanced options', 1
GO
RECONFIGURE
GO
EXEC sp_configure 'xp_cmdshell', 1
GO
RECONFIGURE
GO
EXEC xp_cmdshell 'net use * /d /Y';
EXEC xp_cmdshell 'net use  Z: "\\10.56.0.18\backups" /Y /persistent:no /User:ut mkt123sjb'
DECLARE @BackupFileName varchar(100)
SET @BackupFileName = 'z:\anbar/' + REPLACE(REPLACE(REPLACE(CONVERT(varchar,GETDATE(), 20),'-',''),':',''),' ','') + '- Warehouse.BAK'
BACKUP DATABASE Warehouse
TO DISK=@BackupFileName
go

