#
# gateway of jenkins: every things starts from here
#
import inspect
import re
import sys

from Build.Build import Build
from Build.BuildInfo import *
from Deploy.DeployInfo import DeployInfo
from Deploy.DeployMasterAgent import DeployMasterAgent
from Test.SeleniumMasterAgent import SeleniumMasterAgent
from Test.SendToDeployJenkinsAgent import SendToDeployJenkinsAgent
from Test.TestInfo import TestInfo
from Utils.JenkinsUtils import decode_jenkins_args, mark_current_build_as_unstable, \
    UnstableException, update_current_package_based_on_last_stable_links, get_upstream_build
from Utils.Utils import print_exception


def run_selenium_tests(params):
    info = TestInfo(params)
    tester = SeleniumMasterAgent(info)
    exit_code = tester.run()

    exit(exit_code)


def cluster_deploy(params):
    with DeployInfo(params) as info:
        deploy_agent = DeployMasterAgent(info, params["cluster_config"])

        for version_line in params["version"].split("\n"):
            match = re.match("(.*?)\s*:\s*([\w\d]+(-\d+)?)", version_line)
            if match:
                deploy_agent.force_version(BuildInfo(name=match.group(1), version=match.group(2)))

        exit_code = deploy_agent.deploy()

        if exit_code != 0:
            raise Exception("Error in deploy")

def SendToDeployJenkins(params):
    if configs.jenkins_id != 1:
        raise Exception("only jenkins 0 can do this operation!")
    else:
        info = Operation(params["job_name"], params["build_number"])
        if params["project"]:
            if params["version"]:
                target_build = BuildInfo(params["project"], version=params["version"])
            else:
                raise Exception("VERSION field should not be empty")
        else:

            target_build = get_upstream_build(info)
        sender = SendToDeployJenkinsAgent(target_build, info)
        sender.send_to_deploy_jenkins()

        sys.exit(0)

def build(params):
    try:
        build_info = BuildInfo(params["job_name"], number=params["build_number"])
        agent2 = Build(build_info, params)
        agent2.run()
    except UnstableException:
        mark_current_build_as_unstable()


def sync_current_package(params):
    update_current_package_based_on_last_stable_links()


if __name__ == "__main__":
    if len(sys.argv) == 1:
        sys.argv = ['/scripts/main.py', 'command=run_selenium_tests', 'job_name=PackingList Test', 'build_number=3',
                    'cluster_name=', 'cluster_index=']

        configs.alaki = True

    print("")
    print("sys.argv =", sys.argv)
    print("")

    sys.excepthook = print_exception

    command_handlers = {name: obj for name, obj in inspect.getmembers(sys.modules[__name__]) if
                        inspect.isfunction(obj) and
                        obj.__code__.co_filename == __file__}

    params = decode_jenkins_args(sys.argv)
    command = params["command"]
    if command not in command_handlers:
        raise Exception("no command defined as {}".format(command))

    command_handlers[command](params)
