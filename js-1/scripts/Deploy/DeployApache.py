from os.path import join

import configs
from Deploy.DeployTest import DeployTest
from Utils.Utils import section, try_get_url, green, error
from Utils.jfabric import lcd, local


class DeployApache(DeployTest):
    @section
    def config(self):
        temp_folder = self.deploy_info.get_temp_folder()

        with lcd(temp_folder):
            local("cp -r {} apache".format(configs.apache_docker_folder))

            with lcd("apache"):
                self.adjust_conf_file(join(temp_folder, "apache", "proxy.conf"))

                self.cluster_id = "cluster-{}-{}".format(self.deploy_info.cluster_name, self.cluster_index)
                local("echo {} > index.html".format(self.cluster_id))

                local("docker build -t {} .".format(self.get_image_name()))

    def adjust_conf_file(self, conf_path):
        proxy_conf_content = ""

        with open(conf_path) as file:
            lines = file.readlines()

        for line in lines:
            if "Anbar" in line:
                proxy_conf_content += self.adjust_port_for_cluster_index(line, True)
            else:
                proxy_conf_content += line

        with open(conf_path, "w") as file:
            file.write(proxy_conf_content)

    def check(self, try_count):
        try:
            current_cluster_id = try_get_url(self.get_build_date_url(), try_count=try_count, timeout=30,
                                             sleep_time=30).replace("\n", "")
            deployed_successfully = (current_cluster_id == self.cluster_id)

            if deployed_successfully:
                self.result_description = green("Deployed Successfully")
                self.exit_code = 0
            else:
                self.result_description = error("Error in Deploy")
                self.exit_code = 1

        except Exception as ex:
            self.exit_code = 2
            self.result_description = "Could not check build date"
            raise ex
        finally:
            self.set_status(self.result_description)
            self.inform(self.result_description)

    def need_deploy(self):
        return True
