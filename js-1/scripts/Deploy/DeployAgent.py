"""
Deploy agent is the parent of all deploys that determine deploy steps
Other deploys only changes the necessary steps by overriding the corresponding methods
see DeployAgent.deploy
"""
import os
import re
from datetime import datetime
from os.path import join

from dateutil.parser import parse

import configs
from Deploy.Observable import Observable
from Utils.Utils import section, urljoin, HttpProxy, try_get_url, error, green, warning, get_items_path, \
    recreate_folder, blue, am_i_newjenkins, replace_in_file, get_files_path_recursive
from Utils.jfabric import run, cd, host, lcd, local, proxy, sudo, rsync, skip_output


class DeployAgent(Observable):
    def __init__(self, deploy_info):
        super().__init__()

        self.deploy_info = deploy_info
        self.name = deploy_info.get_deploy_name()
        self.deploying_version = deploy_info.deploying_build.version
        self.print_section_mark = True
        self.warning_exit_code = 1
        self.dockerized = self.name in configs.dockerized_apps
        self.initialize_host()
        self.cluster_index = deploy_info.cluster_index
        self.rsync_des = join("/home", self.user, "jenkins", "temp", self.name + str(self.cluster_index))
        self.password = ""
        self.check_try_count = 10
        self.run_compose_up = True
        self.automatic_deploy = not self.dockerized or self.deploy_info.cluster_name in configs.jenkins_clusters
        self.cluster_name_hash = configs.cluster_hashes[self.deploy_info.cluster_name]

        self.should_print_cluster_setup_instructions = True

        if deploy_info.cluster_index not in configs.allowed_cluster_index:
            raise Exception("this cluster is not allowed yet")

        self.need_container_deploy = False

    def deploy(self):
        self.start_report_telegram()
        if self.automatic_deploy and not self.need_deploy(): return 0
        if not self.is_allowed(): return 1
        self.deploy_start_notify()
        self.check_arrangement()
        self.config()
        if self.automatic_deploy: self.prepare_host()
        self.copy()

        if self.automatic_deploy:
            self.run()
            self.check(self.check_try_count)
            if self.exit_code == 0: self.postdeploy()
            self.report()
            return self.exit_code
        else:
            if self.should_print_cluster_setup_instructions:
                self.print_cluster_setup_instructions()
            return 0

    # if force is false will check if desired version is currently running on target server
    # by getting buildDate
    def need_deploy(self):
        if self.deploy_info.force:
            return True

        try:
            self.check(1)
        except Exception as ex:
            self.inform(str(ex))
            return True

        if self.exit_code == 0:
            # if self.dockerized:
            #     with skip_output():
            #         images_list = local("docker images")
            #         if self.get_image_name() not in images_list:
            #             return True

            self.set_status(blue("Already Deployed"))
            self.inform("Version {} is currently on server {} and no need to deploy", self.deploying_version,
                        self.deploy_info.location.name)
            return False

        return True

    def is_allowed(self):
        return True

    # responsible for setting self.configed_artifact
    @section
    def config(self):
        #
        # comments after each statement stands for sample values
        # please update them after changes
        #

        artifact_path = self.deploy_info.deploying_build.get_artifact_path()
        # artifact_path = /stable-builds/Customs/492/target/Customs-5.5.3-r000.war

        self.artifact_name = self.deploy_info.deploying_build.get_artifact_name()
        # artifact_name = Customs-5.5.3-r000.war

        temp_folder = self.deploy_info.get_temp_folder()
        # temp_folder = /jenkins/temp/Customs_private_deploy/23

        self.configed_artifact = join(temp_folder, self.name)
        # configed_artifact = /jenkins/temp/Customs_private_deploy/23/Customs

        config_path = join(self.prepare_config_files(), "*")
        # config_path = /jenkins/temp/Customs_private_deploy/23/configs/*

        with lcd(temp_folder):
            local("unzip -q {} -d {} | true".format(artifact_path, self.configed_artifact))
            if get_items_path(config_path):
                local("cp -r {} {}".format(config_path, self.configed_artifact))
            else:
                self.inform("No config to add!")

            self.run_config_sh()

        if self.dockerized:
            with lcd(self.configed_artifact):
                webapp_path = "{}-{}".format(self.name, self.deploy_info.location.name)
                local("mkdir {}".format(webapp_path))
                local(
                    "find . -maxdepth 1 -mindepth 1 -not -name {} -not -name Dockerfile -not -name web.xml -exec mv '{{}}' {}/ \;".format(
                        webapp_path, webapp_path))
                local("docker build -t {} --build-arg name={} -q .".format(self.get_image_name(), webapp_path))

    def run_config_sh(self):
        # check if config.sh exists
        if get_items_path(join(self.configed_artifact, "config.sh")):
            with lcd(self.configed_artifact):
                local("/bin/bash config.sh {}".format(self.deploy_info.location.code))

    @section
    def copy(self):
        if self.dockerized:
            with skip_output():
                local("docker push {}".format(self.get_image_name()))
                # local("docker rmi {}".format(self.get_image_name()))
        else:
            with proxy(not am_i_newjenkins()), host(self.host):
                rsync(self.configed_artifact + "/", self.rsync_des)

    def get_image_name(self):
        return "{}/{}-{}-{}-{}".format(configs.docker_hub_url,
                                       self.name,
                                       self.deploy_info.location.code,
                                       self.deploy_info.cluster_name,
                                       self.cluster_index).lower()

    @section
    def run(self):
        if self.dockerized:
            if self.run_compose_up:
                self.compose_up()
            else:
                self.set_status("compose ready")
        else:
            with proxy(not am_i_newjenkins()), host(self.host), sudo(self.password), cd(self.rsync_des):
                if self.deploy_info.dpath == "default":
                    path = self.deploy_info.location.path
                else:
                    path = self.deploy_info.dpath

                args_str = " ".join(
                    [self.name, path, str(self.cluster_index), self.deploy_info.location.code])

                run_command = "/bin/bash deploy.sh {}".format(args_str)

                if not self.deploy_info.fake:
                    if self.need_container_deploy:
                        run("/bin/bash deploy-container.sh {}".format(args_str))

                    run(run_command)
                else:
                    print("instead of '{}' we run 'ls'".format(run_command))
                    run("ls")

                run("rm -rf {}".format(self.rsync_des))

    def initialize_host(self):
        self.user = "ut"

        if self.name == "Tir":
            self.user = "iru"
        if self.name == "Acc":
            with host("2013"):
                line = run("cat auto/Acc/customsnameswithurls | grep {}=".format(self.deploy_info.location.code))
                self.user = re.search("\d+=(.+)@(.+)", line).group(1)

        ip = self.deploy_info.location.ip
        self.host = "{}@{}".format(self.user, ip)

    # create a temp folder containing all configs needed for current deploy
    #
    # we have three sources for configs:
    # 1) per group configs
    # 2) per project configs
    # 3) per location configs
    #
    # this method combine these sources into a folder and return path of that
    #
    def prepare_config_files(self):
        config_path = join(self.deploy_info.get_temp_folder(), "configs")
        # config_path = /jenkins/temp/Customs_private_deploy/23/configs

        per_group_configs = self.deploy_info.deploying_build.get_group_name()
        per_project_configs = self.name + "s"
        per_location_configs = join(self.name, self.deploy_info.location.code)
        config_sources = [per_group_configs, per_project_configs, per_location_configs]

        recreate_folder(config_path)
        with lcd(config_path):
            for config_folder_name in config_sources:
                config_source = join(configs.configs_folder, config_folder_name, ".")
                # '.' at the end of the path is for including hidden files too

                if get_items_path(config_source):
                    local("cp {} . -r".format(config_source))

        self.replace_config_vars(config_path)
        self.check_for_unset_vars(config_path)

        return config_path

    # responsible for setting self.exit_code and self.result_description
    @section
    def check(self, try_count):
        try:
            if "{}-{}".format(self.name, self.deploy_info.location.name) in configs.no_build_date and try_count != 1:
                self.result_description = green("Deployed Successfully")
                self.exit_code = 0
                return

            build_date_from_file = self.get_build_date_from_file()
            self.inform("Expected date is {}", build_date_from_file)

            build_date_from_site = self.get_build_date_from_site(try_count)
            if not build_date_from_site:
                self.result_description = warning(
                    "Error: Cannot get buildDate from {}".format(self.get_build_date_url()))
                self.exit_code = self.warning_exit_code
                return

            self.inform("Current date is {}", build_date_from_site)
            self.inform("Expected date is {}", build_date_from_file)

            deployed_successfully = self.compare_build_dates(build_date_from_file, build_date_from_site)

            if deployed_successfully:
                self.result_description = green("Deployed Successfully")
                self.exit_code = 0
            else:
                self.result_description = error("Error in Deploy")
                self.exit_code = 1

        except Exception as ex:
            self.exit_code = 2
            self.result_description = "Could not check build date"
            raise ex
        finally:
            self.set_status(self.result_description)
            self.inform(self.result_description)

    def get_build_date_from_file(self):
        build_date_from_file_str = self.deploy_info.deploying_build.get_build_date()
        try:
            build_date_from_file = parse(build_date_from_file_str)
        except ValueError:
            raise Exception("invalid date from file: {}", build_date_from_file_str)

        return build_date_from_file

    def get_build_date_from_site(self, try_count):
        build_date_url = self.get_build_date_url()

        with HttpProxy():
            build_date_from_site_str = try_get_url(build_date_url, try_count=try_count, timeout=30, sleep_time=30)
        if not build_date_from_site_str:
            return None

        try:
            if build_date_from_site_str.isdigit():
                build_date_from_site = datetime.fromtimestamp(int(build_date_from_site_str) / 1000)
            else:
                build_date_from_site = parse(build_date_from_site_str)
        except ValueError as ex:
            raise Exception("invalid date from site: {}".format(build_date_from_site_str)) from ex

        return build_date_from_site

    def get_build_date_url(self):
        if self.name in configs.build_date_postfix:
            build_date_postfix = configs.build_date_postfix[self.name]
        else:
            build_date_postfix = "/buildDate"

        changed_ip_url = self.deploy_info.location.url
        if self.dockerized or self.name == "Anbar":
            changed_ip_url = changed_ip_url.replace("172.16.111.113",
                                                    configs.jenkins_clusters[
                                                        self.deploy_info.cluster_name])

        cluster_url = self.adjust_port_for_cluster_index(changed_ip_url, True)

        build_date_url = urljoin("http://" + cluster_url,
                                 ("" if self.deploy_info.dpath == "default" else self.deploy_info.dpath),
                                 build_date_postfix)
        return build_date_url

    def adjust_port_for_cluster_index(self, url, isAnbar=False):
        if isAnbar:
            return self.add_to_port(self.change_anbar_port(url), self.cluster_index * 100)
        else:
            return self.add_to_port(self.change_anbar_port(url), 0)

    def compare_build_dates(self, build_date_from_file, build_date_from_site):
        try:
            build_date_from_file = build_date_from_file.replace(tzinfo=None)
            build_date_from_site = build_date_from_site.replace(tzinfo=None)
            delta_time = abs((build_date_from_file - build_date_from_site).total_seconds())  # seconds
        except TypeError as ex:
            self.inform(str(ex))
            delta_time = 9999999

        deployed_successfully = delta_time < 4 * 60 or abs(delta_time - 3.5 * 3600) < 180 or abs(
            delta_time - 4.5 * 3600) < 180
        return deployed_successfully

    def postdeploy(self):
        pass

    def report(self):
        pass

    def get_name_for_observer(self):
        return "{}-{}".format(self.name, self.deploy_info.location.name)

    def prepare_host(self):
        with proxy(not am_i_newjenkins()), host(self.host):
            run("mkdir -p {}".format(self.rsync_des))

    def check_arrangement(self):
        pass

    def deploy_start_notify(self):
        pass

    def start_report_telegram(self):
        pass

    def replace_config_vars(self, config_path):
        config_vars = self.get_config_vars()

        config_files = get_files_path_recursive(config_path)

        for file_path in config_files:
            if file_path.endswith(".swp"):
                continue

            for config_var in config_vars:
                # if config_var.s
                config_value = self.adjust_port_for_cluster_index(config_vars[config_var],
                                                                  config_var.startswith('anbar'))

                replace_in_file(file_path, "$${{{}}}".format(config_var), config_value)

    def get_config_vars(self):
        result = {}

        config_vars_file = join(configs.configs_folder, "config-vars.txt")

        with open(config_vars_file) as f:
            lines = f.readlines()

            for line in lines:
                match = re.match('([^\s]+)\s+([^\s]+)', line)

                if match:
                    result[match.group(1)] = match.group(2).replace(os.linesep, "")

        return result

    @staticmethod
    def check_for_unset_vars(config_path):
        config_files = get_files_path_recursive(config_path)

        for path in config_files:
            if path.endswith(".swp"):
                continue

            with open(path, 'rb') as file:
                lines = file.readlines()

                for i in range(0, len(lines)):
                    if b"$${" in lines[i]:
                        raise Exception("unset config var in {} line {}".format(path, i + 1))

    @staticmethod
    def add_to_port(address, offset):
        if ':' in address:
            match = re.match(".*?:(\d+).*?", address)

            if match:
                port = int(match.group(1))
                return address.replace(":{}".format(port),
                                       ":{}".format(port + offset))

        return address

    def compose_up(self):
        with host(self.host), sudo(""), cd("jenkins/docker/new_anbar"):
            if self.deploy_info.force and self.deploy_info.cluster_name in configs.swarm_mode:
                run("/bin/bash cluster.sh {} {} down".format(self.deploy_info.cluster_name, self.cluster_index))
            run("/bin/bash cluster.sh {} {} up".format(self.deploy_info.cluster_name, self.cluster_index))

    def print_cluster_setup_instructions(self):
        local("cp {} {}".format(join(configs.cluster_folder, configs.cluster_file_name),
                                join(configs.user_content_folder, configs.cluster_file_name)))

        print("\n\n+{:=<72}+".format(""))
        print("| {:<70} |".format("DEPLOY INSTRUCTION:"))
        print("| {:<70} |".format("  1) Download the following file:"))
        print("| {:<70} |".format("      http://newjenkins.iais.ir/userContent/{}".format(configs.cluster_file_name)))
        print("| {:<70} |".format("  2) Unzip it."))
        print("| {:<70} |".format("  3) Run the following command in the unzipped folder:"))
        print("| {:<70} |".format(
            "      sudo /bin/bash cluster.sh {} {} up".format(self.deploy_info.cluster_name, self.cluster_index)))
        print("+{:=<72}+\n\n".format(""))

    def change_anbar_port(self, input):
        if self.cluster_index == 0:
            input = input.replace("c-113.anbar.test.iais.co", "c-{}.anbar.test.iais.co".format(self.host))
        else:
            input = input.replace("c-113.anbar.test.iais.co",
                                  "c-{}-{}.anbar.test.iais.co".format(self.host, str(self.cluster_index)))
        match = re.match(".*(:100(\d)).*", input)
        if match:
            new_port = str(
                int(self.cluster_name_hash / 10) * 1000 + (self.cluster_name_hash % 10) * 10 + int(match.group(2)))

            return input.replace(match.group(1), ":{}".format(new_port))
        else:
            return input
