from Deploy.DeployProduction import DeployProduction
from Utils.Utils import run_windows_command


class DeployAsp(DeployProduction):
    def __init__(self, deploy_info):
        super().__init__(deploy_info)
        self.warning_exit_code = 1

    def prepare_host(self):
        pass

    def config(self):
        pass

    def copy(self):
        pass

    def is_allowed(self):
        return True

    def run(self):
        run_windows_command("C:\\\\JenkinsScripts\\\\Deploy-Asp.ps1 {}".format(self.get_args()))

    def report(self):
        pass

    def get_args(self):
        # param( $name = "Anbar",
        # $build = "http://jenkins.iais.ir/job/Anbar/446/",
        # $build_url = "http://localhost:8070/job/Anbar%20Deploy/3/",
        # $location = "Mahirood@60604@10.45.64.21",
        # $dpath = "default",
        # $target_path = "",
        # $test_url_postfix = "UI/buildDate.aspx",#Customs/buildDate
        # $only_test = $False,
        # $forti_down = $False,
        # $fake = $False)
        return " ".join([self.name,
                         self.deploying_version,
                         self.deploy_info.BUILD_URL,
                         self.deploy_info.location.long_name,
                         self.deploy_info.dpath,
                         "",
                         "UI/buildDate.aspx",
                         ])
