"""
get a cluster name and deploy to all of its servers the last stable version
by calling deploy agents concurrently and waiting for all of them to finish

you can provide for a project a custom version and master agent will use it
"""
import threading
from copy import copy
from time import sleep

import configs
from Build.BuildInfo import BuildInfo
from Deploy.DeployASP2 import DeployAsp2
from Deploy.DeployApache import DeployApache
from Deploy.DeployTest import DeployTest
from Deploy.DeployUtils.DeployLocation import DeployLocation
from Deploy.Observer import Observer
from Utils.JenkinsUtils import get_current_package
from Utils.Utils import error, remove_color
from Utils.jfabric import output_prefix


class DeployMasterAgent(object):
    def __init__(self, deploy_info, cluster_config):
        self.deploy_info = deploy_info
        self.trigge_jenkins_build = True
        self.observer = Observer("Deploy", self, self.on_status_update)
        self.deploy_counter = 1
        self.cluster = configs.clusters[cluster_config]
        self.exit_code = 0
        self.cluster_index = deploy_info.cluster_index
        self.dockerized_deploys = []
        self.compose_ready = []
        self.compose_done = False
        self.cluster_config = cluster_config
        self.current_package = get_current_package()

        if self.cluster_index not in configs.allowed_cluster_index:
            raise Exception("this cluster is not allowed yet")

        # for forcing a version for a project
        self.force_versions = {}

    def deploy(self):
        self.deploys = [(project, location) for project in self.cluster for location in self.cluster[project]]
        threads = []

        for (project, location) in self.deploys:
            # if project not in ["Anbar"]:
            #     continue
            # if location not in ["Rajae"]:
            #     continue

            if project in configs.dockerized_apps:
                self.dockerized_deploys.append("{}-{}".format(project, location))

            t = threading.Thread(target=self.employ_new_agent, args=(project, location))
            t.start()
            threads.append(t)

        # join all threads
        for t in threads:
            t.join()

        if self.deploy_info.cluster_name not in configs.jenkins_clusters:
            list(self.observer.agents.keys())[0].print_cluster_setup_instructions()

        return self.exit_code

    def employ_new_agent(self, project, location):
        if project in self.force_versions:
            v = self.force_versions[project]
        else:
            if project in self.current_package:
                v = self.current_package[project]
            else:
                self.observer.add_info("{}-{}".format(project, location),
                                       error("No {} in Current Package".format(project)))
                self.exit_code = 1
                return

        with copy(self.deploy_info) as deploy_info:
            deploy_info.name = "{} Private Deploy".format(project)
            deploy_info.deploying_build = BuildInfo(project, version=v)
            deploy_info.location = DeployLocation(self.cluster[project][location])

            # for temp folder name to be unique
            deploy_info.number = "{}-{}".format(self.deploy_info.number, self.deploy_counter)
            self.deploy_counter += 1

            if project == "Apache":
                agent = DeployApache(deploy_info)
            else:
                agent = DeployTest(deploy_info)

            agent.print_section_mark = False
            agent.subscribe_to(self.observer)

            agent.run_compose_up = False
            agent.should_print_cluster_setup_instructions = False

            try:
                with output_prefix("{:<20}: ".format(agent.get_name_for_observer())):
                    agent_exit_code = agent.deploy()
            except Exception as ex:
                agent.inform(error(str(ex)))
                agent.set_status(error("Error in {}: {}".format(agent.get_status(), ex)))
                agent_exit_code = 1

            if agent_exit_code != 0:
                self.exit_code = 1

    def force_version(self, build_info):
        self.force_versions[build_info.name] = build_info.version

    def on_status_update(self, updated_agent):
        code = "{}-{}".format(updated_agent.name, updated_agent.deploy_info.location.name)

        if code in self.dockerized_deploys:
            if remove_color(self.observer.agents[updated_agent]) == "Already Deployed" or \
                            self.observer.agents[updated_agent] == "compose ready":
                self.compose_ready.append(code)

                if set(self.dockerized_deploys) <= set(self.compose_ready):
                    updated_agent.compose_up()
                    self.compose_done = True

                while not self.compose_done: sleep(3)
