from Deploy.DeployAgent import DeployAgent


class DeployTest(DeployAgent):
    def initialize_host(self):
        self.user = "ut"
        self.host = self.deploy_info.location.name

        if self.host == "114":
            self.user = "bazbini"

        if self.dockerized:
            self.host = self.deploy_info.cluster_name
