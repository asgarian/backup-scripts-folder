import re
from os.path import join

from Deploy.DeployProduction import DeployProduction
from Utils.Utils import copy_if_not_exist, run, section
from Utils.jfabric import cd, host


class DeployMaven(DeployProduction):
    auto_folder_2013 = '/home/deploy/auto/'

    def prepare_host(self):
        pass

    @section
    def copy(self):
        if self.name == "Acc":
            local_war_path = self.configed_artifact
        else:
            local_war_path = self.deploy_info.DEPLOYING_BUILD.get_artifact_path()

        server_war_name = re.sub(".war", "", self.name) + "-" + self.deploying_version + ".war"
        server_war_path = join(self.auto_folder_2013, self.name, 'wars', server_war_name)
        self.version = server_war_name.replace(self.name, "").replace(".war", "")  # used in run

        with host("2013"):
            copy_if_not_exist(local_war_path, server_war_path)

    @section
    def run(self):
        with host("2013"), cd(self.auto_folder_2013):
            deploy_command = "echo y | /bin/bash autodeploy.sh {} {} {} {}".format(self.name, self.version,
                                                                                   self.deploy_info.location.code,
                                                                                   self.deploy_info.dpath)
            if not self.deploy_info.fake:
                run(deploy_command)
            else:
                print('instead of running "{}" we just run "ls"'.format(deploy_command))
                run("ls")

                # @section
                # def postdeploy(self):
                #    agent = PostdeployAgent(self.build_info.DEPLOYING_BUILD,self.build_info.location)
                #    agent.perform()


    def run_fake_deploy(self, time_stamp):
        server_war_name = re.sub(".war", "", self.name) + "-" + self.deploying_version + ".war"
        server_war_path = join(self.auto_folder_2013, self.name, 'wars', server_war_name)
        self.version = server_war_name.replace(self.name, "").replace(".war", "")

        with host("2013"):
            deploy_command = "echo y | /bin/bash fake-deploy.sh {} {} {} {} {}".format(self.name, self.version,
                                                                                       self.deploy_info.location.code,
                                                                                       self.deploy_info.dpath,
                                                                                       time_stamp)
            run(deploy_command)
