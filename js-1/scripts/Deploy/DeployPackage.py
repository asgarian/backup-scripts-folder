import sys
import xml.etree.ElementTree as etree

import requests

from Deploy.DeployInfo import DeployInfo
from Utils.JenkinsUtils import get_last_version, get_json_from_jenkins
from Utils.Utils import get_last_stable_version_from_jenkins

__author__ = 'borna'


class DeployPackage(object):
    def __init__(self, sys_args):
        self.versions = self.get_versions_from_jenkins(sys_args[2])
        self.sys_args = sys_args

    @staticmethod
    def must_change_user(target_build_info):
        target_build_info = DeployInfo(sys.argv)
        target_build_json = get_json_from_jenkins(target_build_info.BUILD_URL)
        target_build_start_time = target_build_json["timestamp"]
        last_deploy = get_last_version("Package Deploy")
        last_build_json = get_json_from_jenkins(
            "http://127.0.0.1:8080/job/{}/{}/".format("Package Deploy", last_deploy))
        last_build_start_time = last_build_json["timestamp"]
        return target_build_start_time - last_build_start_time < 10000

    @staticmethod
    def get_last_builds_user():
        last_deploy = get_last_version("Package Deploy")
        last_build_json = get_json_from_jenkins(
            "http://127.0.0.1:8080/job/{}/{}/".format("Package Deploy", last_deploy))
        last_user = last_build_json["actions"][1]["causes"][0]["userName"]
        return last_user

    def get_versions_from_jenkins(self, version_url):
        package_json = get_json_from_jenkins(version_url)
        version_fullname = package_json["actions"][0]["parameters"][0]["value"]
        return version_fullname.split(',')

    def build_jobs(self):
        self.build_corresponding_job("Anbar")
        self.build_corresponding_job("Acc")
        self.build_corresponding_job("Swich")
        self.build_corresponding_job("Customs")

    def build_corresponding_job(self, name):
        server_id = self.sys_args[3].split('@')[1]
        location = self.get_location_for_job(name, server_id)
        if name == "Anbar":
            versions_index = 0
        elif name == "Acc":
            versions_index = 1
        elif name == "Swich":
            versions_index = 2
        else:
            versions_index = 3
        version_id = self.get_build_id(name, self.versions[versions_index].replace("{}=".format(name), ""))
        print(
            "Building Job {} Version {} in {}.".format(name, version_id, location))

        payload = self.get_build_params(name, location, self.sys_args, version_id)
        # request for build
        r = requests.post(
            "http://newjenkins:da00ed9b7af94a088b637d9c312e68c7@172.17.20.32:8080/job/{}%20Deploy/buildWithParameters".format(
                name),
            data=payload)
        return r

    def get_build_params(self, name, location, sysargs, version_id):
        if name != "Customs":
            payload = {'VERSION': "{}#".format(name) + str(version_id)
                , 'location': location, 'dpath': sysargs[4], 'COMMENT': sysargs[5]
                , 'fake': sysargs[6]}
        else:
            payload = {'VERSION': "{}#".format(name) + str(version_id)
                , 'location': location, 'dpath': sysargs[4], 'COMMENT': sysargs[5]
                , 'fake': sysargs[6], 'SEND_SMS': sysargs[7], 'USERNAME': sysargs[8], 'PASSWORD': sysargs[9]}
        return payload

    def get_location_for_job(self, name, server_id):
        tree = etree.parse('/j/jobs/{} Deploy/config.xml'.format(name))  # server config file
        root = tree.getroot()
        if name != "Customs":
            content = root[3][3][0][4][5].text  # path for locationS in the xml file
        else:
            content = root[3][3][0][5][5].text
        locations = content.split(',')
        for location in locations:
            if location.split('@')[1] == server_id:
                return location
        return locations[0]

    def get_build_id(self, name, target_id):
        for current_id in reversed(range(1, int(get_last_stable_version_from_jenkins(name)) + 1)):
            try:
                build_json = get_json_from_jenkins("http://127.0.0.1:8080/job/{}/{}/".format(name, current_id))
            except Exception:
                continue
            if build_json["actions"][0]["parameters"][0]["value"] == target_id:
                return current_id
        return -1
