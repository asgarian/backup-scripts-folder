# -*- coding: utf-8 -*-
import codecs
from os.path import join
import re

from jira import JIRA

from Utils.JenkinsUtils import get_jenkins_build_json, set_deploy_jenkins_job_description
from Utils.Operation import Operation
from Utils.jfabric import local, sudo
import configs
import instance_configs


class ChangesAgent:
    def __init__(self, project_name, build_number):
        self.name = project_name
        self.build_number = build_number
        print("Connecting to issue.iais.co...")
        self.jira = JIRA('https://issue.iais.co', basic_auth=('hhasani', '48138295'))
        print("Conneced to issue.iais.co successfully")

    def set_change_log_in_deploy_jenkins(self, from_build, to_build):
        issues = self.get_issues_in_range(from_build, to_build)
        html = self.generate_html_from_issues(issues)
        self.save_to_file(html)
        set_deploy_jenkins_job_description(self.name, html)

    def generate_html_from_issues(self, issues):
        html = """<!DOCTYPE html>
<html>
<head>
<style>
#change_log_table th {
    border: 1px solid black;
}
#change_log_table td {
    border: 1px solid black;
}
#change_log_table th {
    padding: 5px;
}
#change_log_table td {
    padding: 5px;
}
#change_log_table, th, td {
    border-collapse: collapse;
}
</style>
</head>
<body>

<table id="change_log_table" >
<tr><th>Build</th><th>Issue</th><th>Summary</th></tr>

"""
        for build in sorted(issues, reverse=True):
            (issue_id, summary) = issues[build][0]
            html += "<tr><td>{}</td><td>{}</td><td>{}</td></tr>".format(build, issue_id, summary) + "\n"

            for issue in issues[build][1:]:
                (issue_id, summary) = issue
                html += "<tr><td>{}</td><td>{}</td><td>{}</td></tr>".format("", issue_id, summary) + "\n"

        html += """<table/>
</body>
</html>"""

        return html

    def get_issues_in_range(self, from_build, to_build):
        issues = {}

        for build_number in range(from_build, to_build):
            print("getting build {}".format(build_number))

            try:
                json = get_jenkins_build_json(Operation(self.name, build_number))
                message = " ".join([change["msg"] for change in json["changeSet"]["items"]])
            except:
                message = None

            current_build_issues = []
            if message:
                for issue in self.extract_issue_from_message(message):
                    current_build_issues.append((issue, self.get_jira_task_summary(issue)))

            if current_build_issues:
                current_build_issues = list(set(current_build_issues))
                issues[build_number] = current_build_issues

        return issues

    def extract_issue_from_message(self, message):
        return re.findall("[a-zA-Z]+-\d+", message)

    def get_jira_task_summary(self, task_id):
        try:
            issue = self.jira.issue(task_id)
            return issue.fields.summary
        except Exception as ex:
            return "could not get because of {}".format(ex)

    def save_to_file(self, html):
        report_file = codecs.open(join(configs.issue_report_path, instance_configs.jenkins_name, "{}.html".format(self.build_number)), "w", "utf-8")
        report_file.write(html)
        report_file.close()
        print("| {:<70} |".format("   Download the following file:"))
        print("http://172.16.111.8:8100/userContent/issue_reports/{}".format(join(instance_configs.jenkins_name, self.build_number)))
