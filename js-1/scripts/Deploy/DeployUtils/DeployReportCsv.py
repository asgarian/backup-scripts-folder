from datetime import datetime
from os.path import isfile, join

from dateutil.parser import parse

from Deploy.DeployUtils.Arrangement import Arrangement
from Deploy.DeployUtils.DeployLocation import DeployLocation
from Utils.Utils import remove_if_exist, remove_color, create_folder
import configs


#
# Reperesent a line in a csv file that is report of a deploy
# Responsible for getting appropriate data from deploy agent and adding it to .csv file
# Responsible for parsing a line of .csv file
#
class DeployReportCsv:
    def __init__(self, value):
        if type(value) is str:
            parts = value.replace("\n", "").split(",")
            if len(parts) < 10:
                raise Exception("Can not parse csv report: {}".format(value))
            self.deploy_agent = None
            self.id = parts[0]
            self.time = parse(parts[1])
            self.deploying_number = parts[2]
            self.location = DeployLocation(parts[3])
            self.dpath = parts[4]
            self.BUILD_USER = parts[5]
            self.COMMENT = parts[6]
            self.fake = (parts[7] == "TRUE")
            self.result_description = parts[8]
            self.BUILD_URL = parts[9]
        else:
            self.deploy_agent = value
            self.id = value.deploy_info.number
            self.time = datetime.now()
            self.deploying_number = value.deploying_number
            self.location = value.deploy_info.location
            self.dpath = value.deploy_info.dpath
            self.BUILD_USER = value.deploy_info.BUILD_USER
            self.COMMENT = value.deploy_info.COMMENT
            self.fake = value.deploy_info.fake
            self.result_description = value.result_description
            self.BUILD_URL = value.deploy_info.BUILD_URL

    @staticmethod
    def get_csv_header():
        return ",".join(
            ["Id", "Time", "Build Number", "Location", "Dpath", "User", "Comment", "Fake", "Result", "Url"])

    def add_to_file(self):
        file_path = DeployReportCsv.get_csv_file_path(self.deploy_agent.name)

        seprator_declearation = "sep=,\n"
        header = self.get_csv_header() + "\n"
        new_line = self.to_csv_line() + "\n"
        content = []
        if isfile(file_path):
            with open(file_path) as f:
                content = f.readlines()
                remove_if_exist(content, seprator_declearation)
                remove_if_exist(content, header)

        content = [seprator_declearation, header, new_line] + content

        with open(file_path, "w") as f:
            for item in content:
                f.write(item)

    def to_csv_line(self):
        return remove_color(",".join(
            [self.id, str(self.time), self.deploying_number,
             self.location.long_name, self.dpath,
             self.BUILD_USER, self.COMMENT, str(self.fake),
             self.result_description, self.BUILD_URL]))

    @staticmethod
    def find_first_successful_deploy(deploy_info):
        file_path = DeployReportCsv.get_csv_file_path(deploy_info.get_deploy_name())
        if not isfile(file_path):
            return None

        with open(file_path) as f:
            lines = f.readlines()
            f.close()

        lines = lines[2:]
        lines.reverse()

        for line in lines:
            deploy_report = DeployReportCsv(line)

            if deploy_report.deploying_number == deploy_info.DEPLOYING_BUILD.number \
                    and deploy_report.dpath == "default" \
                    and not deploy_report.fake \
                    and "Deployed Successfully" in deploy_report.result_description \
                    and "99999" not in deploy_report.location.code:
                return deploy_report

        return None

    # sample output: /j/userContent/Customs.csv
    # Responsible for creating the folder
    @staticmethod
    def get_csv_file_path(name):
        folder_path = configs.deploy_reports_folder
        create_folder(folder_path)
        file_path = join(folder_path, name + ".csv")
        return file_path

    @staticmethod
    def get_customs_arragments():
        arrangements = {}

        for project in configs.arrangement_projects:
            report_file = join(configs.deploy_reports_folder, project + ".csv")

            with open(report_file) as f:
                lines = f.readlines()

                for line in lines[2:]:
                    deploy_report = DeployReportCsv(line)

                    if deploy_report.dpath == "default" \
                            and not deploy_report.fake \
                            and "Deployed Successfully" in deploy_report.result_description:
                        if deploy_report.location.name not in arrangements:
                            arrangements[deploy_report.location.name] = Arrangement(deploy_report.location.name)

                        arrangements[deploy_report.location.name].set_version(project, deploy_report.deploying_number,
                                                                              deploy_report.time)

        return arrangements
