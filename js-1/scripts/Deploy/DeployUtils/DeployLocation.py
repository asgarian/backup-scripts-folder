import re

from Utils.Utils import get_ip_port
from Utils.jfabric import host, run


class DeployLocation(object):
    # sample input: kish@50400@10.35.0.18:8080/acc or
    #               13@172.16.111.113:8080/Customs
    def __init__(self, location):
        if ";" in location:
            raise Exception("you are only alowed to select one location")
        parts = location.split('@')
        if len(parts) == 4:
            self.name = parts[0]
            self.code = parts[1]
            self.url = parts[2]
            self.phone_number = parts[3]
        elif len(parts) == 3:
            self.name = parts[0]
            self.code = parts[1]
            self.url = parts[2]
        elif len(parts) == 2:
            self.name = parts[0]
            self.code = parts[0]
            self.url = parts[1]
        else:
            raise Exception("invalid location: {}. must be in format name@code@url or name@url".format(location))

        self.ip, self.port = get_ip_port(self.url)

        if "/" not in self.url:
            self.path = ""
        else:
            # We are not handling multi '/' cases for now
            self.path = re.sub("^.*/", "", self.url)

        self.long_name = location

    # sample output: ut@10.32.0.18
    def get_host_from_2013(self, project_name):
        with host("2013"):
            customs_user_url = run(
                "cat /home/deploy/auto/{}/customsnameswithurls | grep {}".format(project_name, self.code))

            if customs_user_url == "":
                raise Exception("ERROR: Username and IP not provided in server 172.17.20.13!")
            else:
                user = customs_user_url.split("=")[1].split("@")[0]
            return user + "@" + self.ip

    def to_string(self):
        return "@".join([self.name, self.code, self.url])
