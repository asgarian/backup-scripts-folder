from Utils.Operation import Operation


class DeployInfo(Operation):
    def __init__(self, params):
        super().__init__(params["job_name"], params["build_number"])

        self.force = params.get("force")
        self.fake = params.get("fake")
        self.dpath = params.get("dpath") or "default"

        self.cluster_name = params["cluster_name"]
        self.cluster_index = int(params["cluster_index"])

    # sample output: Zobahan/test
    def get_nice_location(self):
        dpath_part = "" if self.dpath == "default" else "/" + self.dpath
        return self.location.name + dpath_part

    # sample output: CRS instead of customs-reporter-server
    def get_nice_deploy_name(self):
        return self.get_deploy_name().replace("customs-reporter-server", "CRS")

    # sample output: Customs or customs-reporter-server or ...
    def get_deploy_name(self):
        return self.name.replace(" Deploy", "").replace(" Private", "")
