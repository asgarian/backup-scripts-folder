import urllib
from contextlib import suppress
from http.cookiejar import CookieJar
from os.path import join

import configs
from Build.BuildInfo import BuildInfo
from Utils.JenkinsUtils import get_job_workspace_path
from Utils.Utils import recreate_folder, get_endpoint_info, get_last_stable_version_from_jenkins, get_items_path, \
    create_folder, \
    section, \
    blue
from Utils.jfabric import host, get, lcd, local


class UnitTestGenerator:
    def __init__(self, info):
        self.info = info
        self.target = get_endpoint_info(info)

        if self.target.name not in configs.endpoints:
            self.target = BuildInfo(["Customs", get_last_stable_version_from_jenkins("Customs")])

        if not isinstance(self.target, BuildInfo):
            self.target = BuildInfo(self.target)

        self.run_id = self.get_run_id()
        self.id = "{}-{}".format(self.target.number, self.info.number, self.run_id)
        self.aut_folder = join("/aut", self.target.name, "builds", self.id)
        self.log_folder = join(self.aut_folder, "logs")
        self.name = self.target.name
        self.should_run = False and [p for p in configs.test_generating_projects if
                                     p in self.target.name] or "Generate Unit Test" == self.target.name

    #
    # Because of performance we split generating into two phase:
    # 1) immediate pahse contains parts that must be run immediately after running tests
    # 2) non-immediate phase contains parts that can be run after a while
    #
    def run_immediate_parts(self):
        if not self.should_run:
            return

        self.send_write_request()
        self.download_logs()

    @section
    def download_logs(self):
        recreate_folder(self.log_folder)
        with host("113"), lcd(self.log_folder):
            get("/tomcat/AUT/{}/*".format(self.name), ".")
            local("ls -l")
            local("du -h")

    @section
    def send_write_request(self):
        self.send_request_to_aut("getSize")
        self.send_request_to_aut("write")

    def send_request_to_aut(self, action):
        if self.name == "PackingList":
            ip = "172.16.111.43"
        else:
            ip = "172.16.111.113"

        url = "http://{}:8080/{}/AUTPanel?action={}".format(ip, self.name, action)
        print("sending {}".format(url))

        # cj = CookieJar()
        # opener = urllib.build_opener(urllib.HTTPCookieProcessor(cj))
        # response = opener.open(url)

        # response = urllib.request.urlopen(
        #    url).read().decode()

        opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(CookieJar()))
        with opener.open(url) as response:
            raw_response = response.read().decode('utf8', errors='ignore')

        print("Response from aut {} request:".format(action))
        print(raw_response)
        print("")

    def run_non_immediate_parts(self):
        if not self.should_run:
            return

        if not get_items_path(self.log_folder):
            self.run_immediate_parts()

        self.checkout_last_version()

        with suppress(Exception):
            self.generate_unit_tests()

        print(blue(
            "{} Valid Tests and {} Invalid Tests were Generated.".format(self.get_tests_count(self.valid_tests_folder),
                                                                         self.get_tests_count(
                                                                             self.invalid_tests_folder))))

        self.commit_new_tests()

    @section
    def checkout_last_version(self):
        temp_folder = self.info.get_temp_folder()
        self.checkout_folder = join(temp_folder, "source")
        local("mkdir -p {}".format(self.checkout_folder))

        with lcd(self.checkout_folder):
            local("git init")
            try:
                if self.name == "PackingList":
                    git_name = "ios"
                else:
                    git_name = "customs-main"

                local("git remote add origin https://newjenkins@bitbucket.iais.co/scm/cus/{}.git".format(git_name))
            except:
                pass

            local("git pull origin {}".format(self.target.branch))
            local("git checkout {}".format(self.target.branch))
            self.tests_folder = join(self.checkout_folder, "src/test/java")

    @section
    def generate_unit_tests(self):
        self.valid_tests_folder = join(self.aut_folder, "valid_tests")
        self.invalid_tests_folder = join(self.aut_folder, "invalid_tests")
        aut_temp_folder = join(self.aut_folder, "temp")
        temp_folder = self.info.get_temp_folder()

        local("unzip -qo {} -d {}".format(self.target.get_artifact_path(), temp_folder))
        classes_folder = join(temp_folder, "WEB-INF/classes")
        lib_folder = join(temp_folder, "WEB-INF/lib")

        create_folder(self.valid_tests_folder)
        create_folder(self.invalid_tests_folder)
        create_folder(aut_temp_folder)

        # providing current tests for generator
        local("cp -r {} {}".format(join(self.tests_folder, "*"), self.valid_tests_folder))

        print(blue("Valid Tests: {}".format(self.valid_tests_folder)))
        print(blue("Invalid Tests: {}".format(self.invalid_tests_folder)))

        generator_path = join(get_job_workspace_path("AUTGenerator"), "target")
        with lcd(generator_path):
            local(
                "java -cp AUTGenerator-1.0-SNAPSHOT.jar -Xmx10000m ir.iais.aut.generator.UnitTestGenerator  '{}' '{}' '{}' '{}' '{}' '{}' '{}' '{}'".format(
                    self.log_folder, self.valid_tests_folder, self.invalid_tests_folder, aut_temp_folder,
                    classes_folder,
                    lib_folder,
                    self.valid_tests_folder,
                    self.run_id))

    @section
    def commit_new_tests(self):
        with lcd(self.checkout_folder):
            local("cp {} {} -r".format(join(self.valid_tests_folder, "*"), self.tests_folder))
            local("git add --all")

            commit_message = "auto commit by AUT, id={}".format(self.id)
            local("git commit -m '{}'".format(commit_message))
            local('git push -f origin {}'.format(self.target.branch))

    def get_tests_count(self, folder):
        with lcd(folder):
            return local("find . -name '*" + self.run_id + ".java' -exec grep '@Test' {} + | wc -l").replace("\n",
                                                                                                             "")

    def get_run_id(self):
        run_id_file = join(configs.jenkins_home, "UnitTestGenerator_runId")

        try:
            with open(run_id_file, 'r') as file:
                lines = file.readlines()
                current_id = int(lines[0])
        except:
            current_id = 0;

        new_id = current_id + 1

        with open(run_id_file, 'w') as file:
            file.write(str(new_id))

        return new_id
