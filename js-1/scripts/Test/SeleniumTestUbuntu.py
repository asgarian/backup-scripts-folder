import os

import time

__author__ = 'borna'


class SeleniumTestUbuntu(object):
    def __init__(self, suit_path, container_name):
        self.suit_path = suit_path
        self.container_name = container_name

    def run_test(self):
        try:
            ip_maps = "6902:6901"
            volume_maps = "-v /home/ut/borna/:/Tests/ -v /home/ut/borna/{}/:/results/ -v /home/ut/borna/configs/:/configs/".format(self.container_name)
            os.system("sudo rm -r /home/ut/borna/{}".format(self.container_name))
            os.system("sudo mkdir  /home/ut/borna/{}".format(self.container_name))
            docker_container_command = "sudo docker run -d -p {} {} -e var=\"{}\" --name=\"{}\" firefox_last".format(ip_maps,
                                                                                                                volume_maps,
                                                                                                                self.suit_path,
                                                                                                                self.container_name)
            os.system(docker_container_command)
            while os.system("[ -e /home/ut/borna/{}/result.txt ]".format(self.container_name)):
                time.sleep(2)
            while os.system("grep -q completed: \"/home/ut/borna/{}/result.txt\"".format(self.container_name)):
                if not os.system("grep -q error \"/home/ut/borna/{}/result.txt\"".format(self.container_name)):
                    raise Exception("Test Not Passed")
                time.sleep(2)
            if os.system("grep -q passed! \"/home/ut/borna/{}/result.txt\"".format(self.container_name)):
                raise Exception("Test Not Passed")
            else:
                print("test passed!")
        finally:
            os.system("sudo docker stop {}".format(self.container_name))
            os.system("sudo docker rm {}".format(self.container_name))
