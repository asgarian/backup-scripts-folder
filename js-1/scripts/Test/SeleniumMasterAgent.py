import threading
from os.path import join, isfile
from random import randint
from time import sleep

import configs
from Deploy.Observer import Observer
from Test.DockerizedSeleniumAgent import DockerizedSeleniumAgent
from Utils.JenkinsUtils import get_running_jobs
from Utils.Utils import error, synchronized
from Utils.jfabric import local, output_prefix, skip_output


class SeleniumMasterAgent(object):
    def __init__(self, test_info):
        self.test_info = test_info
        self.exit_code = 0
        self.observer = Observer("Test", self, None)
        self.semaphore = threading.Semaphore(configs.max_concurrent_test_threads)

    def run(self):
        self.test_names = self.get_tests()
        self.clear_busy_agents()
        # self.test_names = ["Tsc", "Vareat"]

        DockerizedSeleniumAgent.build_image()
        threads = []

        for test_name in self.test_names:
            t = threading.Thread(target=self.employ_new_agent, args=(test_name,))
            t.start()
            threads.append(t)

        for t in threads:
            t.join()

        return self.exit_code

    def employ_new_agent(self, test_name):
        with self.semaphore:
            agent_id = self.get_idle_agent_id()
            try:
                agent = DockerizedSeleniumAgent(agent_id, test_name, self.test_info)
                agent.subscribe_to(self.observer)

                try:
                    with output_prefix("{:<20}: ".format(agent.get_name_for_observer())):
                        agent_exit_code = agent.run()
                except Exception as ex:
                    agent.inform(error(str(ex)))
                    agent.set_status(error("Error in {}: {}".format(agent.get_status(), ex)))
                    agent_exit_code = 1

                if agent_exit_code != 0:
                    self.exit_code = 1
            finally:
                self.free_id(agent_id)

    def get_tests(self):
        with skip_output():
            test_list_content = local("cat {}".format(join(configs.seleniums_folder, "Get-Tests.ps1")))
        app_name = self.test_info.name.replace(" Test", "")
        in_if = False
        result = []

        for line in test_list_content.split("\n"):
            if '$name -eq "{}"'.format(app_name) in line:
                in_if = True
            elif in_if:
                if "}" in line:
                    break

                if "#" not in line:
                    parts = line.split('"')
                    if len(parts) > 1:
                        test_name = parts[1]
                        result.append(test_name)

        return result

    @synchronized
    def get_idle_agent_id(self):
        agent_id = 0

        while True:
            busy_agents = self.get_busy_agents()

            if agent_id not in busy_agents:
                sleep(randint(0, 1000) / 1000)
                busy_agents = self.get_busy_agents()
                if agent_id not in busy_agents:
                    with open(configs.busy_agents_file_path, "a") as file:
                        file.write("{}\n".format(agent_id))
                        break

            agent_id += 1

        return agent_id

    def get_busy_agents(self):
        if isfile(configs.busy_agents_file_path):
            with open(configs.busy_agents_file_path, "r") as file:
                lines = file.readlines()
                return [int(line.replace("\n", "")) for line in lines if line.replace("\n", "").isdigit()]
        else:
            return []

    @synchronized
    def free_id(self, id):
        busy_agents = self.get_busy_agents()
        busy_agents.remove(id)
        with open(configs.busy_agents_file_path, "w") as file:
            file.write("\n".join([str(busy_agent) for busy_agent in busy_agents]) + "\n")

    def clear_busy_agents(self):
        running_jobs = get_running_jobs()

        running_tests = []

        for operation in running_jobs:
            if operation.is_test():
                if operation != self.test_info:
                    running_tests.append(operation.name)

        if not running_tests:
            local("rm -f {}".format(configs.busy_agents_file_path))
