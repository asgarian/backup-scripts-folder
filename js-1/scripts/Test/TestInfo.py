from Build.BuildInfo import BuildInfo
from Utils.JenkinsUtils import get_upstream_build
from Utils.Operation import Operation


class TestInfo(Operation):
    def __init__(self, params):
        super().__init__(params["job_name"], params["build_number"])

        self.testing_project = get_upstream_build(self)
        self.branch = None
        if self.testing_project:
            self.testing_project = BuildInfo(self.testing_project.name, number=self.testing_project.number)
            self.branch = self.testing_project.branch

        if not self.branch:
            self.branch = "master"

        self.params = params
