import sys

from marionette import Marionette
from marionette_driver import By, Wait, expected


def get_session():
    client = Marionette('127.0.0.1', port=2828)
    client.start_session()
    print("session started")

    return client


def start_test(test_suit_path):
    client = get_session()

    client.set_context('chrome')
    client.find_element(By.ID, 'selenium-ide-button').click()
    print("selenium window opened")

    selenium_window = client.window_handles[1]  # needs editing
    client.switch_to_window(selenium_window)

    Wait(client).until(expected.element_present(By.ID, "pausefail-button"))
    print("opening test suit in {}".format(test_suit_path))

    client.find_element(By.ID, 'pausefail-button').click()
    client.find_element(By.ID, 'hilightelement-button').click()
    client.execute_script("window.editor.app.loadTestSuite('{}')".format(test_suit_path))
    client.execute_script("goDoCommand('cmd_selenium_play_suite')")

    client.switch_to_window(client.window_handles[0])
    client.maximize_window()


def save_screen_shot():
    client = get_session()

    screen_shot = client.screenshot(format='binary')
    with open("/screenshot.png", "wb") as file:
        file.write(screen_shot)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("please provide command as arg 1")
    command = sys.argv[1]

    if command == "start_test":
        if len(sys.argv) != 3:
            raise Exception("please provide test suit path as arg 2")
        test_suit_path = sys.argv[2]

        start_test(test_suit_path)

    if command == "save_screenshot":
        save_screen_shot()
