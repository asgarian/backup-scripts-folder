import requests

from Utils.JenkinsUtils import get_json_from_jenkins
from Utils.Utils import get_last_stable_version_from_jenkins

__author__ = 'borna'


class Package(object):
    def last_stable_builds_package_name(self):
        anbar_version = get_last_stable_version_from_jenkins("Anbar")
        acc_version = get_last_stable_version_from_jenkins("Acc")
        swich_version = get_last_stable_version_from_jenkins("Swich")
        customs_version = get_last_stable_version_from_jenkins("Customs")
        return "Anbar={},Acc={},Swich={},Customs={}".format(anbar_version, acc_version
                                                            , swich_version, customs_version)

    def get_build_version(self, name, build_id):  # getting version from build id
        build_json = get_json_from_jenkins("http://127.0.0.1:8080/job/{} Deploy/{}/".format(name, build_id))
        if build_json["actions"][0].__contains__("parameters"):
            build_number_json = get_json_from_jenkins(
                "http://127.0.0.1:8080/job/{}/{}/".format(name, build_json["actions"][0]["parameters"][0]["number"]))
        else:
            build_number_json = get_json_from_jenkins(
                "http://127.0.0.1:8080/job/{}/{}/".format(name, build_json["actions"][2]["parameters"][0]["number"]))
        return build_number_json["actions"][0]["parameters"][0]["value"]

    def build_package(self):
        payload = {'original_build_number': self.last_stable_builds_package_name()}
        r = requests.post(
            "http://newjenkins:da00ed9b7af94a088b637d9c312e68c7@172.17.20.32:8080/job/Package/buildWithParameters",
            data=payload)
        return r.status_code
