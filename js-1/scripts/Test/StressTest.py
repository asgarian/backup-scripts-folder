import os

import time

__author__ = 'borna'


class StressTest(object):
    def __init__(self, suit_path, container_name):
        self.suit_path = suit_path
        self.container_name = container_name

    def run_test(self):
        try:
            for i in range(0, 10):
                volume_maps = "-v /home/ut/borna/Test2:/Tests/ -v /home/ut/borna/results{}/:/results/ -v /home/ut/borna/configs/:/configs/".format(
                    str(i))
                # modify... multiple nodes(result folders) needed
                os.system("rm -r /home/ut/borna/results{}".format(str(i)))
                os.system("mkdir /home/ut/borna/results{}".format(str(i)))
                docker_container_command = "sudo docker run -d -p {}:6901 {} -e var=\"{}\" --name=\"{}\" firefox_last".format(
                    str(6902 + i),
                    volume_maps,
                    self.suit_path,
                    self.container_name + str(i))
                os.system(docker_container_command)
            self.get_results()
        finally:
            for i in range(0, 10):
                os.system("sudo docker stop {}".format(self.container_name + str(i)))
                os.system("sudo docker rm {}".format(self.container_name + str(i)))

    def get_results(self):
        num_of_done = 0
        test_list = [False] * 5
        while num_of_done != 10:
            num_of_done = 0
            for i in range(0, 10):
                if test_list[i]:
                    continue
                print("checking node {}".format(str(i)))
                if self.check_results_existence(i):
                    continue
                if self.check_for_error(i):
                    test_list[i] = True
                    num_of_done += 1
                    continue
                if self.check_finished_test(i):
                    continue
                num_of_done += 1
                test_list[i] = True
                self.check_test_passed(i)

    def check_results_existence(self, i):
        if os.system("[ -e /home/ut/borna/results{}/result.txt ]".format(str(i))):
            time.sleep(2)
            return True

    def check_for_error(self, i):
        if not os.system("grep -q error \"/home/ut/borna/results{}/result.txt\"".format(str(i))):
            print("Test Number {} Not Passed".format(str(i)))
            time.sleep(2)
            return True

    def check_finished_test(self, i):
        if os.system("grep -q completed: \"/home/ut/borna/results{}/result.txt\"".format(str(i))):
            time.sleep(2)
            return True

    def check_test_passed(self, i):
        if os.system("grep -q passed! \"/home/ut/borna/results{}/result.txt\"".format(str(i))):
            print("Test Number {} Not Passed".format(str(i)))
        else:
            print("test Number {} passed!".format(str(i)))
