from contextlib import suppress

import requests

from Deploy.DeployUtils.ChangesAgent import ChangesAgent
from Utils.JenkinsUtils import get_last_version
from Utils.Utils import error
from Utils.jfabric import host, put, local


class SendToDeployJenkinsAgent:
    def __init__(self, target_build, build_number):
        self.target_build = target_build
        self.build_number = build_number

    def send_to_deploy_jenkins(self):
        try:
            self.set_change_log()
        except Exception as ex:
            print(error("cougth error while set change log"))
            print(error(str(ex)))
            print("continue from last error")

        if self.target_build.branch == "master" or self.target_build.branch is None:
            print("informing Deploy Jenkins")

            sb_folder = self.target_build.get_sb_folder()

            local("du -h {}".format(sb_folder))

            with host("2032"):
                put(sb_folder, sb_folder)

            self.build_in_deploy_jenkins()



    def build_in_deploy_jenkins(self):
        payload = {'original_build_number': self.target_build.number}
        r = requests.post(
            "http://newjenkins:da00ed9b7af94a088b637d9c312e68c7@172.17.20.32:8080/job/" +
            self.target_build.name + "/buildWithParameters",
            data=payload)
        return r.status_code

    def set_change_log(self):
        agent = ChangesAgent(self.target_build.name, self.build_number)
        agent.set_change_log_in_deploy_jenkins(1, int(get_last_version(self.target_build.name)))
