from os.path import join
from time import sleep

import configs
from Deploy.Observable import Observable
from Utils.Utils import error, remove_container_if_exist, green
from Utils.jfabric import local, skip_output, skip_echo


class DockerizedSeleniumAgent(Observable):
    def __init__(self, id, test_name, test_info):
        super().__init__()
        self.test_info = test_info
        self.id = id
        self.test_name = test_name
        self.test_suit_path = join(configs.seleniums_folder, 'Test_suits', test_name, 'suit')
        self.tried_count = 0
        self.target_cluster = self.test_info.params["cluster_name"]
        self.exit_code = 1
        self.vnc_port = 6000 + configs.jenkins_id * 100 + int(self.id)
        self.http_port = 7000 + configs.jenkins_id * 100 + int(self.id)
        self.container_name = "selenium-agent-{}-{}".format(configs.jenkins_name, self.id)

    def run(self):
        while True:
            self.last_passed = False
            if self.tried_count < configs.test_max_try_count:
                self.tried_count += 1
            else:
                self.set_status(error("failed"))
                break

            try:
                self.start_test()
                self.wait_for_exit_and_get_result()
            except Exception as ex:
                self.inform(error(str(ex)))
            finally:
                remove_container_if_exist(self.container_name)

            if self.last_passed:
                self.exit_code = 0
                break

        return self.exit_code

    @staticmethod
    def build_image():
        local("docker build -t {} {}".format(configs.selenium_agent_image_name, configs.selenium_agent_config_folder))

    def start_test(self):
        self.passed_test_case = 0
        self.set_status("Starting")

        remove_container_if_exist(self.container_name)

        options = []
        if self.test_name in configs.non_interactive_tests:
            options.append("-viewonly")

        local(
            "docker run -dit --name {} --add-host test.iais.co:{} --add-host remote.captcha.com:127.0.0.1 -p {}:5901 -p {}:5801 {} {}".format(
                self.container_name, configs.jenkins_clusters[self.target_cluster], self.vnc_port, self.http_port,
                configs.selenium_agent_image_name,
                " ".join(options)))
        local("docker cp {} {}:/".format(configs.seleniums_folder, self.container_name))
        local("docker cp {} {}:/".format(configs.run_selenium_python_path, self.container_name))

        self.run_selenium_py(['start_test', self.test_suit_path])

    def run_selenium_py(self, args):
        local("docker exec -i {} python2 -u /{} {}".format(self.container_name,
                                                           configs.run_selenium_python_name,
                                                           " ".join(args)))

    def wait_for_exit_and_get_result(self):
        self.get_total_test_cases()

        has_second_chance = True

        with skip_output(), skip_echo():
            while True:
                sleep(1)

                selenium_log_content = local("docker exec {} cat /selenium-log.txt".format(self.container_name))
                lines = selenium_log_content.strip("\n").split("\n\n")

                if not lines:
                    continue

                errors = [line for line in lines if line.startswith("[error] ")]
                last_line = lines[-1]

                current_passed_test_case = len([line for line in lines if line == "[info] Test case passed"])
                if current_passed_test_case != self.passed_test_case:
                    passed_test_cases = [line.replace("[info] Playing test case ", "") for line in lines if
                                         line.startswith("[info] Playing test case ")]
                    if len(passed_test_cases):
                        current_test_case = passed_test_cases[-1]
                        self.passed_test_case = current_passed_test_case
                        self.set_status(
                            "Running {}th {}/{} ({})".format(self.tried_count, self.passed_test_case,
                                                             self.total_test_cases, current_test_case))

                if last_line.startswith("[info] Test suite completed") or len(errors):
                    self.save_screen_shot()

                    if last_line.endswith("all passed!"):
                        self.set_status(green("Passed"))
                        self.last_passed = True
                    else:
                        self.inform(error("\n".join(errors)))
                        if has_second_chance:
                            has_second_chance = False
                            sleep(0)
                            continue
                        self.last_passed = False
                    break

                has_second_chance = True

    def save_screen_shot(self):
        self.run_selenium_py(['save_screenshot'])
        screen_shot_file_name = "{}-{}-{}-{}th.png".format(self.test_info.name, self.test_info.number,
                                                           self.test_name, self.tried_count).replace(" ", "_")
        screen_shots_folder = join(configs.user_content_folder, configs.test_screenshot_folder)
        local("mkdir -p {}".format(screen_shots_folder))
        local("docker cp {}:/screenshot.png {}".format(self.container_name,
                                                       join(screen_shots_folder,
                                                            screen_shot_file_name)))
        self.inform("screenshot on {}".format(
            join(configs.jenkins_url_outside, "userContent", configs.test_screenshot_folder,
                 screen_shot_file_name)))

    def get_total_test_cases(self):
        with skip_output(), skip_echo():
            test_suit = local("docker exec {} cat {}".format(self.container_name, self.test_suit_path))
            self.total_test_cases = test_suit.count("<tr><td><a href=")

    def get_name_for_observer(self):
        return "{} on port {}".format(self.test_name, self.vnc_port)
