import os

from Utils.Utils import am_i_newjenkins


class FirefoxAutomator(object):
    def __init__(self, suit_path):
        self.suit_path = suit_path

    def start_test(self):
        print("Openning Firefox")
        os.system("firefox -marionette")
        print("Starting Test")
        if am_i_newjenkins():
            server_ip = "172.16.111.85"
        else:
            server_ip = "172.17.20.34"
        print("testing on {}".format(server_ip))
        os.system("python2 /scripts/Test/AutomatorPy2.py {} {}".format(self.suit_path, server_ip))
