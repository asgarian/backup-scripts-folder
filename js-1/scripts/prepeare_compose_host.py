from os.path import join

from Utils.jfabric import host, sudo, cd, run, lcd, local
import configs

update = False
source = "10"
targets = ["113","116","12","36"]

hosts_docker_folder = "/home/ut/jenkins/docker"

if update:
    with host(source), sudo(""), cd(hosts_docker_folder):
        run("rm -f {}".format(configs.cluster_file_name))

        run("zip -q {} docker-compose.yml".format(configs.cluster_file_name))
        run("zip -q {} cluster.sh".format(configs.cluster_file_name))

        run("chmod o+r {}".format(configs.cluster_file_name))

    local("mkdir -p {}".format(configs.cluster_folder))

    with lcd(configs.cluster_folder):
        local("scp {}:{} .".format(source, join(hosts_docker_folder, configs.cluster_file_name)))
        local("cp {} {}".format(join(configs.cluster_folder, configs.cluster_file_name),
                                join(configs.user_content_folder, configs.cluster_file_name)))

for target in targets:
    with host(target), sudo(""):
        run("rm -rf {}".format(hosts_docker_folder))
        run("mkdir -p {}".format(hosts_docker_folder))
        run("chmod -R o+w {}".format(hosts_docker_folder))

    with lcd(configs.cluster_folder):
        local("scp {} {}:{}".format(configs.cluster_file_name, target, join(hosts_docker_folder, ".")))
        local("scp {} {}:{}".format(configs.docker_hub_certificate_path, target, join(hosts_docker_folder, ".")))

    with host(target), sudo(""), cd(hosts_docker_folder):
        #
        # unzip cluster.zip
        #
        run("unzip -q {}".format(configs.cluster_file_name))
        run("rm {}".format(configs.cluster_file_name))

        #
        # copy certificate
        #
        docker_valid_certificate_path = join("/etc/docker/certs.d", configs.docker_hub_url)
        run("mkdir -p {}".format(docker_valid_certificate_path))
        run("cp {} {}".format(join(hosts_docker_folder, "domain.crt"),
                              join(docker_valid_certificate_path, "ca.crt")))


        # docker run --restart=always -dt hub.docker.iais.co:5000/swarm join --addr=172.16.111.116:2375 consul://172.16.111.113:8500     in 116
        # docker run --restart=always -dt hub.docker.iais.co:5000/swarm join --addr=172.16.111.36:2375 consul://172.16.111.113:8500      in 36
        # docker run --restart=always -dt -p 8500:8500 -h host progrium/consul -server -bootstrap                                        in 113
        # docker run --restart=always -dt swarm join --addr=172.16.111.113:2375 consul://172.16.111.113:8500                             in 113
        # docker run --restart=always -dt -p 2385:2375 swarm manage consul://172.16.111.113:8500                                         in 113

        # docker -H :2385 info       check command
