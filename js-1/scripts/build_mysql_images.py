from contextlib import suppress
from os.path import join
from time import sleep

from Utils.Utils import get_items_path
from Utils.jfabric import local, lcd
import configs

temp_folder = "/temp/mysql"
mysql_folder = "/configs/mysql"
mysql_password = "mysql123sjb"

host = "172.16.111.116"
update_dumps = False
update_images = True

mysqls = {
    "rajae": [ "customs"]#, "nezarat", "tir", "jbazbini"],],  # , "baskool", "swich", "center",],  # , "baskool", "swich", "center"
    #"bazargan": ["customs"],  # ,  # , "baskool", "swich"],
   # "tehran": ["customs"]  # , "baskool", "swich"],
}

mysql_ports = {
    "rajae": 3000,
    "bazargan": 3010,
    "tehran": 3020,
}

tables = {
    "customs": ["*"],
    "baskool": ["*"],
    "swich": [],
    "center": ["*"],
    "center-swich": ["*"],
}


def create_mysql_dump(host, port, database, tables):
    no_data_option = "" if len(tables) > 0 and tables[0] == "*" else " --no-data"
    local(
        "mysqldump -h {} -u root -p{} --port={} --result-file {} --databases {}{}", host, mysql_password, port,
        database + "-schema.sql", database, no_data_option)
    if no_data_option:
        for table in tables:
            local(
                "mysqldump -h {} -u root -p{} --port={} --result-file {} {} {}", host, mysql_password, port,
                "{}-{}.sql".format(database, table), database, table)


if update_dumps:
    local("mkdir -p {}", mysql_folder)
    with lcd(mysql_folder):
        # local("sudo rm -rf dumps")
        local("mkdir -p dumps")
        with lcd("dumps"):
            for location in mysqls:
                local("mkdir -p {}".format(location))
                with lcd(location):
                    for database in mysqls[location]:
                        create_mysql_dump(host, mysql_ports[location], database, tables[database])

if update_images:
    local("mkdir -p {}", temp_folder)
    with lcd(temp_folder):
        local("rm -rf docker")
        local("mkdir docker")
        with lcd("docker"):
            local("cp {} . -r".format(join(mysql_folder, "*")))

            for location in mysqls:
                container_name = "mysql-{}-temp".format(location)
                with suppress(Exception):
                    local("docker rm -f {}", container_name)

                local("docker run --name {} -d -e MYSQL_ROOT_PASSWORD={} hub.docker.iais.co:5000/mysql",
                      container_name, mysql_password)
                sleep(15)

                for sql_path in get_items_path(join(mysql_folder, "dumps", location, "*.sql")):
                    local("docker exec -i {} mysql -p{} < {}", container_name, mysql_password, sql_path)
                sleep(30)

                local("docker stop {}", container_name)
                data_folder = "{}-data".format(location)
                local("rm -rf {}".format(data_folder))
                local("docker cp {}:/var/lib/mysql {}", container_name, data_folder)

                image_name = "{}/mysql-{}".format(configs.docker_hub_url, location)
                local("docker build -t {} --build-arg name={} . ".format(image_name, data_folder))

                local("docker push {}", image_name)
