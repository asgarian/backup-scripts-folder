import sys

from StressTest.StressTestGenerator import StressTestGenerator
from Utils.Utils import error
from Utils.jfabric import local, disable_exit_code_check


def free_port_and_run_stg(port):
    with disable_exit_code_check():
        local("fuser -k {}/tcp".format(port))

    generator = StressTestGenerator('', port)
    generator.run()


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print(error("provide a port number as arg 1"))
        exit(1)

    port = int(sys.argv[1])
    free_port_and_run_stg(port)
