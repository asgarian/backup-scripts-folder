from os.path import join
from time import sleep

from Utils.JenkinsUtils import get_json_from_jenkins, set_job_description
from Utils.jfabric import local, cd, run, host, sudo, lcd
import configs


class buildCassandra:
    def __init__(self, cluster_name, table):
        self.cluster_name = cluster_name
        self.table = table

    def run(self):
        description = get_json_from_jenkins("http://172.16.111.8:8100/job/cassandra_dump/api/json")["description"]

        temp_folder = "/jenkins/temp/cassandra"
        cassnadra_folder = "/configs/cassandra"
        new_dump_folder = "/jenkins/cassandra"
        folder_8 = "/jenkins/8"
        keyspace = "test2"
        tables_white_list = str(description).replace("\"", "").split(",")
        tables_white_list.append(self.table)
        # set_job_description("cassandra_dump", tables_white_list)

        server = "172.16.111.10"
        thrift_port = "8031"
        cql_port = "8032"

        cassandra_dump_data_path = join(configs.scripts_home, "cassandradumpdata.py")

        cassnadra_image = "{}/cassandra".format(configs.docker_hub_url)

        update_dumps = True
        update_images = True

        cassnadras = [
            "0",
            "1",
            "2",
        ]

        if update_images:
            with host(self.cluster_name), sudo(""):
                run("rm -rf {}".format(folder_8))
                run("mkdir {}".format(folder_8))
                run("chown ut:ut {}".format(folder_8))
                local("scp -r {} {}:{}".format(join(cassnadra_folder, "*"), self.cluster_name,folder_8))
                local("scp {} {}:{}".format(cassandra_dump_data_path, self.cluster_name,folder_8))

                run("rm -rf {}", temp_folder)
                run("mkdir -p {}", temp_folder)
                with cd(temp_folder):
                    run("cp {} . -r".format(join(folder_8, "*")))

                    run("docker-compose down")

                    try_count = 0
                    while True:
                        try_count += 1
                        run("docker-compose up -d")
                        sleep(30)
                        nodetool_status = run("docker exec cassandra_cassandra0_1 nodetool status")
                        if nodetool_status.count("UN ") == 3:
                            break
                        if try_count > 5:
                            raise Exception("could not find three cassandra node")

                    run("docker cp dump/schema.cql cassandra_cassandra0_1:/schema.cql")

                    print("Restoring schema")
                    run("docker exec cassandra_cassandra0_1 cqlsh -f /schema.cql")

                    print("Restoring data")
                    run('python2.7 -u cassandradumpdata.py restore 127.0.0.1:9012 {} dump/data "{}"'.format(keyspace,
                                                                                                            " ".join(
                                                                                                                tables_white_list)))
                    print("Waiting 30 seconds for distributing data between nodes")
                    sleep(30)
                    run("docker-compose down")

                    run("mkdir build")
                    run("cp Dockerfile build/")
                    for node in cassnadras:
                        run("cp -r {} build/data".format(node))
                        image_name = "{}/cassandra{}:temp".format(configs.docker_hub_url, node)
                        with cd("build"):
                            run("docker build -t {} .".format(image_name))
                            run("rm -r data")
                        run("docker push {}".format(image_name))

        sleep("60")
        if update_dumps:
            local("rm -r {}".format(new_dump_folder))
            local("mkdir {}".format(new_dump_folder))
            with lcd(new_dump_folder):
                local(
                    "sudo docker run --rm {} cqlsh {} {} -e 'DESCRIBE SCHEMA' > schema.cql".format(cassnadra_image,
                                                                                                   server,
                                                                                                   cql_port))

                local("mkdir data")
                local("python2.7 -u {} save {}:{} {} data ''".format(cassandra_dump_data_path, server, thrift_port,
                                                                     keyspace))
