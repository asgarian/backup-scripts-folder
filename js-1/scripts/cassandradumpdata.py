# -*- coding: utf-8 -*-
from os import listdir
from os.path import join
import sys

import pycassa


def get_pool(server_list, keyspace):
    return pycassa.ConnectionPool(keyspace, server_list=server_list)


def encode(input):
    if type(input) == int:
        type_mark = "i"
    else:
        type_mark = ""

    if type(input) == unicode:
        value = input.encode('utf8')
    else:
        value = str(input)

    value = value.encode('string_escape')

    return "{}'{}'".format(type_mark, value)


def decode(input):
    decoded_input = input.decode('string_escape')

    if decoded_input.startswith("'"):
        return decoded_input[1:-1]
    else:
        type_mark = input[0]
        value = decoded_input[2:-1]

        if type_mark == "i":
            return int(value)
        else:
            raise Exception("unknown type mark: {}".format(type_mark))


def save_data(server_list, keyspace, dir_path):
    column_family_names = pycassa.system_manager.SystemManager(server_list[0]).get_keyspace_column_families(
        keyspace).keys()
    for column_family in column_family_names:
        if column_family == "TempFileColumnFamily":
            continue;
        if column_family == "uploadedImage":
            continue;

        print("getting {}".format(column_family))
        cf_list = []
        for i in range(1):
            cf = pycassa.ColumnFamily(get_pool(server_list, keyspace), column_family)
            current_cf_list = list(cf.get_range())

            if len(cf_list) < len(current_cf_list):
                cf_list = current_cf_list

        dump = ""

        for row in cf_list:
            dump += "RowKey: {}".format(row[0]) + "\n"

            values = row[1]

            for key in values:
                key_encoded = encode(key)
                value_encoded = encode(values[key])

                dump += "=> {}={}\n".format(key_encoded, value_encoded)

            dump += "\n"

        print("collected {} rows".format(len(cf_list)))
        print("")

        with open(join(dir_path, column_family), "w") as file:
            file.write(dump)


def restore_data(server_list, keyspace, dir_path, tables_white_list):
    for file_name in listdir(dir_path):
        if file_name not in tables_white_list:
            continue

        print("Restoring {}".format(file_name))
        count = 0

        row_key = ""
        values = {}
        with open(join(dir_path, file_name), "r") as file:

            cf = pycassa.ColumnFamily(get_pool(server_list, keyspace), file_name)

            for line in file.readlines():
                line = line[:-1]
                if line.startswith("RowKey: "):
                    if row_key:
                        cf.insert(row_key, values)
                        count += 1

                    row_key = line[8:]
                    values.clear()
                if line.startswith("=> "):
                    parts = line[3:].split('=', 1)
                    key = decode(parts[0])
                    value = decode(parts[1])

                    values[key] = value

            if row_key:
                cf.insert(row_key, values)
                count += 1

        print("Restored {} rows".format(count))
        print("")


if __name__ == "__main__":
    mode = sys.argv[1]

    server_list = sys.argv[2].split(' ')
    keyspace = sys.argv[3]
    dir_path = sys.argv[4]
    tables_white_list = sys.argv[5].split(' ')

    if mode == "save":
        save_data(server_list, keyspace, dir_path)
    elif mode == "restore":
        restore_data(server_list, keyspace, dir_path, tables_white_list)
    else:
        raise Exception("unknown mode: {}".format(mode))
