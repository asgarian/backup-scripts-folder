import io
import subprocess
import sys
import threading
from copy import copy
from os.path import join

states = {}


def get_state():
    tid = threading.get_ident()
    if tid not in states:
        states[tid] = {}

    return states[tid]


class StateChanger:
    def __init__(self, new_state):
        self.new_state = new_state

    def __enter__(self):
        state = get_state()
        self.saved_state = copy(state)
        for key in self.new_state:
            state[key] = self.new_state[key]

    def __exit__(self, type, value, traceback):
        states[threading.get_ident()] = self.saved_state


def get_path_key(host):
    return "path-{}".format(host)


def path_changer(host, path):
    state = get_state()
    key = get_path_key(host)

    if key not in state:
        current_path = ""
    else:
        current_path = state[key]

    return StateChanger({key: join(current_path, path)})


def lcd(path):
    return path_changer("localhost", path)


def cd(path):
    return path_changer("remotehost", path)


def host(new_host):
    return StateChanger({"host": new_host, get_path_key("remotehost"): "."})


def proxy(condition):
    return StateChanger({"use_proxy": True} if condition else {})


def sudo(password):
    return StateChanger({"use_sudo": True, "password": password})


def skip_output():
    return StateChanger({"skip_output": True})


def skip_echo():
    return StateChanger({"skip_echo": True})


def return_stderr():
    return StateChanger({"return_stderr": True})


def disable_exit_code_check():
    return StateChanger({"disable_exit_code_check": True})


def output_prefix(prefix):
    return StateChanger({"output_prefix": prefix})


def get_current_path(host):
    state = get_state()
    key = get_path_key(host)

    if key not in state:
        return "."
    else:
        return state[key]


def get_current_host():
    state = get_state()
    key = "host"

    if key not in state:
        raise Exception("No host provided")
    else:
        return state[key]


def get_output_prefix():
    state = get_state()
    key = "output_prefix"

    if key in state:
        return state[key]
    else:
        return ""


def is_flag_on(key):
    state = get_state()
    return key in state and state[key]


def get_proxy_part():
    if is_flag_on("use_proxy"):
        return "proxychains "
    else:
        return ""


def get_sudo_part():
    if is_flag_on("use_sudo"):
        state = get_state()
        password = state["password"]
        if password == "":
            return "sudo "
        else:
            return "echo {} | sudo -S ".format(password)
    else:
        return ""


def add_cd_to_command(path, command):
    if path == ".":
        return command
    else:
        return "cd {} >/dev/null && {}".format(path, command)


def get_last_exit_code():
    return get_state()["last_exit_code"]


def print_with_prefix(str):
    print(get_output_prefix() + str)


def execute(command):
    current_path = get_current_path("localhost")
    final_command = add_cd_to_command(current_path, command)

    process = subprocess.Popen(final_command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True,
                               universal_newlines=True)
    stdout = ""
    for line in io.TextIOWrapper(process.stdout.buffer, encoding='utf-8', errors='ignore'):
        if line != "ProxyChains-3.1 (http://proxychains.sf.net)\n":
            if not is_flag_on("skip_output"):
                sys.stdout.write(get_output_prefix() + line)
            stdout += line + "\n"

    stderr = ""
    for line in io.TextIOWrapper(process.stderr.buffer, encoding='utf-8', errors='ignore'):
        if not is_flag_on("skip_output"):
            sys.stdout.write(get_output_prefix() + line)
        stderr += line + "\n"

    exit_code = process.wait()
    get_state()["last_exit_code"] = exit_code
    if not is_flag_on("disable_exit_code_check") and exit_code != 0:
        raise Exception("nonzero exit code for {}".format(final_command))

    if is_flag_on("return_stderr"):
        return stdout, stderr
    else:
        return stdout


def local(*args, **kwargs):
    command = format_string(args, kwargs)

    if not is_flag_on("skip_echo"):
        print_with_prefix("[localhost]: {}".format(command))

    return execute(command)


def run(*args, **kwargs):
    command = format_string(args, kwargs)

    current_host = get_current_host()
    current_path = get_current_path("remotehost")

    if not is_flag_on("skip_echo"):
        print_with_prefix("[{}]: {}".format(current_host, command))

    sudo_command = get_sudo_part() + command
    return execute("{}ssh -q {} 'source /etc/profile; {}'".format(get_proxy_part(), current_host,
                                                                  add_cd_to_command(current_path, sudo_command)))


def format_string(args, kwargs):
    if len(args) > 1:
        command = str.format(*args, **kwargs)
    else:
        command = args[0]
    return command


def put(src, des):
    current_host = get_current_host()
    current_path = get_current_path("remotehost")
    absolute_des = join(current_path, des)

    print_with_prefix("[{}] put: {} -> {}".format(current_host, src, absolute_des))
    execute("{}scp -r {} {}:{}".format(get_proxy_part(), src, current_host, absolute_des))


def rsync(src, des):
    current_host = get_current_host()
    current_path = get_current_path("remotehost")
    absolute_des = join(current_path, des)

    print_with_prefix("[{}] rsync: {} -> {}".format(current_host, src, absolute_des))
    execute("{}rsync {} {}:{} -az --delete".format(get_proxy_part(), src, current_host, absolute_des))


def get(src, des):
    current_host = get_current_host()
    current_path = get_current_path("remotehost")
    absolute_src = join(current_path, src)

    print_with_prefix("[{}] get: {} -> {}".format(current_host, src, des))
    execute("{}scp -r {}:{} {}".format(get_proxy_part(), current_host, absolute_src, des))


def exists(path):
    try:
        run("ls -d {}".format(path))
    except:
        return False
    return True


def get_content(path):
    current_host = get_current_host()
    current_path = get_current_path("remotehost")
    absolute_path = join(current_path, path)

    return_code, stdout = subprocess.getstatusoutput(
        "{}ssh {} 'cat {}'".format(get_proxy_part(), current_host, absolute_path))

    return stdout


def put_content(path, content):
    run('printf "{}" > {}'.format(content.replace("\n", "\\n"), path))
