import functools
import hashlib
import os
import re
import socket
import subprocess
import sys
import time
import traceback
import urllib
from contextlib import suppress
from multiprocessing import Lock
from os.path import dirname
from os.path import join
from urllib.parse import quote

import requests
import socks
import termcolor
import winrm
from termcolor import colored

import configs
from Deploy.Observable import Observable
from Utils import jfabric
from Utils.JenkinsUtils import get_last_stable_version_from_jenkins
from Utils.jfabric import local, put, exists, get_current_host, host, skip_output, lcd

error = lambda text: colored(text, 'white', 'on_red', attrs=['bold'])
warning = lambda text: colored(text, None, 'on_yellow', attrs=['bold'])
green = lambda text: colored(text, None, 'on_green', attrs=['bold'])
blue = lambda text: colored(text, 'white', 'on_blue', attrs=['bold'])

ansi_escape = re.compile(r'\x1b[^m]*m')


def remove_color(text):
    return ansi_escape.sub('', text)


def has_color(text):
    return ansi_escape.findall(text)


def get_color(text):
    colors = ansi_escape.findall(text)
    if len(colors) > 1:
        return colors[:-1]
    else:
        return None


def add_color(text, colors):
    if colors:
        return "".join(colors) + text + termcolor.RESET
    else:
        return text


def print_exception(type, value, tb):
    tbtext = ''.join(traceback.format_exception(type, value, tb))
    sys.stderr.write(error(tbtext))


def getstatusoutput(cmd):
    try:
        data = subprocess.check_output(cmd, shell=True, universal_newlines=True)
        status = 0
    except subprocess.CalledProcessError as ex:
        data = ex.output
        status = ex.returncode
    if data[-1:] == '\n':
        data = data[:-1]
    return status, data


# get list of full path of files and folders from semipath forexample:
# sample input: /home/ut/SuccessfullBuilds/Customs/21*/target/*.war
# sample output: ['/home/ut/SuccessfullBuilds/Customs/21-21/target/Customs-5.5.3-r000.war', '/home/ut/SuccessfullBuilds/Customs/21-22/target/Customs-5.5.3-r000.war']
#
# you can also use it to check whether a path exists like this:
#    if get_item_path("/configs/Customs/13/."):
#
def get_items_path(input):
    return_code, stdout = getstatusoutput("ls -d {}".format(input))
    if return_code != 0:
        return []
    result = stdout.split("\n")
    return result


section_start_mark = lambda title: print(">>> start of {} <<<\n".format(title))
section_end_mark = lambda title: print("\n>>> end  of  {} <<<".format(title))


def call_with_section_mark(f, title, *args, **kwargs):
    section_start_mark(title)
    return_value = f(*args, **kwargs)
    section_end_mark(title)
    return return_value


#
# Mark a function as a section so we add two responsibilities to it:
# 1) It will print section mark at the beginning and the end of function unless
#    we have a variable called 'print_section_mark' set to False in self
# 2) It will call set_status at the beginning of function if self is Observable
#    and set status to '{name}ing'
#
def section(f):
    def wrapper(*args, **kwargs):
        title = f.__name__.replace("_", " ").title()

        if len(args) > 0 and hasattr(args[0], 'print_section_mark'):
            print_section_mark = args[0].print_section_mark
        else:
            print_section_mark = True

        if len(args) > 0 and isinstance(args[0], Observable):
            args[0].set_status("{}ing".format(title))

        if print_section_mark:
            return_value = call_with_section_mark(f, title, *args, **kwargs)
        else:
            return_value = f(*args, **kwargs)

        return return_value

    functools.update_wrapper(wrapper, f)  # update wrapper's metadata
    return wrapper


class make_section:
    def __init__(self, title):
        self.title = title

    def __enter__(self):
        section_start_mark(self.title)

    def __exit__(self, type, value, traceback):
        section_end_mark(self.title)


#
# section that also include args
#
def section_with_args(f):
    def wrapper(*args, **kwargs):
        name = f.__name__ + str(args).replace(",)", ")")
        return call_with_section_mark(f, name, *args, **kwargs)

    functools.update_wrapper(wrapper, f)  # update wrapper's metadata
    return wrapper


def copy_if_not_exist(local_path, server_path):
    print("Checking existence of {} on {}".format(server_path, get_current_host()))

    found_on_server = exists(server_path)
    if found_on_server:
        print("file {} already exist on server, so copy not needed".format(server_path))
    else:
        print("Copying from {} to {} in {}".format(local_path, server_path, get_current_host()))
        put(local_path, server_path)

        if exists(server_path):
            print("copied successfully")
        else:
            print("error in copying to {}".format(get_current_host()))


# return response if can receive in some try otherwise return None
# it will try 'try_count' times with timeout 'timeout' and wait between tries
# as 'sleep_time'
def try_get_url(url, try_count, timeout, sleep_time):
    for i in range(1, try_count + 1):
        try:
            print("({} of {})Trying to get {}".format(i, try_count, url))
            response = requests.get(url, timeout=timeout)

            if response:
                return response.content.decode()
        except Exception as e:
            print(str(e) + "\n")

        if i != try_count:
            time.sleep(sleep_time)
    return None


@section_with_args
def run(command):
    return jfabric.run(command)


@section_with_args
def put(local_path, remote_path):
    return jfabric.put(local_path, remote_path)


def urljoin(*parts):
    parts = list(parts)
    if '' in parts:
        parts.remove('')
    return '/'.join(s.strip('/') for s in parts)


def create_folder(folder_path):
    local("mkdir -p {}".format(folder_path))


def recreate_folder(folder_path):
    local("rm -rf {}".format(folder_path))
    create_folder(folder_path)


def clone_folder(src, des):
    recreate_folder(des)
    if get_items_path(src):  # we dont use path.exist as we want handle cases where src ends with *
        local("cp -r {} {}".format(src, des))


class HttpProxy:
    def __init__(self):
        pass

    def __enter__(self):
        self.saved_proxy = socks.getdefaultproxy()
        self.saved_socket = socket.socket
        if not am_i_newjenkins():
            socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, "127.0.0.1", port=12345)
            socket.socket = socks.socksocket

    def __exit__(self, type, value, traceback):
        socket.socket = self.saved_socket
        socks.setdefaultproxy(self.saved_proxy)


def send_sms(phone, msg):
    try:
        url = 'http://172.17.20.10:8080/SMSManager/send'
        user_agent = 'Mozilla/10.0 (compatible; MSIE 5.5; Windows NT)'
        values = {'Recipient': phone, 'Message': msg, 'USE_CODING': 'false'}
        headers = {'User-Agent': user_agent}
        data = urllib.parse.urlencode(values).encode('UTF-8')
        req = urllib.request.Request(url, data, headers)
        response = urllib.request.urlopen(req, timeout=10)
        r = response.read().decode()
        if "\"isSuccess\":true" in r:
            print("SMS sent successfully to " + phone)
            return
    except:
        pass

    print(error("Error in Sending SMS to " + phone))


def remove_if_exist(list, element):
    if element in list:
        list.remove(element)


def am_i_newjenkins():
    return True


synchronized_lock = Lock()


def synchronized(method):
    """ Work with instance method only !!! """

    def new_synchronized_method(self, *arg, **kws):
        if not hasattr(self, "_auto_lock"):
            with synchronized_lock:
                if not hasattr(self, "_auto_lock"):
                    self._auto_lock = Lock()
        with self._auto_lock:
            return method(self, *arg, **kws)

    return new_synchronized_method


def split_by_size(input, split_size):
    chunks, chunk_size = len(input), split_size
    return [input[i:i + chunk_size] for i in range(0, chunks, chunk_size)]


def run_windows_command(command):
    if am_i_newjenkins():
        server = "84"
    else:
        server = "2034"

    print("Running command: " + command)
    with host(server):
        return jfabric.execute("ssh {} powershell {}".format(server, command))

def run_windows_command_new(command):
    server = "172.16.111.80"
    print(server)
    print("Running command: " + command)
    with host(server):
        s = winrm.Session(server, auth=('administrator', 'Javad1366!'))
        r = s.run_cmd(command, ['/all'])
        r.status_code
        a = r.std_out.decode()
        print(a)


#
# Copy files and folders from base/src to des
# but it will keep directory structure after base
# for example with:
#   base = "/a/b"
#   src = "c/d.txt"
#   des = "/x"
#
# it will copy
#   from /a/b/c/d.txt
#   to /x/c/d.txt
#
def copy_with_structure(base, src, des):
    items = get_items_path(join(base, src))
    for item in items:
        directory = dirname(item).replace(base, "").strip('/')
        local("mkdir -p {}".format(join(des, directory)))
        local("cp {} {}".format(item, join(des, directory, ".")))


def replace_in_file(file_path, find, replace):
    with open(file_path, 'rb') as file:
        content = file.read()

    content = content.replace(find.encode(), replace.encode())

    with open(file_path, 'wb') as file:
        file.write(content)


#
# goes up the 'upstream builds' tree until find an endpoint or the root cause
#
def get_endpoint_info(info):
    current_node = upstream = info

    while upstream:
        current_node = upstream
        if current_node.name in configs.endpoints:
            break

        upstream = get_upstream_build(current_node)

    return current_node


def add_to_csv(csv_dir, csv_name, header, content):
    header += "\n"
    csv_path = join(csv_dir, csv_name)
    seprator_declearation = "sep=,\n"

    with open(csv_path, "w+") as f:
        file_content = f.readlines()

    remove_if_exist(file_content, seprator_declearation)
    remove_if_exist(file_content, header)
    content = [seprator_declearation, header] + file_content + content
    local("mkdir -p {}".format(csv_dir))

    with open(csv_path, "w") as csv_file:
        for item in content:
            csv_file.write(item)


def get_hash_of_dir(directory, verbose=0):
    SHAhash = hashlib.md5()
    if not os.path.exists(directory):
        return -1

    try:
        for root, dirs, files in os.walk(directory):
            for names in files:
                if verbose == 1:
                    print('Hashing', names)
                filepath = os.path.join(root, names)
                try:
                    f1 = open(filepath, 'rb')
                except:
                    # You can't open the file for some reason
                    f1.close()
                    continue

        while 1:
            # Read file in as little chunks
            buf = f1.readlines(4096)
            print(buf)
            if not buf: break
            for line in buf:
                # line.encode('utf-8')
                SHAhash.update(hashlib.md5(line).hexdigest().encode('utf-8'))


    except:
        # Print the stack traceback
        traceback.print_exc()
        return -2

    return SHAhash.hexdigest()


def get_files_path_recursive(path):
    with skip_output():
        return [
            file
            for file in local("find {} -type f".format(path)).split(os.linesep)
            if file
            ]


def get_ip_port(ip_port):
    match = re.match(".*?(\d+\.\d+\.\d+\.\d+):(\d+).*?", ip_port)

    if match:
        ip = match.group(1)
        port = int(match.group(2))

        return ip, port

    if re.match("\d+.\d+.\d+.\d+", ip_port):
        return ip_port, 80

    return None


#
# compare content of dir1 and dir2 and remove similar ones from dir2
#
def remove_similar_items(dir1, dir2):
    with suppress(Exception):
        diff_output = local("diff -rs {} {}".format(dir1, dir2))

    for line in diff_output:
        match = re.match("Files .*? and (.*?) are identical", line)

        if match:
            file_path = match.group(1)
            local("rm {}".format(file_path))

    local("find {}/ -type d -empty -delete".format(dir2))


def update_last_stable_build_links():
    with lcd(configs.sb_folder):
        projects = local("ls").split("\n\n")

        for project in projects:
            lsb = get_last_stable_version_from_jenkins(project)
            if re.match('\d+', lsb):
                with lcd(project):
                    local("rm -f last-stable-build")
                    local("ln -sf {} last-stable-build".format(lsb))


def get_container_id(name):
    return local("docker ps -a --filter Name=^/{}$ -q".format(name))


def remove_container_if_exist(container_name):
    if get_container_id(container_name):
        local("docker rm -f {}".format(container_name))


def get_ci_state():
    return configs.current_ci_state


def get_exact_library_name(semi_name):
    simplify_name = lambda name: name.replace("-", "").lower()
    semi_name_simple = simplify_name(semi_name)

    for lib in configs.libraries:
        if simplify_name(lib) == semi_name_simple:
            return lib

    return None


def get_exact_branch_name(branch_name):
    if not branch_name:
        return None
    return branch_name.replace("origin/", "").replace("remotes/", "").replace("refs/", "")


def is_internal_library(library_full_name):
    matched_prefixes = [internal_lib_prefix for internal_lib_prefix in configs.internal_lib_prefixes if
                        library_full_name.startswith(internal_lib_prefix)]

    return len(matched_prefixes) > 0
