import os

import configs
from Utils.jfabric import local


#
# Every things that is done by a jenkins build
# that has a name(Job name) and number(build number)
# it can be either a Build or a Test or a Deploy.
#
class Operation(object):
    def __init__(self, name, number):
        self.name = name
        self.number = str(number)

    # sample output: /jenkins/temp/Mis_deploy/23
    # Responsible for creating the folder
    def get_temp_folder(self):
        folder_path = self.get_temp_folder_path()
        from Utils import Utils
        Utils.create_folder(folder_path)
        return folder_path

    def get_temp_folder_path(self):
        return os.path.join(configs.temp_folder, self.name.replace(" ", "_"), self.number)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        local("rm -rf {}".format(self.get_temp_folder_path()))

    def is_test(self):
        return self.name.endswith(configs.jenkins_test_jobs_name_prefix)

    def __eq__(self, other):
        return isinstance(other, Operation) and self.name == other.name and self.number == other.number
