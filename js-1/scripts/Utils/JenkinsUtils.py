import os
import re
import time
from contextlib import suppress
from os.path import join
from urllib.parse import quote
from urllib.request import Request

import jenkins
import requests
from jsonpickle import json
from requests.auth import HTTPBasicAuth

import configs
from Utils.Operation import Operation
from Utils.jfabric import local, lcd


class JenkinsGetUrlError(Exception):
    pass


def get_from_jenkins(url):
    try:
        url = url.replace("http://172.16.111.8:8080", "http://127.0.0.1:8080")
        url = url.replace("http://172.17.20.32:8080", "http://127.0.0.1:8080")
        url = url.replace("http://newjenkins.iais.ir", "http://127.0.0.1:8080")

        os.environ['NO_PROXY'] = '127.0.0.1'
        response = requests.get(url, auth=HTTPBasicAuth(configs.jenkins_user, configs.jenkins_password))
        return response.content.decode()
    except Exception as ex:
        raise JenkinsGetUrlError("error while getting {}".format(url)) from ex


# sample input: url=http://172.16.111.8:8080/job/Customs/488/
# output: json object at http://172.16.111.8:8080/job/Customs/488/api/json
def get_json_from_jenkins(url):
    try:
        url = url.strip("/")
        j_str = get_from_jenkins("{}/api/json".format(url))
        return json.loads(j_str)
    except Exception as ex:
        raise JenkinsGetUrlError("error while getting json from {}".format(url)) from ex


def get_jenkins_build_json(operation):
    return get_json_from_jenkins("{}/job/{}/{}/api/json".format(configs.jenkins_url, operation.name, operation.number))


# sample input: url=http://172.16.111.8:8080/job/Customs/11/
# output in deploy jenkins: Customs, 488 (original build number)
# output in jenkins: Customs, 11
def get_build_from_deputy_url(url):
    j = get_json_from_jenkins(url)

    match_obj = re.match(r'http://.+/job/(.+)/\d+', url)
    if match_obj:
        job_name = match_obj.group(1)
    else:
        raise Exception("invalid build url: {}")

    build_number = None
    if "actions" in j and len(j["actions"]) > 0 and "parameters" in j["actions"][0]:
        # Deploy Jenkins
        params = j["actions"][0]["parameters"]
        for param in params:
            if param["name"] == "original_build_number":
                build_number = param["value"]
                break

    if not build_number:
        # Private Deploys
        build_number = str(j["number"])

    return job_name, build_number


def get_last_stable_version_from_jenkins(job_name):
    return get_from_jenkins(
        "http://172.16.111.8:8080/job/{}/lastStableBuild/buildNumber".format(job_name))


def get_last_stable_version_from_link(job_name):
    return local("readlink {}".format(get_last_stable_link_path(job_name))).replace("\n", "")


def get_last_version(job_name):
    return get_from_jenkins(
        "http://172.16.111.8:8080/job/{}/lastBuild/buildNumber".format(job_name))


def set_deploy_jenkins_job_description(job_name, description):
    return set_job_description(configs.deploy_jenkins_address,
                               configs.deploy_jenkins_user,
                               configs.deploy_jenkins_api_token,
                               job_name,
                               description)


def set_job_description(jenkins_address, user, api_token, job_name, description):
    payload = {'description': description}
    r = requests.post(
        "http://{}:{}@{}/job/{}/description".format(user, api_token, jenkins_address, job_name), data=payload)

    return r.status_code


def set_jenkins_build_description(job_name, build_number, description):
    server = get_jenkins_server()

    r = Request(
        "http://{}/job/{}/{}/submitDescription".format(configs.jenkins_address, quote(job_name), build_number),
        data="description={}".format(description).encode())

    return server.jenkins_open(r)


def decode_jenkins_args(args):
    result = {}

    for arg in args:
        if "=" in arg:
            parts = arg.split("=", maxsplit=1)
            name = parts[0]
            value = parts[1]

            if value == "true":
                value = True
            if value == "false":
                value = False

            result[name] = value

    return result


class UnstableException(Exception):
    pass


def mark_current_build_as_unstable():
    print("result=unstable")


jenkins_server = None


def get_jenkins_server():
    global jenkins_server

    if not jenkins_server:
        jenkins_server = jenkins.Jenkins(configs.jenkins_url, username=configs.jenkins_user,
                                         password=configs.jenkins_password)
    return jenkins_server


#
# by now we suppose that target job does not have queue
#
def trigger_job(name, wait_for_exit=False, unstable_on_fail=False, params=None, upstream=None):
    server = get_jenkins_server()
    job_info = server.get_job_info(name)
    next_build_number = job_info["nextBuildNumber"]

    print("building {} {}".format(name, next_build_number))
    print("{}/job/{}/{}/console".format(configs.jenkins_url_outside, quote(name), next_build_number))
    server.build_job(name, parameters=params)
    if upstream:
        time.sleep(1)
        set_jenkins_build_description(name, next_build_number,
                                      "Started By {}#{}".format(upstream.name, upstream.number))

    if wait_for_exit:
        print("waiting for {} {} to exit".format(name, next_build_number))
        while True:
            time.sleep(1)
            build_info = server.get_build_info(name, next_build_number)
            if not build_info["building"]:
                print("{} {} exited result = {}".format(name, next_build_number, build_info["result"]))
                break

    if unstable_on_fail:
        build_info = server.get_build_info(name, next_build_number)
        if build_info["result"] not in ['UNSTABLE', 'SUCCESS']:
            print("mark current build as unstable".format(name, next_build_number))
            raise UnstableException()


def get_scm_urls_from_jenkins_config_files(jobs):
    result_svn = {}
    result_git = {}

    for job in jobs:
        config_file_path = join(configs.jenkins_home, "jobs", job, "config.xml")
        with open(config_file_path, "r") as config_file:
            config_file_content = config_file.read()

            if "hudson.scm.SubversionSCM" in config_file_content:
                url_match = re.match(".*<remote>(.+)</remote>.*", config_file_content, re.DOTALL)

                if url_match:
                    svn_url = url_match.group(1)
                    result_svn[job] = svn_url

            if "hudson.plugins.git.GitSCM" in config_file_content:
                url_match = re.match(".*<url>(.+)</url>.*", config_file_content, re.DOTALL)

                if url_match:
                    git_url = url_match.group(1)
                    result_git[job] = git_url

    return result_svn, result_git


def config_jobs_base_on_existing_job(source, jobs):
    source_scm_url = jobs[source]

    server = get_jenkins_server()
    source_config = server.get_job_config(source)

    for job in jobs:
        if job == source:
            continue

        print("Configuring Job {}".format(job))

        if not server.job_exists(job):
            server.copy_job(source, job)

        scm_url = jobs[job]
        job_config = source_config.replace(source_scm_url, scm_url).replace(source, job)
        server.reconfig_job(job, job_config)

    for view in server.get_views():
        view_name = view["name"]
        if source in get_jobs_in_view(view_name):
            add_jobs_to_view(jobs, view_name)


def get_jobs_in_view(view):
    server = get_jenkins_server()

    view_config = server.get_view_config(view)
    include_match = re.match(".*(<includeRegex>(.*)</includeRegex>).*", view_config, re.DOTALL)
    if include_match:
        return include_match.group(2).split("|")
    else:
        return []


def add_jobs_to_view(jobs, view):
    server = get_jenkins_server()

    view_config = server.get_view_config(view)

    include_match = re.match(".*(<includeRegex>(.*)</includeRegex>).*", view_config, re.DOTALL)
    if include_match:
        jobs_in_view = set(include_match.group(2).split("|") + list(jobs.keys()))

        new_view_config = view_config.replace(include_match.group(1),
                                              "<includeRegex>{}</includeRegex>".format("|".join(jobs_in_view)))

        server.reconfig_view(view, new_view_config)
    else:
        raise Exception("could not find includeRegex in config")


def reconfig_jobs():
    config_jobs_base_on_existing_job('Customs', configs.endpoints_git)
    config_jobs_base_on_existing_job('Baskool', configs.endpoints_svn)
    config_jobs_base_on_existing_job('SalonGomrok', configs.libraries_git)
    config_jobs_base_on_existing_job('Swich-Lib', configs.libraries_svn)

    test_jobs = {"{}{}".format(job, configs.jenkins_test_jobs_name_prefix): "" for job in
                 list(configs.endpoints_git.keys()) + list(configs.endpoints_svn.keys())}

    config_jobs_base_on_existing_job('Customs Test', test_jobs)


def get_upstream_build(operation):
    server = get_jenkins_server()

    build = server.get_build_info(operation.name, int(operation.number))
    build_description = build["description"] or ""

    upstream_build_match = re.match("Started By (.+)#(\d+).*", build_description, re.DOTALL)
    if upstream_build_match:
        upstream_name = upstream_build_match.group(1)
        upstream_number = upstream_build_match.group(2)

        return Operation(upstream_name, upstream_number)


def get_running_jobs():
    server = get_jenkins_server()

    running_jobs = server.get_running_builds()

    return [Operation(job["name"].replace("%20", " "), job["number"]) for job in running_jobs]


def get_current_package():
    result = {}
    server = get_jenkins_server()

    endpoints_config = server.get_view_config(configs.package_view_name)
    description_match = re.match(".*<description>(.*)</description>.*", endpoints_config, re.DOTALL)

    if description_match:
        description = description_match.group(1)

        for version_line in re.finditer("\n\s*(.*?)\s*:\s*([\d\w-]+?)\s&lt;", description):
            project_name = version_line.group(1)
            project_version = version_line.group(2)

            result[project_name] = project_version

    return result


def update_current_package(name_versions):
    versions = get_current_package()

    versions.update(name_versions)

    new_description = "<description>&lt;h3&gt;&#xd;\n{}\n&lt;/h3&gt;</description>".format(
        "\n".join(["{} : {} &lt;br/&gt;&#xd;".format(name, versions[name]) for name in sorted(list(versions.keys()))]))

    server = get_jenkins_server()
    endpoints_config = server.get_view_config(configs.package_view_name)
    description_match = re.match(".*(<description>.*</description>).*", endpoints_config, re.DOTALL)

    if description_match:
        description = description_match.group(1)

        endpoints_config = endpoints_config.replace(description, new_description)

    server.reconfig_view(configs.package_view_name, endpoints_config)


def update_last_stable_link(name, version):
    with lcd(join(configs.sb_folder, name)):
        local("rm -f {}".format(configs.last_stable_build_link_name))
        local("ln -sf {} {}".format(version, configs.last_stable_build_link_name))


def get_last_stable_link_path(name):
    return join(configs.sb_folder, name, configs.last_stable_build_link_name)


def update_current_package_based_on_last_stable_links():
    last_stable_links = {}

    for endpoint in configs.endpoints:
        with suppress(Exception):
            last_stable_links[endpoint] = get_last_stable_version_from_link(endpoint)

    update_current_package(last_stable_links)


def get_job_workspace_path(job_name):
    return join(configs.jenkins_home, "workspace", job_name)
