from os.path import join

import instance_configs

jenkins_home = "/j"
user_content_folder = join(jenkins_home, "userContent")
deploy_reports_folder = join(user_content_folder, "DeployReports")
scripts_home = "/scripts"
temp_folder = "/temp"
configs_folder = "/configs"
sb_folder = "/stable-builds"
seleniums_folder = "/seleniums"
test_screenshot_folder = "test_screenshots"
maven_local_repository = join(jenkins_home, ".m2", "repository")
internal_lib_prefixes = ["ir."]

busy_agents_file_path = join(jenkins_home, "busy_agents")
selenium_agent_config_folder = join(configs_folder, "selenium-agent")
run_selenium_python_name = "selenium.py"
run_selenium_python_path = join(scripts_home, "Test", run_selenium_python_name)
selenium_agent_image_name = "selenium-agent"
max_concurrent_test_threads = 12
jenkins_test_jobs_name_prefix = " Test"

cluster_file_name = "cluster.zip"
issue_report_path = join(user_content_folder, "issue_reports")
cluster_folder = "/configs/cluster"
docker_hub_url = "hub.docker.iais.co:5000"
docker_hub_certificate_path = "/opt/tomcat/certs/domain.crt"

maven_resource_folder = "src/main/resources"

last_stable_build_link_name = "last-stable-build"
package_view_name = 'Current Package'

cluster_hashes = {
    "113": 11,
    "116": 12,
    "farzad": 13,
    "12": 14,
    "36": 15,
    "hamedh": 16,
    "vahid": 17,
    "said": 19,
    "afshar": 20,
    "shayan": 21,
    "hoseini": 22,
    "10": 23,
    "ghadiri-pc": 24,
    "98": 25,
    "13": 26,
    "iranrd": 27,
    "164": 28
}
main_cluster_name = "116"
allowed_cluster_index = [0, 1, 2]
default_target_cluster = "13"
test_max_try_count = 1
non_interactive_tests = ["barname_part1", "barname_part2", "tafkik", "print_without_anbar", "enterpriseBapli",
                         "varedat", "kartabl_saderat_new", "sazman_hamjavar", "tajmi_barname", "havale",
                         "ghabzDastiBaBarname","mojavez_tranzit","ecl"]

jenkins_clusters = {
    "113": "172.16.111.113",
    "116": "172.16.111.116",
    "12": "172.16.111.12",
    "36": "172.16.111.36",
    "10": "172.16.111.10",
    "shayan": "172.17.193.251",
    "13": "172.16.111.13"
}

jenkins_address = "127.0.0.1:8080"
jenkins_url = "http://{}".format(jenkins_address)

jenkins_name = instance_configs.jenkins_name
jenkins_id = instance_configs.jenkins_id
jenkins_url_outside = instance_configs.jenkins_url_outside

apache_docker_folder = "/configs/apache"

ci_info_file_name = "ci_info"
current_ci_state = "3bc4be75922575c6dbd59b1195a67be27b52bb84"

time_monitor_projects = ['Customs']
test_generating_projects = ['Customs', 'PackingList']
aut_projects = time_monitor_projects + test_generating_projects

endpoints_git = {
    'PackingList': 'https://bitbucket.iais.co/scm/cus/ios.git',
    'JBazbini': 'https://git.iais.co/scm/nez/bazbini.git',
    'Customs': 'https://bitbucket.iais.co/scm/cus/customs-main.git',
    'Statistics': 'https://bitbucket.iais.co/scm/cus/statistics.git',
    'Merchant': 'https://git.iais.co/scm/runp/merchant-good.git',
    'Nezarat': 'https://git.iais.co/scm/nez/nezarat.git',
    'Mis': 'https://git.iais.co/scm/mis/mis.git',
    'Coding': 'https://git.iais.co/scm/cod/coding-project.git',
    'Bazbini': 'https://git.iais.co/scm/bazb/bazbini.git',
    'Bazaar': 'https://bitbucket.iais.co/scm/cus/bazaar.git',
    'Khoone': 'https://bitbucket.iais.co/scm/door/khoone.git',
    'Acc': 'https://git.iais.co/scm/acc/acc-local.git',
    'Police': 'https://git.iais.co/scm/pol/policeserver.git',
    'Anbar': 'https://git.iais.co/scm/an/warehouse.git',
}
endpoints_svn = {
    'Baskool': 'http://svn.iais.ir/customs-baskool/Baskool',
    'Center': 'http://svn.iais.ir/center-customs-main/center-customs-main',
    'Tir': 'http://svn.iais.ir/center-customs-iru/center-customs-iru',
    'Center-Swich': 'http://svn.iais.ir/center-customs-swich/center-customs-swich',
    'Nazer': 'http://svn.iais.ir/customs-nazer/Nazer',
    'Swich': 'http://svn.iais.ir/customs-swich/Swich',
}
libraries_git = {
    'AUTCore': 'https://git.iais.co/scm/tdr/autcore.git',
    'SRM-Mali': 'https://git.iais.co/scm/srm/srm-mali.git',
    'G-Utils': 'https://git.iais.co/scm/gut/g-utils.git',
    'AUTGenerator': 'https://git.iais.co/scm/tdr/autgenerator.git',
    'SRM-SDR': 'https://git.iais.co/scm/srm/srm-sdr.git',
    'SRM-Admin': 'https://git.iais.co/scm/srm/srm-admin.git',
    'SalonGomrok': 'https://bitbucket.iais.co/scm/cus/salon.git',
    'Ramtin': 'https://bitbucket.iais.co/scm/cus/ramtin.git',
    'Nezbazlib': 'https://git.iais.co/scm/nez/nezbazlib.git',
    'Rulemanager': 'https://bitbucket.iais.co/scm/cus/rule-manager.git',
    'SessionCheck': 'https://git.iais.co/scm/in/ghalatgir.git',
    'SRM-Student': 'https://git.iais.co/scm/srm/srm-student.git',
    'HN-Utils': 'https://git.iais.co/scm/gut/hn-utils.git',
    'SRM-Services': 'https://git.iais.co/scm/srm/srm-services.git',
    'UI-Utils': 'https://bitbucket.iais.co/scm/cus/ui-utils.git',
}
libraries_svn = {
    'Swich-Lib': 'http://svn.iais.ir/swich-lib/swich-lib',
    'Calendar': 'http://svn.iais.ir/calendar/calendar',
    'Realtime-Safe-TIR': 'http://svn.iais.ir/center-customs-iru-realtime-safetir/realtime-safe-tir',
    'OCM': 'http://svn.iais.ir/ios/OCM',
    'Shared': 'http://svn.iais.ir/customs-shared/customs-shared',
    'DB-Utils': 'http://svn.iais.ir/db-utils/DBUtils',
    'Center-Lib': 'http://svn.iais.ir/center-lib/center-lib',
    'Connections': 'http://svn.iais.ir/connections/connections',
    'Default-Pom': 'http://svn.iais.ir/default-pom/default-pom',
    'Cryptography': 'http://svn.iais.ir/cryptography',
    'Lazy-Connections': 'http://svn.iais.ir/lazy-connections/lazy-connections',
    'ServiceProvider': 'http://svn.iais.ir/ios/ServiceProvider',
    'Center-Customs-IRU-TIR-EPD': 'http://svn.iais.ir/center-customs-iru-tir-epd/center-customs-iru-tir-epd',
}

endpoints = endpoints_svn.copy()
endpoints.update(endpoints_git)
libraries = libraries_svn.copy()
libraries.update(libraries_git)

maven_libraris = ['AUTCore', 'G-Utils', 'AUTGenerator', 'SalonGomrok', 'Ramtin', 'Nezbazlib', 'Rulemanager',
                  'SessionCheck', 'HN-Utils', 'Swich-Lib', 'Calendar', 'Realtime-Safe-TIR', 'OCM', 'Shared',
                  'DB-Utils', 'UI-Utils', 'Center-Lib', 'Connections', 'Default-Pom', 'Cryptography',
                  'Lazy-Connections', 'ServiceProvider', 'Center-Customs-IRU-TIR-EPD']
maven_endpoints = ["Nezarat", "Customs", "Baskool", "Swich", "Center", "Acc", "PackingList", "Tir",
                   "customs-reporter-server", "Guard", "Nazer", "Center-Swich", "Nazer", "Statistics", "JBazbini",
                   "Bazaar"]
maven_projects = maven_endpoints + maven_libraris

php_projects = ["Mis", "Bazbini", "Tina", "Coding", "srm-student", "srm-admin", "srm-bank", "srm-mali", "srm-sdr",
                "srm-services", "Coding-services", "Merchant", "SRM"]
nodejs_projects = ["Khoone"]
asp_projects = ["Anbar", "Reporter"]
SRM_childs = {"srm-admin": "admin", "srm-mali": "mali", "srm-sdr": "sdr", "srm-bank": "bank",
              "srm-services": "services", "srm-student": "student"}

dockerized_apps = maven_endpoints + ["Apache", "Anbar"]

ignore_branch_check = ["G-Utils", "HN-Utils", "Calendar", "ServiceProvider", "Rulemanager"]

#
# Deploy
#
general_method_deploy = ["Tir", "Acc", "Statistics", "Customs"]
non_pilot_customs = ["50100", "45100", "45110", "10300", "30100", "50200", "10103", "40400", "40202"]
confidence_interval = 12  # hour
deploy_superusers = ["mohammad ali tavallaie", "hamidreza ghasemi", "Hamed Hasani", "saeed", "Meysam Mirzaie",
                     "Mostafa Farsi", "Hamed Babaei", "Abouzar Kamaee", "amin",
                     "pouyan momeni", "saeed arash", "mohammad hassan kazemi"]
excluded_projects = ["tir", "Center", "Mis", "Coding", "Acc"]

arrangement_projects = ["Customs", "Baskool", "Swich", "Acc"]

build_date_postfix = {
    "Anbar": "UI/buildDate.aspx",
    "Acc": "rest/build/buildDate",
    "Apache": "",
}

deploy_audiences = [
    "09394760654",  # hasani
    "09365007447",  # babaei
    "09361855091",  # farsi
    "09212834421",  # ghasemi
    "09193240483",  # saeedi
    "09376307633",  # hasanKazemi
]

deploy_audiences_for_project = {
    "PackingList": ["09186093821"],  # said
    "Anbar": ["09301756825", "09360110145", "09367198181"],  # kamaei, sina, afshar
    "SRM": ["09370102416"],  # morteza
    "Mis": ["09370102416"],  # morteza
    "Bazbini": ["09370102416", "09376682416"]  # morteza, darioush
}

pip = ["termcolor", "python-dateutil", "jenkinsapi", "pysocks", 'jira', 'python-telegram-bot']

authorized_keys_filename = "authorized_keys"

exclude_from_code_analyse = ["Acc", "Center", "Center-lib", "Nazer", "Center-Swich"]

jenkins_user = "scripter"
jenkins_password = "1qaz2wsx"
jenkins_api_token = "0a283fb4a30934b25cd1da295d6e4d91"

deploy_jenkins_address = "172.17.20.32"
deploy_jenkins_user = "newjenkins"
deploy_jenkins_api_token = "da00ed9b7af94a088b637d9c312e68c7"

alaki = False

clusters = {
    "general": {
        "Customs": {
            "Rajae": "Rajae@172.16.111.113:8080/Customs-Rajae",
            "Bazargan": "Bazargan@172.16.111.113:8080/Customs-Bazargan",
            "Tehran": "Tehran@172.16.111.113:8080/Customs-Tehran",
        },

        "Swich": {
            "Rajae": "Rajae@172.16.111.113:8080/Swich-Rajae",
            "Bazargan": "Bazargan@172.16.111.113:8080/Swich-Bazargan",
            "Tehran": "Tehran@172.16.111.113:8080/Swich-Tehran",
        },

        "Baskool": {
            "Rajae": "Rajae@172.16.111.113:8080/Baskool-Rajae",
            "Bazargan": "Bazargan@172.16.111.113:8080/Baskool-Bazargan",
            "Tehran": "Tehran@172.16.111.113:8080/Baskool-Tehran",
        },

        "Acc": {
            "Rajae": "Rajae@dockerTest@172.16.111.113:8080/Acc-Rajae",
            "Tax": "Tax@taxtest@172.16.111.113:8080/Acc-Tax",
        },

        "Anbar": {
            "Rajae": "Rajae@172.16.111.113:8080/Anbar-Rajae",
            "Bazargan": "Bazargan@172.16.111.113:8080/Anbar-Bazargan",
            "Tehran": "Tehran@172.16.111.113:8080/Anbar-Tehran",
        },

        "Center": {
            "Rajae": "Rajae@172.16.111.113:8080/Center-Rajae",
        },

        "Center-Swich": {
            "Rajae": "Rajae@172.16.111.113:8080/Center-Swich-Rajae",
        },

        "PackingList": {
            "Rajae": "Rajae@172.16.111.113:8080/PackingList-Rajae",
        },

        "Mis": {
            "jenkins": "114@jenkins@172.16.111.114/mis-test2/site",
        },

        "Bazbini": {
            "jenkins": "114@jenkins@172.16.111.114/bazbini",
        },

        "Apache": {
            "Rajae": "Rajae@172.16.111.113:8080"
        },

        "Tir": {
            "Rajae": "Rajae@172.16.111.113:8080/Tir-Rajae"
        },
        #
        # "Nezarat": {
        #     "Rajae": "Rajae@172.16.111.113:8080/Nezarat-Rajae"
        # },

        "JBazbini": {
            "Rajae": "Rajae@172.16.111.113:8080/JBazbini-Rajae"
        },
    },

    "Khoone": {
        "Khoone": {
            "193138": "193136@172.17.193.138",
        },
    },

    "Statistics": {
        "Statistics": {
            "113": "113@172.16.111.113:8080/Statistics",
        },
    },

    "tax": {
        "Acc": {
            "Tax": "Tax@taxtest@172.16.111.113:8080/Acc-Tax",
        },
    }
}

swarm_mode = {}

no_build_date = ["Tir-Rajae", "Acc-Tax", "Nezarat-Rajae", "JBazbini-Rajae", "Police-Rajae"]
