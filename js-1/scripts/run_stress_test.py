from pydoc import locate
import sys

from StressTest.StressTestRunner import StressTestRunner
from Utils.Utils import error

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print(error("provide a class name like 'StressTest.Varedat.Test'"))
        exit(1)

    test_class = locate(sys.argv[1])

    agent = StressTestRunner(test_class)
    agent.run(1)
