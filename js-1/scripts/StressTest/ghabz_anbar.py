# -*- coding: utf-8 -*-
from StressTest.StressTest import StressTest


class Test(StressTest):
    def __init__(self, index):
        super().__init__(index)
        self.selenium_var("notlogined2", "true")
        self.selenium_var("PortTe", "1022")
        self.selenium_var("not", "false")
        self.selenium_var("ghabzNo", "142968")
        self.selenium_var("barnameNum", "1470569099159")
        self.selenium_var("baygani", "60902")
        self.selenium_var("PortRa", "1020")
        self.selenium_var("PortBa", "1021")
        self.selenium_var("tozin1", "1470569099368")

    def setGeneralPort(self):
        self.selenium_run("storeEval",
                          "\"8080\"",
                          "generalPort")
        self.selenium_var("generalPort", "8080")

    def setUploadPath(self):
        self.selenium_run("storeEval",
                          "\"C:\\\\Test\\\\upload_Files\\\\\"",
                          "uploadPath")

    def PortTe(self):
        self.selenium_run("storeEval",
                          "\"1022\"",
                          "PortTe")
        self.selenium_var("PortTe", "1022")


    def PortRa(self):
        self.selenium_run("storeEval",
                          "\"1020\"",
                          "PortRa")
        self.selenium_var("PortRa", "1020")


    def PortBa(self):
        self.selenium_run("storeEval",
                          "\"1021\"",
                          "PortBa")
        self.selenium_var("PortBa", "1021")


    def LoginAnbar8093(self):
        self.selenium_run("setSpeed",
                          "2000",
                          "")

        self.selenium_run("open",
                          "http://172.16.111.51:${PortBa}",
                          "Anbar")

        self.send(
            b'GET http://172.16.111.51:1021/ HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Account/Login.aspx\r\nConnection: keep-alive\r\n\r\n')

        self.selenium_run("waitForText",
                          "id=LoginContent_Label3",
                          "برای ورود به سیستم مدیریت انبار نام کاربری و شناسه عبور را وارد      .")

        self.send(
            b'GET http://172.16.111.51:1021/UI/Account/Login.aspx?ReturnUrl=%2f HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Account/Login.aspx\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/BotDetectCaptcha.ashx?get=image&c=c_ui_account_login_logincontent_captchabox&t=5ea8f9529bc448c0a03e63fcb64fe113 HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: */*\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Account/Login.aspx?ReturnUrl=%2f\r\nCookie: ASP.NET_SessionId=1qn2lkuoprhyoiruhgc4f1gz\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/Styles/Images/Public/bg/1.jpg HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: */*\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Account/Login.aspx?ReturnUrl=%2f\r\nCookie: ASP.NET_SessionId=1qn2lkuoprhyoiruhgc4f1gz\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Account/Login.aspx?ReturnUrl=%2fStyles%2fImages%2fPublic%2fbg%2f1.jpg HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: */*\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Account/Login.aspx?ReturnUrl=%2f\r\nCookie: ASP.NET_SessionId=1qn2lkuoprhyoiruhgc4f1gz\r\nConnection: keep-alive\r\n\r\n')

        self.selenium_run("setSpeed",
                          "10",
                          "")

        self.selenium_run("type",
                          "css=input[name='ctl00$LoginContent$txtUsername']",
                          "afish")

        self.selenium_run("type",
                          "css=input[name='ctl00$LoginContent$txtPassword']",
                          "3521584")

        self.selenium_run("type",
                          "css=input[name='ctl00$LoginContent$txtCaptcha']",
                          "TEST")

        self.selenium_run("clickAndWait",
                          "id=btnSubmit",
                          "")

        self.send(
            b'POST http://172.16.111.51:1021/UI/Account/Login.aspx?ReturnUrl=%2f HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Account/Login.aspx?ReturnUrl=%2f\r\nCookie: ASP.NET_SessionId=1qn2lkuoprhyoiruhgc4f1gz\r\nConnection: keep-alive\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: 944\r\n\r\n__VIEWSTATE=%2FwEPDwUKLTM4NTU1NjYwNQ8WAh4TVmFsaWRhdGVSZXF1ZXN0TW9kZQIBFgJmD2QWAgIBD2QWBAIBDw8WAh4HVmlzaWJsZWhkFgRmDw9kFgIeBXN0eWxlBTliYWNrZ3JvdW5kLXJlcGVhdC14Om5vLXJlcGVhdDtiYWNrZ3JvdW5kLXNpemU6MTAwJSAxNDZweDsWAmYPDxYCHgRUZXh0BTXYs9in2YXYp9mG2Ycg2YXYr9uM2LHbjNiqINin2YbYqNin2LHZh9in24wg2LnZhdmI2YXbjGRkAgQPZBYEAgEPDxYCHwMFBjMuMTQ5NmRkAgMPDxYCHwMFCjEzOTUvMDUvMTZkZAIDD2QWAgIBD2QWAgIRD2QWBAIBDw8WAh8DBQYzLjE0OTZkZAIDDw8WAh8DBQoxMzk1LzA1LzE2ZGQYAQUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgEFHmN0bDAwJExvZ2luQ29udGVudCRjaGtSZW1lbWJlcg%3D%3D&__EVENTTARGET=&__EVENTARGUMENT=&ctl00%24LoginContent%24txtUsername=afish&ctl00%24LoginContent%24txtPassword=3521584&LBD_VCID_c_ui_account_login_logincontent_captchabox=5ea8f9529bc448c0a03e63fcb64fe113&LBD_BackWorkaround_c_ui_account_login_logincontent_captchabox=1&ctl00%24LoginContent%24txtCaptcha=TEST&ctl00%24LoginContent%24btnSubmit=%D9%88%D8%B1%D9%88%D8%AF+%D8%A8%D9%87+%D8%B3%DB%8C%D8%B3%D8%AA%D9%85')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Default.aspx HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Account/Login.aspx?ReturnUrl=%2f\r\nCookie: ASP.NET_SessionId=1qn2lkuoprhyoiruhgc4f1gz; .ASPXAUTH=AF5FDA4285E6BA05C5A0C0BA0CC2B9AED9C6FF2848A13F619C24333DF5509DA55E250A049CA03EC4D1EA2B0033B2017BD6D2F260D80962B1C8F15D091E66C5AC0B7567979A59A3A8434BD9421B29F50457D85D69F3C065806257A33EB4B059E8043488DC1E6CD87D4E94D6FEDFEA327F11ECF73599E820D123349125DAF203CE\r\nConnection: keep-alive\r\n\r\n')

        self.selenium_run("waitForText",
                          "css=ol.breadcrumb > li > a",
                          "خانه")

    def qabzDasti(self):
        self.selenium_run("clickAndWait",
                          "link=صدور قبض انبار دستی",
                          "")

        self.send(
            b'GET http://172.16.111.51:1021/UI/WarehouseBill/CreateManualWarehouseBill.aspx HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Default.aspx\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.selenium_run("waitForElementPresent",
                          "id=txtWaybill",
                          "")

        self.selenium_run("storeEval",
                          "Date.now()",
                          "barnameNum")
        self.selenium_var("barnameNum", "1470569916384")

        self.selenium_run("type",
                          "id=txtWaybill",
                          "${barnameNum}")

        self.selenium_run("storeEval",
                          "60902",
                          "baygani")
        self.selenium_var("baygani", "60902")

        self.selenium_run("type",
                          "id=txtEnterNumber",
                          "${baygani}")

        self.selenium_run("type",
                          "id=txtCompany",
                          "تولایی-0081674902")

        self.selenium_run("type",
                          "id=txtOwner",
                          "تولایی-0081674902")

        self.selenium_run("type",
                          "id=txtCompanyAgent",
                          "تولایی-0081674902")

        self.selenium_run("type",
                          "id=MainContent_InnerMainContent_uWarehousebillDetails_txtEvacuationCosts",
                          "0")

        self.selenium_run("storeEval",
                          "Date.now()",
                          "tozin1")
        self.selenium_var("tozin1", "1470569916536")

        self.selenium_run("type",
                          "id=txtWarehouseBillNumber",
                          "1394000${tozin1}")

        self.selenium_run("type",
                          "id=txtWarehouseNumber3",
                          "1")

        self.selenium_run("type",
                          "id=txtWarehouseNumber2",
                          "2")

        self.selenium_run("type",
                          "id=txtWarehouseNumber1",
                          "3")

        self.selenium_run("clickAt",
                          "id=select2-selectStocks-container",
                          "")

        self.selenium_run("type",
                          "css=input.select2-search__field",
                          "مرکزی")

        self.selenium_run("sendKeys",
                          "css=input.select2-search__field",
                          "${KEY_ENTER}")

        self.selenium_run("clickAndWait",
                          "id=MainContent_InnerMainContent_btnNext",
                          "مرحله بعد")

        self.send(
            b'POST http://172.16.111.51:1021/UI/WarehouseBill/CreateManualWarehouseBill.aspx HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/WarehouseBill/CreateManualWarehouseBill.aspx\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: 3681\r\n\r\n__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwUJMTU4MTY0NTE3DxYCHhNWYWxpZGF0ZVJlcXVlc3RNb2RlAgEWAmYPZBYCZg9kFgICAQ9kFgICAQ9kFgZmDw9kFgIeBXN0eWxlBTliYWNrZ3JvdW5kLXJlcGVhdC14Om5vLXJlcGVhdDtiYWNrZ3JvdW5kLXNpemU6MTAwJSAxNDZweDsWAmYPDxYCHgRUZXh0BTXYs9in2YXYp9mG2Ycg2YXYr9uM2LHbjNiqINin2YbYqNin2LHZh9in24wg2LnZhdmI2YXbjGRkAgMPZBYEAgEPZBYCZg9kFgICAQ8UKwACDxYEHgtfIURhdGFCb3VuZGceC18hSXRlbUNvdW50AgNkZBYCZg9kFgICAQ8WAh4KZGF0YS1zdGVwcwUBMxYGAgEPZBYCAgEPFgIeBWNsYXNzBQVkb2luZxYCZg8VAzAvVUkvV2FyZWhvdXNlQmlsbC9DcmVhdGVNYW51YWxXYXJlaG91c2VCaWxsLmFzcHgrL1N0eWxlcy9JbWFnZXMvbWluaW1hbGlzdGljYS8zMngzMi9ob21lLnBuZyDYp9i32YTYp9i52KfYqiDZgtio2LYg2KfZhtio2KfYsWQCAg9kFgICAQ9kFgJmDxUDKC9VSS9XYXJlaG91c2VCaWxsL0NyZWF0ZU1hbnVhbFRhYWxpLmFzcHgsL1N0eWxlcy9JbWFnZXMvbWluaW1hbGlzdGljYS8zMngzMi9hcHBsZS5wbmcX2KfYt9mE2KfYudin2Kog2qnYp9mE2KdkAgMPZBYCAgEPZBYCZg8VAycvVUkvV2FyZWhvdXNlQmlsbC9DcmVhdGVNYW51YWxUcmlwLmFzcHgsL1N0eWxlcy9JbWFnZXMvbWluaW1hbGlzdGljYS8zMngzMi90cnVjay5wbmcX2KfYt9mE2KfYudin2Kog2K3Yp9mF2YRkAgMPZBYCAgMPZBYEAgEPDxYCHgdWaXNpYmxlZ2RkAgIPZBYEAgsPDxYCHwdnZBYCAgEPZBYCAgEPEA8WCh4ORGF0YVZhbHVlRmllbGQFAklEHg1EYXRhVGV4dEZpZWxkBQlTdG9ja05hbWUeDEF1dG9Qb3N0QmFja2gfA2ceDVNlbGVjdGlvbk1vZGULKitTeXN0ZW0uV2ViLlVJLldlYkNvbnRyb2xzLkxpc3RTZWxlY3Rpb25Nb2RlAGQQFQEK2YXYsdqp2LLbjBUBATEUKwMBZxYAZAIND2QWBAIBDw8WAh8HZ2RkAgMPDxYCHwdnZGQCBA9kFgQCAQ8PFgIfAgUGMy4xNDk2ZGQCAw8PFgIfAgUKMTM5NS8wNS8xNmRkGAIFHl9fQ29udHJvbHNSZXF1aXJlUG9zdEJhY2tLZXlfXxYCBSFjdGwwMCRjdGwwMCRIZWFkTG9naW5TdGF0dXMkY3RsMDEFIWN0bDAwJGN0bDAwJEhlYWRMb2dpblN0YXR1cyRjdGwwMwVBY3RsMDAkY3RsMDAkTWFpbkNvbnRlbnQkdVN0ZXBJbmRpY2F0b3IkU3RlcEluZGljYXRvcl9MaXN0Vmlld1N0ZXAPFCsADmRkZGRkZGQUKwADZGRkAgNkZGRmAv%2F%2F%2F%2F8PZA%3D%3D&ctl00%24ctl00%24MainContent%24InnerMainContent%24uWarehousebillDetails%24txtWaybill=1470569916384&ctl00%24ctl00%24MainContent%24InnerMainContent%24uWarehousebillDetails%24txtEnterNumber=60902&ctl00%24ctl00%24MainContent%24InnerMainContent%24uWarehousebillDetails%24txtCompany=%D8%AA%D9%88%D9%84%D8%A7%DB%8C%DB%8C-0081674902&ctl00%24ctl00%24MainContent%24InnerMainContent%24uWarehousebillDetails%24txtOwner=%D8%AA%D9%88%D9%84%D8%A7%DB%8C%DB%8C-0081674902&ctl00%24ctl00%24MainContent%24InnerMainContent%24uWarehousebillDetails%24txtCompanyAgent=%D8%AA%D9%88%D9%84%D8%A7%DB%8C%DB%8C-0081674902&ctl00%24ctl00%24MainContent%24InnerMainContent%24uWarehousebillDetails%24txtEvacuationCosts=0&ctl00%24ctl00%24MainContent%24InnerMainContent%24uWarehousebillDetails%24txtWarehouseBillNumber=13940001470569916536&ctl00%24ctl00%24MainContent%24InnerMainContent%24uWarehousebillDetails%24txtWarehouseNumber1=3&ctl00%24ctl00%24MainContent%24InnerMainContent%24uWarehousebillDetails%24txtWarehouseNumber2=2&ctl00%24ctl00%24MainContent%24InnerMainContent%24uWarehousebillDetails%24txtWarehouseNumber3=1&ctl00%24ctl00%24MainContent%24InnerMainContent%24uWarehousebillDetails%24DropDownWarehouseTypes=+%D9%85%D8%B3%D9%82%D9%81+&ctl00%24ctl00%24MainContent%24InnerMainContent%24uWarehousebillDetails%24USelectStock%24selectStocks=1&ctl00%24ctl00%24MainContent%24InnerMainContent%24uWarehousebillDetails%24txtBurucraticCode=&ctl00%24ctl00%24MainContent%24InnerMainContent%24uWarehousebillDetails%24txtdesctiption=&ctl00%24ctl00%24MainContent%24InnerMainContent%24btnNext=%D9%85%D8%B1%D8%AD%D9%84%D9%87+%D8%A8%D8%B9%D8%AF&ctl00%24ctl00%24MainContent%24InnerMainContent%24uWarehousebillDetails%24uPersonDetails%24txtPersonName=&ctl00%24ctl00%24MainContent%24InnerMainContent%24uWarehousebillDetails%24uPersonDetails%24txtPersonCode=&ctl00%24ctl00%24MainContent%24InnerMainContent%24uWarehousebillDetails%24uPersonDetails%24txtCompanyName=&ctl00%24ctl00%24MainContent%24InnerMainContent%24uWarehousebillDetails%24uPersonDetails%24txtCompanyCode=')

        self.send(
            b'GET http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTaali.aspx HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/WarehouseBill/CreateManualWarehouseBill.aspx\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')


    def commodityInsert(self):
        self.selenium_run("setSpeed",
                          "100",
                          "")

        self.selenium_run("type",
                          "id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityName",
                          "میوه")

        self.selenium_run("type",
                          "id=MainContent_InnerMainContent_uTaaliTransferInsert_txtHsCode",
                          "21050000")

        self.selenium_run("type",
                          "id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityUnit",
                          "کارتن")

        self.selenium_run("type",
                          "id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityNumber",
                          "150")

        self.selenium_run("type",
                          "id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityWeight",
                          "۲۰۰۰")

        self.selenium_run("click",
                          "id=MainContent_InnerMainContent_uTaaliTransferInsert_ImageButtonAdd",
                          "")

        self.send(
            b'POST http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTaali.aspx HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nX-Requested-With: XMLHttpRequest\r\nX-MicrosoftAjax: Delta=true\r\nCache-Control: no-cache\r\nContent-Type: application/x-www-form-urlencoded; charset=utf-8\r\nReferer: http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTaali.aspx\r\nContent-Length: 2412\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\nctl00%24ctl00%24MainContent%24InnerMainContent%24ctl00=ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24ctl01%7Cctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24ImageButtonAdd&__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwUKMTkxNzUxNDI3Mg8WAh4TVmFsaWRhdGVSZXF1ZXN0TW9kZQIBFgJmD2QWAmYPZBYCAgEPZBYCAgEPZBYGZg8PZBYCHgVzdHlsZQU5YmFja2dyb3VuZC1yZXBlYXQteDpuby1yZXBlYXQ7YmFja2dyb3VuZC1zaXplOjEwMCUgMTQ2cHg7FgJmDw8WAh4EVGV4dAU12LPYp9mF2KfZhtmHINmF2K%2FbjNix24zYqiDYp9mG2KjYp9ix2YfYp9uMINi52YXZiNmF24xkZAIDD2QWAgIBD2QWAmYPZBYCAgEPFCsAAg8WBB4LXyFEYXRhQm91bmRnHgtfIUl0ZW1Db3VudAIDZGQWAmYPZBYCAgEPFgIeCmRhdGEtc3RlcHMFATMWBgIBD2QWAgIBDxYCHgVjbGFzcwUEZG9uZRYCZg8VAzAvVUkvV2FyZWhvdXNlQmlsbC9DcmVhdGVNYW51YWxXYXJlaG91c2VCaWxsLmFzcHgrL1N0eWxlcy9JbWFnZXMvbWluaW1hbGlzdGljYS8zMngzMi9ob21lLnBuZyDYp9i32YTYp9i52KfYqiDZgtio2LYg2KfZhtio2KfYsWQCAg9kFgICAQ8WAh8GBQVkb2luZxYCZg8VAygvVUkvV2FyZWhvdXNlQmlsbC9DcmVhdGVNYW51YWxUYWFsaS5hc3B4LC9TdHlsZXMvSW1hZ2VzL21pbmltYWxpc3RpY2EvMzJ4MzIvYXBwbGUucG5nF9in2LfZhNin2LnYp9iqINqp2KfZhNinZAIDD2QWAgIBD2QWAmYPFQMnL1VJL1dhcmVob3VzZUJpbGwvQ3JlYXRlTWFudWFsVHJpcC5hc3B4LC9TdHlsZXMvSW1hZ2VzL21pbmltYWxpc3RpY2EvMzJ4MzIvdHJ1Y2sucG5nF9in2LfZhNin2LnYp9iqINit2KfZhdmEZAIED2QWBAIBDw8WAh8CBQYzLjE0OTZkZAIDDw8WAh8CBQoxMzk1LzA1LzE2ZGQYAgUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgMFIWN0bDAwJGN0bDAwJEhlYWRMb2dpblN0YXR1cyRjdGwwMQUhY3RsMDAkY3RsMDAkSGVhZExvZ2luU3RhdHVzJGN0bDAzBUxjdGwwMCRjdGwwMCRNYWluQ29udGVudCRJbm5lck1haW5Db250ZW50JHVUYWFsaVRyYW5zZmVySW5zZXJ0JEltYWdlQnV0dG9uQWRkBUFjdGwwMCRjdGwwMCRNYWluQ29udGVudCR1U3RlcEluZGljYXRvciRTdGVwSW5kaWNhdG9yX0xpc3RWaWV3U3RlcA8UKwAOZGRkZGRkZBQrAANkZGQCA2RkZGYC%2F%2F%2F%2F%2Fw9k&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtCommodityName=%D9%85%DB%8C%D9%88%D9%87&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtHsCode=21050000&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtCommodityUnit=%DA%A9%D8%A7%D8%B1%D8%AA%D9%86&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtCommodityNumber=150&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtCommodityWeight=%DB%B2%DB%B0%DB%B0%DB%B0&__ASYNCPOST=true&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24ImageButtonAdd.x=-63&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24ImageButtonAdd.y=-441')

        self.selenium_run("waitForElementPresent",
                          "name=ctl00$ctl00$MainContent$InnerMainContent$uTaaliTransferInsert$ctl05",
                          "")

        self.selenium_run("type",
                          "id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityName",
                          "اگر این کالا را می بینید یعنی انبار مشکل دارد")

        self.selenium_run("type",
                          "id=MainContent_InnerMainContent_uTaaliTransferInsert_txtHsCode",
                          "باید حذف شده باشد")

        self.selenium_run("type",
                          "id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityUnit",
                          "غلط")

        self.selenium_run("type",
                          "id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityNumber",
                          "1230")

        self.selenium_run("type",
                          "id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityWeight",
                          "2345")

        self.selenium_run("click",
                          "id=MainContent_InnerMainContent_uTaaliTransferInsert_ImageButtonAdd",
                          "")

        self.send(
            b'POST http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTaali.aspx HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nX-Requested-With: XMLHttpRequest\r\nX-MicrosoftAjax: Delta=true\r\nCache-Control: no-cache\r\nContent-Type: application/x-www-form-urlencoded; charset=utf-8\r\nReferer: http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTaali.aspx\r\nContent-Length: 2777\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\nctl00%24ctl00%24MainContent%24InnerMainContent%24ctl00=ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24ctl01%7Cctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24ImageButtonAdd&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtCommodityName=%D8%A7%DA%AF%D8%B1%20%D8%A7%DB%8C%D9%86%20%DA%A9%D8%A7%D9%84%D8%A7%20%D8%B1%D8%A7%20%D9%85%DB%8C%20%D8%A8%DB%8C%D9%86%DB%8C%D8%AF%20%DB%8C%D8%B9%D9%86%DB%8C%20%D8%A7%D9%86%D8%A8%D8%A7%D8%B1%20%D9%85%D8%B4%DA%A9%D9%84%20%D8%AF%D8%A7%D8%B1%D8%AF&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtHsCode=%D8%A8%D8%A7%DB%8C%D8%AF%20%D8%AD%D8%B0%D9%81%20%D8%B4%D8%AF%D9%87%20%D8%A8%D8%A7%D8%B4%D8%AF&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtCommodityUnit=%D8%BA%D9%84%D8%B7&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtCommodityNumber=1230&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtCommodityWeight=2345&__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwUKMTkxNzUxNDI3Mg8WAh4TVmFsaWRhdGVSZXF1ZXN0TW9kZQIBFgJmD2QWAmYPZBYCAgEPZBYCAgEPZBYGZg8PZBYCHgVzdHlsZQU5YmFja2dyb3VuZC1yZXBlYXQteDpuby1yZXBlYXQ7YmFja2dyb3VuZC1zaXplOjEwMCUgMTQ2cHg7FgJmDw8WAh4EVGV4dAU12LPYp9mF2KfZhtmHINmF2K%2FbjNix24zYqiDYp9mG2KjYp9ix2YfYp9uMINi52YXZiNmF24xkZAIDD2QWAgIBD2QWAmYPZBYCAgEPFCsAAg8WBB4LXyFEYXRhQm91bmRnHgtfIUl0ZW1Db3VudAIDZGQWAmYPZBYCAgEPFgIeCmRhdGEtc3RlcHMFATMWBgIBD2QWAgIBDxYCHgVjbGFzcwUEZG9uZRYCZg8VAzAvVUkvV2FyZWhvdXNlQmlsbC9DcmVhdGVNYW51YWxXYXJlaG91c2VCaWxsLmFzcHgrL1N0eWxlcy9JbWFnZXMvbWluaW1hbGlzdGljYS8zMngzMi9ob21lLnBuZyDYp9i32YTYp9i52KfYqiDZgtio2LYg2KfZhtio2KfYsWQCAg9kFgICAQ8WAh8GBQVkb2luZxYCZg8VAygvVUkvV2FyZWhvdXNlQmlsbC9DcmVhdGVNYW51YWxUYWFsaS5hc3B4LC9TdHlsZXMvSW1hZ2VzL21pbmltYWxpc3RpY2EvMzJ4MzIvYXBwbGUucG5nF9in2LfZhNin2LnYp9iqINqp2KfZhNinZAIDD2QWAgIBD2QWAmYPFQMnL1VJL1dhcmVob3VzZUJpbGwvQ3JlYXRlTWFudWFsVHJpcC5hc3B4LC9TdHlsZXMvSW1hZ2VzL21pbmltYWxpc3RpY2EvMzJ4MzIvdHJ1Y2sucG5nF9in2LfZhNin2LnYp9iqINit2KfZhdmEZAIED2QWBAIBDw8WAh8CBQYzLjE0OTZkZAIDDw8WAh8CBQoxMzk1LzA1LzE2ZGQYAgUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgQFIWN0bDAwJGN0bDAwJEhlYWRMb2dpblN0YXR1cyRjdGwwMQUhY3RsMDAkY3RsMDAkSGVhZExvZ2luU3RhdHVzJGN0bDAzBUNjdGwwMCRjdGwwMCRNYWluQ29udGVudCRJbm5lck1haW5Db250ZW50JHVUYWFsaVRyYW5zZmVySW5zZXJ0JGN0bDA1BUxjdGwwMCRjdGwwMCRNYWluQ29udGVudCRJbm5lck1haW5Db250ZW50JHVUYWFsaVRyYW5zZmVySW5zZXJ0JEltYWdlQnV0dG9uQWRkBUFjdGwwMCRjdGwwMCRNYWluQ29udGVudCR1U3RlcEluZGljYXRvciRTdGVwSW5kaWNhdG9yX0xpc3RWaWV3U3RlcA8UKwAOZGRkZGRkZBQrAANkZGQCA2RkZGYC%2F%2F%2F%2F%2Fw9k&__ASYNCPOST=true&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24ImageButtonAdd.x=-63&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24ImageButtonAdd.y=-491')

        self.selenium_run("waitForElementPresent",
                          "name=ctl00$ctl00$MainContent$InnerMainContent$uTaaliTransferInsert$ctl06",
                          "")

        self.selenium_run("type",
                          "id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityName",
                          "چوب")

        self.selenium_run("type",
                          "id=MainContent_InnerMainContent_uTaaliTransferInsert_txtHsCode",
                          "72189900")

        self.selenium_run("type",
                          "id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityUnit",
                          "نگله")

        self.selenium_run("type",
                          "id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityNumber",
                          "150")

        self.selenium_run("type",
                          "id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityWeight",
                          "3600")

        self.selenium_run("click",
                          "id=MainContent_InnerMainContent_uTaaliTransferInsert_ImageButtonAdd",
                          "")

        self.send(
            b'POST http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTaali.aspx HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nX-Requested-With: XMLHttpRequest\r\nX-MicrosoftAjax: Delta=true\r\nCache-Control: no-cache\r\nContent-Type: application/x-www-form-urlencoded; charset=utf-8\r\nReferer: http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTaali.aspx\r\nContent-Length: 2564\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\nctl00%24ctl00%24MainContent%24InnerMainContent%24ctl00=ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24ctl01%7Cctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24ImageButtonAdd&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtCommodityName=%DA%86%D9%88%D8%A8&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtHsCode=72189900&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtCommodityUnit=%D9%86%DA%AF%D9%84%D9%87&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtCommodityNumber=150&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtCommodityWeight=3600&__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwUKMTkxNzUxNDI3Mg8WAh4TVmFsaWRhdGVSZXF1ZXN0TW9kZQIBFgJmD2QWAmYPZBYCAgEPZBYCAgEPZBYGZg8PZBYCHgVzdHlsZQU5YmFja2dyb3VuZC1yZXBlYXQteDpuby1yZXBlYXQ7YmFja2dyb3VuZC1zaXplOjEwMCUgMTQ2cHg7FgJmDw8WAh4EVGV4dAU12LPYp9mF2KfZhtmHINmF2K%2FbjNix24zYqiDYp9mG2KjYp9ix2YfYp9uMINi52YXZiNmF24xkZAIDD2QWAgIBD2QWAmYPZBYCAgEPFCsAAg8WBB4LXyFEYXRhQm91bmRnHgtfIUl0ZW1Db3VudAIDZGQWAmYPZBYCAgEPFgIeCmRhdGEtc3RlcHMFATMWBgIBD2QWAgIBDxYCHgVjbGFzcwUEZG9uZRYCZg8VAzAvVUkvV2FyZWhvdXNlQmlsbC9DcmVhdGVNYW51YWxXYXJlaG91c2VCaWxsLmFzcHgrL1N0eWxlcy9JbWFnZXMvbWluaW1hbGlzdGljYS8zMngzMi9ob21lLnBuZyDYp9i32YTYp9i52KfYqiDZgtio2LYg2KfZhtio2KfYsWQCAg9kFgICAQ8WAh8GBQVkb2luZxYCZg8VAygvVUkvV2FyZWhvdXNlQmlsbC9DcmVhdGVNYW51YWxUYWFsaS5hc3B4LC9TdHlsZXMvSW1hZ2VzL21pbmltYWxpc3RpY2EvMzJ4MzIvYXBwbGUucG5nF9in2LfZhNin2LnYp9iqINqp2KfZhNinZAIDD2QWAgIBD2QWAmYPFQMnL1VJL1dhcmVob3VzZUJpbGwvQ3JlYXRlTWFudWFsVHJpcC5hc3B4LC9TdHlsZXMvSW1hZ2VzL21pbmltYWxpc3RpY2EvMzJ4MzIvdHJ1Y2sucG5nF9in2LfZhNin2LnYp9iqINit2KfZhdmEZAIED2QWBAIBDw8WAh8CBQYzLjE0OTZkZAIDDw8WAh8CBQoxMzk1LzA1LzE2ZGQYAgUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgUFIWN0bDAwJGN0bDAwJEhlYWRMb2dpblN0YXR1cyRjdGwwMQUhY3RsMDAkY3RsMDAkSGVhZExvZ2luU3RhdHVzJGN0bDAzBUNjdGwwMCRjdGwwMCRNYWluQ29udGVudCRJbm5lck1haW5Db250ZW50JHVUYWFsaVRyYW5zZmVySW5zZXJ0JGN0bDA1BUNjdGwwMCRjdGwwMCRNYWluQ29udGVudCRJbm5lck1haW5Db250ZW50JHVUYWFsaVRyYW5zZmVySW5zZXJ0JGN0bDA2BUxjdGwwMCRjdGwwMCRNYWluQ29udGVudCRJbm5lck1haW5Db250ZW50JHVUYWFsaVRyYW5zZmVySW5zZXJ0JEltYWdlQnV0dG9uQWRkBUFjdGwwMCRjdGwwMCRNYWluQ29udGVudCR1U3RlcEluZGljYXRvciRTdGVwSW5kaWNhdG9yX0xpc3RWaWV3U3RlcA8UKwAOZGRkZGRkZBQrAANkZGQCA2RkZGYC%2F%2F%2F%2F%2Fw9k&__ASYNCPOST=true&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24ImageButtonAdd.x=-61&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24ImageButtonAdd.y=-558')

        self.selenium_run("waitForElementPresent",
                          "name=ctl00$ctl00$MainContent$InnerMainContent$uTaaliTransferInsert$ctl07",
                          "")

        self.selenium_run("click",
                          "name=ctl00$ctl00$MainContent$InnerMainContent$uTaaliTransferInsert$ctl06",
                          "")

        self.send(
            b'POST http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTaali.aspx HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nX-Requested-With: XMLHttpRequest\r\nX-MicrosoftAjax: Delta=true\r\nCache-Control: no-cache\r\nContent-Type: application/x-www-form-urlencoded; charset=utf-8\r\nReferer: http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTaali.aspx\r\nContent-Length: 2572\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\nctl00%24ctl00%24MainContent%24InnerMainContent%24ctl00=ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24ctl01%7Cctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24ctl06&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtCommodityName=&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtHsCode=&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtCommodityUnit=&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtCommodityNumber=&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtCommodityWeight=&__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwUKMTkxNzUxNDI3Mg8WAh4TVmFsaWRhdGVSZXF1ZXN0TW9kZQIBFgJmD2QWAmYPZBYCAgEPZBYCAgEPZBYGZg8PZBYCHgVzdHlsZQU5YmFja2dyb3VuZC1yZXBlYXQteDpuby1yZXBlYXQ7YmFja2dyb3VuZC1zaXplOjEwMCUgMTQ2cHg7FgJmDw8WAh4EVGV4dAU12LPYp9mF2KfZhtmHINmF2K%2FbjNix24zYqiDYp9mG2KjYp9ix2YfYp9uMINi52YXZiNmF24xkZAIDD2QWAgIBD2QWAmYPZBYCAgEPFCsAAg8WBB4LXyFEYXRhQm91bmRnHgtfIUl0ZW1Db3VudAIDZGQWAmYPZBYCAgEPFgIeCmRhdGEtc3RlcHMFATMWBgIBD2QWAgIBDxYCHgVjbGFzcwUEZG9uZRYCZg8VAzAvVUkvV2FyZWhvdXNlQmlsbC9DcmVhdGVNYW51YWxXYXJlaG91c2VCaWxsLmFzcHgrL1N0eWxlcy9JbWFnZXMvbWluaW1hbGlzdGljYS8zMngzMi9ob21lLnBuZyDYp9i32YTYp9i52KfYqiDZgtio2LYg2KfZhtio2KfYsWQCAg9kFgICAQ8WAh8GBQVkb2luZxYCZg8VAygvVUkvV2FyZWhvdXNlQmlsbC9DcmVhdGVNYW51YWxUYWFsaS5hc3B4LC9TdHlsZXMvSW1hZ2VzL21pbmltYWxpc3RpY2EvMzJ4MzIvYXBwbGUucG5nF9in2LfZhNin2LnYp9iqINqp2KfZhNinZAIDD2QWAgIBD2QWAmYPFQMnL1VJL1dhcmVob3VzZUJpbGwvQ3JlYXRlTWFudWFsVHJpcC5hc3B4LC9TdHlsZXMvSW1hZ2VzL21pbmltYWxpc3RpY2EvMzJ4MzIvdHJ1Y2sucG5nF9in2LfZhNin2LnYp9iqINit2KfZhdmEZAIED2QWBAIBDw8WAh8CBQYzLjE0OTZkZAIDDw8WAh8CBQoxMzk1LzA1LzE2ZGQYAgUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgYFIWN0bDAwJGN0bDAwJEhlYWRMb2dpblN0YXR1cyRjdGwwMQUhY3RsMDAkY3RsMDAkSGVhZExvZ2luU3RhdHVzJGN0bDAzBUNjdGwwMCRjdGwwMCRNYWluQ29udGVudCRJbm5lck1haW5Db250ZW50JHVUYWFsaVRyYW5zZmVySW5zZXJ0JGN0bDA1BUNjdGwwMCRjdGwwMCRNYWluQ29udGVudCRJbm5lck1haW5Db250ZW50JHVUYWFsaVRyYW5zZmVySW5zZXJ0JGN0bDA2BUNjdGwwMCRjdGwwMCRNYWluQ29udGVudCRJbm5lck1haW5Db250ZW50JHVUYWFsaVRyYW5zZmVySW5zZXJ0JGN0bDA3BUxjdGwwMCRjdGwwMCRNYWluQ29udGVudCRJbm5lck1haW5Db250ZW50JHVUYWFsaVRyYW5zZmVySW5zZXJ0JEltYWdlQnV0dG9uQWRkBUFjdGwwMCRjdGwwMCRNYWluQ29udGVudCR1U3RlcEluZGljYXRvciRTdGVwSW5kaWNhdG9yX0xpc3RWaWV3U3RlcA8UKwAOZGRkZGRkZBQrAANkZGQCA2RkZGYC%2F%2F%2F%2F%2Fw9k&__ASYNCPOST=true&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24ctl06.x=-64&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24ctl06.y=-499')

        self.selenium_run("waitForNotText",
                          "//table[@id='MainContent_InnerMainContent_uTaaliTransferInsert_TableCommodity']/tbody/tr[3]/td",
                          "اگر این کالا را می بینید یعنی انبار مشکل دارد")

        self.selenium_run("waitForElementPresent",
                          "id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityName",
                          "")

        self.selenium_run("type",
                          "id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityName",
                          "اهن")

        self.selenium_run("type",
                          "id=MainContent_InnerMainContent_uTaaliTransferInsert_txtHsCode",
                          "21050000")

        self.selenium_run("type",
                          "id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityUnit",
                          "کارتن")

        self.selenium_run("type",
                          "id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityNumber",
                          "150")

        self.selenium_run("type",
                          "id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityWeight",
                          "30000")

        self.selenium_run("click",
                          "id=MainContent_InnerMainContent_uTaaliTransferInsert_ImageButtonAdd",
                          "")

        self.selenium_run("waitForElementPresent",
                          "id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityName",
                          "")
        self.send(
            b'POST http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTaali.aspx HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nX-Requested-With: XMLHttpRequest\r\nX-MicrosoftAjax: Delta=true\r\nCache-Control: no-cache\r\nContent-Type: application/x-www-form-urlencoded; charset=utf-8\r\nReferer: http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTaali.aspx\r\nContent-Length: 2571\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\nctl00%24ctl00%24MainContent%24InnerMainContent%24ctl00=ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24ctl01%7Cctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24ImageButtonAdd&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtCommodityName=%D8%A7%D9%87%D9%86&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtHsCode=21050000&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtCommodityUnit=%DA%A9%D8%A7%D8%B1%D8%AA%D9%86&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtCommodityNumber=150&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtCommodityWeight=30000&__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwUKMTkxNzUxNDI3Mg8WAh4TVmFsaWRhdGVSZXF1ZXN0TW9kZQIBFgJmD2QWAmYPZBYCAgEPZBYCAgEPZBYGZg8PZBYCHgVzdHlsZQU5YmFja2dyb3VuZC1yZXBlYXQteDpuby1yZXBlYXQ7YmFja2dyb3VuZC1zaXplOjEwMCUgMTQ2cHg7FgJmDw8WAh4EVGV4dAU12LPYp9mF2KfZhtmHINmF2K%2FbjNix24zYqiDYp9mG2KjYp9ix2YfYp9uMINi52YXZiNmF24xkZAIDD2QWAgIBD2QWAmYPZBYCAgEPFCsAAg8WBB4LXyFEYXRhQm91bmRnHgtfIUl0ZW1Db3VudAIDZGQWAmYPZBYCAgEPFgIeCmRhdGEtc3RlcHMFATMWBgIBD2QWAgIBDxYCHgVjbGFzcwUEZG9uZRYCZg8VAzAvVUkvV2FyZWhvdXNlQmlsbC9DcmVhdGVNYW51YWxXYXJlaG91c2VCaWxsLmFzcHgrL1N0eWxlcy9JbWFnZXMvbWluaW1hbGlzdGljYS8zMngzMi9ob21lLnBuZyDYp9i32YTYp9i52KfYqiDZgtio2LYg2KfZhtio2KfYsWQCAg9kFgICAQ8WAh8GBQVkb2luZxYCZg8VAygvVUkvV2FyZWhvdXNlQmlsbC9DcmVhdGVNYW51YWxUYWFsaS5hc3B4LC9TdHlsZXMvSW1hZ2VzL21pbmltYWxpc3RpY2EvMzJ4MzIvYXBwbGUucG5nF9in2LfZhNin2LnYp9iqINqp2KfZhNinZAIDD2QWAgIBD2QWAmYPFQMnL1VJL1dhcmVob3VzZUJpbGwvQ3JlYXRlTWFudWFsVHJpcC5hc3B4LC9TdHlsZXMvSW1hZ2VzL21pbmltYWxpc3RpY2EvMzJ4MzIvdHJ1Y2sucG5nF9in2LfZhNin2LnYp9iqINit2KfZhdmEZAIED2QWBAIBDw8WAh8CBQYzLjE0OTZkZAIDDw8WAh8CBQoxMzk1LzA1LzE2ZGQYAgUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgUFIWN0bDAwJGN0bDAwJEhlYWRMb2dpblN0YXR1cyRjdGwwMQUhY3RsMDAkY3RsMDAkSGVhZExvZ2luU3RhdHVzJGN0bDAzBUNjdGwwMCRjdGwwMCRNYWluQ29udGVudCRJbm5lck1haW5Db250ZW50JHVUYWFsaVRyYW5zZmVySW5zZXJ0JGN0bDA1BUNjdGwwMCRjdGwwMCRNYWluQ29udGVudCRJbm5lck1haW5Db250ZW50JHVUYWFsaVRyYW5zZmVySW5zZXJ0JGN0bDA3BUxjdGwwMCRjdGwwMCRNYWluQ29udGVudCRJbm5lck1haW5Db250ZW50JHVUYWFsaVRyYW5zZmVySW5zZXJ0JEltYWdlQnV0dG9uQWRkBUFjdGwwMCRjdGwwMCRNYWluQ29udGVudCR1U3RlcEluZGljYXRvciRTdGVwSW5kaWNhdG9yX0xpc3RWaWV3U3RlcA8UKwAOZGRkZGRkZBQrAANkZGQCA2RkZGYC%2F%2F%2F%2F%2Fw9k&__ASYNCPOST=true&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24ImageButtonAdd.x=-63&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24ImageButtonAdd.y=-541')

        self.selenium_run("click",
                          "id=MainContent_InnerMainContent_uTaaliTransferInsert_btnSubmit",
                          "")

        self.send(
            b'POST http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTaali.aspx HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTaali.aspx\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: 2317\r\n\r\nctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtCommodityName=&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtHsCode=&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtCommodityUnit=&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtCommodityNumber=&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24txtCommodityWeight=&ctl00%24ctl00%24MainContent%24InnerMainContent%24uTaaliTransferInsert%24btnSubmit=%D8%AB%D8%A8%D8%AA+%D8%A7%D8%B7%D9%84%D8%A7%D8%B9%D8%A7%D8%AA&__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwUKMTkxNzUxNDI3Mg8WAh4TVmFsaWRhdGVSZXF1ZXN0TW9kZQIBFgJmD2QWAmYPZBYCAgEPZBYCAgEPZBYGZg8PZBYCHgVzdHlsZQU5YmFja2dyb3VuZC1yZXBlYXQteDpuby1yZXBlYXQ7YmFja2dyb3VuZC1zaXplOjEwMCUgMTQ2cHg7FgJmDw8WAh4EVGV4dAU12LPYp9mF2KfZhtmHINmF2K%2FbjNix24zYqiDYp9mG2KjYp9ix2YfYp9uMINi52YXZiNmF24xkZAIDD2QWAgIBD2QWAmYPZBYCAgEPFCsAAg8WBB4LXyFEYXRhQm91bmRnHgtfIUl0ZW1Db3VudAIDZGQWAmYPZBYCAgEPFgIeCmRhdGEtc3RlcHMFATMWBgIBD2QWAgIBDxYCHgVjbGFzcwUEZG9uZRYCZg8VAzAvVUkvV2FyZWhvdXNlQmlsbC9DcmVhdGVNYW51YWxXYXJlaG91c2VCaWxsLmFzcHgrL1N0eWxlcy9JbWFnZXMvbWluaW1hbGlzdGljYS8zMngzMi9ob21lLnBuZyDYp9i32YTYp9i52KfYqiDZgtio2LYg2KfZhtio2KfYsWQCAg9kFgICAQ8WAh8GBQVkb2luZxYCZg8VAygvVUkvV2FyZWhvdXNlQmlsbC9DcmVhdGVNYW51YWxUYWFsaS5hc3B4LC9TdHlsZXMvSW1hZ2VzL21pbmltYWxpc3RpY2EvMzJ4MzIvYXBwbGUucG5nF9in2LfZhNin2LnYp9iqINqp2KfZhNinZAIDD2QWAgIBD2QWAmYPFQMnL1VJL1dhcmVob3VzZUJpbGwvQ3JlYXRlTWFudWFsVHJpcC5hc3B4LC9TdHlsZXMvSW1hZ2VzL21pbmltYWxpc3RpY2EvMzJ4MzIvdHJ1Y2sucG5nF9in2LfZhNin2LnYp9iqINit2KfZhdmEZAIED2QWBAIBDw8WAh8CBQYzLjE0OTZkZAIDDw8WAh8CBQoxMzk1LzA1LzE2ZGQYAgUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgYFIWN0bDAwJGN0bDAwJEhlYWRMb2dpblN0YXR1cyRjdGwwMQUhY3RsMDAkY3RsMDAkSGVhZExvZ2luU3RhdHVzJGN0bDAzBUNjdGwwMCRjdGwwMCRNYWluQ29udGVudCRJbm5lck1haW5Db250ZW50JHVUYWFsaVRyYW5zZmVySW5zZXJ0JGN0bDA1BUNjdGwwMCRjdGwwMCRNYWluQ29udGVudCRJbm5lck1haW5Db250ZW50JHVUYWFsaVRyYW5zZmVySW5zZXJ0JGN0bDA2BUNjdGwwMCRjdGwwMCRNYWluQ29udGVudCRJbm5lck1haW5Db250ZW50JHVUYWFsaVRyYW5zZmVySW5zZXJ0JGN0bDA3BUxjdGwwMCRjdGwwMCRNYWluQ29udGVudCRJbm5lck1haW5Db250ZW50JHVUYWFsaVRyYW5zZmVySW5zZXJ0JEltYWdlQnV0dG9uQWRkBUFjdGwwMCRjdGwwMCRNYWluQ29udGVudCR1U3RlcEluZGljYXRvciRTdGVwSW5kaWNhdG9yX0xpc3RWaWV3U3RlcA8UKwAOZGRkZGRkZBQrAANkZGQCA2RkZGYC%2F%2F%2F%2F%2Fw9k')

        self.selenium_run("waitForElementPresent",
                          "id=MainContent_InnerMainContent_btnAddTaaliTransfer",
                          "")

        self.send(
            b'GET http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTrip.aspx HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTaali.aspx\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')


    def sabteTaliDasti(self):
        self.selenium_run("click",
                          "id=MainContent_InnerMainContent_btnAddTaaliTransfer",
                          "")

        self.send(
            b'POST http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTrip.aspx HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTrip.aspx\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: 1680\r\n\r\n__EVENTTARGET=ctl00%24ctl00%24MainContent%24InnerMainContent%24btnAddTaaliTransfer&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwUKMTY4Njk5MzY5MA8WAh4TVmFsaWRhdGVSZXF1ZXN0TW9kZQIBFgJmD2QWAmYPZBYCAgEPZBYCAgEPZBYGZg8PZBYCHgVzdHlsZQU5YmFja2dyb3VuZC1yZXBlYXQteDpuby1yZXBlYXQ7YmFja2dyb3VuZC1zaXplOjEwMCUgMTQ2cHg7FgJmDw8WAh4EVGV4dAU12LPYp9mF2KfZhtmHINmF2K%2FbjNix24zYqiDYp9mG2KjYp9ix2YfYp9uMINi52YXZiNmF24xkZAIDD2QWBAIBD2QWAmYPZBYCAgEPFCsAAg8WBB4LXyFEYXRhQm91bmRnHgtfIUl0ZW1Db3VudAIDZGQWAmYPZBYCAgEPFgIeCmRhdGEtc3RlcHMFATMWBgIBD2QWAgIBDxYCHgVjbGFzcwUEZG9uZRYCZg8VAzAvVUkvV2FyZWhvdXNlQmlsbC9DcmVhdGVNYW51YWxXYXJlaG91c2VCaWxsLmFzcHgrL1N0eWxlcy9JbWFnZXMvbWluaW1hbGlzdGljYS8zMngzMi9ob21lLnBuZyDYp9i32YTYp9i52KfYqiDZgtio2LYg2KfZhtio2KfYsWQCAg9kFgICAQ8WAh8GBQRkb25lFgJmDxUDKC9VSS9XYXJlaG91c2VCaWxsL0NyZWF0ZU1hbnVhbFRhYWxpLmFzcHgsL1N0eWxlcy9JbWFnZXMvbWluaW1hbGlzdGljYS8zMngzMi9hcHBsZS5wbmcX2KfYt9mE2KfYudin2Kog2qnYp9mE2KdkAgMPZBYCAgEPFgIfBgUFZG9pbmcWAmYPFQMnL1VJL1dhcmVob3VzZUJpbGwvQ3JlYXRlTWFudWFsVHJpcC5hc3B4LC9TdHlsZXMvSW1hZ2VzL21pbmltYWxpc3RpY2EvMzJ4MzIvdHJ1Y2sucG5nF9in2LfZhNin2LnYp9iqINit2KfZhdmEZAIDD2QWAgIFDzwrABEDAA8WBB8DZx8EZmQBEBYAFgAWAAwUKwAAZAIED2QWBAIBDw8WAh8CBQYzLjE0OTZkZAIDDw8WAh8CBQoxMzk1LzA1LzE2ZGQYBAUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgIFIWN0bDAwJGN0bDAwJEhlYWRMb2dpblN0YXR1cyRjdGwwMQUhY3RsMDAkY3RsMDAkSGVhZExvZ2luU3RhdHVzJGN0bDAzBTdjdGwwMCRjdGwwMCRNYWluQ29udGVudCRJbm5lck1haW5Db250ZW50JEdyaWR2aWV3VGFhbGlzDzwrAAwBCGZkBUZjdGwwMCRjdGwwMCRNYWluQ29udGVudCRJbm5lck1haW5Db250ZW50JEVudGl0eURhdGFTb3VyY2VUYWFsaVRyYW5zZmVyDzwrAAkBAQ9nZGQFQWN0bDAwJGN0bDAwJE1haW5Db250ZW50JHVTdGVwSW5kaWNhdG9yJFN0ZXBJbmRpY2F0b3JfTGlzdFZpZXdTdGVwDxQrAA5kZGRkZGRkFCsAA2RkZAIDZGRkZgL%2F%2F%2F%2F%2FD2Q%3D')

        self.selenium_run("waitForElementPresent",
                          "id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_txtContainerName",
                          "")

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14428&rid=2 HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTrip.aspx\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Scripts/others/jquery-ui-1.9.1.custom.js HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: */*\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14428&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Scripts/others/jquery.ui.datepicker-cc-fa.js HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: */*\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14428&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Styles/css/jquery-ui-1.9.1.custom.css HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/css,*/*;q=0.1\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14428&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Scripts/others/jquery.ui.datepicker-cc.js HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: */*\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14428&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Styles/css/jquery-ui-1.9.1-fa.css HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/css,*/*;q=0.1\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14428&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Scripts/others/jquery-ui-1.9.1.custom.js HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: */*\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14428&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Scripts/others/jquery.ui.datepicker-cc.js HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: */*\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14428&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Scripts/others/jquery.ui.datepicker-cc-fa.js HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: */*\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14428&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Styles/css/jquery-ui-1.9.1.custom.css HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/css,*/*;q=0.1\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14428&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Styles/css/jquery-ui-1.9.1-fa.css HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/css,*/*;q=0.1\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14428&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.selenium_run("type",
                          "id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_txtContainerName",
                          "IRSU1234-5")

        self.selenium_run("type",
                          "id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_GridViewCommodityPermit_TaaliTransfer_txtCommodityPermitTaaliTransfer_CommodityNumber_0",
                          "50")

        self.selenium_run("type",
                          "id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_GridViewCommodityPermit_TaaliTransfer_txtCommodityPermitTaaliTransfer_CommodityNumber_1",
                          "50")

        self.selenium_run("waitForElementPresent",
                          "id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_GridViewCommodityPermit_TaaliTransfer_txtCommodityPermitTaaliTransfer_CommodityNumber_2",
                          "50")

        self.selenium_run("type",
                          "id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_GridViewCommodityPermit_TaaliTransfer_txtCommodityPermitTaaliTransfer_CommodityNumber_2",
                          "50")

        self.selenium_run("click",
                          "id=MainContent_uTaaliTransfer_btnPrint",
                          "")

        self.send(
            b'POST http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14428&rid=2 HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14428&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: 3190\r\n\r\n__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwULLTE0NTUwMzQwOTQPFgIeE1ZhbGlkYXRlUmVxdWVzdE1vZGUCARYCZg9kFgICAQ9kFgICAQ9kFgZmDw9kFgIeBXN0eWxlBTliYWNrZ3JvdW5kLXJlcGVhdC14Om5vLXJlcGVhdDtiYWNrZ3JvdW5kLXNpemU6MTAwJSAxNDZweDsWAmYPDxYCHgRUZXh0BTXYs9in2YXYp9mG2Ycg2YXYr9uM2LHbjNiqINin2YbYqNin2LHZh9in24wg2LnZhdmI2YXbjGRkAgMPZBYCAgEPZBYCZg9kFgoCAw8PFgIfAgUR2YHYp9mC2K8g2K3Yp9mF2YRkZAIFDw8WAh8CBRHZgdin2YLYryDYrdin2YXZhGRkAgcPZBYYAgEPDxYCHwIFCUFOIzE0Mjk2OWRkAgMPDxYCHwIFFDEzOTQwMDAxNDcwNTY5OTE2NTM2ZGQCBQ8PFgIfAgUM2KrZiNmE2KfbjNuMZGQCBw8PFgIfAgUM2KrZiNmE2KfbjNuMZGQCCQ8PFgIfAgUKMDA4MTY3NDkwMmRkAgsPDxYCHwIFCjEzOTUvMDUvMTdkZAINDw8WAh8CBQzYqtmI2YTYp9uM24xkZAIPDw8WAh8CBQowMDgxNjc0OTAyZGQCEQ8PFgIfAgUBMGRkAhMPDxYCHwIFATBkZAIVDw8WAh8CBQEwZGQCFw8PFgIfAgUBMGRkAgkPZBYGZg8PFgIeB1Zpc2libGVoZGQCAQ8PFgQfAgUFMTQ0MjgfA2hkZAIKDzwrABEDAA8WBB4LXyFEYXRhQm91bmRnHgtfIUl0ZW1Db3VudAIDZAEQFgYCBAIFAgYCBwIIAgkWBjwrAAUBABYCHwNnPCsABQEAFgIfA2c8KwAFAQAWAh8DZzwrAAUBABYCHwNnPCsABQEAFgIfA2g8KwAFAQAWAh8DaBYGAgYCBgIGAgYCBgIGDBQrAAAWAmYPZBYIAgEPZBYUZg8PFgIfAgUFMTQwMzdkZAIBDw8WAh8CBQbahtmI2KhkZAICDw8WAh8CBQjZhtqv2YTZh2RkAgMPDxYCHwIFCDcyMTg5OTAwZGQCBA9kFgICAQ8PFgIfAgUDMTUwZGQCBQ9kFgICAQ8PFgIfAgUBMBYCHhFkYXRhLWRlZmF1bHR2YWx1ZQUBMGQCBg9kFgICAQ8PFgIfAgUDMTUwFgIfBgUDMTUwZAIHD2QWAgIBDw9kFgIfBgUBMGQCCA9kFgICAQ8PFgIfAgUEMzYwMGRkAgkPZBYCAgEPDxYCHwIFATBkZAICD2QWFGYPDxYCHwIFBTE0MDM1ZGQCAQ8PFgIfAgUI2YXbjNmI2YdkZAICDw8WAh8CBQraqdin2LHYqtmGZGQCAw8PFgIfAgUIMjEwNTAwMDBkZAIED2QWAgIBDw8WAh8CBQMxNTBkZAIFD2QWAgIBDw8WAh8CBQEwFgIfBgUBMGQCBg9kFgICAQ8PFgIfAgUDMTUwFgIfBgUDMTUwZAIHD2QWAgIBDw9kFgIfBgUBMGQCCA9kFgICAQ8PFgIfAgUEMjAwMGRkAgkPZBYCAgEPDxYCHwIFATBkZAIDD2QWFGYPDxYCHwIFBTE0MDM2ZGQCAQ8PFgIfAgUG2KfZh9mGZGQCAg8PFgIfAgUK2qnYp9ix2KrZhmRkAgMPDxYCHwIFCDIxMDUwMDAwZGQCBA9kFgICAQ8PFgIfAgUDMTUwZGQCBQ9kFgICAQ8PFgIfAgUBMBYCHwYFATBkAgYPZBYCAgEPDxYCHwIFAzE1MBYCHwYFAzE1MGQCBw9kFgICAQ8PZBYCHwYFATBkAggPZBYCAgEPDxYCHwIFBTMwMDAwZGQCCQ9kFgICAQ8PFgIfAgUBMGRkAgQPDxYCHwNoZGQCCw9kFgICAw8PFgIfA2dkZAIED2QWBAIBDw8WAh8CBQYzLjE0OTZkZAIDDw8WAh8CBQoxMzk1LzA1LzE2ZGQYAgUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgIFG2N0bDAwJEhlYWRMb2dpblN0YXR1cyRjdGwwMQUbY3RsMDAkSGVhZExvZ2luU3RhdHVzJGN0bDAzBWRjdGwwMCRNYWluQ29udGVudCR1VGFhbGlUcmFuc2ZlciRVQ29tbW9kaXR5UGVybWl0VGFhbGlUcmFuc2ZlciRHcmlkVmlld0NvbW1vZGl0eVBlcm1pdF9UYWFsaVRyYW5zZmVyDzwrAAwBCAIBZA%3D%3D&ctl00%24MainContent%24uTaaliTransfer%24UCommodityPermitTaaliTransfer%24txtDischargeDate=&ctl00%24MainContent%24uTaaliTransfer%24UCommodityPermitTaaliTransfer%24txtContainerName=IRSU1234-5&ctl00%24MainContent%24uTaaliTransfer%24UCommodityPermitTaaliTransfer%24txtPaymentTariffCode=&ctl00%24MainContent%24uTaaliTransfer%24UCommodityPermitTaaliTransfer%24GridViewCommodityPermit_TaaliTransfer%24ctl02%24txtCommodityPermitTaaliTransfer_CommodityNumber=50&ctl00%24MainContent%24uTaaliTransfer%24UCommodityPermitTaaliTransfer%24GridViewCommodityPermit_TaaliTransfer%24ctl03%24txtCommodityPermitTaaliTransfer_CommodityNumber=50&ctl00%24MainContent%24uTaaliTransfer%24UCommodityPermitTaaliTransfer%24GridViewCommodityPermit_TaaliTransfer%24ctl04%24txtCommodityPermitTaaliTransfer_CommodityNumber=50&ctl00%24MainContent%24uTaaliTransfer%24btnPrint=%D8%AB%D8%A8%D8%AA+%D9%88%D8%B1%D9%88%D8%AF+%DA%A9%D8%A7%D9%84%D8%A7')

        self.selenium_run("waitForElementPresent",
                          "id=MainContent_InnerMainContent_btnAddTaaliTransfer",
                          "")

        self.send(
            b'GET http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTrip.aspx HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14428&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.selenium_run("click",
                          "id=MainContent_InnerMainContent_btnAddTaaliTransfer",
                          "")

        self.send(
            b'POST http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTrip.aspx HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTrip.aspx\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: 2216\r\n\r\n__EVENTTARGET=ctl00%24ctl00%24MainContent%24InnerMainContent%24btnAddTaaliTransfer&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwUKMTY4Njk5MzY5MA8WAh4TVmFsaWRhdGVSZXF1ZXN0TW9kZQIBFgJmD2QWAmYPZBYCAgEPZBYCAgEPZBYGZg8PZBYCHgVzdHlsZQU5YmFja2dyb3VuZC1yZXBlYXQteDpuby1yZXBlYXQ7YmFja2dyb3VuZC1zaXplOjEwMCUgMTQ2cHg7FgJmDw8WAh4EVGV4dAU12LPYp9mF2KfZhtmHINmF2K%2FbjNix24zYqiDYp9mG2KjYp9ix2YfYp9uMINi52YXZiNmF24xkZAIDD2QWBAIBD2QWAmYPZBYCAgEPFCsAAg8WBB4LXyFEYXRhQm91bmRnHgtfIUl0ZW1Db3VudAIDZGQWAmYPZBYCAgEPFgIeCmRhdGEtc3RlcHMFATMWBgIBD2QWAgIBDxYCHgVjbGFzcwUEZG9uZRYCZg8VAzAvVUkvV2FyZWhvdXNlQmlsbC9DcmVhdGVNYW51YWxXYXJlaG91c2VCaWxsLmFzcHgrL1N0eWxlcy9JbWFnZXMvbWluaW1hbGlzdGljYS8zMngzMi9ob21lLnBuZyDYp9i32YTYp9i52KfYqiDZgtio2LYg2KfZhtio2KfYsWQCAg9kFgICAQ8WAh8GBQRkb25lFgJmDxUDKC9VSS9XYXJlaG91c2VCaWxsL0NyZWF0ZU1hbnVhbFRhYWxpLmFzcHgsL1N0eWxlcy9JbWFnZXMvbWluaW1hbGlzdGljYS8zMngzMi9hcHBsZS5wbmcX2KfYt9mE2KfYudin2Kog2qnYp9mE2KdkAgMPZBYCAgEPFgIfBgUFZG9pbmcWAmYPFQMnL1VJL1dhcmVob3VzZUJpbGwvQ3JlYXRlTWFudWFsVHJpcC5hc3B4LC9TdHlsZXMvSW1hZ2VzL21pbmltYWxpc3RpY2EvMzJ4MzIvdHJ1Y2sucG5nF9in2LfZhNin2LnYp9iqINit2KfZhdmEZAIDD2QWAgIFDzwrABEDAA8WBB8DZx8EAgFkARAWABYAFgAMFCsAABYCZg9kFgYCAQ9kFgxmD2QWAgIBDw8WAh4PQ29tbWFuZEFyZ3VtZW50BQUxNDQyOGRkAgEPZBYCAgEPZBYCZg8VAQUxNDQyOGQCAg9kFgJmDxUBVNmC2KjYtiDYp9mG2KjYp9ixINmF2LHYqNmI2Lcg2KjZhyDYp9uM2YYg2KrYp9mE24wg2KvYqNiqINmG2YfYp9uM24wg2YbYtNiv2Ycg2KfYs9iqLmQCAw9kFgJmDxUBATNkAgQPZBYCZg8VAQMxNTBkAgUPZBYCAgEPDxYCHwcFBTE0NDI4ZGQCAg8PFgIeB1Zpc2libGVoZGQCAw8PFgIfCGhkZAIED2QWBAIBDw8WAh8CBQYzLjE0OTZkZAIDDw8WAh8CBQoxMzk1LzA1LzE2ZGQYBAUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgQFIWN0bDAwJGN0bDAwJEhlYWRMb2dpblN0YXR1cyRjdGwwMQUhY3RsMDAkY3RsMDAkSGVhZExvZ2luU3RhdHVzJGN0bDAzBU9jdGwwMCRjdGwwMCRNYWluQ29udGVudCRJbm5lck1haW5Db250ZW50JEdyaWR2aWV3VGFhbGlzJGN0bDAyJEltYWdlQnV0dG9uRGVsZXRlBUVjdGwwMCRjdGwwMCRNYWluQ29udGVudCRJbm5lck1haW5Db250ZW50JEdyaWR2aWV3VGFhbGlzJGN0bDAyJGJ0bkVkaXQFN2N0bDAwJGN0bDAwJE1haW5Db250ZW50JElubmVyTWFpbkNvbnRlbnQkR3JpZHZpZXdUYWFsaXMPPCsADAEIAgFkBUZjdGwwMCRjdGwwMCRNYWluQ29udGVudCRJbm5lck1haW5Db250ZW50JEVudGl0eURhdGFTb3VyY2VUYWFsaVRyYW5zZmVyDzwrAAkBAQ9nZGQFQWN0bDAwJGN0bDAwJE1haW5Db250ZW50JHVTdGVwSW5kaWNhdG9yJFN0ZXBJbmRpY2F0b3JfTGlzdFZpZXdTdGVwDxQrAA5kZGRkZGRkFCsAA2RkZAIDZGRkZgL%2F%2F%2F%2F%2FD2Q%3D')

        self.selenium_run("waitForElementPresent",
                          "id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_txtContainerName",
                          "")

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14429&rid=2 HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTrip.aspx\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Scripts/others/jquery-ui-1.9.1.custom.js HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: */*\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14429&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Scripts/others/jquery.ui.datepicker-cc.js HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: */*\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14429&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Scripts/others/jquery.ui.datepicker-cc-fa.js HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: */*\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14429&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Styles/css/jquery-ui-1.9.1.custom.css HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/css,*/*;q=0.1\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14429&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Styles/css/jquery-ui-1.9.1-fa.css HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/css,*/*;q=0.1\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14429&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Scripts/others/jquery-ui-1.9.1.custom.js HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: */*\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14429&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Scripts/others/jquery.ui.datepicker-cc.js HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: */*\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14429&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Scripts/others/jquery.ui.datepicker-cc-fa.js HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: */*\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14429&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Styles/css/jquery-ui-1.9.1.custom.css HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/css,*/*;q=0.1\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14429&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Styles/css/jquery-ui-1.9.1-fa.css HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/css,*/*;q=0.1\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14429&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.selenium_run("waitForElementPresent",
                          "id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_txtContainerName",
                          "")

        self.selenium_run("type",
                          "id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_txtContainerName",
                          "IRSU1234-5")

        self.selenium_run("click",
                          "id=MainContent_uTaaliTransfer_btnCancel",
                          "")

        self.send(
            b'POST http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14429&rid=2 HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14429&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: 3164\r\n\r\n__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwULLTE0NTUwMzQwOTQPFgIeE1ZhbGlkYXRlUmVxdWVzdE1vZGUCARYCZg9kFgICAQ9kFgICAQ9kFgZmDw9kFgIeBXN0eWxlBTliYWNrZ3JvdW5kLXJlcGVhdC14Om5vLXJlcGVhdDtiYWNrZ3JvdW5kLXNpemU6MTAwJSAxNDZweDsWAmYPDxYCHgRUZXh0BTXYs9in2YXYp9mG2Ycg2YXYr9uM2LHbjNiqINin2YbYqNin2LHZh9in24wg2LnZhdmI2YXbjGRkAgMPZBYCAgEPZBYCZg9kFgoCAw8PFgIfAgUR2YHYp9mC2K8g2K3Yp9mF2YRkZAIFDw8WAh8CBRHZgdin2YLYryDYrdin2YXZhGRkAgcPZBYYAgEPDxYCHwIFCUFOIzE0Mjk2OWRkAgMPDxYCHwIFFDEzOTQwMDAxNDcwNTY5OTE2NTM2ZGQCBQ8PFgIfAgUM2KrZiNmE2KfbjNuMZGQCBw8PFgIfAgUM2KrZiNmE2KfbjNuMZGQCCQ8PFgIfAgUKMDA4MTY3NDkwMmRkAgsPDxYCHwIFCjEzOTUvMDUvMTdkZAINDw8WAh8CBQzYqtmI2YTYp9uM24xkZAIPDw8WAh8CBQowMDgxNjc0OTAyZGQCEQ8PFgIfAgUBMGRkAhMPDxYCHwIFATBkZAIVDw8WAh8CBQEwZGQCFw8PFgIfAgUBMGRkAgkPZBYGZg8PFgIeB1Zpc2libGVoZGQCAQ8PFgQfAgUFMTQ0MjkfA2hkZAIKDzwrABEDAA8WBB4LXyFEYXRhQm91bmRnHgtfIUl0ZW1Db3VudAIDZAEQFgYCBAIFAgYCBwIIAgkWBjwrAAUBABYCHwNnPCsABQEAFgIfA2c8KwAFAQAWAh8DZzwrAAUBABYCHwNnPCsABQEAFgIfA2g8KwAFAQAWAh8DaBYGAgYCBgIGAgYCBgIGDBQrAAAWAmYPZBYIAgEPZBYUZg8PFgIfAgUFMTQwMzdkZAIBDw8WAh8CBQbahtmI2KhkZAICDw8WAh8CBQjZhtqv2YTZh2RkAgMPDxYCHwIFCDcyMTg5OTAwZGQCBA9kFgICAQ8PFgIfAgUDMTUwZGQCBQ9kFgICAQ8PFgIfAgUCNTAWAh4RZGF0YS1kZWZhdWx0dmFsdWUFAjUwZAIGD2QWAgIBDw8WAh8CBQMxMDAWAh8GBQMxMDBkAgcPZBYCAgEPD2QWAh8GBQEwZAIID2QWAgIBDw8WAh8CBQQzNjAwZGQCCQ9kFgICAQ8PFgIfAgUBMGRkAgIPZBYUZg8PFgIfAgUFMTQwMzVkZAIBDw8WAh8CBQjZhduM2YjZh2RkAgIPDxYCHwIFCtqp2KfYsdiq2YZkZAIDDw8WAh8CBQgyMTA1MDAwMGRkAgQPZBYCAgEPDxYCHwIFAzE1MGRkAgUPZBYCAgEPDxYCHwIFAjUwFgIfBgUCNTBkAgYPZBYCAgEPDxYCHwIFAzEwMBYCHwYFAzEwMGQCBw9kFgICAQ8PZBYCHwYFATBkAggPZBYCAgEPDxYCHwIFBDIwMDBkZAIJD2QWAgIBDw8WAh8CBQEwZGQCAw9kFhRmDw8WAh8CBQUxNDAzNmRkAgEPDxYCHwIFBtin2YfZhmRkAgIPDxYCHwIFCtqp2KfYsdiq2YZkZAIDDw8WAh8CBQgyMTA1MDAwMGRkAgQPZBYCAgEPDxYCHwIFAzE1MGRkAgUPZBYCAgEPDxYCHwIFAjUwFgIfBgUCNTBkAgYPZBYCAgEPDxYCHwIFAzEwMBYCHwYFAzEwMGQCBw9kFgICAQ8PZBYCHwYFATBkAggPZBYCAgEPDxYCHwIFBTMwMDAwZGQCCQ9kFgICAQ8PFgIfAgUBMGRkAgQPDxYCHwNoZGQCCw9kFgICAw8PFgIfA2dkZAIED2QWBAIBDw8WAh8CBQYzLjE0OTZkZAIDDw8WAh8CBQoxMzk1LzA1LzE2ZGQYAgUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgIFG2N0bDAwJEhlYWRMb2dpblN0YXR1cyRjdGwwMQUbY3RsMDAkSGVhZExvZ2luU3RhdHVzJGN0bDAzBWRjdGwwMCRNYWluQ29udGVudCR1VGFhbGlUcmFuc2ZlciRVQ29tbW9kaXR5UGVybWl0VGFhbGlUcmFuc2ZlciRHcmlkVmlld0NvbW1vZGl0eVBlcm1pdF9UYWFsaVRyYW5zZmVyDzwrAAwBCAIBZA%3D%3D&ctl00%24MainContent%24uTaaliTransfer%24UCommodityPermitTaaliTransfer%24txtDischargeDate=&ctl00%24MainContent%24uTaaliTransfer%24UCommodityPermitTaaliTransfer%24txtContainerName=IRSU1234-5&ctl00%24MainContent%24uTaaliTransfer%24UCommodityPermitTaaliTransfer%24txtPaymentTariffCode=&ctl00%24MainContent%24uTaaliTransfer%24UCommodityPermitTaaliTransfer%24GridViewCommodityPermit_TaaliTransfer%24ctl02%24txtCommodityPermitTaaliTransfer_CommodityNumber=0&ctl00%24MainContent%24uTaaliTransfer%24UCommodityPermitTaaliTransfer%24GridViewCommodityPermit_TaaliTransfer%24ctl03%24txtCommodityPermitTaaliTransfer_CommodityNumber=0&ctl00%24MainContent%24uTaaliTransfer%24UCommodityPermitTaaliTransfer%24GridViewCommodityPermit_TaaliTransfer%24ctl04%24txtCommodityPermitTaaliTransfer_CommodityNumber=0&ctl00%24MainContent%24uTaaliTransfer%24btnCancel=%D8%A7%D9%86%D8%B5%D8%B1%D8%A7%D9%81')

        self.selenium_run("waitForElementPresent",
                          "id=MainContent_InnerMainContent_btnAddTaaliTransfer",
                          "")

        self.send(
            b'GET http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTrip.aspx HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14429&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.selenium_run("waitForElementNotPresent",
                          "//table[@id='MainContent_InnerMainContent_GridviewTaalis']/tbody/tr[3]/td[2]",
                          "")

        self.selenium_run("click",
                          "id=MainContent_InnerMainContent_btnAddTaaliTransfer",
                          "")

        self.send(
            b'POST http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTrip.aspx HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTrip.aspx\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: 2216\r\n\r\n__EVENTTARGET=ctl00%24ctl00%24MainContent%24InnerMainContent%24btnAddTaaliTransfer&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwUKMTY4Njk5MzY5MA8WAh4TVmFsaWRhdGVSZXF1ZXN0TW9kZQIBFgJmD2QWAmYPZBYCAgEPZBYCAgEPZBYGZg8PZBYCHgVzdHlsZQU5YmFja2dyb3VuZC1yZXBlYXQteDpuby1yZXBlYXQ7YmFja2dyb3VuZC1zaXplOjEwMCUgMTQ2cHg7FgJmDw8WAh4EVGV4dAU12LPYp9mF2KfZhtmHINmF2K%2FbjNix24zYqiDYp9mG2KjYp9ix2YfYp9uMINi52YXZiNmF24xkZAIDD2QWBAIBD2QWAmYPZBYCAgEPFCsAAg8WBB4LXyFEYXRhQm91bmRnHgtfIUl0ZW1Db3VudAIDZGQWAmYPZBYCAgEPFgIeCmRhdGEtc3RlcHMFATMWBgIBD2QWAgIBDxYCHgVjbGFzcwUEZG9uZRYCZg8VAzAvVUkvV2FyZWhvdXNlQmlsbC9DcmVhdGVNYW51YWxXYXJlaG91c2VCaWxsLmFzcHgrL1N0eWxlcy9JbWFnZXMvbWluaW1hbGlzdGljYS8zMngzMi9ob21lLnBuZyDYp9i32YTYp9i52KfYqiDZgtio2LYg2KfZhtio2KfYsWQCAg9kFgICAQ8WAh8GBQRkb25lFgJmDxUDKC9VSS9XYXJlaG91c2VCaWxsL0NyZWF0ZU1hbnVhbFRhYWxpLmFzcHgsL1N0eWxlcy9JbWFnZXMvbWluaW1hbGlzdGljYS8zMngzMi9hcHBsZS5wbmcX2KfYt9mE2KfYudin2Kog2qnYp9mE2KdkAgMPZBYCAgEPFgIfBgUFZG9pbmcWAmYPFQMnL1VJL1dhcmVob3VzZUJpbGwvQ3JlYXRlTWFudWFsVHJpcC5hc3B4LC9TdHlsZXMvSW1hZ2VzL21pbmltYWxpc3RpY2EvMzJ4MzIvdHJ1Y2sucG5nF9in2LfZhNin2LnYp9iqINit2KfZhdmEZAIDD2QWAgIFDzwrABEDAA8WBB8DZx8EAgFkARAWABYAFgAMFCsAABYCZg9kFgYCAQ9kFgxmD2QWAgIBDw8WAh4PQ29tbWFuZEFyZ3VtZW50BQUxNDQyOGRkAgEPZBYCAgEPZBYCZg8VAQUxNDQyOGQCAg9kFgJmDxUBVNmC2KjYtiDYp9mG2KjYp9ixINmF2LHYqNmI2Lcg2KjZhyDYp9uM2YYg2KrYp9mE24wg2KvYqNiqINmG2YfYp9uM24wg2YbYtNiv2Ycg2KfYs9iqLmQCAw9kFgJmDxUBATNkAgQPZBYCZg8VAQMxNTBkAgUPZBYCAgEPDxYCHwcFBTE0NDI4ZGQCAg8PFgIeB1Zpc2libGVoZGQCAw8PFgIfCGhkZAIED2QWBAIBDw8WAh8CBQYzLjE0OTZkZAIDDw8WAh8CBQoxMzk1LzA1LzE2ZGQYBAUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgQFIWN0bDAwJGN0bDAwJEhlYWRMb2dpblN0YXR1cyRjdGwwMQUhY3RsMDAkY3RsMDAkSGVhZExvZ2luU3RhdHVzJGN0bDAzBU9jdGwwMCRjdGwwMCRNYWluQ29udGVudCRJbm5lck1haW5Db250ZW50JEdyaWR2aWV3VGFhbGlzJGN0bDAyJEltYWdlQnV0dG9uRGVsZXRlBUVjdGwwMCRjdGwwMCRNYWluQ29udGVudCRJbm5lck1haW5Db250ZW50JEdyaWR2aWV3VGFhbGlzJGN0bDAyJGJ0bkVkaXQFN2N0bDAwJGN0bDAwJE1haW5Db250ZW50JElubmVyTWFpbkNvbnRlbnQkR3JpZHZpZXdUYWFsaXMPPCsADAEIAgFkBUZjdGwwMCRjdGwwMCRNYWluQ29udGVudCRJbm5lck1haW5Db250ZW50JEVudGl0eURhdGFTb3VyY2VUYWFsaVRyYW5zZmVyDzwrAAkBAQ9nZGQFQWN0bDAwJGN0bDAwJE1haW5Db250ZW50JHVTdGVwSW5kaWNhdG9yJFN0ZXBJbmRpY2F0b3JfTGlzdFZpZXdTdGVwDxQrAA5kZGRkZGRkFCsAA2RkZAIDZGRkZgL%2F%2F%2F%2F%2FD2Q%3D')

        self.selenium_run("waitForText",
                          "id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_GridViewCommodityPermit_TaaliTransfer_lblUnusedOfCommodity_0",
                          "100")

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14430&rid=2 HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTrip.aspx\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Scripts/others/jquery-ui-1.9.1.custom.js HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: */*\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14430&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Scripts/others/jquery.ui.datepicker-cc-fa.js HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: */*\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14430&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Styles/css/jquery-ui-1.9.1.custom.css HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/css,*/*;q=0.1\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14430&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Styles/css/jquery-ui-1.9.1-fa.css HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/css,*/*;q=0.1\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14430&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Scripts/others/jquery.ui.datepicker-cc.js HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: */*\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14430&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Scripts/others/jquery-ui-1.9.1.custom.js HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: */*\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14430&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Scripts/others/jquery.ui.datepicker-cc.js HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: */*\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14430&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Scripts/others/jquery.ui.datepicker-cc-fa.js HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: */*\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14430&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Styles/css/jquery-ui-1.9.1.custom.css HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/css,*/*;q=0.1\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14430&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Enter/Styles/css/jquery-ui-1.9.1-fa.css HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/css,*/*;q=0.1\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14430&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.selenium_run("waitForText",
                          "id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_GridViewCommodityPermit_TaaliTransfer_lblUnusedOfCommodity_1",
                          "100")

        self.selenium_run("type",
                          "id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_GridViewCommodityPermit_TaaliTransfer_txtCommodityPermitTaaliTransfer_CommodityNumber_0",
                          "100")

        self.selenium_run("type",
                          "id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_GridViewCommodityPermit_TaaliTransfer_txtCommodityPermitTaaliTransfer_CommodityNumber_1",
                          "100")

        self.selenium_run("type",
                          "id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_GridViewCommodityPermit_TaaliTransfer_txtCommodityPermitTaaliTransfer_CommodityNumber_2",
                          "100")

        self.selenium_run("type",
                          "id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_txtContainerName",
                          "IRSU2222-2")

        self.selenium_run("click",
                          "id=MainContent_uTaaliTransfer_btnPrint",
                          "")

        self.send(
            b'POST http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14430&rid=2 HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14430&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: 3201\r\n\r\n__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwULLTE0NTUwMzQwOTQPFgIeE1ZhbGlkYXRlUmVxdWVzdE1vZGUCARYCZg9kFgICAQ9kFgICAQ9kFgZmDw9kFgIeBXN0eWxlBTliYWNrZ3JvdW5kLXJlcGVhdC14Om5vLXJlcGVhdDtiYWNrZ3JvdW5kLXNpemU6MTAwJSAxNDZweDsWAmYPDxYCHgRUZXh0BTXYs9in2YXYp9mG2Ycg2YXYr9uM2LHbjNiqINin2YbYqNin2LHZh9in24wg2LnZhdmI2YXbjGRkAgMPZBYCAgEPZBYCZg9kFgoCAw8PFgIfAgUR2YHYp9mC2K8g2K3Yp9mF2YRkZAIFDw8WAh8CBRHZgdin2YLYryDYrdin2YXZhGRkAgcPZBYYAgEPDxYCHwIFCUFOIzE0Mjk2OWRkAgMPDxYCHwIFFDEzOTQwMDAxNDcwNTY5OTE2NTM2ZGQCBQ8PFgIfAgUM2KrZiNmE2KfbjNuMZGQCBw8PFgIfAgUM2KrZiNmE2KfbjNuMZGQCCQ8PFgIfAgUKMDA4MTY3NDkwMmRkAgsPDxYCHwIFCjEzOTUvMDUvMTdkZAINDw8WAh8CBQzYqtmI2YTYp9uM24xkZAIPDw8WAh8CBQowMDgxNjc0OTAyZGQCEQ8PFgIfAgUBMGRkAhMPDxYCHwIFATBkZAIVDw8WAh8CBQEwZGQCFw8PFgIfAgUBMGRkAgkPZBYGZg8PFgIeB1Zpc2libGVoZGQCAQ8PFgQfAgUFMTQ0MzAfA2hkZAIKDzwrABEDAA8WBB4LXyFEYXRhQm91bmRnHgtfIUl0ZW1Db3VudAIDZAEQFgYCBAIFAgYCBwIIAgkWBjwrAAUBABYCHwNnPCsABQEAFgIfA2c8KwAFAQAWAh8DZzwrAAUBABYCHwNnPCsABQEAFgIfA2g8KwAFAQAWAh8DaBYGAgYCBgIGAgYCBgIGDBQrAAAWAmYPZBYIAgEPZBYUZg8PFgIfAgUFMTQwMzZkZAIBDw8WAh8CBQbYp9mH2YZkZAICDw8WAh8CBQraqdin2LHYqtmGZGQCAw8PFgIfAgUIMjEwNTAwMDBkZAIED2QWAgIBDw8WAh8CBQMxNTBkZAIFD2QWAgIBDw8WAh8CBQI1MBYCHhFkYXRhLWRlZmF1bHR2YWx1ZQUCNTBkAgYPZBYCAgEPDxYCHwIFAzEwMBYCHwYFAzEwMGQCBw9kFgICAQ8PZBYCHwYFATBkAggPZBYCAgEPDxYCHwIFBTMwMDAwZGQCCQ9kFgICAQ8PFgIfAgUBMGRkAgIPZBYUZg8PFgIfAgUFMTQwMzdkZAIBDw8WAh8CBQbahtmI2KhkZAICDw8WAh8CBQjZhtqv2YTZh2RkAgMPDxYCHwIFCDcyMTg5OTAwZGQCBA9kFgICAQ8PFgIfAgUDMTUwZGQCBQ9kFgICAQ8PFgIfAgUCNTAWAh8GBQI1MGQCBg9kFgICAQ8PFgIfAgUDMTAwFgIfBgUDMTAwZAIHD2QWAgIBDw9kFgIfBgUBMGQCCA9kFgICAQ8PFgIfAgUEMzYwMGRkAgkPZBYCAgEPDxYCHwIFATBkZAIDD2QWFGYPDxYCHwIFBTE0MDM1ZGQCAQ8PFgIfAgUI2YXbjNmI2YdkZAICDw8WAh8CBQraqdin2LHYqtmGZGQCAw8PFgIfAgUIMjEwNTAwMDBkZAIED2QWAgIBDw8WAh8CBQMxNTBkZAIFD2QWAgIBDw8WAh8CBQI1MBYCHwYFAjUwZAIGD2QWAgIBDw8WAh8CBQMxMDAWAh8GBQMxMDBkAgcPZBYCAgEPD2QWAh8GBQEwZAIID2QWAgIBDw8WAh8CBQQyMDAwZGQCCQ9kFgICAQ8PFgIfAgUBMGRkAgQPDxYCHwNoZGQCCw9kFgICAw8PFgIfA2dkZAIED2QWBAIBDw8WAh8CBQYzLjE0OTZkZAIDDw8WAh8CBQoxMzk1LzA1LzE2ZGQYAgUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgIFG2N0bDAwJEhlYWRMb2dpblN0YXR1cyRjdGwwMQUbY3RsMDAkSGVhZExvZ2luU3RhdHVzJGN0bDAzBWRjdGwwMCRNYWluQ29udGVudCR1VGFhbGlUcmFuc2ZlciRVQ29tbW9kaXR5UGVybWl0VGFhbGlUcmFuc2ZlciRHcmlkVmlld0NvbW1vZGl0eVBlcm1pdF9UYWFsaVRyYW5zZmVyDzwrAAwBCAIBZA%3D%3D&ctl00%24MainContent%24uTaaliTransfer%24UCommodityPermitTaaliTransfer%24txtDischargeDate=&ctl00%24MainContent%24uTaaliTransfer%24UCommodityPermitTaaliTransfer%24txtContainerName=IRSU2222-2&ctl00%24MainContent%24uTaaliTransfer%24UCommodityPermitTaaliTransfer%24txtPaymentTariffCode=&ctl00%24MainContent%24uTaaliTransfer%24UCommodityPermitTaaliTransfer%24GridViewCommodityPermit_TaaliTransfer%24ctl02%24txtCommodityPermitTaaliTransfer_CommodityNumber=100&ctl00%24MainContent%24uTaaliTransfer%24UCommodityPermitTaaliTransfer%24GridViewCommodityPermit_TaaliTransfer%24ctl03%24txtCommodityPermitTaaliTransfer_CommodityNumber=100&ctl00%24MainContent%24uTaaliTransfer%24UCommodityPermitTaaliTransfer%24GridViewCommodityPermit_TaaliTransfer%24ctl04%24txtCommodityPermitTaaliTransfer_CommodityNumber=100&ctl00%24MainContent%24uTaaliTransfer%24btnPrint=%D8%AB%D8%A8%D8%AA+%D9%88%D8%B1%D9%88%D8%AF+%DA%A9%D8%A7%D9%84%D8%A7')

        self.selenium_run("waitForElementPresent",
                          "//table[@id='MainContent_InnerMainContent_GridviewTaalis']/tbody/tr[3]/td[2]",
                          "")

        self.send(
            b'GET http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTrip.aspx HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Enter/Container/EditCommodityDetails.aspx?tid=14430&rid=2\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.selenium_run("click",
                          "id=MainContent_InnerMainContent_btnSubmit",
                          "")

        self.selenium_run("waitForText",
                          "id=MainContent_lblInfo",
                          "اطلاعات قبض انبار دستی با موفقیت ثبت گردید.")
        self.send(
            b'POST http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTrip.aspx HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTrip.aspx\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: 2769\r\n\r\n__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwUKMTY4Njk5MzY5MA8WAh4TVmFsaWRhdGVSZXF1ZXN0TW9kZQIBFgJmD2QWAmYPZBYCAgEPZBYCAgEPZBYGZg8PZBYCHgVzdHlsZQU5YmFja2dyb3VuZC1yZXBlYXQteDpuby1yZXBlYXQ7YmFja2dyb3VuZC1zaXplOjEwMCUgMTQ2cHg7FgJmDw8WAh4EVGV4dAU12LPYp9mF2KfZhtmHINmF2K%2FbjNix24zYqiDYp9mG2KjYp9ix2YfYp9uMINi52YXZiNmF24xkZAIDD2QWBAIBD2QWAmYPZBYCAgEPFCsAAg8WBB4LXyFEYXRhQm91bmRnHgtfIUl0ZW1Db3VudAIDZGQWAmYPZBYCAgEPFgIeCmRhdGEtc3RlcHMFATMWBgIBD2QWAgIBDxYCHgVjbGFzcwUEZG9uZRYCZg8VAzAvVUkvV2FyZWhvdXNlQmlsbC9DcmVhdGVNYW51YWxXYXJlaG91c2VCaWxsLmFzcHgrL1N0eWxlcy9JbWFnZXMvbWluaW1hbGlzdGljYS8zMngzMi9ob21lLnBuZyDYp9i32YTYp9i52KfYqiDZgtio2LYg2KfZhtio2KfYsWQCAg9kFgICAQ8WAh8GBQRkb25lFgJmDxUDKC9VSS9XYXJlaG91c2VCaWxsL0NyZWF0ZU1hbnVhbFRhYWxpLmFzcHgsL1N0eWxlcy9JbWFnZXMvbWluaW1hbGlzdGljYS8zMngzMi9hcHBsZS5wbmcX2KfYt9mE2KfYudin2Kog2qnYp9mE2KdkAgMPZBYCAgEPFgIfBgUFZG9pbmcWAmYPFQMnL1VJL1dhcmVob3VzZUJpbGwvQ3JlYXRlTWFudWFsVHJpcC5hc3B4LC9TdHlsZXMvSW1hZ2VzL21pbmltYWxpc3RpY2EvMzJ4MzIvdHJ1Y2sucG5nF9in2LfZhNin2LnYp9iqINit2KfZhdmEZAIDD2QWBAIFDzwrABEDAA8WBB8DZx8EAgJkARAWABYAFgAMFCsAABYCZg9kFggCAQ9kFgxmD2QWAgIBDw8WAh4PQ29tbWFuZEFyZ3VtZW50BQUxNDQzMGRkAgEPZBYCAgEPZBYCZg8VAQUxNDQzMGQCAg9kFgJmDxUBVNmC2KjYtiDYp9mG2KjYp9ixINmF2LHYqNmI2Lcg2KjZhyDYp9uM2YYg2KrYp9mE24wg2KvYqNiqINmG2YfYp9uM24wg2YbYtNiv2Ycg2KfYs9iqLmQCAw9kFgJmDxUBATNkAgQPZBYCZg8VAQMzMDBkAgUPZBYCAgEPDxYCHwcFBTE0NDMwZGQCAg9kFgxmD2QWAgIBDw8WAh8HBQUxNDQyOGRkAgEPZBYCAgEPZBYCZg8VAQUxNDQyOGQCAg9kFgJmDxUBVNmC2KjYtiDYp9mG2KjYp9ixINmF2LHYqNmI2Lcg2KjZhyDYp9uM2YYg2KrYp9mE24wg2KvYqNiqINmG2YfYp9uM24wg2YbYtNiv2Ycg2KfYs9iqLmQCAw9kFgJmDxUBATNkAgQPZBYCZg8VAQMxNTBkAgUPZBYCAgEPDxYCHwcFBTE0NDI4ZGQCAw8PFgIeB1Zpc2libGVoZGQCBA8PFgIfCGhkZAIHD2QWAgIBDw8WAh4HRW5hYmxlZGdkZAIED2QWBAIBDw8WAh8CBQYzLjE0OTZkZAIDDw8WAh8CBQoxMzk1LzA1LzE2ZGQYBAUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgYFIWN0bDAwJGN0bDAwJEhlYWRMb2dpblN0YXR1cyRjdGwwMQUhY3RsMDAkY3RsMDAkSGVhZExvZ2luU3RhdHVzJGN0bDAzBU9jdGwwMCRjdGwwMCRNYWluQ29udGVudCRJbm5lck1haW5Db250ZW50JEdyaWR2aWV3VGFhbGlzJGN0bDAyJEltYWdlQnV0dG9uRGVsZXRlBUVjdGwwMCRjdGwwMCRNYWluQ29udGVudCRJbm5lck1haW5Db250ZW50JEdyaWR2aWV3VGFhbGlzJGN0bDAyJGJ0bkVkaXQFT2N0bDAwJGN0bDAwJE1haW5Db250ZW50JElubmVyTWFpbkNvbnRlbnQkR3JpZHZpZXdUYWFsaXMkY3RsMDMkSW1hZ2VCdXR0b25EZWxldGUFRWN0bDAwJGN0bDAwJE1haW5Db250ZW50JElubmVyTWFpbkNvbnRlbnQkR3JpZHZpZXdUYWFsaXMkY3RsMDMkYnRuRWRpdAU3Y3RsMDAkY3RsMDAkTWFpbkNvbnRlbnQkSW5uZXJNYWluQ29udGVudCRHcmlkdmlld1RhYWxpcw88KwAMAQgCAWQFRmN0bDAwJGN0bDAwJE1haW5Db250ZW50JElubmVyTWFpbkNvbnRlbnQkRW50aXR5RGF0YVNvdXJjZVRhYWxpVHJhbnNmZXIPPCsACQEBD2dkZAVBY3RsMDAkY3RsMDAkTWFpbkNvbnRlbnQkdVN0ZXBJbmRpY2F0b3IkU3RlcEluZGljYXRvcl9MaXN0Vmlld1N0ZXAPFCsADmRkZGRkZGQUKwADZGRkAgNkZGRmAv%2F%2F%2F%2F8PZA%3D%3D&ctl00%24ctl00%24MainContent%24InnerMainContent%24btnSubmit=%D8%AA%D8%A7%DB%8C%DB%8C%D8%AF+%D9%86%D9%87%D8%A7%DB%8C%DB%8C')

        self.send(
            b'GET http://172.16.111.51:1021/UI/Account/InformationPage.aspx HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/WarehouseBill/CreateManualTrip.aspx\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')


    def getGhabzNum(self):
        self.selenium_run("setSpeed",
                          "5000",
                          "")

        self.selenium_run("setSpeed",
                          "500",
                          "")

        self.send(
            b'GET http://172.16.111.51:1021/UI/Default.aspx HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Account/InformationPage.aspx\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.selenium_run("click",
                          "link=ویرایش یا حدف قبض انبار",
                          "")

        self.selenium_run("waitForElementPresent",
                          "id=MainContent_uSearchWarehouseBill_txtWarehouseBill",
                          "")

        self.send(
            b'GET http://172.16.111.51:1021/UI/WarehouseBill/DeleteWarehouseBill.aspx HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/Default.aspx\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\n\r\n')

        self.selenium_run("type",
                          "id=MainContent_uSearchWarehouseBill_txtWarehouseBillNumber",
                          "1394000${tozin1}")

        self.selenium_run("click",
                          "id=MainContent_uSearchWarehouseBill_btnSearch",
                          "")

        self.send(
            b'POST http://172.16.111.51:1021/UI/WarehouseBill/DeleteWarehouseBill.aspx HTTP/1.1\r\nHost: 172.16.111.51:1021\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://172.16.111.51:1021/UI/WarehouseBill/DeleteWarehouseBill.aspx\r\nCookie: ASP.NET_SessionId=cj4zlswcmgcfjupz5yfm0fpn; .ASPXAUTH=81DFBBB31B2DD5FA0BDFED1622DF8F744B84730E09E5AA58DAFB7AE7C559D49C23F33A72289F52A66489ED12007A39F7652362C5BB10AE5C6D4FBA421C756477F44C4FB005613CD73D7B542952C4604A729ABC125209430DBCDCFFBBB24ED2977DEC585278D1E04971B2DA29E3A6DA2C75D79967E1FC60FDCEF6B739F3F722A3\r\nConnection: keep-alive\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: 18137\r\n\r\n__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwULLTEzNTE4ODE3ODQPFgIeE1ZhbGlkYXRlUmVxdWVzdE1vZGUCARYCZg9kFgICAQ9kFgICAQ9kFgZmDw9kFgIeBXN0eWxlBTliYWNrZ3JvdW5kLXJlcGVhdC14Om5vLXJlcGVhdDtiYWNrZ3JvdW5kLXNpemU6MTAwJSAxNDZweDsWAmYPDxYCHgRUZXh0BTXYs9in2YXYp9mG2Ycg2YXYr9uM2LHbjNiqINin2YbYqNin2LHZh9in24wg2LnZhdmI2YXbjGRkAgMPZBYIAgMPZBYCZg9kFgQCAQ8PFgIfAmVkZAIDD2QWAgIDDxYCHgdWaXNpYmxlaGQCBQ9kFgICAQ9kFgICAQ8QDxYCHg1TZWxlY3Rpb25Nb2RlCyorU3lzdGVtLldlYi5VSS5XZWJDb250cm9scy5MaXN0U2VsZWN0aW9uTW9kZQFkZBYAZAIHDw8WAh8DaGQWBAIBDw8WAh8CBQoxMzk1LzA1LzE2ZGQCAw8PFgIfAgUKMTM5NS8wNS8xOGRkAgkPZBYGAgEPZBYGAgIPDxYCHwNoZGQCAw8PFgIfA2hkZAIEDw8WAh8DaGRkAgMPDxYCHwJlZGQCBQ88KwARAwAPFgYeDERhdGFTb3VyY2VJRAUeRW50aXR5RGF0YVNvdXJjZVdhcmVob3VzZUJpbGxzHgtfIURhdGFCb3VuZGceC18hSXRlbUNvdW50AsILZAEQFgQCCAILAgwCFhYEPCsABQEAFgIfA2g8KwAFAQAWAh8DaDwrAAUBABYCHwNoPCsABQEAFgIfA2gWBAIGAgYCBgIGDBQrAAAWAmYPZBYWAgEPZBYkAgEPZBYCAgEPDxYCHwIFBjE0Mjk2OWRkAgIPZBYCZg8VARfYqtmI2YTYp9uM24wtMDA4MTY3NDkwMmQCAw9kFgJmDxUBF9i52YTbjNix2LbYpyDYp9mB2LTYp9ixZAIED2QWAmYPFQEK2YXYsdqp2LLbjGQCBQ9kFgJmDxUBDzEzOTUvMDUvMTcgMTY6OWQCBg9kFgJmDxUBAGQCBw9kFgJmDxUBCUFOIzE0Mjk2OWQCCA9kFgJmDxUBDTE0NzA1Njk5MTYzODRkAgkPZBYCZg8VARQxMzk0MDAwMTQ3MDU2OTkxNjUzNmQCCg9kFgICAQ9kFgJmDxUBHNmC2KjYtiDYp9mG2KjYp9ixINin2YjZhNuM2YdkAgsPZBYCZg8VAQEwZAIMD2QWAmYPFQEBMGQCEA9kFgICAQ8PFgIeD0NvbW1hbmRBcmd1bWVudAUGMTQyOTY5ZGQCEQ9kFgICAQ8PFgIfCAUGMTQyOTY5ZGQCEw9kFgICAQ8PFgIfCAUGMTQyOTY5ZGQCFA9kFgICAQ8PFgIfCAUGMTQyOTY5ZGQCFQ9kFgICAQ8PFgIfCAUGMTQyOTY5ZGQCFg9kFgJmDxUBAGQCAg9kFiQCAQ9kFgICAQ8PFgIfAgUGMTQyOTY4ZGQCAg9kFgJmDxUBF9iq2YjZhNin24zbjC0wMDgxNjc0OTAyZAIDD2QWAmYPFQEX2LnZhNuM2LHYttinINin2YHYtNin2LFkAgQPZBYCZg8VAQrZhdix2qnYstuMZAIFD2QWAmYPFQEQMTM5NS8wNS8xNyAxNTo1NWQCBg9kFgJmDxUBAGQCBw9kFgJmDxUBCUFOIzE0Mjk2OGQCCA9kFgJmDxUBDTE0NzA1NjkwOTkxNTlkAgkPZBYCZg8VARQxMzk0MDAwMTQ3MDU2OTA5OTM2OGQCCg9kFgICAQ9kFgJmDxUBHNmC2KjYtiDYp9mG2KjYp9ixINin2YjZhNuM2YdkAgsPZBYCZg8VAQEwZAIMD2QWAmYPFQEBMGQCEA9kFgICAQ8PFgIfCAUGMTQyOTY4ZGQCEQ9kFgICAQ8PFgIfCAUGMTQyOTY4ZGQCEw9kFgICAQ8PFgIfCAUGMTQyOTY4ZGQCFA9kFgICAQ8PFgIfCAUGMTQyOTY4ZGQCFQ9kFgICAQ8PFgIfCAUGMTQyOTY4ZGQCFg9kFgJmDxUBAGQCAw9kFiQCAQ9kFgICAQ8PFgIfAgUGMTQyOTY0ZGQCAg9kFgJmDxUBF9iq2YjZhNin24zbjC0wMDgxNjc0OTAyZAIDD2QWAmYPFQEX2LnZhNuM2LHYttinINin2YHYtNin2LFkAgQPZBYCZg8VAQrZhdix2qnYstuMZAIFD2QWAmYPFQEQMTM5NS8wNS8xNyAxNToxNWQCBg9kFgJmDxUBAGQCBw9kFgJmDxUBCUFOIzE0Mjk2NGQCCA9kFgJmDxUBDTE0NzA1NjY3MDEwNjBkAgkPZBYCZg8VARQxMzk0MDAwMTQ3MDU2NjcwMTE4OWQCCg9kFgICAQ9kFgJmDxUBHNmC2KjYtiDYp9mG2KjYp9ixINin2YjZhNuM2YdkAgsPZBYCZg8VAQEwZAIMD2QWAmYPFQEBMGQCEA9kFgICAQ8PFgIfCAUGMTQyOTY0ZGQCEQ9kFgICAQ8PFgIfCAUGMTQyOTY0ZGQCEw9kFgICAQ8PFgIfCAUGMTQyOTY0ZGQCFA9kFgICAQ8PFgIfCAUGMTQyOTY0ZGQCFQ9kFgICAQ8PFgIfCAUGMTQyOTY0ZGQCFg9kFgJmDxUBAGQCBA9kFiQCAQ9kFgICAQ8PFgIfAgUGMTQyOTYyZGQCAg9kFgJmDxUBF9iq2YjZhNin24zbjC0wMDgxNjc0OTAyZAIDD2QWAmYPFQEX2LnZhNuM2LHYttinINin2YHYtNin2LFkAgQPZBYCZg8VAQrZhdix2qnYstuMZAIFD2QWAmYPFQEPMTM5NS8wNS8xNyAxNToyZAIGD2QWAmYPFQEAZAIHD2QWAmYPFQEJQU4jMTQyOTYyZAIID2QWAmYPFQENMTQ3MDU2NTk0ODIxN2QCCQ9kFgJmDxUBFDEzOTQwMDAxNDcwNTY1OTQ4MzI0ZAIKD2QWAgIBD2QWAmYPFQEc2YLYqNi2INin2YbYqNin2LEg2KfZiNmE24zZh2QCCw9kFgJmDxUBATBkAgwPZBYCZg8VAQEwZAIQD2QWAgIBDw8WAh8IBQYxNDI5NjJkZAIRD2QWAgIBDw8WAh8IBQYxNDI5NjJkZAITD2QWAgIBDw8WAh8IBQYxNDI5NjJkZAIUD2QWAgIBDw8WAh8IBQYxNDI5NjJkZAIVD2QWAgIBDw8WAh8IBQYxNDI5NjJkZAIWD2QWAmYPFQEAZAIFD2QWJAIBD2QWAgIBDw8WAh8CBQYxNDI5NjFkZAICD2QWAmYPFQEX2KrZiNmE2KfbjNuMLTAwODE2NzQ5MDJkAgMPZBYCZg8VARfYudmE24zYsdi22Kcg2KfZgdi02KfYsWQCBA9kFgJmDxUBCtmF2LHaqdiy24xkAgUPZBYCZg8VAQ8xMzk1LzA1LzE3IDEzOjFkAgYPZBYCZg8VAQBkAgcPZBYCZg8VAQlBTiMxNDI5NjFkAggPZBYCZg8VAQ0xNDcwNTU4NjY1OTI0ZAIJD2QWAmYPFQEUMTM5NDAwMDE0NzA1NTg2NjYwMzFkAgoPZBYCAgEPZBYCZg8VARzZgtio2LYg2KfZhtio2KfYsSDYp9mI2YTbjNmHZAILD2QWAmYPFQEBMGQCDA9kFgJmDxUBATBkAhAPZBYCAgEPDxYCHwgFBjE0Mjk2MWRkAhEPZBYCAgEPDxYCHwgFBjE0Mjk2MWRkAhMPZBYCAgEPDxYCHwgFBjE0Mjk2MWRkAhQPZBYCAgEPDxYCHwgFBjE0Mjk2MWRkAhUPZBYCAgEPDxYCHwgFBjE0Mjk2MWRkAhYPZBYCZg8VAQBkAgYPZBYkAgEPZBYCAgEPDxYCHwIFBjE0Mjk2MGRkAgIPZBYCZg8VARfYqtmI2YTYp9uM24wtMDA4MTY3NDkwMmQCAw9kFgJmDxUBF9i52YTbjNix2LbYpyDYp9mB2LTYp9ixZAIED2QWAmYPFQEK2YXYsdqp2LLbjGQCBQ9kFgJmDxUBEDEzOTUvMDUvMTcgMTI6MzVkAgYPZBYCZg8VAQBkAgcPZBYCZg8VAQlBTiMxNDI5NjBkAggPZBYCZg8VAQ0xNDcwNTU3MDk3MDU3ZAIJD2QWAmYPFQEUMTM5NDAwMDE0NzA1NTcwOTcxNjVkAgoPZBYCAgEPZBYCZg8VARzZgtio2LYg2KfZhtio2KfYsSDYp9mI2YTbjNmHZAILD2QWAmYPFQEBMGQCDA9kFgJmDxUBATBkAhAPZBYCAgEPDxYCHwgFBjE0Mjk2MGRkAhEPZBYCAgEPDxYCHwgFBjE0Mjk2MGRkAhMPZBYCAgEPDxYCHwgFBjE0Mjk2MGRkAhQPZBYCAgEPDxYCHwgFBjE0Mjk2MGRkAhUPZBYCAgEPDxYCHwgFBjE0Mjk2MGRkAhYPZBYCZg8VAQBkAgcPZBYkAgEPZBYCAgEPDxYCHwIFBjE0Mjk1OWRkAgIPZBYCZg8VARfYqtmI2YTYp9uM24wtMDA4MTY3NDkwMmQCAw9kFgJmDxUBF9i52YTbjNix2LbYpyDYp9mB2LTYp9ixZAIED2QWAmYPFQEK2YXYsdqp2LLbjGQCBQ9kFgJmDxUBEDEzOTUvMDUvMTcgMTE6NTdkAgYPZBYCZg8VAQBkAgcPZBYCZg8VAQlBTiMxNDI5NTlkAggPZBYCZg8VAQ0xNDcwNTU0ODA0NjE0ZAIJD2QWAmYPFQEUMTM5NDAwMDE0NzA1NTQ4MDQ3NDJkAgoPZBYCAgEPZBYCZg8VARzZgtio2LYg2KfZhtio2KfYsSDYp9mI2YTbjNmHZAILD2QWAmYPFQEBMGQCDA9kFgJmDxUBATBkAhAPZBYCAgEPDxYCHwgFBjE0Mjk1OWRkAhEPZBYCAgEPDxYCHwgFBjE0Mjk1OWRkAhMPZBYCAgEPDxYCHwgFBjE0Mjk1OWRkAhQPZBYCAgEPDxYCHwgFBjE0Mjk1OWRkAhUPZBYCAgEPDxYCHwgFBjE0Mjk1OWRkAhYPZBYCZg8VAQBkAggPZBYkAgEPZBYCAgEPDxYCHwIFBjE0Mjk1OGRkAgIPZBYCZg8VARfYqtmI2YTYp9uM24wtMDA4MTY3NDkwMmQCAw9kFgJmDxUBF9i52YTbjNix2LbYpyDYp9mB2LTYp9ixZAIED2QWAmYPFQEK2YXYsdqp2LLbjGQCBQ9kFgJmDxUBEDEzOTUvMDUvMTcgMTE6NTRkAgYPZBYCZg8VAQBkAgcPZBYCZg8VAQlBTiMxNDI5NThkAggPZBYCZg8VAQ0xNDcwNTU0NjQxNjQ5ZAIJD2QWAmYPFQEUMTM5NDAwMDE0NzA1NTQ2NDE3NTRkAgoPZBYCAgEPZBYCZg8VARzZgtio2LYg2KfZhtio2KfYsSDYp9mI2YTbjNmHZAILD2QWAmYPFQEBMGQCDA9kFgJmDxUBATBkAhAPZBYCAgEPDxYCHwgFBjE0Mjk1OGRkAhEPZBYCAgEPDxYCHwgFBjE0Mjk1OGRkAhMPZBYCAgEPDxYCHwgFBjE0Mjk1OGRkAhQPZBYCAgEPDxYCHwgFBjE0Mjk1OGRkAhUPZBYCAgEPDxYCHwgFBjE0Mjk1OGRkAhYPZBYCZg8VAQBkAgkPZBYkAgEPZBYCAgEPDxYCHwIFBjE0Mjk1N2RkAgIPZBYCZg8VARfYqtmI2YTYp9uM24wtMDA4MTY3NDkwMmQCAw9kFgJmDxUBF9i52YTbjNix2LbYpyDYp9mB2LTYp9ixZAIED2QWAmYPFQEK2YXYsdqp2LLbjGQCBQ9kFgJmDxUBEDEzOTUvMDUvMTcgMTE6MTVkAgYPZBYCZg8VAQBkAgcPZBYCZg8VAQlBTiMxNDI5NTdkAggPZBYCZg8VAQ0xNDcwNTUyMzI0ODEwZAIJD2QWAmYPFQEUMTM5NDAwMDE0NzA1NTIzMjQ5MzRkAgoPZBYCAgEPZBYCZg8VARzZgtio2LYg2KfZhtio2KfYsSDYp9mI2YTbjNmHZAILD2QWAmYPFQEBMGQCDA9kFgJmDxUBATBkAhAPZBYCAgEPDxYCHwgFBjE0Mjk1N2RkAhEPZBYCAgEPDxYCHwgFBjE0Mjk1N2RkAhMPZBYCAgEPDxYCHwgFBjE0Mjk1N2RkAhQPZBYCAgEPDxYCHwgFBjE0Mjk1N2RkAhUPZBYCAgEPDxYCHwgFBjE0Mjk1N2RkAhYPZBYCZg8VAQBkAgoPZBYkAgEPZBYCAgEPDxYCHwIFBjE0Mjk1NmRkAgIPZBYCZg8VARfYqtmI2YTYp9uM24wtMDA4MTY3NDkwMmQCAw9kFgJmDxUBF9i52YTbjNix2LbYpyDYp9mB2LTYp9ixZAIED2QWAmYPFQEK2YXYsdqp2LLbjGQCBQ9kFgJmDxUBDzEzOTUvMDUvMTcgODo0OGQCBg9kFgJmDxUBAGQCBw9kFgJmDxUBCUFOIzE0Mjk1NmQCCA9kFgJmDxUBDTE0NzA1NDM0Mzk0MTVkAgkPZBYCZg8VARQxMzk0MDAwMTQ3MDU0MzQzOTY0NmQCCg9kFgICAQ9kFgJmDxUBHNmC2KjYtiDYp9mG2KjYp9ixINin2YjZhNuM2YdkAgsPZBYCZg8VAQExZAIMD2QWAmYPFQEBMWQCEA9kFgICAQ8PFgIfCAUGMTQyOTU2ZGQCEQ9kFgICAQ8PFgIfCAUGMTQyOTU2ZGQCEw9kFgICAQ8PFgIfCAUGMTQyOTU2ZGQCFA9kFgICAQ8PFgIfCAUGMTQyOTU2ZGQCFQ9kFgICAQ8PFgIfCAUGMTQyOTU2ZGQCFg9kFgJmDxUBAGQCCw8PFgIfA2hkZAIED2QWBAIBDw8WAh8CBQYzLjE0OTZkZAIDDw8WAh8CBQoxMzk1LzA1LzE2ZGQYAwUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFl4FG2N0bDAwJEhlYWRMb2dpblN0YXR1cyRjdGwwMQUbY3RsMDAkSGVhZExvZ2luU3RhdHVzJGN0bDAzBT1jdGwwMCRNYWluQ29udGVudCR1U2VhcmNoV2FyZWhvdXNlQmlsbCRjaGVja2JveE5vdERpc3NvY2lhdGVkBTBjdGwwMCRNYWluQ29udGVudCR1U2VhcmNoV2FyZWhvdXNlQmlsbCRidG5TZWFyY2gFVmN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDAyJEltYWdlQnV0dG9uRGVsZXRlBVFjdGwwMCRNYWluQ29udGVudCR1V2FyZWhvdXNlQmlsbEdyaWRWaWV3JEdyaWR2aWV3V2FyZWhvdXNlYmlsbCRjdGwwMiRJbWFnZUJ1dHRvbjIFSmN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDAyJGN0bDAwBVdjdGwwMCRNYWluQ29udGVudCR1V2FyZWhvdXNlQmlsbEdyaWRWaWV3JEdyaWR2aWV3V2FyZWhvdXNlYmlsbCRjdGwwMiRpbWdDb250YWluZXJEZXRhaWwFZ2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDAyJGltZ0NyZWF0ZU1hbnVhbFdhcmVob3VzZV9UcmFuc2ZlcnMFWGN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDAyJFRyYW5zaGlwUmVkaXJlY3RCdG4FT2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDAyJGJ0blJlc3RvcmUFT2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDAyJFNob3dUYWFsaXMFVWN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDAyJGJ0blJlbWFpbmluZ0l0ZW0FVmN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDAzJEltYWdlQnV0dG9uRGVsZXRlBVFjdGwwMCRNYWluQ29udGVudCR1V2FyZWhvdXNlQmlsbEdyaWRWaWV3JEdyaWR2aWV3V2FyZWhvdXNlYmlsbCRjdGwwMyRJbWFnZUJ1dHRvbjIFSmN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDAzJGN0bDAwBVdjdGwwMCRNYWluQ29udGVudCR1V2FyZWhvdXNlQmlsbEdyaWRWaWV3JEdyaWR2aWV3V2FyZWhvdXNlYmlsbCRjdGwwMyRpbWdDb250YWluZXJEZXRhaWwFZ2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDAzJGltZ0NyZWF0ZU1hbnVhbFdhcmVob3VzZV9UcmFuc2ZlcnMFWGN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDAzJFRyYW5zaGlwUmVkaXJlY3RCdG4FT2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDAzJGJ0blJlc3RvcmUFT2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDAzJFNob3dUYWFsaXMFVWN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDAzJGJ0blJlbWFpbmluZ0l0ZW0FVmN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA0JEltYWdlQnV0dG9uRGVsZXRlBVFjdGwwMCRNYWluQ29udGVudCR1V2FyZWhvdXNlQmlsbEdyaWRWaWV3JEdyaWR2aWV3V2FyZWhvdXNlYmlsbCRjdGwwNCRJbWFnZUJ1dHRvbjIFSmN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA0JGN0bDAwBVdjdGwwMCRNYWluQ29udGVudCR1V2FyZWhvdXNlQmlsbEdyaWRWaWV3JEdyaWR2aWV3V2FyZWhvdXNlYmlsbCRjdGwwNCRpbWdDb250YWluZXJEZXRhaWwFZ2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA0JGltZ0NyZWF0ZU1hbnVhbFdhcmVob3VzZV9UcmFuc2ZlcnMFWGN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA0JFRyYW5zaGlwUmVkaXJlY3RCdG4FT2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA0JGJ0blJlc3RvcmUFT2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA0JFNob3dUYWFsaXMFVWN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA0JGJ0blJlbWFpbmluZ0l0ZW0FVmN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA1JEltYWdlQnV0dG9uRGVsZXRlBVFjdGwwMCRNYWluQ29udGVudCR1V2FyZWhvdXNlQmlsbEdyaWRWaWV3JEdyaWR2aWV3V2FyZWhvdXNlYmlsbCRjdGwwNSRJbWFnZUJ1dHRvbjIFSmN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA1JGN0bDAwBVdjdGwwMCRNYWluQ29udGVudCR1V2FyZWhvdXNlQmlsbEdyaWRWaWV3JEdyaWR2aWV3V2FyZWhvdXNlYmlsbCRjdGwwNSRpbWdDb250YWluZXJEZXRhaWwFZ2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA1JGltZ0NyZWF0ZU1hbnVhbFdhcmVob3VzZV9UcmFuc2ZlcnMFWGN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA1JFRyYW5zaGlwUmVkaXJlY3RCdG4FT2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA1JGJ0blJlc3RvcmUFT2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA1JFNob3dUYWFsaXMFVWN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA1JGJ0blJlbWFpbmluZ0l0ZW0FVmN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA2JEltYWdlQnV0dG9uRGVsZXRlBVFjdGwwMCRNYWluQ29udGVudCR1V2FyZWhvdXNlQmlsbEdyaWRWaWV3JEdyaWR2aWV3V2FyZWhvdXNlYmlsbCRjdGwwNiRJbWFnZUJ1dHRvbjIFSmN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA2JGN0bDAwBVdjdGwwMCRNYWluQ29udGVudCR1V2FyZWhvdXNlQmlsbEdyaWRWaWV3JEdyaWR2aWV3V2FyZWhvdXNlYmlsbCRjdGwwNiRpbWdDb250YWluZXJEZXRhaWwFZ2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA2JGltZ0NyZWF0ZU1hbnVhbFdhcmVob3VzZV9UcmFuc2ZlcnMFWGN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA2JFRyYW5zaGlwUmVkaXJlY3RCdG4FT2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA2JGJ0blJlc3RvcmUFT2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA2JFNob3dUYWFsaXMFVWN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA2JGJ0blJlbWFpbmluZ0l0ZW0FVmN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA3JEltYWdlQnV0dG9uRGVsZXRlBVFjdGwwMCRNYWluQ29udGVudCR1V2FyZWhvdXNlQmlsbEdyaWRWaWV3JEdyaWR2aWV3V2FyZWhvdXNlYmlsbCRjdGwwNyRJbWFnZUJ1dHRvbjIFSmN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA3JGN0bDAwBVdjdGwwMCRNYWluQ29udGVudCR1V2FyZWhvdXNlQmlsbEdyaWRWaWV3JEdyaWR2aWV3V2FyZWhvdXNlYmlsbCRjdGwwNyRpbWdDb250YWluZXJEZXRhaWwFZ2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA3JGltZ0NyZWF0ZU1hbnVhbFdhcmVob3VzZV9UcmFuc2ZlcnMFWGN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA3JFRyYW5zaGlwUmVkaXJlY3RCdG4FT2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA3JGJ0blJlc3RvcmUFT2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA3JFNob3dUYWFsaXMFVWN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA3JGJ0blJlbWFpbmluZ0l0ZW0FVmN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA4JEltYWdlQnV0dG9uRGVsZXRlBVFjdGwwMCRNYWluQ29udGVudCR1V2FyZWhvdXNlQmlsbEdyaWRWaWV3JEdyaWR2aWV3V2FyZWhvdXNlYmlsbCRjdGwwOCRJbWFnZUJ1dHRvbjIFSmN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA4JGN0bDAwBVdjdGwwMCRNYWluQ29udGVudCR1V2FyZWhvdXNlQmlsbEdyaWRWaWV3JEdyaWR2aWV3V2FyZWhvdXNlYmlsbCRjdGwwOCRpbWdDb250YWluZXJEZXRhaWwFZ2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA4JGltZ0NyZWF0ZU1hbnVhbFdhcmVob3VzZV9UcmFuc2ZlcnMFWGN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA4JFRyYW5zaGlwUmVkaXJlY3RCdG4FT2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA4JGJ0blJlc3RvcmUFT2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA4JFNob3dUYWFsaXMFVWN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA4JGJ0blJlbWFpbmluZ0l0ZW0FVmN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA5JEltYWdlQnV0dG9uRGVsZXRlBVFjdGwwMCRNYWluQ29udGVudCR1V2FyZWhvdXNlQmlsbEdyaWRWaWV3JEdyaWR2aWV3V2FyZWhvdXNlYmlsbCRjdGwwOSRJbWFnZUJ1dHRvbjIFSmN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA5JGN0bDAwBVdjdGwwMCRNYWluQ29udGVudCR1V2FyZWhvdXNlQmlsbEdyaWRWaWV3JEdyaWR2aWV3V2FyZWhvdXNlYmlsbCRjdGwwOSRpbWdDb250YWluZXJEZXRhaWwFZ2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA5JGltZ0NyZWF0ZU1hbnVhbFdhcmVob3VzZV9UcmFuc2ZlcnMFWGN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA5JFRyYW5zaGlwUmVkaXJlY3RCdG4FT2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA5JGJ0blJlc3RvcmUFT2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA5JFNob3dUYWFsaXMFVWN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDA5JGJ0blJlbWFpbmluZ0l0ZW0FVmN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDEwJEltYWdlQnV0dG9uRGVsZXRlBVFjdGwwMCRNYWluQ29udGVudCR1V2FyZWhvdXNlQmlsbEdyaWRWaWV3JEdyaWR2aWV3V2FyZWhvdXNlYmlsbCRjdGwxMCRJbWFnZUJ1dHRvbjIFSmN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDEwJGN0bDAwBVdjdGwwMCRNYWluQ29udGVudCR1V2FyZWhvdXNlQmlsbEdyaWRWaWV3JEdyaWR2aWV3V2FyZWhvdXNlYmlsbCRjdGwxMCRpbWdDb250YWluZXJEZXRhaWwFZ2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDEwJGltZ0NyZWF0ZU1hbnVhbFdhcmVob3VzZV9UcmFuc2ZlcnMFWGN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDEwJFRyYW5zaGlwUmVkaXJlY3RCdG4FT2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDEwJGJ0blJlc3RvcmUFT2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDEwJFNob3dUYWFsaXMFVWN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDEwJGJ0blJlbWFpbmluZ0l0ZW0FVmN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDExJEltYWdlQnV0dG9uRGVsZXRlBVFjdGwwMCRNYWluQ29udGVudCR1V2FyZWhvdXNlQmlsbEdyaWRWaWV3JEdyaWR2aWV3V2FyZWhvdXNlYmlsbCRjdGwxMSRJbWFnZUJ1dHRvbjIFSmN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDExJGN0bDAwBVdjdGwwMCRNYWluQ29udGVudCR1V2FyZWhvdXNlQmlsbEdyaWRWaWV3JEdyaWR2aWV3V2FyZWhvdXNlYmlsbCRjdGwxMSRpbWdDb250YWluZXJEZXRhaWwFZ2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDExJGltZ0NyZWF0ZU1hbnVhbFdhcmVob3VzZV9UcmFuc2ZlcnMFWGN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDExJFRyYW5zaGlwUmVkaXJlY3RCdG4FT2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDExJGJ0blJlc3RvcmUFT2N0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDExJFNob3dUYWFsaXMFVWN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsJGN0bDExJGJ0blJlbWFpbmluZ0l0ZW0FPmN0bDAwJE1haW5Db250ZW50JHVXYXJlaG91c2VCaWxsR3JpZFZpZXckR3JpZHZpZXdXYXJlaG91c2ViaWxsDzwrAAwBCAKUAWQFMGN0bDAwJE1haW5Db250ZW50JEVudGl0eURhdGFTb3VyY2VXYXJlaG91c2VCaWxscw88KwAJAQEPZ2Rk&ctl00%24MainContent%24uSearchWarehouseBill%24txtWarehouseBill=&ctl00%24MainContent%24uSearchWarehouseBill%24txtWarehouseBillNumber=13940001470569916536&ctl00%24MainContent%24uSearchWarehouseBill%24txtBaskoolBillNumber=&ctl00%24MainContent%24uSearchWarehouseBill%24txtParentID=&ctl00%24MainContent%24uSearchWarehouseBill%24btnSearch.x=0&ctl00%24MainContent%24uSearchWarehouseBill%24btnSearch.y=0')

        self.selenium_run("waitForElementPresent",
                          "id=MainContent_uWarehouseBillGridView_GridviewWarehousebill_lblId_0",
                          "")

        self.selenium_run("waitForElementNotPresent",
                          "id=MainContent_uWarehouseBillGridView_GridviewWarehousebill_lblId_1",
                          "")

        self.selenium_run("setSpeed",
                          "1000",
                          "")

        self.selenium_run("setSpeed",
                          "100",
                          "")

        self.selenium_run("storeText",
                          "id=MainContent_uWarehouseBillGridView_GridviewWarehousebill_lblId_0",
                          "ghabzNo")
        self.selenium_var("ghabzNo", "142969")


    def tavalLoginEpl(self):
        self.selenium_run("label",
                          "start",
                          "")


        self.selenium_run("setSpeed",
                          "2000",
                          "")

        self.selenium_run("open",
                          "http://test.iais.co:${generalPort}/PackingList-Rajae/",
                          "")

        self.send(
            b'GET http://test.iais.co:8080/PackingList-Rajae/ HTTP/1.1\r\nHost: test.iais.co:8080\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://test.iais.co:8080/PackingList-Rajae/?5\r\nConnection: keep-alive\r\n\r\n')

        self.selenium_run("setSpeed",
                          "100",
                          "")

        self.send(
            b'GET http://test.iais.co:8080/PackingList-Rajae/wicket/bookmarkable/ir.customs.ui.web.user.login.LoginPage HTTP/1.1\r\nHost: test.iais.co:8080\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://test.iais.co:8080/PackingList-Rajae/?5\r\nCookie: JSESSIONID=226059BE292E4EAD2E3E17932BAF22B6\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://test.iais.co:8080/PackingList-Rajae/wicket/bookmarkable/ir.customs.ui.web.user.login.LoginPage?1 HTTP/1.1\r\nHost: test.iais.co:8080\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://test.iais.co:8080/PackingList-Rajae/?5\r\nCookie: JSESSIONID=226059BE292E4EAD2E3E17932BAF22B6\r\nConnection: keep-alive\r\n\r\n')

        self.selenium_run("storeElementPresent",
                          "id=uname",
                          "not")

        self.selenium_run("gotoIf",
                          "storedVars['not']==true",
                          "login")

        self.selenium_run("waitForElementPresent",
                          "id=uname",
                          "")

        self.selenium_run("type",
                          "id=uname",
                          "0081674902")

        self.selenium_run("type",
                          "id=code",
                          "123456Aa")

        self.selenium_run("storeEval",
                          "\"0081674902\"",
                          "user")

        self.selenium_run("click",
                          "id=new-login-button",
                          "")

        self.selenium_run("setSpeed",
                          "2000",
        "")
        self.send(
            b'POST http://test.iais.co:8080/PackingList-Rajae/wicket/bookmarkable/ir.customs.ui.web.user.login.LoginPage?1-1.IFormSubmitListener-form HTTP/1.1\r\nHost: test.iais.co:8080\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://test.iais.co:8080/PackingList-Rajae/wicket/bookmarkable/ir.customs.ui.web.user.login.LoginPage?1\r\nCookie: JSESSIONID=226059BE292E4EAD2E3E17932BAF22B6\r\nConnection: keep-alive\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: 80\r\n\r\nlogin-form_hf_0=&latitude=1470572534912&user=0081674902&pass=123456Aa&secureKey=')

        self.send(
            b'GET http://test.iais.co:8080/PackingList-Rajae/ HTTP/1.1\r\nHost: test.iais.co:8080\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://test.iais.co:8080/PackingList-Rajae/wicket/bookmarkable/ir.customs.ui.web.user.login.LoginPage?1\r\nCookie: JSESSIONID=226059BE292E4EAD2E3E17932BAF22B6\r\nConnection: keep-alive\r\n\r\n')

        self.selenium_run("setSpeed",
                          "100",
                          "")

        self.send(
            b'GET http://test.iais.co:8080/PackingList-Rajae/?3 HTTP/1.1\r\nHost: test.iais.co:8080\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://test.iais.co:8080/PackingList-Rajae/wicket/bookmarkable/ir.customs.ui.web.user.login.LoginPage?1\r\nCookie: JSESSIONID=226059BE292E4EAD2E3E17932BAF22B6\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://test.iais.co:8080/PackingList-Rajae/wicket/resource/ir.apache.wicket.bootstrap.Bootstrap/css/defaultBootstrapLessTheme-rtl.css HTTP/1.1\r\nHost: test.iais.co:8080\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/css,*/*;q=0.1\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://test.iais.co:8080/PackingList-Rajae/?3\r\nCookie: JSESSIONID=226059BE292E4EAD2E3E17932BAF22B6\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://test.iais.co:8080/PackingList-Rajae/wicket/resource/ir.apache.wicket.bootstrap.Bootstrap/css/defaultBootstrapLessTheme-rtl.css HTTP/1.1\r\nHost: test.iais.co:8080\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/css,*/*;q=0.1\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://test.iais.co:8080/PackingList-Rajae/?3\r\nCookie: JSESSIONID=226059BE292E4EAD2E3E17932BAF22B6\r\nConnection: keep-alive\r\n\r\n')

        self.selenium_run("open",
                          "http://test.iais.co:${generalPort}/PackingList-Rajae/",
                          "")

        self.send(
            b'GET http://test.iais.co:8080/PackingList-Rajae/ HTTP/1.1\r\nHost: test.iais.co:8080\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://test.iais.co:8080/PackingList-Rajae/?3\r\nCookie: JSESSIONID=226059BE292E4EAD2E3E17932BAF22B6\r\nConnection: keep-alive\r\n\r\n')

        self.selenium_run("storeElementPresent",
                          "css=h3[wicketpath$='ajaxIndicatorContainer_pageTitle']",
                          "dashboard")
        self.selenium_var("dashboard", "true")

        self.selenium_run("gotoIf",
                          "storedVars[\"dashboard\"]==true",
                          "logined")

        self.send(
            b'GET http://test.iais.co:8080/PackingList-Rajae/?4 HTTP/1.1\r\nHost: test.iais.co:8080\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://test.iais.co:8080/PackingList-Rajae/?3\r\nCookie: JSESSIONID=226059BE292E4EAD2E3E17932BAF22B6\r\nConnection: keep-alive\r\n\r\n')

        self.send(
            b'GET http://test.iais.co:8080/PackingList-Rajae/wicket/resource/ir.apache.wicket.bootstrap.Bootstrap/css/defaultBootstrapLessTheme-rtl.css HTTP/1.1\r\nHost: test.iais.co:8080\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/css,*/*;q=0.1\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://test.iais.co:8080/PackingList-Rajae/?4\r\nCookie: JSESSIONID=226059BE292E4EAD2E3E17932BAF22B6\r\nConnection: keep-alive\r\n\r\n')

        self.selenium_run("waitForText",
                          "css=h3[wicketpath$='ajaxIndicatorContainer_pageTitle']",
                          "داشبورد")

        self.send(
            b'GET http://test.iais.co:8080/PackingList-Rajae/wicket/resource/ir.apache.wicket.bootstrap.Bootstrap/css/defaultBootstrapLessTheme-rtl.css HTTP/1.1\r\nHost: test.iais.co:8080\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\r\nAccept: text/css,*/*;q=0.1\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nReferer: http://test.iais.co:8080/PackingList-Rajae/?4\r\nCookie: JSESSIONID=226059BE292E4EAD2E3E17932BAF22B6\r\nConnection: keep-alive\r\n\r\n')





    def start_test(self):
        self.setGeneralPort()
        self.setUploadPath()
        self.PortTe()
        self.PortRa()
        self.PortBa()
        self.LoginAnbar8093()
        self.qabzDasti()
        self.commodityInsert()
        self.sabteTaliDasti()
        self.getGhabzNum()
        self.LoginAnbar8093()
        self.qabzDasti()
        self.commodityInsert()
        self.sabteTaliDasti()
        self.getGhabzNum()
        self.tavalLoginEpl()