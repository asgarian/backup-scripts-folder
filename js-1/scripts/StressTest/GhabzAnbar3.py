# -*- coding: utf-8 -*-
from random import randint

from StressTest.StressTest import StressTest


class GhabzAnbar2(StressTest):
    def __init__(self, index):
        super().__init__(index)

        self.barnameh_number = randint(0, 100000000)
        self.tozin1 = randint(0, 100000000)

    def start_test(self):
        # setSpeed | 2000 |  | LoginAnbar8093 | {}


        # open | http://172.16.111.51:1040 | Anbar | LoginAnbar8093 | {}


        # setSpeed | 10 |  | LoginAnbar8093 | {}


        # storeEval | window.location.pathname.contains("Login") | notlogined2 | LoginAnbar8093 | {}


        # gotoIf | storedVars['notlogined2']!=false | login | LoginAnbar8093 | {'notlogined2': 'true'}


        # type | id=txtUsername | test1 | LoginAnbar8093 | {'notlogined2': 'true'}


        # type | id=txtPassword | test@1 | LoginAnbar8093 | {'notlogined2': 'true'}


        # type | id=LoginContent_txtCaptcha | TEST | LoginAnbar8093 | {'notlogined2': 'true'}


        # clickAndWait | id=btnSubmit |  | LoginAnbar8093 | {'notlogined2': 'true'}


        # label | afterlogin |  | LoginAnbar8093 | {'notlogined2': 'true'}


        # waitForElementPresent | link=خانه |  | LoginAnbar8093 | {'notlogined2': 'true'}


        # clickAndWait | link=صدور قبض انبار دستی |  | qabzDasti | {'notlogined2': 'true'}


        # waitForElementPresent | id=txtWaybill |  | qabzDasti | {'notlogined2': 'true'}


        # storeEval | Date.now() | barnameNum | qabzDasti | {'notlogined2': 'true'}


        # type | id=txtWaybill | ${barnameNum} | qabzDasti | {'notlogined2': 'true','barnameNum': '1468130937273'}


        # storeEval | 60902 | baygani | qabzDasti | {'notlogined2': 'true','barnameNum': '1468130937273'}


        # type | id=txtEnterNumber | ${baygani} | qabzDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902'}


        # type | id=txtCompany | تولایی-0081674902 | qabzDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902'}


        # type | id=txtOwner | تولایی-0081674902 | qabzDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902'}


        # type | id=txtCompanyAgent | تولایی-0081674902 | qabzDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902'}


        # type | id=MainContent_InnerMainContent_uWarehousebillDetails_txtEvacuationCosts | 0 | qabzDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902'}


        # storeEval | Date.now() | tozin1 | qabzDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902'}


        # type | id=txtWarehouseBillNumber | 1394000${tozin1} | qabzDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=txtWarehouseNumber3 | 1 | qabzDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=txtWarehouseNumber2 | 2 | qabzDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=txtWarehouseNumber1 | 3 | qabzDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # clickAt | id=select2-selectStocks-container |  | qabzDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | css=input.select2-search__field | مرکزی | qabzDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # sendKeys | css=input.select2-search__field | ${KEY_ENTER} | qabzDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # clickAndWait | id=MainContent_InnerMainContent_btnNext | مرحله بعد | qabzDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # setSpeed | 500 |  | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityName | میوه | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_InnerMainContent_uTaaliTransferInsert_txtHsCode | 21050000 | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityUnit | کارتن | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityNumber | 150 | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityWeight | ۲۰۰۰ | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # click | id=MainContent_InnerMainContent_uTaaliTransferInsert_ImageButtonAdd |  | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # waitForElementPresent | name=ctl00$ctl00$MainContent$InnerMainContent$uTaaliTransferInsert$ctl05 |  | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityName | اگر این کالا را می بینید یعنی انبار مشکل دارد | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_InnerMainContent_uTaaliTransferInsert_txtHsCode | باید حذف شده باشد | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityUnit | غلط | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityNumber | 1230 | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityWeight | 2345 | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # click | id=MainContent_InnerMainContent_uTaaliTransferInsert_ImageButtonAdd |  | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # waitForElementPresent | name=ctl00$ctl00$MainContent$InnerMainContent$uTaaliTransferInsert$ctl06 |  | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityName | چوب | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_InnerMainContent_uTaaliTransferInsert_txtHsCode | 72189900 | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityUnit | نگله | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityNumber | 150 | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityWeight | 3600 | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # click | id=MainContent_InnerMainContent_uTaaliTransferInsert_ImageButtonAdd |  | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # waitForElementPresent | name=ctl00$ctl00$MainContent$InnerMainContent$uTaaliTransferInsert$ctl07 |  | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # click | name=ctl00$ctl00$MainContent$InnerMainContent$uTaaliTransferInsert$ctl06 |  | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # waitForNotText | //table[@id='MainContent_InnerMainContent_uTaaliTransferInsert_TableCommodity']/tbody/tr[3]/td | اگر این کالا را می بینید یعنی انبار مشکل دارد | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # waitForElementPresent | id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityName |  | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityName | اهن | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_InnerMainContent_uTaaliTransferInsert_txtHsCode | 21050000 | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityUnit | کارتن | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityNumber | 150 | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_InnerMainContent_uTaaliTransferInsert_txtCommodityWeight | 30000 | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # click | id=MainContent_InnerMainContent_uTaaliTransferInsert_ImageButtonAdd |  | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # click | id=MainContent_InnerMainContent_uTaaliTransferInsert_btnSubmit |  | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # waitForElementPresent | id=MainContent_InnerMainContent_btnAddTaaliTransfer |  | commodityInsert | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # click | id=MainContent_InnerMainContent_btnAddTaaliTransfer |  | sabteTaliDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # waitForElementPresent | id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_txtContainerName |  | sabteTaliDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_txtContainerName | IRSU1234-5 | sabteTaliDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_GridViewCommodityPermit_TaaliTransfer_txtCommodityPermitTaaliTransfer_CommodityNumber_0 | 50 | sabteTaliDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_GridViewCommodityPermit_TaaliTransfer_txtCommodityPermitTaaliTransfer_CommodityNumber_1 | 50 | sabteTaliDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # waitForElementPresent | id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_GridViewCommodityPermit_TaaliTransfer_txtCommodityPermitTaaliTransfer_CommodityNumber_2 | 50 | sabteTaliDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_GridViewCommodityPermit_TaaliTransfer_txtCommodityPermitTaaliTransfer_CommodityNumber_2 | 50 | sabteTaliDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # click | id=MainContent_uTaaliTransfer_btnPrint |  | sabteTaliDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # waitForElementPresent | id=MainContent_InnerMainContent_btnAddTaaliTransfer |  | sabteTaliDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # click | id=MainContent_InnerMainContent_btnAddTaaliTransfer |  | sabteTaliDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # waitForElementPresent | id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_txtContainerName |  | sabteTaliDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # waitForElementPresent | id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_txtContainerName |  | sabteTaliDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_txtContainerName | IRSU1234-5 | sabteTaliDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # click | id=MainContent_uTaaliTransfer_btnCancel |  | sabteTaliDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # waitForElementPresent | id=MainContent_InnerMainContent_btnAddTaaliTransfer |  | sabteTaliDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # waitForElementNotPresent | //table[@id='MainContent_InnerMainContent_GridviewTaalis']/tbody/tr[3]/td[2] |  | sabteTaliDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # click | id=MainContent_InnerMainContent_btnAddTaaliTransfer |  | sabteTaliDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # waitForText | id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_GridViewCommodityPermit_TaaliTransfer_lblUnusedOfCommodity_0 | 100 | sabteTaliDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # waitForText | id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_GridViewCommodityPermit_TaaliTransfer_lblUnusedOfCommodity_1 | 100 | sabteTaliDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_GridViewCommodityPermit_TaaliTransfer_txtCommodityPermitTaaliTransfer_CommodityNumber_0 | 100 | sabteTaliDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_GridViewCommodityPermit_TaaliTransfer_txtCommodityPermitTaaliTransfer_CommodityNumber_1 | 100 | sabteTaliDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_GridViewCommodityPermit_TaaliTransfer_txtCommodityPermitTaaliTransfer_CommodityNumber_2 | 100 | sabteTaliDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_uTaaliTransfer_UCommodityPermitTaaliTransfer_txtContainerName | IRSU2222-2 | sabteTaliDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # click | id=MainContent_uTaaliTransfer_btnPrint |  | sabteTaliDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # waitForElementPresent | //table[@id='MainContent_InnerMainContent_GridviewTaalis']/tbody/tr[3]/td[2] |  | sabteTaliDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # click | id=MainContent_InnerMainContent_btnSubmit |  | sabteTaliDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # waitForText | id=MainContent_lblInfo | اطلاعات قبض انبار دستی با موفقیت ثبت گردید. | sabteTaliDasti | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # setSpeed | 5000 |  | getGhabzNum | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # setSpeed | 500 |  | getGhabzNum | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # click | link=ویرایش یا حدف قبض انبار |  | getGhabzNum | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # waitForElementPresent | id=MainContent_uSearchWarehouseBill_txtWarehouseBill |  | getGhabzNum | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # type | id=MainContent_uSearchWarehouseBill_txtWarehouseBillNumber | 1394000${tozin1} | getGhabzNum | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # click | id=MainContent_uSearchWarehouseBill_btnSearch |  | getGhabzNum | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # waitForElementPresent | id=MainContent_uWarehouseBillGridView_GridviewWarehousebill_lblId_0 |  | getGhabzNum | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # waitForElementNotPresent | id=MainContent_uWarehouseBillGridView_GridviewWarehousebill_lblId_1 |  | getGhabzNum | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # setSpeed | 1000 |  | getGhabzNum | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # setSpeed | 500 |  | getGhabzNum | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # storeText | id=MainContent_uWarehouseBillGridView_GridviewWarehousebill_lblId_0 | ghabzNo | getGhabzNum | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519'}


        # label | start |  | tavalLoginEpl | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519','ghabzNo': '203594'}


        # setSpeed | 2000 |  | tavalLoginEpl | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519','ghabzNo': '203594'}


        # open | http://test.iais.co:8080/PackingList-Rajae/ |  | tavalLoginEpl | {'notlogined2': 'true','barnameNum': '1468130937273','baygani': '60902','tozin1': '1468130937519','ghabzNo': '203594'}
