import gzip
from io import BytesIO
from urllib.parse import urlparse, urljoin

from StressTest.StressTestGenerator import StressTestGenerator


class HttpPacket:
    def __init__(self, data):
        self.data = data

        self.headers = {}
        self.url_bytes = None
        self.line0_parts = None

        lines = self.data.split(b"\r\n")
        self.line0 = lines[0]

        for i in range(1, len(lines)):
            line = lines[i]
            if line == b"":
                self.body_bytes = b'\r\n'.join(lines[i + 1:])
                break
            header_parts = line.split(b': ', maxsplit=1)
            self.headers[header_parts[0]] = header_parts[1]

        self.body = self.body_bytes
        if self.contains_header(b"Transfer-Encoding", b"chunked") and self.body_bytes:
            chunks = []
            remain_part = self.body_bytes

            while True:
                next_crlf = remain_part.index(b'\r\n')

                chunck_size_line = remain_part[:next_crlf]
                remain_part = remain_part[next_crlf + 2:]

                chunck_size = int(chunck_size_line, 16)
                if chunck_size == 0:
                    break

                chunk = remain_part[:chunck_size]
                remain_part = remain_part[chunck_size + 2:]

                chunks.append(chunk)

            self.body = b''.join(chunks)

        if self.contains_header(b"Content-Type", b"text") or self.contains_header(b"Content-Type",
                                                                                  b"x-www-form-urlencode"):
            if self.contains_header(b"Content-Encoding", b"gzip"):
                self.body = self.un_gzip(self.body)

            self.body = self.body.decode()

    #
    # check if we have key in headers and value contains value_part
    #
    def contains_header(self, key, value_part):
        return key in self.headers and value_part in self.headers[key]

    def adjust_content_length(self):
        data_length = len(self.body_bytes)

        if data_length:
            self.headers[b"Content-Length"] = str(data_length).encode()

    @staticmethod
    def un_gzip(gziped_data):
        out = BytesIO(gziped_data)
        with gzip.GzipFile(fileobj=out, mode="r") as gzip_file:
            return gzip_file.read()

    def send(self):
        response_data = []
        StressTestGenerator.send_request(self.generate_data(), self.get_url_bytes().decode(),
                                         lambda d: response_data.append(d))
        response_data = b"".join(response_data)

        return response_data

    def get_url_bytes(self):
        if not self.url_bytes:
            self.url_bytes = self.get_line0_parts()[1]
        return self.url_bytes

    def get_line0_parts(self):
        if not self.line0_parts:
            self.line0_parts = self.line0.split(b' ', maxsplit=2)
        return self.line0_parts

    def generate_data(self):
        headers_part = b''
        for header in self.headers:
            headers_part += header + b': ' + self.headers[header] + b'\r\n'

        return self.line0 + b'\r\n' + headers_part + b'\r\n' + self.body_bytes

    def update_body(self):
        self.body_bytes = self.body.encode()

    def get_request_type(self):
        return self.get_line0_parts()[0]

    def get_url(self):
        return urlparse(self.get_url_bytes())

    def set_url(self, new_url, new_method=None):
        self.line0_parts = self.get_line0_parts()

        if new_method:
            self.line0_parts[0] = new_method

        self.line0_parts[1] = urljoin(self.line0_parts[1], new_url)
        self.line0 = b' '.join(self.line0_parts)
        self.url_bytes = None

    def get_method(self):
        return self.get_line0_parts()[0]
