# -*- coding: utf-8 -*-
import re
import types
import urllib
from os.path import join
from urllib.parse import urlparse

import execjs

from StressTest.HttpPacket import HttpPacket
from StressTest.InMemoryZip import InMemoryZip
from Utils.Utils import recreate_folder, warning, error, blue

log_level_debug = 1
log_level_info = 2
log_level_warning = 3
log_level_error = 4


class StressTest:
    def __init__(self, index):
        self.index = index
        self.cookies = {}
        self.params = {}
        self.selects = {}
        self.last_text_data = ""
        self.last2_text_data = ""
        self.last3_text_data = ""
        self.last_text_response = b""
        self.check_in_next_pattern = None
        self.files = {}
        self.last_has_redirect = False
        self.log_level = log_level_debug
        self.text_responses = []
        self.get_params = {}
        self.result_folder = '/stg/result/'
        self.recording_time_values = {}
        self.auto_redirected_urls = []
        self.current_test_case = ""

        self.make_methods_self_informing()

    def make_methods_self_informing(self):
        for method_name in dir(self):
            if not method_name.startswith("__") and method_name != self.start_test.__func__.__name__:
                method = getattr(self, method_name)
                if callable(method):
                    if type(method) != types.FunctionType and \
                                    method.__func__.__code__.co_filename != __file__:  # get only child method
                        setattr(self, method_name, self.get_self_informing_method(method))

    def get_self_informing_method(self, method):

        def new_method(*args, **kargs):
            name = method.__func__.__name__
            self.current_test_case = name
            self.inform("", log_level_info)
            self.inform(blue("starting {}".format(name)), log_level_info)
            return method.__call__(*args, **kargs)

        return new_method

    def inform(self, msg, level):
        if level >= self.log_level:
            if level == log_level_warning:
                msg = warning(msg)
            if level == log_level_error:
                msg = error(msg)

            print("[{}]: {}".format(self.index, msg))

    def send(self, request_data, *params):
        request_data = StressTest.replace_params(request_data, params)
        request = HttpPacket(request_data)

        self.send_packet(request)

    def send_packet(self, request):
        self.change_get_params(request)

        if not self.last_has_redirect and self.auto_redirected_urls and request.contains_header(b"Accept",
                                                                                                b"text/html"):
            current_url = request.get_url_bytes().split(b"?", maxsplit=1)[0]
            expected_url = self.auto_redirected_urls[0].split(b"?", maxsplit=1)[0]
            if expected_url == current_url:
                self.inform("ignore {}".format(current_url), log_level_debug)
                self.auto_redirected_urls.pop(0)
                return
            else:
                self.inform("unexpected url {}, but we expect {}".format(current_url, expected_url), log_level_warning)

        self.set_cookies(request)
        self.set_params(request)
        request.headers[b"Connection"] = b"close"
        request.adjust_content_length()

        response_data = request.send()
        response = HttpPacket(response_data)

        self.search_for_cookie_set(response)
        self.last_has_redirect = b"Location" in response.headers

        if response.contains_header(b"Content-Type", b"text") and response.get_line0_parts()[1] != b"404":
            self.search_for_inputs(response)

            self.last_text_response = response_data
            self.last3_text_data = self.last2_text_data
            self.last2_text_data = self.last_text_data
            self.last_text_data = response.body
            self.save_text_response(request)

            if self.check_in_next_pattern:
                try:
                    self.search_for_pattern(self.last_text_response, self.check_in_next_pattern,
                                            self.last_text_data, self.check_in_next_var_names)
                    self.check_in_next_pattern = None
                except Exception as ex:
                    if not self.last_has_redirect:
                        raise ex
        self.last_response = response_data
        self.last_data = response.body
        self.search_for_downloaded_file(response)

        if self.last_has_redirect:
            redirect_to = request
            redirect_to.set_url(response.headers[b"Location"], b"GET" if redirect_to.get_method() == b"POST" else None)
            redirect_to.body_bytes = b''
            if b"Content-Type" in redirect_to.headers:
                redirect_to.headers.pop(b"Content-Type")
            if b"Content-Length" in redirect_to.headers:
                redirect_to.headers.pop(b"Content-Length")

            redirect_to_url = redirect_to.get_url_bytes()
            self.auto_redirected_urls.append(redirect_to_url)
            self.inform("auto redirect to {}".format(redirect_to_url), log_level_debug)
            self.send_packet(redirect_to)

    def search_for_downloaded_file(self, response):
        if response.contains_header(b"content-disposition", b"attachment;"):
            disposition = response.headers[b"content-disposition"].decode()
            file_name_match = re.match(".*filename=(.*)", disposition)
            if file_name_match:
                download_file_name = file_name_match.group(1)
                self.download(download_file_name)
            else:
                self.inform("found file but could not get file name", log_level_warning)

    def change_get_params(self, request):
        if self.get_params:
            url = request.get_url_bytes()
            for get_param in self.get_params:
                new_value = self.get_params[get_param]
                param_name = get_param.split(b'=')[0]+b'='
                if param_name in url:
                    # url = url.replace(get_param, new_value)
                    url = re.sub(param_name+b'[^\s&]*', new_value, url)
                    self.inform("change GET param to {}".format(new_value), log_level_debug)
            request.set_url(url)

    def save_text_response(self, request):
        url = request.get_url()
        url_mark = url.path.strip(b'/')
        url_last_slash = url_mark.rfind(b'/')
        if url_last_slash != -1:
            url_mark = url_mark[url_last_slash + 1:]
            url_mark = url_mark[-30:]
        if not url_mark:
            url_mark = url.netloc
        url_mark += b'_' + url.query[:30]

        response_name = "{:04d}-{}@{}".format(len(self.text_responses), self.current_test_case, url_mark.decode())

        self.text_responses.append((response_name, self.last_text_data))

    def check_for(self, pattern, var_names=[]):
        try:
            self.search_for_pattern(self.last_text_response, pattern,
                                    self.last_text_data + self.last2_text_data + self.last3_text_data,
                                    var_names)
        except:
            self.check_in_next_pattern = pattern
            self.check_in_next_var_names = var_names

    def search_for_pattern(self, h, pattern, r, var_names):
        if pattern:
            space_handling_pattern = pattern.replace(" ", "\s*")
            match = re.match(".*?(" + space_handling_pattern + ").*?", r, re.DOTALL)
            if not match:
                raise AssertionError("{} not found".format(pattern))
            else:
                if not var_names:
                    self.inform("seen '{}'".format(match.group(1)), log_level_info)

                for i in range(len(var_names)):
                    var_name = var_names[i]
                    if var_name:
                        value = match.group(i + 2)
                        self.__dict__[var_name] = value
                        self.inform("set {}={}".format(var_name, value), log_level_debug)

    def start_test(self):
        pass

    def search_for_cookie_set(self, response):
        if b'Set-Cookie' in response.headers:
            cookie_parts = response.headers[b'Set-Cookie'].split(b"; ")

            cookie_key_value = cookie_parts[0].split(b"=", maxsplit=1)
            cookie_key = cookie_key_value[0]
            cookie_value = cookie_key_value[1]

            options = {}
            for cookie_option in cookie_parts[1:]:
                option_key_value = cookie_option.split(b"=", maxsplit=1)
                option_key = option_key_value[0]

                if len(option_key_value) > 1:
                    option_value = option_key_value[1]
                else:
                    option_value = None

                options[option_key] = option_value

            path = options[b"Path"] if b"Path" in options else b""
            self.cookies[(path, cookie_key)] = cookie_value
            self.inform("set Cookie({}) {}={}".format(path, cookie_key, cookie_value), log_level_debug)

    def set_cookies(self, request):
        request_path = request.get_url().path

        cookies_value = b""
        for path, cookie in self.cookies:
            if request_path.startswith(path):
                cookie_value = self.cookies[(path, cookie)]
                if cookies_value:
                    cookies_value += b"; "

                cookies_value += b"%s=%s" % (cookie, cookie_value)

        if cookies_value:
            request.headers[b"Cookie"] = cookies_value

    @staticmethod
    def replace_params(request_data, params):
        for param in params:
            request_data = request_data.replace(b'{}', str(param).encode(), 1)

        return request_data

    def search_for_inputs(self, response):
        if response.body.find("<form ") != -1:
            # for param in self.params:
            #     self.inform("unused param {}".format(param), log_level_warning)
            self.params.clear()
            self.selects.clear()
            self.inform("", log_level_debug)
            self.inform("clear params", log_level_debug)

        for input_match in re.finditer('<input (.*?)>', response.body, re.DOTALL):
            input_attrs = input_match.group(1).replace('\n', '')
            if "type=\"radio\"" in input_attrs:
                if "checked=\"checked\"" not in input_attrs:
                    continue
            name_match = re.match(r'.*name=\"(.*?)\".*', input_attrs, re.DOTALL)
            if name_match:
                name = name_match.group(1)
                if input_attrs.__contains__("type=\"checkbox\""):
                    if input_attrs.__contains__("checked=\"checked\""):
                        value = "on"
                    else:
                        value = "off"  # needs to be tested
                else:
                    value_match = re.match('.*value=\"(.*?)\".*', input_attrs, re.DOTALL)
                    if value_match:
                        value = value_match.group(1)
                    else:
                        value = ""

                self.params[name] = value
                self.inform('set input param {}={}'.format(name, value), log_level_debug)

        for text_area_match in re.finditer('<textarea (.*?)>(.*?)</textarea>', response.body, re.DOTALL):
            text_area_attrs = text_area_match.group(1).replace('\n', '')

            name_match = re.match(r'.*name=\"(.*?)\".*', text_area_attrs, re.DOTALL)
            if name_match:
                name = name_match.group(1)
                value = text_area_match.group(2)

                self.params[name] = value
                self.inform('set textarea param {}={}'.format(name, value), log_level_debug)

        for select_match in re.finditer('<select[^>]*? name=\"(.*?)\"[^>]*?>(.*?)</select>', response.body, re.DOTALL):
            select_name = select_match.group(1)
            select_html = select_match.group(2)
            if "</select>" in select_html:
                continue

            self.selects[select_name] = {}

            for option_match in re.finditer('<option (.*?)>(.*?)</option>', select_html):
                option_attrs = option_match.group(1)
                option_label = option_match.group(2)

                value_match = re.match('.*value=\"(.*?)\".*', option_attrs, re.DOTALL)
                if value_match:
                    option_value = value_match.group(1)
                else:
                    option_value = ""
                    self.inform("no value for option {}".format(option_match.group(0)), log_level_warning)

                if 'selected="selected"' in option_attrs:
                    self.params[select_name] = option_value
                    self.inform('set select param {}={} ({})'.format(select_name, option_value, option_label),
                                log_level_debug)

                self.selects[select_name][option_label] = option_value

    def set_params(self, request):
        if request.get_request_type() == b"POST":
            new_body = None

            if request.contains_header(b"Content-Type", b"multipart/form-data;"):
                new_body = self.set_upload_params(request)
            elif type(request.body) is str:
                new_body = self.set_normal_params(request)
            else:
                self.inform("unsupported request", log_level_warning)

            if new_body:
                request.body_bytes = new_body

    def set_normal_params(self, request):
        params_in_request = request.body_bytes.split(b"&")
        new_body = b""
        for param_in_request in params_in_request:
            param_in_request_key, param_in_request_value = param_in_request.split(b"=", maxsplit=1)
            normal_param_key = urllib.parse.unquote_plus(param_in_request_key.decode())

            if new_body:
                new_body += b"&"

            if normal_param_key in self.params:
                new_value = urllib.parse.quote_plus(self.params[normal_param_key])
                new_body += param_in_request_key + b"=" + new_value.encode()
                # self.params.pop(normal_param_key)
                self.inform("use {}={}".format(normal_param_key, new_value), log_level_debug)
            else:
                self.inform("unset param {}".format(normal_param_key), log_level_warning)
                new_body += param_in_request
        return new_body

    def set_upload_params(self, request):
        boundary_start_index = request.headers[b"Content-Type"].index(b"boundary=") + 9
        boundary = b'--' + request.headers[b"Content-Type"][boundary_start_index:]
        parts = request.body_bytes.split(boundary)
        for i in range(len(parts)):
            if not parts[i] or parts[i] == b'--\r\n':
                continue

            param_match = re.match(br'\r\nContent-Disposition: form-data; name=\"(.*?)\"\r\n\r\n(.*?)\r\n',
                                   parts[i])
            if param_match:
                param_name = param_match.group(1).decode()
                if param_name in self.params:
                    value = self.params[param_name]
                    parts[i] = parts[i][:parts[i].index(b'\r\n\r\n')] + b'\r\n\r\n' + value.encode() + b'\r\n'
                    self.params.pop(param_name)
                    self.inform("use {}={}".format(param_name, value), log_level_debug)
                else:
                    self.inform("unset upload param {}".format(param_name), log_level_warning)
            else:
                file_match = re.match(
                    br'\r\nContent-Disposition: form-data; name=\"(.*?)\"; filename=\"(.*?)\"\r\nContent-Type: (.*)\r\n\r\n(.|\n)*?\r\n',
                    parts[i])
                if file_match:
                    param_name = file_match.group(1).decode()
                    file_name = file_match.group(2).decode()

                    if param_name in self.params:
                        value = self.params[param_name]
                        last_slash = value.rfind('\\')
                        if last_slash != -1:
                            new_file_name = value[last_slash + 1:]
                        else:
                            new_file_name = value
                        parts[i] = parts[i].replace(b'filename="' + file_name.encode() + b'"',
                                                    b'filename="' + new_file_name.encode() + b'"')
                        file_name = new_file_name
                        self.params.pop(param_name)
                        self.inform("use {}={}".format(param_name, value), log_level_debug)
                    else:
                        self.inform("unset filename param {}".format(param_name), log_level_warning)

                    if file_name in self.files:
                        upload_content = self.files[file_name]
                        self.inform("found upload file {}".format(file_name), log_level_debug)

                        content_start_index = parts[i].index(b'\r\n\r\n') + 4
                        parts[i] = parts[i][:content_start_index] + upload_content + b'\r\n'
                        self.inform("change upload file to {}".format(upload_content), log_level_debug)
                    else:
                        self.inform("found upload file {}, but it is not in downloaded list".format(file_name),
                                    log_level_warning)
                else:
                    self.inform("unknown part in upload request", log_level_warning)
        return boundary.join(parts)

    def selenium_run(self, command, target, value):
        if command == "storeElementPresent":
            css_selector_match = self.match_css_selector(target)
            if css_selector_match or "id" in target:
                if "id" in target:
                    element_name = target.replace("id=", "")
                else:
                    element_name = css_selector_match.group(3)
                if element_name in self.last_text_data + self.last2_text_data + self.last3_text_data :
                    self.__dict__[value] = True
                    self.inform("set {}={}".format(value, "True"), log_level_debug)
                else:
                    self.__dict__[value] = False
                    self.inform("set {}={}".format(value, "False"), log_level_debug)

        if command == "type" or command == "select":
            selector_match = re.match("css=(input|select|textarea)\[name=\'(.*)\'\]", target)
            if selector_match:
                name = selector_match.group(2)
                if command == "select":
                    if value.startswith("label="):
                        value = value[6:]
                    if name in self.selects:
                        if value in self.selects[name]:
                            value = self.selects[name][value]
                        else:
                            self.inform("unexpected option for select with name {}: {}".format(name, value),
                                        log_level_warning)
                    else:
                        self.inform("unexpected select with name {}".format(name), log_level_warning)

                value = self.inject_stored_vars(value)

                self.params[name] = value
                self.inform('change param {}={}'.format(name, value), log_level_debug)
            else:
                self.inform("unsupported selector: {}".format(target), log_level_warning)

        if command == "waitForText":
            value = self.inject_stored_vars(value)
            self.check_for(value.replace("*", "[^<>]*?"))

        if command == "waitForElementPresent":
            css_selector_match = self.match_css_selector(target)
            attribute_value = None
            if css_selector_match:
                attribute_value = css_selector_match.group(3)
            else:
                general_match = re.match("[^\[\]=]*=([^\[\]=]*)", target)
                if general_match:
                    attribute_value = general_match.group(1)

            if attribute_value:
                if self.search_in_text_responses(attribute_value, 3):
                    self.inform("seen {}".format(attribute_value), log_level_info)
                else:
                    self.check_for(attribute_value)
            else:
                self.inform("unsupported selector: {}".format(target), log_level_warning)

        if command == "waitForAttribute":
            target_parts = target.split("@", maxsplit=1)
            css_selector_match = self.match_css_selector(target_parts[0])
            if css_selector_match:
                attribute_value = css_selector_match.group(3)

                self.check_for("{}\"[^<>]*? {}=\"{}\"".format(attribute_value, target_parts[1], value))
            else:
                self.inform("unsupported selector: {}".format(target), log_level_warning)

        if command == "storeText":
            if "id=" in target:
                css_selector_match = re.match("id=(.*)", target)
                element_name = css_selector_match.group(1)
                self.check_for("{}\"[^<]*?>([^<]*?)<".format(element_name), [value])
            else:
                css_selector_match = self.match_css_selector(target)
                if css_selector_match:
                    element_name = css_selector_match.group(1)
                    attribute_value = css_selector_match.group(3)
                    self.check_for("{}\">([^<]*?)</{}>".format(attribute_value, element_name), [value])
                else:
                    if target == "css=body":
                        self.check_for("(.*)", [value])
                    else:
                        self.inform("unsupported selector: {}".format(target), log_level_warning)

        if command == "storeEval":
            for stored_vars_use_match in re.finditer(r"storedVars\[(\"|\')(.*?)(\"|\')\]", target):
                var_name = stored_vars_use_match.group(2)
                target = target.replace(stored_vars_use_match.group(0), "'{}'".format(self.__dict__[var_name]))

            target = self.inject_stored_vars(target)

            eval_result = str(execjs.eval(target))
            self.__dict__[value] = eval_result
            self.inform("set {}={}".format(value, eval_result), log_level_debug)

        if command == "open":
            for stored_vars_use_match in re.finditer("(\?|&)([^=&]+?=[^=&]*?(\$\{([^=&]*?)\})[^=&]*)", target):
                match_str = stored_vars_use_match.group(2)
                stored_var_str = stored_vars_use_match.group(3)
                var_name = stored_vars_use_match.group(4)

                recording_time_param = match_str.replace(stored_var_str, self.recording_time_values[var_name])
                new_param = match_str.replace(stored_var_str, self.__dict__[var_name])
                for var_match in re.finditer("(\$\{(([^=&]*?))\})", recording_time_param):
                    var_name = var_match.group(2)
                    var_full_name = var_match.group(1)
                    recording_time_param = recording_time_param.replace(var_full_name, self.recording_time_values[var_name])
                for var_match in re.finditer("(\$\{(([^=&]*?))\})", new_param):
                    var_name = var_match.group(2)
                    var_full_name = var_match.group(1)
                    new_param = new_param.replace(var_full_name, self.__dict__[var_name])
                self.get_params[recording_time_param.encode()] = new_param.encode()
                self.inform("set GET param {}".format(new_param), log_level_debug)

        if command == "storeTable":
            target_value = self.extract_target_value(target)
            self.__dict__[value] = target_value
            self.inform("set {}={}".format(value, target_value), log_level_debug)

        if command == "click":
            css_selector_match = self.match_css_selector(target)
            if css_selector_match:
                element_name = css_selector_match.group(3)
                if not self.search_for_click_input(element_name, self.last_text_data):
                    if not self.search_for_click_input(element_name, self.last2_text_data):
                        self.search_for_click_input(element_name, self.last3_text_data)

    def search_for_click_input(self, element_name, context):
        match = re.match(
            ".*?(<input[^>]*?name=\"([^\"]*?)\"[^>]*?value=\"(\d*?)\"[^>]*?{}[^>]*?/>).*?".format(element_name),
            context, re.DOTALL)
        if match:
            if "type=\"radio\"" in match.group(1):
                parameter_name = match.group(2)
                parameter_value = match.group(3)
                self.params[parameter_name] = parameter_value
                self.__dict__[parameter_name] = parameter_value
                self.inform("set {}={}".format(parameter_name, parameter_value), log_level_debug)
                return True
        return False

    def extract_target_value(self, target):
        target = target.replace("id=", "")
        table_y = target[target.__len__() - 3]
        table_x = target[target.__len__() - 1]
        table_name = target[0:target.__len__() - 4]
        self.check_for("{}\"[^<]*?>(.*?)</table>".format(table_name), ["table_content"])
        table_rows = self.table_content.split("</tr>")
        target_row = table_rows[int(table_y)].replace("<tr>", "")
        row_parts = target_row.split("</td>")
        target_value = row_parts[int(table_x)].replace("<td>", "")
        return target_value

    def match_css_selector(self, target):
        return re.match("css=(.*)\[(.*?)\=\'(.*)\'\]", target)

    def inject_stored_vars(self, expresion):
        for stored_vars_use_match in re.finditer("\$\{(.*?)\}", expresion):
            match_str = stored_vars_use_match.group(0)
            var_name = stored_vars_use_match.group(1)
            var_value = self.__dict__[var_name]
            expresion = expresion.replace(match_str, var_value)
        return expresion

    def selenium_var(self, name, value):
        self.recording_time_values[name] = value

    def zip(self, file_name, file_content):
        return InMemoryZip(None).add_file(file_name, file_content).close().get_bytes()

    def download(self, file_name):
        self.files[file_name] = self.last_data
        self.inform("set file content {}={}".format(file_name, self.last_data), log_level_debug)

    def save_text_responses_to_file(self):
        recreate_folder(self.result_folder)

        for name, response in self.text_responses:
            with open('{}.html'.format(join(self.result_folder, name.replace(":", "_"))), 'w') as file:
                file.write(response)

    def search_in_text_responses(self, str, limit):
        text_responses_size = len(self.text_responses)
        if text_responses_size < limit:
            limit = text_responses_size

        return [i for i in range(limit) if
                str in self.text_responses[text_responses_size - 1 - i][1]]
