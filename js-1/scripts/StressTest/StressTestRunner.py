import threading
import traceback


class StressTestRunner:
    def __init__(self, test):
        self.threads = []
        self.result = {}
        self.test = test

    def run_a_thread(self, index):
        agent = self.test(index)

        try:
            agent.start_test()
            self.add_result("done")
        except Exception as ex:
            agent.save_text_responses_to_file()

            self.add_result(agent.current_test_case)
            traceback.print_exc()

    def add_result(self, type):
        if type not in self.result:
            self.result[type] = 0
        self.result[type] += 1

    def run(self, number_of_threads):
        for i in range(number_of_threads):
            t = threading.Thread(target=self.run_a_thread, args=(i,))
            t.start()
            self.threads.append(t)

        for t in self.threads:
            t.join()

        print(self.result)
