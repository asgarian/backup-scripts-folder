class Logger:
    OFF = 0
    DEBUG_MODE = 1

    def __init__(self, mode):
        self.mode = mode

    def debug(self, message):
        if self.mode == Logger.DEBUG_MODE:
            print("# " + message)
