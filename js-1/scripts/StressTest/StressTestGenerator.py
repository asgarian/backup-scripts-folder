import ast
import socket
from _socket import SHUT_RD
from urllib.parse import urlparse

from StressTest.HttpServer import HttpServer
from StressTest.Logger import Logger
from Utils.Utils import synchronized
from Utils.jfabric import local


class StressTestGenerator:
    sockets = []

    def __init__(self, host, port):
        self.port = port
        self.logger = Logger(Logger.OFF)
        self.last_selenium_test_case = ""
        self.selenium_test_cases = []
        self.test_cases_counter = {}
        self.last_selenium_vars = {}
        self.server = HttpServer(host, port, self.server_on_receive,
                                 ["test.iais.co",
                                  "172.16.111.8",
                                  "172.16.111.51",
                                  "172.16.111.85", ],
                                 is_multi_thread=True,
                                 logger=self.logger)
        self.file_path = "/stg/test-{}.py".format(port)
        local("rm -f {}".format(self.file_path))
        self.indent = 0

        self.add_line("# -*- coding: utf-8 -*-")
        self.add_line("from StressTest.StressTest import StressTest")
        self.add_line("from goto import with_goto")
        self.add_line("")
        self.add_line("")
        self.add_line("class Test(StressTest):")
        self.add_line("def __init__(self, index):")
        self.add_line("super().__init__(index)")

    def run(self):
        try:
            self.server.run()
        except KeyboardInterrupt:
            self.add_line("")
            self.indent -= 1
            self.add_line("def start_test(self):")
            for selenium_test_case in self.selenium_test_cases:
                self.add_line("self.{}()".format(selenium_test_case))

    def server_on_receive(self, current_socket, received_data, http_parser):
        if "172.16.111.8:" in http_parser.get_headers()["Host"]:
            self.selenium_server_on_receive(current_socket, received_data, http_parser)
        else:
            self.proxy_server_on_receive(current_socket, received_data, http_parser)

    def proxy_server_on_receive(self, current_socket, received_data, http_parser):
        self.add_line('')
        self.add_line('self.send({})\n'.format(received_data))

        StressTestGenerator.send_request(received_data, http_parser.get_url(),
                                         lambda data: current_socket.send(data))

    @staticmethod
    def normalize(str):
        return str.replace("\\", "\\\\").replace("\"", "\\\"")

    def selenium_server_on_receive(self, current_socket, received_data, http_parser):
        body = http_parser.recv_body().decode()

        lines = body.split("\\n")

        changed_selenium_vars = self.get_changed_selenium_vars(lines[4])
        for changed_selenium_var in changed_selenium_vars:
            self.add_line("self.selenium_var(\"{}\", \"{}\")".format(changed_selenium_var,
                                                                     self.normalize(
                                                                         changed_selenium_vars[changed_selenium_var])))

        current_slenium_test_case = lines[3].replace("-", "_")
        if self.last_selenium_test_case != current_slenium_test_case:
            current_slenium_test_case_with_postfix = current_slenium_test_case + self.get_test_case_index_postfix(
                current_slenium_test_case)
            self.add_line("")
            self.add_line("@with_goto")
            self.add_line("def {}(self):".format(current_slenium_test_case_with_postfix))
            self.last_selenium_test_case = current_slenium_test_case
            self.selenium_test_cases.append(current_slenium_test_case_with_postfix)

        self.add_line("\n\n")
        # self.add_line("# {}".format(" | ".join(lines)))
        if lines[0] == "stg":
            self.add_line(lines[1])
        elif lines[0] == "gotoIf":
            self.add_line(
                "if {}:".format(self.normalize_condition(self.normalize(lines[1]))))
            self.add_line("    goto .{}".format(self.normalize(lines[2])))
        elif lines[0] == "label":
            self.add_line("label .{}".format(self.normalize(lines[1])))
        else:
            self.add_line("")
            self.add_line("self.selenium_run(\"{}\",".format(self.normalize(lines[0])))
            self.add_line("                  \"{}\",".format(self.normalize(lines[1])))
            self.add_line("                  \"{}\")".format(self.normalize(lines[2])).replace("exact:", ""))

        current_socket.sendall("HTTP/1.0 200 OK\r\n\r\n".encode("utf-8"))
        # the following line is very important because we want to sync with selenium and
        # tcp by default wait for next request to send response
        current_socket.shutdown(SHUT_RD)

    def normalize_condition(self, condition):
        new_condition = ""
        condition = condition.replace("[", "").replace("]", "").replace("'", "").replace(
            "storedVars", "self.").replace("true", "True").replace("false", "False").replace(
            "\\", "")
        condition_part = condition.split("==", maxsplit=1)
        condition_part[0] += "=="
        for part in condition_part:
            if "storedVars" in part:
                new_condition += part.replace("\"", "")
        return new_condition

    def get_changed_selenium_vars(self, selenium_vars_str):
        current_selenium_vars = ast.literal_eval(selenium_vars_str.replace("\\", "\\\\"))
        changed_selenium_vars = {}

        for current_selenium_var in current_selenium_vars:
            if current_selenium_var not in self.last_selenium_vars or current_selenium_vars[current_selenium_var] != \
                    self.last_selenium_vars[current_selenium_var]:
                changed_selenium_vars[current_selenium_var] = current_selenium_vars[current_selenium_var]

        self.last_selenium_vars = current_selenium_vars

        return changed_selenium_vars

    @staticmethod
    def extract_destination_address(url_str):
        if "://" not in url_str:
            url_str = "http://" + url_str

        url = urlparse(url_str)

        if url.port:
            port = url.port
        else:
            if "http://" in url_str:
                port = 80
            elif "ftp://" in url_str:
                port = 21
            else:
                raise Exception("Unsupported protocol for {}".format(url_str))

        return url.hostname, port

    @staticmethod
    def send_request(request, url, on_receive):
        destination_ip, destination_port = StressTestGenerator.extract_destination_address(url)

        try:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as destination_socket:
                destination_socket.connect((destination_ip, destination_port))

                if destination_ip == "172.16.111.85" and destination_port == 9090:
                    destination_socket.settimeout(3)
                    request = request.replace(b"http://172.16.111.85:9090", b"")

                destination_socket.send(request)

                while True:
                    response_data = destination_socket.recv(HttpServer.buffer_size)

                    if response_data:
                        on_receive(response_data)
                    else:
                        break
        except Exception as ex:
            print("Error: {} while sending {}".format(ex, request))

    @synchronized
    def add_line(self, line):
        if line.startswith("@"):
            self.indent -= 1

        if self.indent:
            indented_line = ("{:" + str(self.indent * 4) + "}{}").format("", line)
        else:
            indented_line = line

        with open(self.file_path, "a") as file:
            file.write(indented_line + "\n")

        print(indented_line)

        if line.startswith("def "):
            self.indent += 1

        if line.startswith("class "):
            self.indent += 1

    def get_test_case_index_postfix(self, test_case):
        if test_case in self.test_cases_counter:
            self.test_cases_counter[test_case] += 1
            return "_{}".format(self.test_cases_counter[test_case])
        else:
            self.test_cases_counter[test_case] = 1
            return ""
