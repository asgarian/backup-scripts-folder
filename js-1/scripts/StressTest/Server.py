import socket
import select
import time


# Changing the buffer_size and delay, you can improve the speed and bandwidth.
# But when buffer get to high or delay go too down, you can broke things


class Server:
    buffer_size = 65536
    delay = 0.0001

    def __init__(self, host, port, on_receive):
        self.port = port
        self.listen_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.listen_socket.bind((host, port))
        self.listen_socket.listen(200)
        self.on_receive = on_receive
        self.sockets = []

    def run(self):
        self.sockets.append(self.listen_socket)

        while True:
            time.sleep(self.delay)
            ss = select.select
            input_ready, output_ready, except_ready = ss(self.sockets, [], [])

            for current_socket in input_ready:
                # t = threading.Thread(target=self.handle_socket_operation, args=(current_socket,))
                # t.start()
                self.handle_socket_operation(current_socket)

    def handle_socket_operation(self, current_socket):
        if current_socket == self.listen_socket:
            self.on_accept()
        else:
            # received_data = current_socket.recv(self.buffer_size)
            # if received_data:
            #     if len(received_data) == self.buffer_size:
            #         raise Exception("buffer size exceed")
            #     self.on_receive(current_socket, received_data)
            # else:
            #     self.on_close(current_socket)

            current_socket.setblocking(0)
            received_datas = b''
            while True:
                try:
                    received_data = current_socket.recv(self.buffer_size)
                except:
                    break
                if not received_data:
                    break
                received_datas += received_data
            current_socket.setblocking(1)

            if received_datas:
                self.on_receive(current_socket, received_datas)
            else:
                self.on_close(current_socket)

    def on_accept(self):
        new_client_socket, new_client_addr = self.listen_socket.accept()
        self.sockets.append(new_client_socket)

    def on_close(self, current_socket):
        self.sockets.remove(current_socket)
        current_socket.close()
