import zipfile

from io import BytesIO


class InMemoryZip(object):
    def __init__(self, content):
        # Create the in-memory file-like object
        self.in_memory_data = BytesIO(content)

        self.in_memory_zip = zipfile.ZipFile(
            self.in_memory_data, "a", zipfile.ZIP_DEFLATED, False)
        self.in_memory_zip.debug = 3

    def add_file(self, file_name, file_content):
        self.in_memory_zip.writestr(file_name, file_content)
        return self

    def close(self):
        # Mark the files as having been created on Windows so that
        # Unix permissions are not inferred as 0000
        for file in self.in_memory_zip.filelist:
            file.create_system = 0

        self.in_memory_zip.close()
        return self

    def get_bytes(self):
        return self.in_memory_data.getvalue()

    def write_to_file(self, file_path):
        with open(file_path, 'wb') as f:
            f.write(self.in_memory_data.getvalue())

    def get_content(self):
        return {name: self.in_memory_zip.read(name) for name in self.in_memory_zip.namelist()}


# Sample Use
if __name__ == "__main__":
    imz = InMemoryZip(
        b'PK\x03\x04\x14\x00\x00\x00\x08\x00\xb3\x93\xf2H\xc2A$5\x05\x00\x00\x00\x03\x00\x00\x00\x08\x00\x00\x00test.txtKLJ\x06\x00PK\x01\x02\x14\x00\x14\x00\x00\x00\x08\x00\xb3\x93\xf2H\xc2A$5\x05\x00\x00\x00\x03\x00\x00\x00\x08\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80\x01\x00\x00\x00\x00test.txtPK\x05\x06\x00\x00\x00\x00\x01\x00\x01\x006\x00\x00\x00+\x00\x00\x00\x00\x00')

    print(imz.get_content())

    imz.add_file("test2.txt", b'def')

    print(imz.get_content())
    imz.close()
    print(imz.get_bytes())
