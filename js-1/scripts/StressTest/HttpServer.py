from _socket import SHUT_RD
import socket
import select
import threading
import time


# Changing the buffer_size and delay, you can improve the speed and bandwidth.
# But when buffer get to high or delay go too down, you can broke things
from http_parser.parser import HttpParser


class HttpServer:
    buffer_size = 65536
    delay = 0.0001

    def __init__(self, host, port, on_receive, white_list, is_multi_thread, logger):
        self.host = host
        self.port = port
        self.on_receive = on_receive
        self.sockets = []
        self.receiving_sockets = []
        self.white_list = white_list
        self.is_multi_thread = is_multi_thread
        self.parsers = {}
        self.logger = logger

        self.listen_socket = self.create_socket(port)
        self.sockets.append(self.listen_socket)

    def create_socket(self, port):
        listen_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        listen_socket.bind((self.host, port))
        listen_socket.listen(200)

        return listen_socket

    def run(self):
        while True:
            time.sleep(self.delay)
            ss = select.select
            input_ready, output_ready, except_ready = ss(self.sockets, [], [])

            for current_socket in input_ready:
                self.handle_a_socket_operation(current_socket)

    def handle_a_socket_operation(self, current_socket):
        if current_socket == self.listen_socket:
            # open a new connection
            new_client_socket, new_client_addr = self.listen_socket.accept()
            self.sockets.append(new_client_socket)
        else:
            parser, received_data = self.receive_a_complete_http_request(current_socket)

            if received_data:
                if [allowed_url for allowed_url in self.white_list if allowed_url in parser.get_headers()["Host"]]:
                    if self.is_multi_thread:
                        t = threading.Thread(target=self.on_receive, args=(current_socket, received_data, parser))
                        t.start()
                    else:
                        self.on_receive(current_socket, received_data, parser)
                else:
                    self.logger.debug("{} not in white list".format(parser.get_headers()["Host"]))
                    current_socket.sendall("HTTP/1.0 403 FORBIDDEN\r\n\r\n".encode("utf-8"))
                    current_socket.shutdown(SHUT_RD)
            else:
                # close the connection
                if current_socket in self.sockets:
                    self.sockets.remove(current_socket)
                current_socket.close()

    def receive_a_complete_http_request(self, current_socket):
        parser = HttpParser()
        complete_data = b''

        try:
            while True:
                data = current_socket.recv(self.buffer_size)
                self.logger.debug("received {}".format(data))

                if not data:
                    break
                complete_data += data

                received_len = len(data)
                parsed_len = parser.execute(data, received_len)
                if parsed_len != received_len:
                    raise Exception(
                        "parsed_len({}) != received_len({}) for {}".format(parsed_len, received_len, complete_data))

                if parser.is_message_complete():
                    break

            self.logger.debug("received complete http request {}".format(complete_data))

            return parser, complete_data
        except:
            return None, None
