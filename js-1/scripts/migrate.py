from Utils.Utils import update_last_stable_build_links
from Utils.jfabric import local

update_last_stable_build_links()

local("sudo rm -r /configs/selenium-agent")
local("sudo cp -r /js/0/configs/selenium-agent /configs/")
local("sudo cp -r /configs/* /js/0/configs/")

local("sudo sed -i 's/sudo //g' /js/0/configs/Accs/config.sh")
