#
# gateway of jenkins: every things starts from here
#
import inspect
import re
import sys
import time

from Build import AutAgent
from Build.Build import Build
from Build.BuildInfo import *
from Build.PostBuild import PostBuild
from Build.PreBuild import PreBuild
from Deploy.DeployASP2 import DeployAsp2
from Deploy.DeployApache import DeployApache
from Deploy.DeployAsp import DeployAsp
from Deploy.DeployInfo import DeployInfo
from Deploy.DeployMasterAgent import DeployMasterAgent
from Deploy.DeployMaven import DeployMaven
from Deploy.DeployPackage import DeployPackage
from Deploy.DeployProduction import DeployProduction
from Deploy.DeployTest import DeployTest
from Deploy.MultiDeployTrigger import MultiDeployTrigger
from Monitoring.CommandRunner import CommandRunner
from Monitoring.CompareWithOriginal import CompareWithOriginal
from Monitoring.DiagnosticsAgent import DiagnosticsAgent
from Monitoring.MasterInspector import MasterInspector
from Test.SeleniumTestAgent import SeleniumTestAgent
from Test.SendToDeployJenkinsAgent import SendToDeployJenkinsAgent
from Test.TestInfo import TestInfo
from Test.UnitTestGenerator import UnitTestGenerator
from Utils.Backup import Backup
from Utils.DiskCleaner import DiskCleaner
from Utils.JenkinsUtils import get_upstream_build, trigger_job, decode_jenkins_args, mark_current_build_as_unstable, \
    UnstableException
from Utils.PKM import add_public_key
from Utils.PKM import remove_public_key
from Utils.Utils import print_exception, am_i_newjenkins, HttpProxy, run_windows_command, send_sms
from Utils.jfabric import local


def run_selenium_tests(params):
    info = TestInfo(params)

    tester = SeleniumTestAgent(info)
    tester.run()


if __name__ == "__main__":
    if len(sys.argv) == 1:
        sys.argv = ['/scripts/main.py', 'command=build', 'job_name=swich-lib', 'build_number=5']

        configs.alaki = True

    print("")
    print("sys.argv =", sys.argv)
    print("")

    sys.excepthook = print_exception

    command_handlers = {name: obj for name, obj in inspect.getmembers(sys.modules[__name__]) if
                        inspect.isfunction(obj) and
                        obj.__code__.co_filename == __file__}

    params = decode_jenkins_args(sys.argv)
    command = params["command"]
    if command not in command_handlers:
        raise Exception("not command defined as {}".format(command))

    command_handlers[command](params)


    if params["command"] == "Deploy" or params["command"] == "Deploy2":
        with DeployInfo(sys.argv) as info:
            name = info.get_deploy_name()

            if info.build_user == "New Jenkins":
                if DeployPackage.must_change_user(info):
                    info.build_user = DeployPackage.get_last_builds_user()

            if name in configs.general_method_deploy:
                deploy_agent = DeployProduction(info)
            elif name in configs.maven_endpoints:
                deploy_agent = DeployMaven(info)
            elif name in configs.php_projects:
                deploy_agent = DeployProduction(info)
            elif name in configs.asp_projects:
                if am_i_newjenkins():
                    deploy_agent = DeployAsp(info)
                else:
                    deploy_agent = DeployAsp2(info)
            else:
                raise Exception("Deploy not supported yet for {}!".format(name))

            exit_code = deploy_agent.deploy()
            sys.exit(exit_code)

    if params["command"] == "PrivateDeploy":
        project_name = sys.argv[2].replace(" Private Deploy", "")
        if project_name in configs.clusters:
            sys.argv[7] = configs.clusters[project_name][project_name][sys.argv[7]]
        else:
            sys.argv[7] = configs.clusters["general"][project_name][sys.argv[7]]

        with DeployInfo(sys.argv) as info:
            name = info.DEPLOYING_BUILD.name

            if name == "Apache":
                deploy_agent = DeployApache(info)
            elif name in configs.asp_projects and info.cluster_name != "98":
                deploy_agent = DeployAsp2(info)
            else:
                deploy_agent = DeployTest(info)

            exit_code = deploy_agent.deploy()
            sys.exit(exit_code)

    if params["command"] == "cluster_deploy":
        with DeployInfo(params) as info:
            deploy_agent = DeployMasterAgent(info, params["cluster_config"])

            for version_line in params["version"].split("\n"):
                match = re.match("(.*?)\s*:\s*(\d+(-\d+)?)", version_line)
                if match:
                    deploy_agent.force_version(BuildInfo(name=match.group(1), version=match.group(2)))

            exit_code = deploy_agent.deploy()
            sys.exit(exit_code)

    if params["command"] == "MultiDeployTrigger":
        with BuildInfo(params["command"], number=sys.argv[2]) as info:
            exit_code = MultiDeployTrigger(info)
            sys.exit(exit_code)

    if params["command"] == "Backup":
        info = BuildInfo(params["command"], number=sys.argv[2])
        if am_i_newjenkins():
            folders = ["/home/ut/Scripts/", "/home/ut/DeployConfigs/", "/home/ut/jenkins/public_keys/"]
            repository = "https://newjenkins@git.iais.co/scm/tdr/newjenkins.git"
            run_windows_command("C:\\\\JenkinsScripts\\\\Save-Backup.ps1")
            server = "newjenkins"
        else:
            folders = ["/home/ut/jenkins/scripts/", "/home/ut/jenkins/configs/", "/j/userContent/DeployReports/"]
            repository = "https://newjenkins@git.iais.co/scm/tdr/deployjenkins.git"
            server = "deployJenkins"
        try:
            backup_agent = Backup(folders, repository)
        except Exception as Ex:
            send_sms("09190398241", "back up field\n in {}\n buildBumber: {}".format(server, info.number))
            raise Exception(str(Ex))

        sys.exit(0)

    if params["command"] == "SendToDeployJenkins":
        info = Operation(sys.argv[2], sys.argv[3])
        if sys.argv[4]:
            if sys.argv[5]:
                target_build = BuildInfo(sys.argv[4], number=sys.argv[5])
            else:
                raise Exception("UP_STREAM_NUMBER field should not be empty")
        else:
            target_build_operation = get_upstream_build(info)
            target_build = BuildInfo(target_build_operation.name, number=target_build_operation.number)

        sender = SendToDeployJenkinsAgent(target_build)
        sender.send_to_deploy_jenkins()

        sys.exit(0)

    if params["command"] == "Monitoring":
        with HttpProxy():
            agent = MasterInspector(sys.argv[2], sys.argv[3])
            agent.check_build_dates()
        sys.exit(0)

    if params["command"] == "Aut":
        info = BuildInfo(params["command"], number=sys.argv[2])
        agent = AutAgent.post_deploy(info)
        agent.run()
        exit_code = 1
        sys.exit(exit_code)

    if params["command"] == "AddPublicKey":
        add_public_key(sys.argv[2], sys.argv[3])

        local("cp /opt/tomcat/.ssh/config {} -f".format(join(configs.user_content_folder, "config")))

        print("\n\n===================================================================\n")
        print("download this file and put it in your ~/.ssh/ and then just type 'ssh 113' :")
        print("http://newjenkins.iais.ir/userContent/config")
        print("\n===================================================================\n\n")
        sys.exit(0)

    if params["command"] == "RemovePublicKey":
        remove_public_key(sys.argv[2], sys.argv[3])
        sys.exit(0)

    if params["command"] == "build":
        try:
            build_info = BuildInfo(params["job_name"], number=params["build_number"])

            agent1 = PreBuild(build_info)
            agent1.run()

            agent2 = Build(build_info)
            agent2.run()

            agent3 = PostBuild(build_info)
            agent3.run()

            if build_info.name in configs.endpoints:
                if params["run_test"]:
                    trigger_job("Cluster Deploy", wait_for_exit=True, unstable_on_fail=True, params={
                        "cluster_name": params["cluster_name"],
                        "cluster_index": params["cluster_index"],
                        "version": "{}:{}".format(build_info.name, build_info.version),
                    })
                    trigger_job("{} Test".format(build_info.name), wait_for_exit=True, unstable_on_fail=True)
                else:
                    raise UnstableException()
        except UnstableException:
            mark_current_build_as_unstable()

        exit(0)

    if params["command"] == "GenerateUnitTest":
        info = Operation(sys.argv[2], sys.argv[3])

        generator = UnitTestGenerator(info)
        generator.run_non_immediate_parts()
        exit(0)
    if params["command"] == "diskCleanUp":
        agent = DiskCleaner()
        agent.run()
        exit(0)
    if params["command"] == "customsDiagnostics":
        if sys.argv[2] or sys.argv[3]:
            agent = DiagnosticsAgent(sys.argv[2], sys.argv[3])

            agent.run()
        else:
            raise Exception("Fill All Items")
        exit(0)
    if params["command"] == "runCommand":
        if sys.argv[2] or sys.argv[3]:
            agent = CommandRunner(sys.argv[2], sys.argv[3])
            agent.run()
        else:
            raise Exception("Fill All Items")
        exit(0)

    if params["command"] == "checkConfigs":
        time_of_now = time.time()
        original_job_name = sys.argv[2] + "-" + str(time_of_now)

        info = DeployInfo(sys.argv)
        name = info.DEPLOYING_BUILD.name
        deploy_agent = DeployMaven(info)
        deploy_agent.run_fake_deploy(time_of_now)
        compare_agent = CompareWithOriginal(sys.argv[2], sys.argv[7], original_job_name)
        compare_agent.run()

        exit(0)

    if params["command"] == "SetHostName":
        ip = sys.argv[2]
        host_name = sys.argv[3]

        run_windows_command("C:\\\\JenkinsScripts\\\\Set-Host-Name.ps1 {} {}".format(ip, host_name))

        exit(0)

    raise Exception("Error: should not reach here( maybe wrong command )")
