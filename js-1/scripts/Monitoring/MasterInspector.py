import threading
from os.path import join

import xmltodict

from Deploy.DeployUtils.DeployLocation import DeployLocation
from Deploy.Observer import Observer
from Monitoring.Inspector import Inspector
from Utils.Utils import remove_color
from Utils.jfabric import local

__author__ = 'mohammad'


class MasterInspector:
    def __init__(self, job_name, number):
        self.job_name = job_name
        self.observer = Observer("Locations", self, None)
        self.build_dates = []
        self.errors = []
        self.number = number

    # Sample output is: result.name = Rajaee
    #                   result.code = 50100 (if exist)
    #                   result.url = 172.16.111.113:8080/Customs
    def get_info_from_jenkins(self):
        config_file_path = "/j/jobs/{} Deploy/config.xml".format(self.job_name)

        with open(config_file_path) as xml_file:
            doc = xmltodict.parse(xml_file.read())

        all_locations = \
            doc['project']['properties']['hudson.model.ParametersDefinitionProperty']['parameterDefinitions'][
                'com.cwctravel.hudson.plugins.extended__choice__parameter.ExtendedChoiceParameterDefinition'][
                'value']
        all_locations = all_locations.split(",")
        result = []
        for location in all_locations:
            result.append(DeployLocation(location))

        return result

    # the program start with this:
    def check_build_dates(self):
        threads = []
        locations = self.get_info_from_jenkins()
        for location in locations:
            t = threading.Thread(target=self.employ_new_inspector(location))
            t.start()
            threads.append(t)

        # join all threads
        for t in threads:
            t.join()

    def employ_new_inspector(self, location):

        # this Line make a new Inspector
        agent_check = Inspector(location, self.job_name)
        agent_check.subscribe_to(self.observer)

        # Call Url to get buildDate
        # type of output this line is Datetime
        build_date = agent_check.get_build_date()
        if not build_date:
            self.errors.append((location.name, remove_color(agent_check.get_status())))
        else:
            self.build_dates.append((location.name, build_date))

        self.save_to_csv()
        self.print_csv_download_message()

    def save_to_csv(self):
        seprator_declearation = "sep=,\n"
        header = "Location,BuildDate" + "\n"
        content = []

        sorted_build_date = sorted(self.build_dates, key=lambda location_build_date: location_build_date[1])
        for location, error_message in self.errors:
            content.append("{},{}\n".format(location, error_message))

        for location, build_date in sorted_build_date:
            content.append("{},{}\n".format(location, build_date))

        content = [seprator_declearation, header] + content
        csv_folder = "/j/userContent/Monitoring"
        local("mkdir -p {}".format(csv_folder))
        with open(join(csv_folder, self.get_build_date_file_name()), "w") as csv_file:
            for item in content:
                csv_file.write(item)

    def get_build_date_file_name(self):
        return "buildDates-{}-{}.csv".format(self.job_name, self.number)

    def print_csv_download_message(self):
        print("")
        print("Excel report available at http://172.17.20.32:8080/userContent/Monitoring/{}".format(
            self.get_build_date_file_name()))
        print("")
