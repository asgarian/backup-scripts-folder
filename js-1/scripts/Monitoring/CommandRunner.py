from Monitoring.DiagnosticsAgent import DiagnosticsAgent
from Utils.Utils import make_section, am_i_newjenkins
from Utils.jfabric import host, run, proxy
# from Telegram.TelegramBot import TelegramBot

__author__ = 'mohammad'


class CommandRunner:
    def __init__(self, command, location):
        self.command = command
        self.location = location
    def run(self):
        #       agent = TelegramBot()
        #     agent.send_message_to_bot(text="command Runner is now runnig ")


        connector = DiagnosticsAgent(job_name="Customs", location=self.location)
        self.host = connector.host

        with proxy(not am_i_newjenkins()), host(self.host):
            with make_section(self.command):
                run(self.command)
