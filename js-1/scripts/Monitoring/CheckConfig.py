import re

from Monitoring.DiagnosticsAgent import DiagnosticsAgent
from Utils.Utils import blue, warning, error

__author__ = 'mohammad'


class CheckConfig(DiagnosticsAgent):
    def __init__(self, content):
        self.content = content

    def check_asycuda_properties(self):
        gomrok_code = re.search('gomrok=(\d\d\d\d\d+?)', self.content)
        if gomrok_code:
            gomrok_code = gomrok_code.group(1)
            print("current code is: " + gomrok_code)
            print("original code is: " + self.location_code)
            if gomrok_code == self.location_code:
                print(blue("so current code is true"))
            else:
                print(warning("so current code is false"))
        else:
            print(error("code gomrok not found"))

    def check_maliat_properties(self):
        this_year = "1395"
        temp = re.search(this_year, self.content)
        if temp:
            print(blue("OK {} is exist".format(this_year)))
        else:
            print(warning("Warning 1395 dont exist"))

    def check_customsdecleration_properties(self):
        temp = re.search(self.location_code, self.content)
        if temp:
            print(blue("code gomrok FOUND"))
        else:
            print(warning("code gomrok DONT FOUND"))
