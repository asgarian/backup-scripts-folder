import time

from AlarmManager.FreeSpaceCheckAgent import FreeSpaceCheckAgent
from Utils.Utils import send_sms
from telegram.AlarmManagerBot import AlarmManagerBot


class AlarmManager:
    def __init__(self):
        self.check_agents = []

    def subscribe(self, check_agent):
        self.check_agents.append(check_agent)

    def run_check_agents(self):
        for agent in self.check_agents:
            try:
                agent.check()
            except Exception as EX:
                self.notify(EX)

    def run(self):
        while True:
            self.run_check_agents()
            time.sleep(60)

    def notify(self, exception):
        print(str(exception))
        send_to_bot_agent = AlarmManagerBot()
        send_to_bot_agent.send_message_to_bot(exception)
        send_sms("09190398241", exception)


alarm_manager = AlarmManager()

alarm_manager.subscribe(FreeSpaceCheckAgent())

alarm_manager.run()
