from AlarmManager.CheckAgent import CheckAgent
from Utils.jfabric import local, host


class CheckDisk(CheckAgent):
    def check(self):
        with host():
            local("df -Th > temp_disk1 && sed -n '4p' < temp_disk1 >temp_disk2 && cut -c 44-45 temp_disk2 >temp_disk3")
            if int(local("cat temp_disk3")) > 80:
                local("rm temp_disk*")
                raise Exception("send alarm to numbers: 09354002476, 09122704834")
            else:
                print ("all good")
                local("rm temp_disk*")
