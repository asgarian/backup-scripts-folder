from AlarmManager.CheckAgent import CheckAgent
from Utils.jfabric import local, skip_output


class FreeSpaceCheckAgent(CheckAgent):
    def check(self):
        content = local("df -h")
        content = content.split("\n")[6]
        free_space = content.split(" ")[13]
        free_space_percent = int(free_space.replace("%", ""))
        if free_space_percent > 60:
            raise Exception("Alarm : hard is full")

