import ntpath
from os.path import join

import configs
from Utils.JenkinsUtils import get_jenkins_build_json, get_job_workspace_path
from Utils.Operation import Operation
from Utils.Utils import get_items_path


class BuildInfo(Operation):
    def __init__(self, name, number=None, version=None):
        if not number:
            if version:
                if "-" in version:
                    number = version.split("-", maxsplit=1)[1]
                else:
                    number = version
            else:
                raise Exception("could not construct buildInfo object neither of number or version specified")

        super().__init__(name, number)

        if version:
            self.version = version
        else:
            self.version = "{}-{}".format(configs.jenkins_name, self.number)

        try:
            self.json = get_jenkins_build_json(self)
            commit_info = [action["lastBuiltRevision"]["branch"][0] for action in self.json["actions"] if
                           "lastBuiltRevision" in action][0]
            self.commit_hash = commit_info["SHA1"]
            self.branch = commit_info["name"]
        except:
            self.json = None
            self.branch = None
            self.commit_hash = None

    # get Stable build folder path
    # sample output: /stable-builds/Customs/21
    def get_sb_folder(self):
        return join(configs.sb_folder, self.name, self.version)

    # get artifact build folder path
    # sample output: /stable-builds/Customs/21/artifacts
    def get_artifact_folder(self):
        return join(self.get_sb_folder(), "artifacts")

    # sample output(mavens): /stable-builds/Customs/492/artifacts/Customs-5.5.3-r000.war
    # sample output(phps): /stable-builds/Mis/492/artifacts/Mis.zip
    def get_artifact_path(self):
        artifact_folder = self.get_artifact_folder()
        artifacts = get_items_path(join(artifact_folder, "*.war"))
        if artifacts:
            return artifacts[0]
        else:
            raise Exception("No war in {}".format(artifact_folder))

    # sample output(maven): Customs-5.5.3-r000.war
    # sample output(php): Mis.zip
    def get_artifact_name(self):
        artifact_path = self.get_artifact_path()
        return ntpath.basename(artifact_path)

    def get_build_date(self):
        sb_folder = self.get_sb_folder()
        build_date_path = join(sb_folder, "buildDate.txt")
        with open(build_date_path, "r") as build_date_file:
            return build_date_file.read().replace('\n', '')

    def get_group_name(self):
        if self.name in configs.maven_endpoints:
            return "Maven"
        if self.name in configs.php_projects:
            return "Php"
        if self.name in configs.asp_projects:
            return "Asp"
        if self.name in configs.nodejs_projects:
            return "Nodejs"

    # sample output: /j/jobs/workspace/Customs
    def get_workspace(self):
        return get_job_workspace_path(self.name)

    def get_job_param(self, param_name):
        if self.json:
            actions = self.json["actions"]

            for action in actions:
                if "parameters" in action:
                    for param in action["parameters"]:
                        if param["name"] == param_name:
                            return param["value"]

        return None
