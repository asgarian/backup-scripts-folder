import json
import ntpath
from os.path import join
from time import strftime, localtime

import configs
import instance_configs
from Utils.JenkinsUtils import trigger_job, update_current_package, update_last_stable_link, UnstableException, \
    get_job_workspace_path
from Utils.Utils import get_items_path, run_windows_command, get_ci_state, section, is_internal_library, \
    get_exact_library_name, warning, get_exact_branch_name, error, getstatusoutput, recreate_folder, \
    run_windows_command_new
from Utils.jfabric import local, lcd, skip_output, skip_echo


class Build:
    def __init__(self, build_info, params):
        self.build_info = build_info
        self.params = params
        self.name = build_info.name
        self.number = build_info.number
        self.maven_profile = self.params["maven_profile"]
        self.workspace = params["workspace"]
        self.build_date = self.get_build_date_string()
        self.alpha2 = instance_configs.jenkins_name

    def run(self):
        if self.name in configs.aut_projects:
            self.aspect_pre_build()

        if self.name not in configs.exclude_from_code_analyse:
            self.run_code_analyse()

        self.save_build_number()
        self.build_artifact()

        if self.name in configs.aut_projects:
            self.aspect_post_build()

        if self.name in configs.endpoints:
            self.deploy_artifact_to_shared_builds()

            if self.params["run_test"]:
                self.prepare_cluster()
                self.run_test()
            else:
                raise UnstableException()

    def build_artifact(self):
        if self.name in configs.maven_projects:
            self.check_library_branches()
            self.add_ci_info()
            self.run_maven_build()
        elif self.name in configs.php_projects:
            self.save_build_date(self.workspace)
        elif self.name in configs.asp_projects:
            self.build_asp()

    @section
    def aspect_post_build(self):
        src_backup_folder = join(self.build_info.get_temp_folder(), "aut-src-backup")
        with lcd(src_backup_folder):
            target_folder = join(self.workspace, "target")
            war_paths = get_items_path(join(target_folder, "*.war"))
            if war_paths:
                war_path = war_paths[0]
            else:
                raise Exception("no war in {}".format(target_folder))

            local("zip -mr {} *".format(war_path))

    @section
    def prepare_cluster(self):
        trigger_job("Cluster Deploy", wait_for_exit=True, unstable_on_fail=True, upstream=self.build_info, params={
            "cluster_name": self.params["cluster_name"],
            "cluster_index": self.params["cluster_index"],
            "version": "{}:{}".format(self.build_info.name, self.build_info.version),
        })

    @section
    def run_test(self):
        trigger_job("{} Test".format(self.build_info.name), wait_for_exit=True, unstable_on_fail=True,
                    upstream=self.build_info,
                    params={"cluster_name": self.params["cluster_name"],
                            "cluster_index": self.params["cluster_index"],
                            })
        update_current_package({self.build_info.name: self.build_info.version})

        if configs.jenkins_name == "master":
            update_last_stable_link(self.build_info.name, self.build_info.version)

    @section
    def run_maven_build(self):
        # remove target folder
        target_folder = join(self.workspace, "target")
        if get_items_path(target_folder):
            local("rm -r {}".format(target_folder))

        with lcd(self.workspace):
            local("mvn clean install -P {}".format(self.maven_profile))

    def create_artifact_folder(self):
        artifact_folder = self.build_info.get_artifact_folder()
        local("mkdir -p {}".format(artifact_folder))

    def save_build_date(self, folder):
        with lcd(folder):
            local("echo '{}' > buildDate.txt".format(self.build_date))

    def get_build_date_string(self):
        return strftime("%Y-%m-%d %H:%M:%S %z", localtime())

    def save_build_number(self):
        build_number_folder = join(self.workspace, configs.maven_resource_folder)
        if get_items_path(build_number_folder):
            build_number_path = join(build_number_folder, self.name + "-BUILD_NUMBER.properties")
            content = "build_number=" + self.number
        else:
            build_number_path = join(self.workspace, "BUILD_NUMBER.txt")
            content = self.number

        with open(build_number_path, 'w') as f:
            f.write(str(content))
            f.close()

    @section
    def build_asp(self):
        # we have to do the following because of special chars
        build_date_parts = self.build_date.split(' ')
        build_date1 = build_date_parts[0]
        build_date2 = " ".join(build_date_parts[1:])

        # run_windows_command(
        #     'C:\\\\JenkinsScripts\\\\Build-Asp.ps1 {} {} {} {{"{}"}}'.format(self.name, self.build_info.version,
        #                                                                      build_date1,
        #                                                                      build_date2))

        run_windows_command_new(
            'powershell C:\\\\JenkinsScripts\\\\Build-Asp.ps1 {} {} {} {{"{}"}} {}'.format(self.name, self.build_info.version,
                                                                             build_date1,
                                                                             build_date2,self.alpha2))

    @section
    def add_ci_info(self):
        ci_info_folder = join(self.workspace, "src/main/java")
        ci_info_path = join(ci_info_folder, configs.ci_info_file_name)

        ci_info = {
            "branch": self.build_info.branch,
            "commit_hash": self.build_info.commit_hash,
            "build_number": self.number,
            "version": self.build_info.version,
            "build_date": self.build_date,
            "ci_state": get_ci_state(),
        }

        ci_info_str = json.dumps(ci_info, sort_keys=True, indent=4)
        local("mkdir -p {}".format(ci_info_folder))
        with open(ci_info_path, "w") as file:
            file.write(ci_info_str)

        print("ci_info added: {}".format(ci_info_str))

    @section
    def check_library_branches(self):
        abort_build = False

        for jar in self.get_dependencies():
            folders = jar.split("/")
            library_full_name = ".".join(folders[folders.index("repository") + 1:-2])
            if is_internal_library(library_full_name):
                library_name = get_exact_library_name(folders[-3])
                if not library_name:
                    print(warning("library {} is not in jenkins so branch of the library could not be checked".format(
                        library_full_name)))
                    continue

                if library_name in configs.ignore_branch_check:
                    print(warning("branch check library {} is ignored".format(library_name)))
                    continue

                library_branch = self.get_library_branch(jar, library_name)
                if library_branch is None:  # library is on svn
                    continue

                incoming_branch = get_exact_branch_name(self.build_info.branch)
                must_be_branch = 'master'

                if incoming_branch in ['master', None]:
                    if library_branch != 'master':
                        must_be_branch = 'master'
                else:
                    try:
                        if self.does_library_has_branch(library_name, incoming_branch):
                            must_be_branch = incoming_branch
                        else:
                            must_be_branch = 'master'
                    except:
                        library_branch = "not available"

                if must_be_branch != library_branch:
                    print(error(
                        "Wrong branch for library {}, should be {} but current is {}.".format(library_name,
                                                                                              must_be_branch,
                                                                                              library_branch)))
                    if self.maven_profile == "Offline":
                        build_params = self.params.copy()
                        build_params["branch"] = must_be_branch
                        trigger_job(library_name, params=build_params)

                    abort_build = True

        if abort_build:
            if self.maven_profile != "Offline":
                print(error("Because the maven profile is not offline triggering library builds is disabled"))
            raise Exception("need library build. please rebuild this build after library builds finished")

    def get_library_branch(self, jar, library_name):
        try:
            with skip_output():
                ci_info_str = local("unzip -p {} {}".format(jar, configs.ci_info_file_name))
            ci_info = json.loads(ci_info_str)
            library_branch = get_exact_branch_name(ci_info["branch"])
        except:
            library_branch = "not available"
            print("no ci_info for {0}".format(library_name))
        return library_branch

    @section
    def run_code_analyse(self):
        with lcd(get_job_workspace_path("SessionCheck")), skip_output(), skip_echo():
            return_code, stdout = getstatusoutput(
                "find {} -type f -name *.java -printf '%p ' ".format(
                    join(get_job_workspace_path("SessionCheck"), "src/main/java"), self.name))
            files = stdout

            if return_code == 0:
                local(
                    'mvn exec:java -Dexec.mainClass="ir.iais.ghalatgir.CorrectSessionClosingInsurance" -Dexec.args="{}"'.format(
                        files))

    @section
    def aspect_pre_build(self):
        #
        # Backup hibernate.cfg
        #
        temp_folder = join(self.build_info.get_temp_folder(), "aut-src-backup")
        recreate_folder(temp_folder)

        hibernate_path = join(self.workspace, "src/main/java/hibernate.cfg.xml")
        log_4j_path = join(self.workspace, "src/main/java/log4j.properties")
        if not get_items_path(hibernate_path):
            hibernate_path = join(self.workspace, "src/main/resources/hibernate.cfg.xml")

        if get_items_path(hibernate_path):
            classes_path = join(temp_folder, "WEB-INF/classes/")
            local("mkdir -p {}".format(classes_path))
            local("cp {} {}".format(hibernate_path, classes_path))

            if get_items_path(log_4j_path):
                local("cp {} {}".format(log_4j_path, classes_path))
        else:
            raise Exception("Could not found hibernate.cfg.xml")

        #
        # Add Aspect
        #
        aspects_paths = join("/home/ut/AUT", self.name, "Aspects/*")
        src_folder = join(self.workspace, "src/main/java")
        aspect_files = get_items_path(aspects_paths)
        if aspect_files:
            local("cp -r {} {}".format(aspects_paths, src_folder))
            print("adding {}".format(", ".join(aspect_files)))

            #
            # Change pom to enable aspectj plugin
            #
            # pom_path = join(self.workspace, "pom.xml")
            # replace_in_file(pom_path, '<!--<goal>compile</goal>-->', '<goal>compile</goal>')

    def does_library_has_branch(self, library_name, incoming_branch):
        library_path = get_job_workspace_path(library_name)
        with lcd(library_path), skip_output(), skip_echo():
            all_branch = local("git branch -a | awk '{if(NR>1) print $1;}'")

            for full_branch_name in all_branch.splitlines():
                exact_branch_name = get_exact_branch_name(full_branch_name)
                if exact_branch_name == incoming_branch:
                    return True
        return False

    def get_dependencies(self):
        class_path_file = join(self.build_info.get_temp_folder(), "class_path")
        with lcd(self.workspace):
            local(
                "mvn dependency:build-classpath -P {} -Dmdep.outputFile={}".format(self.maven_profile, class_path_file))
        with open(class_path_file) as file:
            class_path = file.read()
        return class_path.split(":")

    @section
    def deploy_artifact_to_shared_builds(self):
        self.create_artifact_folder()
        self.save_build_date(self.build_info.get_sb_folder())

        artifact_folder = self.build_info.get_artifact_folder()

        if self.name in configs.maven_endpoints:
            target_folder = join(self.workspace, "target")
            war_path = get_items_path(join(target_folder, "*.war"))
            if war_path:
                war_name = ntpath.basename(war_path[0])
                with lcd(artifact_folder):
                    local("cp {} {}".format(war_path[0], war_name))
            else:
                raise Exception("no war in {}".format(target_folder))

        if self.name in (configs.php_projects + configs.nodejs_projects):
            with lcd(self.workspace):
                local("jar -cf  {}.war .".format(join(artifact_folder, self.name)))
