var calculatorTab = null;

function after_fetch_command(command) {
	if( command ) {
	    keysOfSet = Object.keys(storedVars).slice(76, Object.keys(storedVars).length);
	    var keyslength = keysOfSet.length;
	    storedVarsStr = "";  
	    for (var i = 0; i<keyslength ; i++){
	      var k = keysOfSet[i];
	          storedVarsStr += '\''+k+'\''+': '+'\''+storedVars[k]+'\''+',';
		}
		if(storedVarsStr.length>0) {
			storedVarsStr = storedVarsStr.substr(0, storedVarsStr.length-1)
		}
    	storedVarsStr = '{' + storedVarsStr + '}';
    	
    	LOG.info( command.command + " | " + command.target + " | " + command.value + " | " + editor.app.getTestCase().getTitle() + " | " + storedVarsStr );  
      	
      	var xhttp = new XMLHttpRequest();
      	//xhttp.open("POST", "http://172.16.111.8:8888", false);
        //xhttp.send( command.command + "\n" + command.target + "\n" + command.value + "\n" + editor.app.getTestCase().getTitle() + "\n" + storedVarsStr );
  	}
}


function openAndReuseOneTabPerURL(url) {
  var wm = Components.classes["@mozilla.org/appshell/window-mediator;1"]
                     .getService(Components.interfaces.nsIWindowMediator);
  var browserEnumerator = wm.getEnumerator("navigator:browser");
  
  
  var mainWindow = window.QueryInterface(Components.interfaces.nsIInterfaceRequestor)
                       .getInterface(Components.interfaces.nsIWebNavigation)
                       .QueryInterface(Components.interfaces.nsIDocShellTreeItem)
                       .rootTreeItem
                       .QueryInterface(Components.interfaces.nsIInterfaceRequestor)
                       .getInterface(Components.interfaces.nsIDOMWindow);
  
  //	for( var i = 0; i < mainWindow.getBrowser().tabs.length; i++){
	
//}
	while (!found && browserEnumerator.hasMoreElements()) {
    var browserWin = browserEnumerator.getNext();
    var tabbrowser = browserWin.gBrowser;
    	LOG.info(browserWin);

    // Check each tab of this browser instance
    var numTabs = tabbrowser.browsers.length;
    for (var index = 0; index < numTabs; index++) {
    
    	
      var currentBrowser = tabbrowser.getBrowserAtIndex(index);
      
      	LOG.info(currentBrowser);
	    // LOG.info(Object.getOwnPropertyNames(currentBrowser));
	 }
    }
	
	
	
	
	
  // Check each browser instance for our URL
  var found = false;
  while (!found && browserEnumerator.hasMoreElements()) {
    var browserWin = browserEnumerator.getNext();
    var tabbrowser = browserWin.gBrowser;

    // Check each tab of this browser instance
    var numTabs = tabbrowser.browsers.length;
    for (var index = 0; index < numTabs; index++) {
      var currentBrowser = tabbrowser.getBrowserAtIndex(index);
      if (url == currentBrowser.currentURI.spec) {

        // The URL is already opened. Select this tab.
        tabbrowser.selectedTab = tabbrowser.tabContainer.childNodes[index];

        // Focus *this* browser-window
        browserWin.focus();

        found = true;
        break;
      }
    }
  }

  // Our URL isn't open. Open it now.
  if (!found) {
    var recentWindow = wm.getMostRecentWindow("navigator:browser");
    if (recentWindow) {
      // Use an existing browser window
      recentWindow.delayedOpenTab(url, null, null, null, null);
    }
    else {
      // No browser windows are open, so open a new one.
      window.open(url);
    }
  }
}

Selenium.prototype.doGoToCalculator = function(locator, text) {
       /* var mainWindow = window.QueryInterface(Components.interfaces.nsIInterfaceRequestor)
                       .getInterface(Components.interfaces.nsIWebNavigation)
                       .QueryInterface(Components.interfaces.nsIDocShellTreeItem)
                       .rootTreeItem
                       .QueryInterface(Components.interfaces.nsIInterfaceRequestor)
                       .getInterface(Components.interfaces.nsIDOMWindow);

		var gBrowser = mainWindow.gBrowser;
		
		mainWindow.maximize();
		
		if( calculatorTab == null || calculatorTab.closed )
		{
		    var url = "http://www.math.com/students/calculators/source/basic.htm";		
            calculatorTab = gBrowser.addTab(url,'tab1');
		}*/
		//gBrowser.selectedTab = calculatorTab;
		openAndReuseOneTabPerURL("http://www.math.com/students/calculators/source/basic.htm");
		
		/*this.browserbot.selectWindow("title=" +this.browserbot.getCurrentWindow().title);
		this.browserbot.resetPopups();*/
	};
	
	Selenium.prototype.doMaximaze = function(locator, text) {
        var mainWindow = window.QueryInterface(Components.interfaces.nsIInterfaceRequestor)
                       .getInterface(Components.interfaces.nsIWebNavigation)
                       .QueryInterface(Components.interfaces.nsIDocShellTreeItem)
                       .rootTreeItem
                       .QueryInterface(Components.interfaces.nsIInterfaceRequestor)
                       .getInterface(Components.interfaces.nsIDOMWindow);

		mainWindow.maximize();
	};
	
	Selenium.prototype.doSerialNumber = function(locator, text) {
        storedVars['sn'] = storedVars['sn'].replace("شماره سريال:","");
	};
	
	Selenium.prototype.doGoToPanel = function(locator, text) {
	   this.doClick("link=" + locator,"");
       this.doClick("//a[contains(text(),'" + text +"')]","");
	};
	
	Selenium.prototype.doLogin = function(locator, text) {
		open("http://erd.sjb.ir/",null);
	
       var url = this.getTitle();

	};
	
	Selenium.prototype.doComment = function(locator, text) {
	};
	
	Selenium.prototype.doStg = function(locator, text) {
	};
	
	//Selenium.prototype.doOpen = function(locator, text) {
	//	this.doRunScript('window.open("' + locator + '","_self")');
//};
	
	Selenium.prototype.doWait = function(locator, text) {
       this.doSetImplicitWaitCondition(locator,false);
	};
	

LocatorBuilders.add('wicketpath', function(e) {
		if (e.nodeName == "A" ) {
			if( e.parentNode && e.parentNode.nodeName == "LI" )
				if( e.parentNode.parentNode && e.parentNode.parentNode.nodeName == "UL" )
					if( e.parentNode.parentNode.parentNode && e.parentNode.parentNode.parentNode.nodeName == "LI" )
						if( e.parentNode.parentNode.parentNode.childNodes && e.parentNode.parentNode.parentNode.childNodes.length > 0 )
						{
							return "//parent::span[text()=\"" + e.parentNode.parentNode.parentNode.childNodes[1].childNodes[5].innerHTML + "\"]/../../ul/li/a[text()=\"" + e.innerHTML + "\"]";
						}
		}
		if (e.attributes && (e.nodeName == "SELECT" || e.nodeName == "INPUT" || e.nodeName == "TEXTAREA") && e.hasAttribute("name")) {
			var name = this.attributeValue(e.getAttribute("name"));
			return "css=" + this.xpathHtmlElement(e.nodeName.toLowerCase()) + "[name=" + name +"]";
        }
        if (e.attributes && e.hasAttribute("wicketpath")) {
            console.log("found attribute " + e.getAttribute("wicketpath"));
			var wp = this.attributeValue(e.getAttribute("wicketpath"));
			var index1 = wp.lastIndexOf('_');
			var result = wp;
			var d = 5;
			var count = 1;
			if( index1 == -1 )
			{
				result = wp;
			}
			else
			{
				count = 2;
				var index2 = wp.lastIndexOf('_',index1-1);
				
				while( true )
				{
					index1 = index2;
					index2 = wp.lastIndexOf('_',index1-1);
					count++;
					
					if(index2 == -1)
					{
						result = wp.substring(index1+1);
						break;
					}
					
					if( count >= d)
					{
						result = wp.substring(index2+1);
						break;
					}
				}
			}
			return "css=" + this.xpathHtmlElement(e.nodeName.toLowerCase()) + "[wicketpath$=\'" + result +"]";
        }
        return null;
    });
LocatorBuilders.order.unshift(LocatorBuilders.order.pop());
/*
LocatorBuilders.add('sidepanel', function(e) {
		console.error("sidepanel: e=");
        console.log("sidepanel: e=" + e);
		if (e.nodeName == "A" ) {
			if( e.parentNode && e.parentNode.nodeName == "LI" )
				if( e.parentNode.parentNode && e.parentNode.parentNode.nodeName == "UL" )
					if( e.parentNode.parentNode.parentNode && e.parentNode.parentNode.parentNode.nodeName == "LI" )
						if( e.parentNode.parentNode.parentNode.childNodes && e.parentNode.parentNode.parentNode.childNodes.length > 0 )
						{
							return "//parent::span[text()=\"" + e.parentNode.parentNode.parentNode.childNodes[1].childNodes[5].innerHTML + "\"]/../../ul/li/a[text()=\"" + e.innerHTML + "\"]";
						}
		}
        return null;
    });
*/
//LocatorBuilders.order.unshift(LocatorBuilders.order.pop());
