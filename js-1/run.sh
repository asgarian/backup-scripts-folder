#!/bin/bash

if [ -z "$1" ]; then
    echo "No name supplied"
    return
fi
name=$1

instance_configs_path="scripts/instance_configs.py"
if [ ! -f $instance_configs_path ]; then
   echo "no $instance_configs_path"
   return
else
   cat $instance_configs_path
fi


#
# find Id
#
instances=( \
   master  \
   beta    \
   alpha1  \
   alpha2  \
   alpha3  \
)

id=""
for i in "${!instances[@]}"; do
   if [[ "${instances[$i]}" = $name ]]; then
       id="${i}";
   fi
done
if [ -z "$id" ]; then
    echo "jenkins $name is not registred"
    return
fi
echo "Id is $id"


#
# Set vars
#
jenkins_folder=$(pwd)
container_name="jenkins-$name"

jenkins_port=$(($id*100 + 8000))
ssh_port=$(($id*100 + 8001))



#
# Run
#
if [[ "$(docker ps -a -q -f Name=^/$container_name$ 2> /dev/null)" != "" ]]; then
   docker rm -f $container_name
fi

docker run --restart=always -dit --name $container_name -p $jenkins_port:8080 \
   -v $jenkins_folder/opt/forti:/opt/forti \
   -v $jenkins_folder/jenkins_home:/var/jenkins_home \
   -v $jenkins_folder/scripts:/scripts \
   -v $jenkins_folder/configs:/configs \
   -v $jenkins_folder/seleniums:/seleniums \
   -v $jenkins_folder/dist-packages:/usr/local/lib/python3.4/dist-packages \
   -v /stable-builds:/stable-builds \
   -v $jenkins_folder/temp:/temp \
   -v /var/run/docker.sock:/var/run/docker.sock \
   -p $ssh_port:22 \
   --add-host bitbucket.iais.co:194.225.0.50 \
   --add-host svn.iais.ir:172.16.111.100 \
   --add-host git.iais.co:194.225.0.50 \
   --add-host nexus.iais.ir:194.225.0.50 \
   --restart always \
   hub.docker.iais.co:5000/jenkins:$name

docker exec -u root $container_name chown -R jenkins:jenkins /var/jenkins_home scripts stable-builds temp seleniums
docker exec -u root $container_name chmod -R o+r /configs
docker exec -u root $container_name chown root:docker /var/run/docker.sock

docker exec -u root $container_name nohup /usr/sbin/sshd -D &
