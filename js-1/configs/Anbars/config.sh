#!/bin/bash

sed -i 's/[$]/unique1/g' must-be-config
sed -i 's/[\\\]/unique2/g' must-be-config

application_setting=$(awk '/<appSettings>/,/<\/appSettings>/' must-be-config)
connection_string=$(awk '/<connectionStrings>/,/<\/connectionStrings>/' must-be-config)
botDetect=$(awk '/<botDetect>/,/<\/botDetect>/' must-be-config)
system_webServer=$(awk '/<system.webServer>/,/<\/system.webServer>/' must-be-config)

sed -i 's/unique1/'\$'/g' must-be-config
sed -i 's/unique2/\\/g' must-be-config 



perl -p0e 's#\<botDetect\>.*?\<\/botDetect\>#'"$botDetect"'#s' Web.config > temp1
dos2unix temp1
perl -p0e 's#\<appSettings\>.*?\<\/appSettings\>#'"$application_setting"'#s' temp1 > temp2
dos2unix temp2
perl -p0e 's#\<connectionStrings\>.*?\<\/connectionStrings\>#'"$connection_string"'#s' temp2 > temp3
dos2unix temp3
perl -p0e 's#\<system.webServer\>.*?\<\/system.webServer\>#'"$system_webServer"'#s' temp3 > Web.config
dos2unix Web.config
sed -i 's/unique1/'\$'/g' Web.config
sed -i 's/unique2/\\/g' Web.config 



rm temp1
rm temp2
rm temp3
