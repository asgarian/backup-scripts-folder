#!/bin/bash

if [ ! -d "/kvm/0" ]; then
  apt-get -y install samba
  mkdir -p "/kvm/0"
  chmod 777 /kvm -R
  echo "[kvm]
  path = /kvm
  available = yes
  valid users = ut
  read only = no
  browsable = yes
  public = yes
  writable = yes
  " >> /etc/samba/smb.conf
  echo -e "mkt123sjb\nmkt123sjb" | smbpasswd -a ut
  service smbd restart  
fi

echo -e "\t\t\t\t\t*******  samba is ok  *******"

