#!/bin/bash

if [ $1 == "taxtest" ]; then
   context="tax"
else
   context="customs"
fi

/bin/bash ghif.sh $context $1

sed -i -- 's/postgres:5432/postgres-rajae:5432/g' WEB-INF/classes/hibernate.cfg.xml
sed -i -- 's/127.0.0.1:8080\/Customs/customs-rajae:8080\/Customs-Rajae/g' WEB-INF/classes/customs.properties
