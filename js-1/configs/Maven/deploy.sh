#!/bin/bash

. set-vars-dockerized.sh "$@"

mkdir $1
mv * $1
mv $1/Dockerfile .

docker build -t $image_name --build-arg name=$1 -q .

sudo docker rm -f  $container_name

sudo docker run -p $tomcat_port:8080 -dit --name $container_name --link $mysql_name:mysql   $image_name

