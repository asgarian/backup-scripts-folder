#!/bin/bash

#
# ports
#
case "$1" in
  "Customs")      tomcat_port=8000
  ;;
  "Baskool")      tomcat_port=8001
  ;;
  "Swich")        tomcat_port=8002
  ;;
  "acc")          tomcat_port=8003
  ;;
  "Center")       tomcat_port=8004
  ;;
  "Center-Swich") tomcat_port=8005
  ;;
  "PackingList") tomcat_port=8030
  ;;
  "Nezarat")      tomcat_port=8040
esac

tomcat_port=$(( $tomcat_port + $3*100  ))

apache_port=$(( 8006 + $3*100  ))
mysql_port=$(( 3000 + $3*100  ))

if [ "$4" == "116" ]; then
  tomcat_port=$(( $tomcat_port + 10 ))
  mysql_port=$(( $mysql_port + 10 ))
fi
if [ "$4" == "12" ]; then
  tomcat_port=$(( $tomcat_port + 20 ))
  mysql_port=$(( $mysql_port + 20 ))
fi

#
# names
#
image_name="$1-$4-$3i"
image_name="${image_name,,}"

container_name="$1-$4-$3"
mysql_name="mysql$3"
apache_name="apache$3"


#
# directories
#
mounted_dir="/docker/cluster$3"
mysql_data_dir="$mounted_dir/mysql"


#
# echo
#
echo ""
echo "tomcat_port    = $tomcat_port"
echo "apache_port    = $apache_port"
echo "mysql_port     = $mysql_port"

echo "image_name     = $image_name"

echo "container_name = $container_name"
echo "mysql_name     = $mysql_name"
echo "apache_name     = $apache_name"

echo "mounted_dir    = $mounted_dir"
echo "mysql_data_dir = $mysql_data_dir"
echo ""
