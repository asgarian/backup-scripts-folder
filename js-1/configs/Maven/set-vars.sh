#!/bin/bash

tomcat_port=$(( 8080 + $3 ))
apache_port=$(( 80 + $3 ))
mysql_port=$(( 3300 + $3  ))

container_name="tomcat$3"
mysql_name="mysql$3"

mounted_dir="/docker/cluster$3"

webapps_dir="$mounted_dir/webapps"
mysql_data_dir="$mounted_dir/mysql"

echo ""
echo "tomcat_port    = $tomcat_port"
echo "apache_port    = $apache_port"
echo "mysql_port     = $mysql_port"

echo "container_name = $container_name"
echo "mysql_name     = $mysql_name"

echo "mounted_dir    = $mounted_dir"
echo "webapps_dir    = $webapps_dir"
echo "mysql_data_dir = $mysql_data_dir"
echo ""


case "$1" in
  "Customs") port=8000
  ;;
  "Baskool") port=8001
  ;;
  "Swich") port=8002
  ;;
  "Acc") port=8003
  ;;
  "Center") port=8004
  ;;
  "Center-Swich") port=8005
  ;;
  "Packing-List") port=8030
  ;;
esac

if [ "$4" == "116" ]; then
  port=$(( $port + 10 ))
fi
if [ "$4" == "12" ]; then
  port=$(( $port + 20 ))
fi