sudo rm -r /home/bazbini/www/bazbini-private
sudo mkdir -p /home/bazbini/www/bazbini-private
sudo cp -r /jenkins/Bazbini/. /home/bazbini/www/bazbini-private
cd /home/bazbini/www/bazbini-private

sudo chown -fvR www-data log
#sudo chmod -fvR 0755 *
sudo chmod -fvR 0777 log
cd app/plugin/doctrine
sudo php bin/doctrine.php orm:generate-proxies
sudo php bin/doctrine.php orm:schema-tool:update --force
