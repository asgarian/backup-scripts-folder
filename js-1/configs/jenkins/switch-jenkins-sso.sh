DATE=`date +%Y-%m-%d:%H:%M:%S`
#echo "$DATE"
#file="/j/config.xml"
file="/j/config.xml"
cp "$file" "${DATE}-config.xml"
#cp "$file" last.xml
current_security="$(cat $file | grep '<authorization')"
raw_line="$(cat $file | grep -n '<authorization')"
IFS=':' read -ra line_array  <<< "$raw_line"
line="${line_array[0]}"
#echo "$line"
#echo "$current_security"
#if [ "$current_security" == "  <securityRealm class=\"hudson.security.LDAPSecurityRealm\" plugin=\"ldap@1.12\">" ]; then
if test -f  last.xml; then
cp last.xml /j/config.xml
rm last.xml
else 
cp /j/config.xml last.xml
l=${line}
l2=$((l+71))
#echo "$l2"
sed -i.bak  -e "${l},${l2}d" "$file"
sed -i "${line}i\  <authorizationStrategy class=\"hudson.security.FullControlOnceLoggedInAuthorizationStrategy\"/>\n  <securityRealm class=\"hudson.security.HudsonPrivateSecurityRealm\">\n    <disableSignup>true</disableSignup>\n    <enableCaptcha>false</enableCaptcha> " "$file"

#else
#l=${line}
#l2=$((l+2))
#sed -i.bak -e  "${l},${l2}d" "$file"
#sed -i '67i \  <securityRealm class=\"hudson.security.LDAPSecurityRealm\" plugin=\"ldap@1.12\">\n    <server>ldap://sso.iais.ir</server>\n    <rootDN>dc=iais,dc=ir</rootDN>\n    <inhibitInferRootDN>false</inhibitInferRootDN>\n    <userSearchBase>cn=users,cn=accounts</userSearchBase>\n    <userSearch>uid={0}</userSearch>\n    <groupMembershipStrategy class=\"jenkins.security.plugins.ldap.FromGroupSearchLDAPGroupMembershipStrategy\">\n      <filter></filter>\n    </groupMembershipStrategy>\n    <managerDN>uid=connector,cn=users,cn=accounts,dc=iais,dc=ir</managerDN>\n    <managerPasswordSecret>axf9CZHnc55yx2LbWvUc/H7PGy/D3HQXhAStDIE++gE=</managerPasswordSecret>\n    <disableMailAddressResolver>false</disableMailAddressResolver>\n    <displayNameAttributeName>displayname</displayNameAttributeName>\n    <mailAddressAttributeName>mail</mailAddressAttributeName>\n    <userIdStrategy class=\"jenkins.model.IdStrategy$CaseInsensitive\"/>\n    <groupIdStrategy class=\"jenkins.model.IdStrategy$CaseInsensitive\"/>' "$file"
fi
service tomcat restart
