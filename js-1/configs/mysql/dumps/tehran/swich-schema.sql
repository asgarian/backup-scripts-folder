-- MySQL dump 10.13  Distrib 5.5.47, for debian-linux-gnu (x86_64)
--
-- Host: 172.16.111.12    Database: swich
-- ------------------------------------------------------
-- Server version	5.7.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `swich`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `swich` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `swich`;

--
-- Table structure for table `BijakMaghsad`
--

DROP TABLE IF EXISTS `BijakMaghsad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BijakMaghsad` (
  `DBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `pate` longtext,
  `pateUniqueId` varchar(255) DEFAULT NULL,
  `uniqueSerial` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`DBId`),
  KEY `BijakMaghsad_pateUniqueId_Index` (`pateUniqueId`),
  KEY `BijakMaghsad_uniqueSerial_Index` (`uniqueSerial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ConnectionInfo`
--

DROP TABLE IF EXISTS `ConnectionInfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ConnectionInfo` (
  `DBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `dst` varchar(255) DEFAULT NULL,
  `failureCount` int(11) NOT NULL,
  `sid` int(11) DEFAULT NULL,
  `src` varchar(255) DEFAULT NULL,
  `successCount` int(11) NOT NULL,
  PRIMARY KEY (`DBId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `GomrokEvent`
--

DROP TABLE IF EXISTS `GomrokEvent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GomrokEvent` (
  `DBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `eventId` int(11) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `userId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`DBId`),
  KEY `IDX_date` (`date`),
  KEY `IDX_eventId` (`eventId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `GomrokEvent_Parameter`
--

DROP TABLE IF EXISTS `GomrokEvent_Parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GomrokEvent_Parameter` (
  `GomrokEvent_DBId` bigint(20) NOT NULL,
  `Ids_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_c7lxdec3v77gqhoen9p8n9ktn` (`Ids_id`),
  KEY `FK_c7lxdec3v77gqhoen9p8n9ktn` (`Ids_id`),
  KEY `FK_7tacv7gxkvk9b8ti8po4a611g` (`GomrokEvent_DBId`),
  CONSTRAINT `FK_7tacv7gxkvk9b8ti8po4a611g` FOREIGN KEY (`GomrokEvent_DBId`) REFERENCES `GomrokEvent` (`DBId`),
  CONSTRAINT `FK_c7lxdec3v77gqhoen9p8n9ktn` FOREIGN KEY (`Ids_id`) REFERENCES `Parameter` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Parameter`
--

DROP TABLE IF EXISTS `Parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Parameter` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TransObj`
--

DROP TABLE IF EXISTS `TransObj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TransObj` (
  `DBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `direction` int(11) NOT NULL,
  `obj` longtext,
  `rcvTime` datetime DEFAULT NULL,
  `sid` int(11) DEFAULT NULL,
  `sndTime` datetime DEFAULT NULL,
  `tid` bigint(20) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`DBId`),
  KEY `TransObj_tid_Index` (`tid`),
  KEY `IDX_sid_direction_tid` (`sid`,`direction`,`tid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `swichLib_Employee`
--

DROP TABLE IF EXISTS `swichLib_Employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `swichLib_Employee` (
  `EmployeeDBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `currentGomrok` varchar(255) DEFAULT NULL,
  `gomrokZone` varchar(255) DEFAULT NULL,
  `melliCode` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phoneNumber` varchar(255) DEFAULT NULL,
  `secretKey` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`EmployeeDBId`),
  UNIQUE KEY `UK_d2k9fr6uk19d0uv3422ssmlud` (`melliCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `swichLib_Employee_swichLib_Responsibility`
--

DROP TABLE IF EXISTS `swichLib_Employee_swichLib_Responsibility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `swichLib_Employee_swichLib_Responsibility` (
  `swichLib_Employee_EmployeeDBId` bigint(20) NOT NULL,
  `responsiblities_ResponsibilityDBId` bigint(20) NOT NULL,
  UNIQUE KEY `UK_7a112slt147eo39dy6cx8u9p6` (`responsiblities_ResponsibilityDBId`),
  KEY `FK_7a112slt147eo39dy6cx8u9p6` (`responsiblities_ResponsibilityDBId`),
  KEY `FK_asb1ijadrx7dn2p8wxg5xjj2b` (`swichLib_Employee_EmployeeDBId`),
  CONSTRAINT `FK_7a112slt147eo39dy6cx8u9p6` FOREIGN KEY (`responsiblities_ResponsibilityDBId`) REFERENCES `swichLib_Responsibility` (`ResponsibilityDBId`),
  CONSTRAINT `FK_asb1ijadrx7dn2p8wxg5xjj2b` FOREIGN KEY (`swichLib_Employee_EmployeeDBId`) REFERENCES `swichLib_Employee` (`EmployeeDBId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `swichLib_Responsibility`
--

DROP TABLE IF EXISTS `swichLib_Responsibility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `swichLib_Responsibility` (
  `DTYPE` varchar(31) NOT NULL,
  `ResponsibilityDBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `end_date` datetime DEFAULT NULL,
  `gomrok` varchar(255) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `empl_EmployeeDBId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ResponsibilityDBId`),
  KEY `FK_k7mhp4qatif8mv564la2mf028` (`empl_EmployeeDBId`),
  CONSTRAINT `FK_k7mhp4qatif8mv564la2mf028` FOREIGN KEY (`empl_EmployeeDBId`) REFERENCES `swichLib_Employee` (`EmployeeDBId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-22  6:39:38
