-- MySQL dump 10.13  Distrib 5.5.47, for debian-linux-gnu (x86_64)
--
-- Host: 172.16.111.12    Database: baskool
-- ------------------------------------------------------
-- Server version	5.7.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `baskool`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `baskool` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `baskool`;

--
-- Table structure for table `AccessDate`
--

DROP TABLE IF EXISTS `AccessDate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AccessDate` (
  `dbId` bigint(20) NOT NULL AUTO_INCREMENT,
  `accessDate` datetime DEFAULT NULL,
  `resultValue` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `routedObj` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `rule_dbId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`dbId`),
  KEY `FK_2yyxn4hex6nhl3p1dmytvolp0` (`rule_dbId`),
  CONSTRAINT `FK_2yyxn4hex6nhl3p1dmytvolp0` FOREIGN KEY (`rule_dbId`) REFERENCES `Rule` (`dbId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AccessDate`
--

LOCK TABLES `AccessDate` WRITE;
/*!40000 ALTER TABLE `AccessDate` DISABLE KEYS */;
/*!40000 ALTER TABLE `AccessDate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AccessPermission`
--

DROP TABLE IF EXISTS `AccessPermission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AccessPermission` (
  `dBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `regex` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `tozinType` int(11) DEFAULT NULL,
  PRIMARY KEY (`dBId`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AccessPermission`
--

LOCK TABLES `AccessPermission` WRITE;
/*!40000 ALTER TABLE `AccessPermission` DISABLE KEYS */;
INSERT INTO `AccessPermission` VALUES (1,'تست','Baskool1,',1),(2,'دوگارد','Baskool4,Baskool4,',1),(4,'رويه تست','Guard2,Baskool1,Baskool1,Baskool1,Baskool1,Baskool1,Guard2,',1),(5,'تست فور ترنسپورت','Baskool1,Baskool1,',1),(7,'رويه تست انبار','Baskool1,',1),(8,'کانتينر','Anbar3,',1),(9,'انبار-تست','Guard2,Baskool9,',1),(10,'تست فور ترنسپورت 2','Baskool1,Baskool1,Anbar3,',1),(11,'رويه حامد بابا','Baskool11,',1),(13,'تست شايان','Baskool7,',1),(14,'انبار','Guard2,Baskool1,',1),(15,'شايان2','Baskool7,',1),(16,'معين تست','Baskool12,',1),(17,'پويان تست','Baskool13,',1),(18,'نيو تست مسير','Baskool1,Anbar3,',1),(19,'اسم رويه که ميخواهيد','Guard2,Baskool1,Baskool4,Anbar3Guard13,',1),(20,'تست','Guard2,Baskool1,Baskool4,Anbar3,Guard5,',1),(21,'مسير اصلي تست','Baskool1,Anbar11,',1),(23,'تست فور ترنسپورت 3','Baskool1,Baskool1,Anbar11,',1),(24,'واردات و ترانزيت مبدا','(Baskool(292|284|294|287|286|299|300|301|302|303),Anbar(\\d+),)|(Baskool(\\d+),((Anbar(\\d+),Baskool(\\d+))|(Baskool(\\d+),Anbar(\\d+)))),',1),(25,'رويه تست محمدزاده','Guard2,Baskool4,Baskool19,Anbar10,',1),(26,'رويه عمومي','Guard\\d+,Baskool\\d+,(Baskool\\d+,)?(Anbar\\d+,)?Guard\\d+,',1),(27,'واردات انباري','Guard\\d+,Baskool\\d+,Baskool\\d+,Anbar\\d+,Guard\\d+,',1),(28,'واردات-حمل يکسره-مرزي','Guard2,Baskool1,',1),(29,'بنجل يه سره','Guard\\d+,Baskool\\d+,(Baskool22,)',0),(30,'حمل يکسره از طرف پل چوبي','(Guard2,|Guard5,)Baskool1,Guard2,',0),(31,'صادرات مقصد از طرف پل چوبي','(Guard2,|Guard5,|Guard13,|Guard18,)(Baskool1,|Baskool4,)Guard2,',1),(32,'صادرات مبدا جلفا','(Guard2,|Guard5,|Guard13,)Baskool1,Anbar10,?Guard2,',1),(33,'ترانزيت مبدا جلفا','(Guard2,|Guard5,|Guard13,)Baskool1,Baskool1,Anbar10,?Guard2,',1),(34,'ترانزيت مقصد گمرک جلفا','(Guard2,|Guard5,|Guard13,)Baskool1,Baskool1,Guard2,',1),(35,'کارنه تير مقصد','(Guard2,|Guard5,|Guard13,)Baskool1,Baskool1,Guard2,',1),(36,'ورود موقت','(Guard2,|Guard5,)Guard2,',0),(37,'رويه ارسال بار از پل چوبي به گمرک','(Guard2,|Guard5,)Baskool1,(Anbar10,)?Baskool1,Guard2,',1),(38,'رويه ارسال بار از پل چوبي به گمرک','(Guard2,|Guard5,)Baskool1,(Anbar10,)?Baskool1,Guard2,',1),(39,'تست آرين','Baskool26,Baskool26,',1),(40,'milad','Guard25,Baskool24,Baskool24,Guard25,',0);
/*!40000 ALTER TABLE `AccessPermission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Agent`
--

DROP TABLE IF EXISTS `Agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Agent` (
  `dBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `limitRegistration` int(11) DEFAULT NULL,
  `melliCode` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`dBId`),
  UNIQUE KEY `UK_9v04d38qvit1xtijmle78y90e` (`melliCode`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Agent`
--

LOCK TABLES `Agent` WRITE;
/*!40000 ALTER TABLE `Agent` DISABLE KEYS */;
INSERT INTO `Agent` VALUES (6,200,'01122233334','شرکت حمل'),(7,200,'98877766665','شرکت حمل 2'),(8,200,'0081674902','تولايي'),(9,200,'0012344321','تست ابي'),(14,200,'121212121','بروس وين'),(15,200,'12121212121','شرکت حمل تست');
/*!40000 ALTER TABLE `Agent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `BijakMaghsad`
--

DROP TABLE IF EXISTS `BijakMaghsad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BijakMaghsad` (
  `DBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `pate` longtext,
  `pateUniqueId` varchar(255) DEFAULT NULL,
  `uniqueSerial` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`DBId`),
  KEY `BijakMaghsad_pateUniqueId_Index` (`pateUniqueId`),
  KEY `BijakMaghsad_uniqueSerial_Index` (`uniqueSerial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `BijakMaghsad`
--

LOCK TABLES `BijakMaghsad` WRITE;
/*!40000 ALTER TABLE `BijakMaghsad` DISABLE KEYS */;
/*!40000 ALTER TABLE `BijakMaghsad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ConnectionInfo`
--

DROP TABLE IF EXISTS `ConnectionInfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ConnectionInfo` (
  `DBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `dst` varchar(255) DEFAULT NULL,
  `failureCount` int(11) NOT NULL,
  `sid` int(11) DEFAULT NULL,
  `src` varchar(255) DEFAULT NULL,
  `successCount` int(11) NOT NULL,
  PRIMARY KEY (`DBId`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ConnectionInfo`
--

LOCK TABLES `ConnectionInfo` WRITE;
/*!40000 ALTER TABLE `ConnectionInfo` DISABLE KEYS */;
INSERT INTO `ConnectionInfo` VALUES (1,'2016-01-18',NULL,0,1000001,NULL,1),(2,'2016-01-18',NULL,0,40000001,NULL,1),(3,'2016-01-18',NULL,0,1000003,NULL,1),(4,'2016-01-18',NULL,0,2000001,NULL,1),(5,'2016-01-18',NULL,0,1000002,NULL,1),(6,'2016-01-18',NULL,0,40000002,NULL,1),(7,'2016-01-18',NULL,0,2000000001,NULL,1),(8,'2016-01-18',NULL,0,1000002,NULL,1),(9,'2016-01-18',NULL,0,1000003,NULL,1),(10,'2016-01-18',NULL,0,1000001,NULL,1),(11,'2016-01-18',NULL,0,40000001,NULL,1),(12,'2016-01-18',NULL,0,2000001,NULL,1),(13,'2016-01-18',NULL,0,2000000001,NULL,1),(14,'2016-01-18',NULL,0,40000002,NULL,1),(15,'2016-01-18',NULL,0,1000002,NULL,1),(16,'2016-01-18',NULL,0,1000001,NULL,1),(17,'2016-01-18',NULL,0,2000001,NULL,1),(18,'2016-01-18',NULL,0,40000001,NULL,1),(19,'2016-01-18',NULL,0,1000003,NULL,1),(20,'2016-01-18',NULL,0,40000002,NULL,1),(21,'2016-01-18',NULL,0,2000000001,NULL,1),(22,'2016-01-18',NULL,0,1000003,NULL,1),(23,'2016-01-18',NULL,0,1000002,NULL,1),(24,'2016-01-18',NULL,0,40000001,NULL,1),(25,'2016-01-18',NULL,0,2000001,NULL,1),(26,'2016-01-18',NULL,0,1000001,NULL,1),(27,'2016-01-18',NULL,0,40000002,NULL,1),(28,'2016-01-18',NULL,0,2000000001,NULL,1),(29,'2016-01-18',NULL,0,1000002,NULL,1),(30,'2016-01-18',NULL,0,2000001,NULL,1),(31,'2016-01-18',NULL,0,1000003,NULL,1),(32,'2016-01-18',NULL,0,1000001,NULL,1),(33,'2016-01-18',NULL,0,40000001,NULL,1),(34,'2016-01-18',NULL,0,40000002,NULL,1),(35,'2016-01-18',NULL,0,2000000001,NULL,1),(36,'2016-01-18',NULL,0,40000001,NULL,1),(37,'2016-01-18',NULL,0,2000001,NULL,1),(38,'2016-01-18',NULL,0,1000003,NULL,1),(39,'2016-01-18',NULL,0,1000002,NULL,1),(40,'2016-01-18',NULL,0,1000001,NULL,1),(41,'2016-01-18',NULL,0,2000000001,NULL,1),(42,'2016-01-18',NULL,0,40000002,NULL,1),(43,'2016-01-18',NULL,0,1000001,NULL,1),(44,'2016-01-18',NULL,0,1000002,NULL,1),(45,'2016-01-18',NULL,0,1000003,NULL,1),(46,'2016-01-18',NULL,0,2000001,NULL,1),(47,'2016-01-18',NULL,0,40000001,NULL,1),(48,'2016-01-18',NULL,0,2000000001,NULL,1),(49,'2016-01-18',NULL,0,40000002,NULL,1);
/*!40000 ALTER TABLE `ConnectionInfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Employee`
--

DROP TABLE IF EXISTS `Employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Employee` (
  `EmployeeDBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `currentGomrok` varchar(255) DEFAULT NULL,
  `gomrokZone` varchar(255) DEFAULT NULL,
  `melliCode` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`EmployeeDBId`),
  UNIQUE KEY `UK_1ndh1xg7odg4cvrlny2fv7q6o` (`melliCode`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Employee`
--

LOCK TABLES `Employee` WRITE;
/*!40000 ALTER TABLE `Employee` DISABLE KEYS */;
INSERT INTO `Employee` VALUES (1,'منطقه ويژه اقتصادي شهيد رجايي','تمام مجموعه','admin','مدیر','123456Aa'),(2,'منطقه ويژه اقتصادي شهيد رجايي','تمام مجموعه','system','سیستم','system'),(3,'منطقه ويژه اقتصادي شهيد رجايي','تمام مجموعه','0014472619','تست تست','0014472619'),(22,'منطقه ويژه اقتصادي شهيد رجايي','تمام مجموعه','0017520495','arian arian','bahmanz'),(23,'منطقه ويژه اقتصادي شهيد رجايي','ØªÙ…Ø§Ù… Ù…Ø¬Ù…ÙˆØ¹Ù‡','0016398726','حامد گرجي آرا','shoghal'),(24,'منطقه ويژه اقتصادي شهيد رجايي','تمام مجموعه','2740713381','milad .','Mb7839094');
/*!40000 ALTER TABLE `Employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Employee_Responsibility`
--

DROP TABLE IF EXISTS `Employee_Responsibility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Employee_Responsibility` (
  `Employee_EmployeeDBId` bigint(20) NOT NULL,
  `responsiblities_ResponsibilityDBId` bigint(20) NOT NULL,
  UNIQUE KEY `UK_ma1k6yhpqr7mqoocf7o2wq1vw` (`responsiblities_ResponsibilityDBId`),
  KEY `FK_ma1k6yhpqr7mqoocf7o2wq1vw` (`responsiblities_ResponsibilityDBId`),
  KEY `FK_b68sgw1qdl7159tvyb63gktwg` (`Employee_EmployeeDBId`),
  CONSTRAINT `FK_b68sgw1qdl7159tvyb63gktwg` FOREIGN KEY (`Employee_EmployeeDBId`) REFERENCES `Employee` (`EmployeeDBId`),
  CONSTRAINT `FK_ma1k6yhpqr7mqoocf7o2wq1vw` FOREIGN KEY (`responsiblities_ResponsibilityDBId`) REFERENCES `Responsibility` (`ResponsibilityDBId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Employee_Responsibility`
--

LOCK TABLES `Employee_Responsibility` WRITE;
/*!40000 ALTER TABLE `Employee_Responsibility` DISABLE KEYS */;
INSERT INTO `Employee_Responsibility` VALUES (1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(1,8),(1,9),(1,10),(1,11),(1,12),(1,13),(1,14),(1,15),(1,16),(1,17),(1,18),(1,19),(1,20),(1,21),(1,22),(1,23),(1,24),(1,25),(1,26),(1,27),(1,28),(1,29),(1,30),(1,58),(1,59),(1,62),(1,63),(1,64),(1,65),(1,66),(1,67),(1,68),(1,69),(1,70),(1,71),(1,72),(1,73),(1,74),(1,75),(1,76),(1,77),(1,78),(1,79),(1,80),(1,81),(1,82),(1,83),(1,84),(1,85),(1,86),(1,87),(1,88),(3,31),(3,32),(3,33),(3,34),(3,35),(3,36),(3,37),(3,38),(3,39),(3,40),(3,41),(3,42),(3,43),(3,44),(3,45),(3,46),(3,47),(3,48),(3,49),(3,50),(3,51),(3,52),(3,53),(3,54),(3,55),(3,56),(3,57),(24,60),(24,61);
/*!40000 ALTER TABLE `Employee_Responsibility` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FeatureData`
--

DROP TABLE IF EXISTS `FeatureData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FeatureData` (
  `dbId` bigint(20) NOT NULL AUTO_INCREMENT,
  `beanClassName` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `ruleName` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `andOr` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `secondFieldPath` longtext COLLATE utf8_bin,
  PRIMARY KEY (`dbId`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FeatureData`
--

LOCK TABLES `FeatureData` WRITE;
/*!40000 ALTER TABLE `FeatureData` DISABLE KEYS */;
INSERT INTO `FeatureData` VALUES (1,'EzharnameyeGomroki.ezharnameHayeGomroki',4,'RoatSafarService::قانون چند گارد',0,'accessControlRavie',NULL),(2,'EzharnameyeGomroki.ezharnameHayeGomroki',4,'RoatSafarService::قانون چند گارد',0,'mojavezBargiri',NULL),(3,'EzharnameyeGomroki.ezharnameHayeGomroki',4,'RoatSafarService::قانون عمومی تست',0,'mojavezBargiri',NULL),(4,'EzharnameyeGomroki.ezharnameHayeGomroki',4,'RoatSafarService::خالی',0,'codingSahebKala',NULL),(5,'EzharnameyeGomroki.ezharnameHayeGomroki',4,'RoatSafarService::خالی',0,'ravie',NULL),(6,'EzharnameyeGomroki.ezharnameHayeGomroki',4,'RoatSafarService::کانتینر خالی',0,'codingSahebKala',NULL),(7,'EzharnameyeGomroki.ezharnameHayeGomroki',4,'RoatSafarService::کانتینر خالی',0,'ravie',NULL),(11,'EzharnameyeGomroki.ezharnameHayeGomroki',4,'RoatSafarService::واردات-حمل یکسره-مرزی',0,'ravie',NULL),(12,'EzharnameyeGomroki.ezharnameHayeGomroki',4,'RoatSafarService::واردات-حمل یکسره-مرزی',0,'accessControlRavie',NULL),(13,'EzharnameyeGomroki.ezharnameHayeGomroki',4,'RoatSafarService::واردات-حمل یکسره-مرزی',0,'hamlKind',NULL),(14,'EzharnameyeGomroki.ezharnameHayeGomroki',4,'RoatSafarService::ورود موقت',0,'ravie',NULL),(15,'EzharnameyeGomroki.ezharnameHayeGomroki',4,'RoatSafarService::ورود موقت',0,'hamlKind',NULL),(16,'EzharnameyeGomroki.ezharnameHayeGomroki',4,'RoatSafarService::ترانزیت مقصد گمرک جلفا',0,'ravie',NULL),(17,'EzharnameyeGomroki.ezharnameHayeGomroki',4,'RoatSafarService::ترانزیت مقصد گمرک جلفا',0,'hamlKind',NULL),(18,'EzharnameyeGomroki.ezharnameHayeGomroki',4,'RoatSafarService::صادرات مبدا حمل یکسره',0,'ravie',NULL),(19,'EzharnameyeGomroki.ezharnameHayeGomroki',4,'RoatSafarService::صادرات مبدا حمل یکسره',0,'hamlKind',NULL),(20,'EzharnameyeGomroki.ezharnameHayeGomroki',4,'RoatSafarService::ترانزیت مبدا پل چوبی',0,'ravie',NULL),(21,'EzharnameyeGomroki.ezharnameHayeGomroki',4,'RoatSafarService::ترانزیت مبدا پل چوبی',0,'hamlKind',NULL),(22,'EzharnameyeGomroki.ezharnameHayeGomroki',4,'RoatSafarService::کارنه تیر مقصد',0,'ravie',NULL),(23,'EzharnameyeGomroki.ezharnameHayeGomroki',4,'RoatSafarService::کارنه تیر مقصد',0,'hamlKind',NULL),(24,'EzharnameyeGomroki.ezharnameHayeGomroki',4,'RoatSafarService::حمل یکسره کارنه تیر',0,'ravie',NULL),(25,'EzharnameyeGomroki.ezharnameHayeGomroki',4,'RoatSafarService::حمل یکسره کارنه تیر',0,'hamlKind',NULL),(26,'EzharnameyeGomroki.ezharnameHayeGomroki',4,'RoatSafarService::ترانزیت مبدا گمرک جلفا',0,'ravie',NULL),(27,'EzharnameyeGomroki.ezharnameHayeGomroki',4,'RoatSafarService::ترانزیت مبدا گمرک جلفا',0,'hamlKind',NULL),(28,'EzharnameyeGomroki.ezharnameHayeGomroki',4,'RoatSafarService::صادرات مقصد پل چوبی',0,'ravie',NULL),(29,'EzharnameyeGomroki.ezharnameHayeGomroki',4,'RoatSafarService::صادرات مقصد پل چوبی',0,'hamlKind',NULL);
/*!40000 ALTER TABLE `FeatureData` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FeatureData_FeatureValue`
--

DROP TABLE IF EXISTS `FeatureData_FeatureValue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FeatureData_FeatureValue` (
  `FeatureData_dbId` bigint(20) NOT NULL,
  `featureValues_dbId` bigint(20) NOT NULL,
  UNIQUE KEY `UK_7n4s575waxm7qax5u8rru9udj` (`featureValues_dbId`),
  KEY `FK_7n4s575waxm7qax5u8rru9udj` (`featureValues_dbId`),
  KEY `FK_1qcby7mln08agqfghwmdqstjw` (`FeatureData_dbId`),
  CONSTRAINT `FK_1qcby7mln08agqfghwmdqstjw` FOREIGN KEY (`FeatureData_dbId`) REFERENCES `FeatureData` (`dbId`),
  CONSTRAINT `FK_7n4s575waxm7qax5u8rru9udj` FOREIGN KEY (`featureValues_dbId`) REFERENCES `FeatureValue` (`dbId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FeatureData_FeatureValue`
--

LOCK TABLES `FeatureData_FeatureValue` WRITE;
/*!40000 ALTER TABLE `FeatureData_FeatureValue` DISABLE KEYS */;
INSERT INTO `FeatureData_FeatureValue` VALUES (11,12),(12,13),(13,14),(14,15),(15,16),(16,17),(17,18),(18,19),(19,20),(20,21),(21,22),(22,23),(23,24),(24,25),(25,26),(26,27),(27,28),(28,29),(29,30);
/*!40000 ALTER TABLE `FeatureData_FeatureValue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FeatureValue`
--

DROP TABLE IF EXISTS `FeatureValue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FeatureValue` (
  `dbId` bigint(20) NOT NULL AUTO_INCREMENT,
  `beanClassName` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `ruleName` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `comparator` int(11) DEFAULT NULL,
  `val` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `sampleSet_dbId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`dbId`),
  KEY `FK_rxtu9mjcpn0gtm53q5g6l9cfg` (`sampleSet_dbId`),
  CONSTRAINT `FK_rxtu9mjcpn0gtm53q5g6l9cfg` FOREIGN KEY (`sampleSet_dbId`) REFERENCES `SampleSet` (`dbId`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FeatureValue`
--

LOCK TABLES `FeatureValue` WRITE;
/*!40000 ALTER TABLE `FeatureValue` DISABLE KEYS */;
INSERT INTO `FeatureValue` VALUES (1,'EzharnameyeGomroki.ezharnameHayeGomroki',5,'RoatSafarService::قانون چند گارد',0,NULL,3),(2,'EzharnameyeGomroki.ezharnameHayeGomroki',5,'RoatSafarService::قانون چند گارد',0,NULL,4),(3,'EzharnameyeGomroki.ezharnameHayeGomroki',5,'RoatSafarService::قانون عمومی تست',0,NULL,4),(4,'EzharnameyeGomroki.ezharnameHayeGomroki',5,'RoatSafarService::خالی',0,NULL,7),(5,'EzharnameyeGomroki.ezharnameHayeGomroki',5,'RoatSafarService::خالی',0,'Varedat',NULL),(6,'EzharnameyeGomroki.ezharnameHayeGomroki',5,'RoatSafarService::کانتینر خالی',0,NULL,7),(7,'EzharnameyeGomroki.ezharnameHayeGomroki',5,'RoatSafarService::کانتینر خالی',0,'Varedat',NULL),(8,'EzharnameyeGomroki.ezharnameHayeGomroki',5,'RoatSafarService::kantiner',0,NULL,7),(12,'EzharnameyeGomroki.ezharnameHayeGomroki',5,'RoatSafarService::واردات-حمل یکسره-مرزی',0,'Varedat',NULL),(13,'EzharnameyeGomroki.ezharnameHayeGomroki',5,'RoatSafarService::واردات-حمل یکسره-مرزی',0,NULL,13),(14,'EzharnameyeGomroki.ezharnameHayeGomroki',5,'RoatSafarService::واردات-حمل یکسره-مرزی',0,'HAMLYEKSARE',NULL),(15,'EzharnameyeGomroki.ezharnameHayeGomroki',5,'RoatSafarService::ورود موقت',0,'Varedat',NULL),(16,'EzharnameyeGomroki.ezharnameHayeGomroki',5,'RoatSafarService::ورود موقت',0,'ARZYABIDARMAHAL',NULL),(17,'EzharnameyeGomroki.ezharnameHayeGomroki',5,'RoatSafarService::ترانزیت مقصد گمرک جلفا',0,'TranzitMabda',NULL),(18,'EzharnameyeGomroki.ezharnameHayeGomroki',5,'RoatSafarService::ترانزیت مقصد گمرک جلفا',0,'ANBARI',NULL),(19,'EzharnameyeGomroki.ezharnameHayeGomroki',5,'RoatSafarService::صادرات مبدا حمل یکسره',0,'Saderat',NULL),(20,'EzharnameyeGomroki.ezharnameHayeGomroki',5,'RoatSafarService::صادرات مبدا حمل یکسره',0,'HAMLYEKSARE',NULL),(21,'EzharnameyeGomroki.ezharnameHayeGomroki',5,'RoatSafarService::ترانزیت مبدا پل چوبی',0,'TranzitMabda',NULL),(22,'EzharnameyeGomroki.ezharnameHayeGomroki',5,'RoatSafarService::ترانزیت مبدا پل چوبی',0,'HAMLYEKSARE',NULL),(23,'EzharnameyeGomroki.ezharnameHayeGomroki',5,'RoatSafarService::کارنه تیر مقصد',0,'KarneTir',NULL),(24,'EzharnameyeGomroki.ezharnameHayeGomroki',5,'RoatSafarService::کارنه تیر مقصد',0,'ANBARI',NULL),(25,'EzharnameyeGomroki.ezharnameHayeGomroki',5,'RoatSafarService::حمل یکسره کارنه تیر',0,'KarneTir',NULL),(26,'EzharnameyeGomroki.ezharnameHayeGomroki',5,'RoatSafarService::حمل یکسره کارنه تیر',0,'HAMLYEKSARE',NULL),(27,'EzharnameyeGomroki.ezharnameHayeGomroki',5,'RoatSafarService::ترانزیت مبدا گمرک جلفا',0,'TranzitMabda',NULL),(28,'EzharnameyeGomroki.ezharnameHayeGomroki',5,'RoatSafarService::ترانزیت مبدا گمرک جلفا',0,'ANBARI',NULL),(29,'EzharnameyeGomroki.ezharnameHayeGomroki',5,'RoatSafarService::صادرات مقصد پل چوبی',0,'Saderat',NULL),(30,'EzharnameyeGomroki.ezharnameHayeGomroki',5,'RoatSafarService::صادرات مقصد پل چوبی',0,'HAMLYEKSARE',NULL);
/*!40000 ALTER TABLE `FeatureValue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GateStatus`
--

DROP TABLE IF EXISTS `GateStatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GateStatus` (
  `dBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `emp_EmployeeDBId` bigint(20) DEFAULT NULL,
  `gate_dBId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`dBId`),
  KEY `FK_65egrmi4y0vfrb9fqs077mu6j` (`emp_EmployeeDBId`),
  KEY `FK_c8sswylqu1cqoy8rf153s4syd` (`gate_dBId`),
  CONSTRAINT `FK_65egrmi4y0vfrb9fqs077mu6j` FOREIGN KEY (`emp_EmployeeDBId`) REFERENCES `Employee` (`EmployeeDBId`),
  CONSTRAINT `FK_c8sswylqu1cqoy8rf153s4syd` FOREIGN KEY (`gate_dBId`) REFERENCES `baskool` (`dBId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GateStatus`
--

LOCK TABLES `GateStatus` WRITE;
/*!40000 ALTER TABLE `GateStatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `GateStatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GuardState`
--

DROP TABLE IF EXISTS `GuardState`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GuardState` (
  `dBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `khoroj` tinyint(1) DEFAULT NULL,
  `khorojTime` datetime DEFAULT NULL,
  `voroud` tinyint(1) DEFAULT NULL,
  `voroudTime` datetime DEFAULT NULL,
  `guardKhoruj_dBId` bigint(20) DEFAULT NULL,
  `guardKhorujEmployee_EmployeeDBId` bigint(20) DEFAULT NULL,
  `guardVorud_dBId` bigint(20) DEFAULT NULL,
  `guardVorudEmployee_EmployeeDBId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`dBId`),
  KEY `FK_1hoduv4hak1irnce8dmr0yvfb` (`guardKhoruj_dBId`),
  KEY `FK_1ucb9amcpo1plu3p1drnheipw` (`guardKhorujEmployee_EmployeeDBId`),
  KEY `FK_absk25t8juprvuql8edl8x38` (`guardVorud_dBId`),
  KEY `FK_iwq7ekqkd5uvarplyplmjbcqr` (`guardVorudEmployee_EmployeeDBId`),
  CONSTRAINT `FK_1hoduv4hak1irnce8dmr0yvfb` FOREIGN KEY (`guardKhoruj_dBId`) REFERENCES `baskool` (`dBId`),
  CONSTRAINT `FK_1ucb9amcpo1plu3p1drnheipw` FOREIGN KEY (`guardKhorujEmployee_EmployeeDBId`) REFERENCES `Employee` (`EmployeeDBId`),
  CONSTRAINT `FK_absk25t8juprvuql8edl8x38` FOREIGN KEY (`guardVorud_dBId`) REFERENCES `baskool` (`dBId`),
  CONSTRAINT `FK_iwq7ekqkd5uvarplyplmjbcqr` FOREIGN KEY (`guardVorudEmployee_EmployeeDBId`) REFERENCES `Employee` (`EmployeeDBId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GuardState`
--

LOCK TABLES `GuardState` WRITE;
/*!40000 ALTER TABLE `GuardState` DISABLE KEYS */;
/*!40000 ALTER TABLE `GuardState` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Hamel`
--

DROP TABLE IF EXISTS `Hamel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Hamel` (
  `dBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `barcode` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `isIranian` tinyint(1) DEFAULT NULL,
  `kind` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `shenase` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `shenasehColor` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `weight` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`dBId`),
  UNIQUE KEY `UK_r9y5rfs94nqoaxkbthsyf7kay` (`shenase`),
  KEY `shenaseha` (`shenase`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Hamel`
--

LOCK TABLES `Hamel` WRITE;
/*!40000 ALTER TABLE `Hamel` DISABLE KEYS */;
INSERT INTO `Hamel` VALUES (1,'',1,'کاميون يخچالي','11ع11111','زرد',1200),(2,'',1,'تريلر تانکر','22ع22222','زرد',15000),(3,'232323',1,'نيسان','67ع67776','قرمز',232323),(6,'1',1,'کفی تریلی','23ع38287','',NULL),(7,'1',1,'کفی تریلی','35ع28378','',NULL),(8,'',1,'تريلر کفي','12ع12121','زرد',1500),(9,'',1,'تريلر کفي','12ع34567','زرد',1),(10,'',1,'وانت','33ع33333','زرد',123123),(11,NULL,1,'وانت','44ع44444','زرد',1000),(12,NULL,1,'وانت','66ع66666','زرد',NULL),(13,'',1,'تريلر کفي','77ع77777','زرد',1200),(14,'1233598556',1,'کفی تریلی','35ع28387','',NULL),(15,'',1,'تريلر کفي','13ع13131','زرد',15000),(16,'',1,'تريلر کفي','16ع16161','زرد',15000),(17,'',1,'تريلر کفي','12ع11112','زرد',1200),(18,'',1,'تريلر کفي','13ع11113','زرد',1200),(19,'',0,'تريلر کفي','aa1111,bb2222','زرد',100),(20,'',0,'تريلر کفي','AA1111,BB2222','زرد',35460),(21,NULL,1,'وانت','21ع12121','زرد',NULL),(22,'',1,'تريلر کفي','15ع11115','زرد',1200),(23,'',1,'کمپرسي تک','88ع88888','زرد',1212),(24,'',1,'تريلر کفي','99ع99999','زرد',33),(25,NULL,1,'کاميون تک','95ع95959','زرد',NULL),(26,'',0,'تريلر کفي','12ab3','زرد',1500),(27,'',1,'تريلر کفي','31ع11131','زرد',1200),(28,'',1,'تريلر کفي','43ع11143','زرد',1200),(29,'',1,'تريلر کفي','41ع11141','زرد',1200),(30,'',1,'تريلر کفي','51ع11151','زرد',1200),(31,'',1,'تريلر کفي','32ع12132','زرد',1200),(32,'',1,'تريلر کفي','61ع11161','زرد',1200),(33,'',0,'تريلر کفي','2222','زرد',233),(34,'',0,'تريلر کفي','222','زرد',345),(35,'',0,'تريلر کفي','1234567','زرد',1200),(36,NULL,1,'وانت','12ع11121','زرد',NULL),(37,'',1,'تريلر کفي','55ع55555','زرد',42424),(38,'',0,'تريلر کفي','12fd367','سفید',14000),(39,'0123',1,'تريلر کفي','25ع25235','زرد',10),(40,'12345',1,'تريلر کفي','22ع55533','زرد',10),(41,NULL,1,'کمرشکن','11ع11112','سفید',NULL),(42,NULL,1,'تريلر چادری','11ع11122','زرد',NULL),(43,NULL,1,'کمپرسي تک','11ع11133','زرد',NULL),(44,'-',1,'تريلر کفي','12ع11111','زرد',0);
/*!40000 ALTER TABLE `Hamel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `HamelParkingState`
--

DROP TABLE IF EXISTS `HamelParkingState`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HamelParkingState` (
  `dBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `ParingNo` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `parkingKhorouj` datetime DEFAULT NULL,
  `parkingVoroud` datetime DEFAULT NULL,
  PRIMARY KEY (`dBId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `HamelParkingState`
--

LOCK TABLES `HamelParkingState` WRITE;
/*!40000 ALTER TABLE `HamelParkingState` DISABLE KEYS */;
/*!40000 ALTER TABLE `HamelParkingState` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `HamelState`
--

DROP TABLE IF EXISTS `HamelState`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HamelState` (
  `dbId` bigint(20) NOT NULL AUTO_INCREMENT,
  `GuardState_dBId` bigint(20) DEFAULT NULL,
  `hamelParkingState_dBId` bigint(20) DEFAULT NULL,
  `tozinGomrokiPolicy_dBId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`dbId`),
  KEY `FK_2ab0jmgjv6tq2j1n56cxk8o81` (`GuardState_dBId`),
  KEY `FK_bcdv5c954chpl52tui3xl28bd` (`hamelParkingState_dBId`),
  KEY `FK_l5hadm8elkxl1359ddhhboexy` (`tozinGomrokiPolicy_dBId`),
  CONSTRAINT `FK_2ab0jmgjv6tq2j1n56cxk8o81` FOREIGN KEY (`GuardState_dBId`) REFERENCES `GuardState` (`dBId`),
  CONSTRAINT `FK_bcdv5c954chpl52tui3xl28bd` FOREIGN KEY (`hamelParkingState_dBId`) REFERENCES `HamelParkingState` (`dBId`),
  CONSTRAINT `FK_l5hadm8elkxl1359ddhhboexy` FOREIGN KEY (`tozinGomrokiPolicy_dBId`) REFERENCES `TozinGomrokiPolicy` (`dBId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `HamelState`
--

LOCK TABLES `HamelState` WRITE;
/*!40000 ALTER TABLE `HamelState` DISABLE KEYS */;
/*!40000 ALTER TABLE `HamelState` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Parking`
--

DROP TABLE IF EXISTS `Parking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Parking` (
  `dbId` bigint(20) NOT NULL AUTO_INCREMENT,
  `parkingCap` int(11) DEFAULT NULL,
  `parkingName` char(1) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`dbId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Parking`
--

LOCK TABLES `Parking` WRITE;
/*!40000 ALTER TABLE `Parking` DISABLE KEYS */;
/*!40000 ALTER TABLE `Parking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ParkingManager`
--

DROP TABLE IF EXISTS `ParkingManager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ParkingManager` (
  `dbId` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`dbId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ParkingManager`
--

LOCK TABLES `ParkingManager` WRITE;
/*!40000 ALTER TABLE `ParkingManager` DISABLE KEYS */;
/*!40000 ALTER TABLE `ParkingManager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ParkingManager_Parking`
--

DROP TABLE IF EXISTS `ParkingManager_Parking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ParkingManager_Parking` (
  `ParkingManager_dbId` int(11) NOT NULL,
  `cParkings_dbId` bigint(20) NOT NULL,
  UNIQUE KEY `UK_8y8ttow18ajhdnk2iythrsw0l` (`cParkings_dbId`),
  KEY `FK_8y8ttow18ajhdnk2iythrsw0l` (`cParkings_dbId`),
  KEY `FK_3v1rjihbe0c9nid9s5ytow5h1` (`ParkingManager_dbId`),
  CONSTRAINT `FK_3v1rjihbe0c9nid9s5ytow5h1` FOREIGN KEY (`ParkingManager_dbId`) REFERENCES `ParkingManager` (`dbId`),
  CONSTRAINT `FK_8y8ttow18ajhdnk2iythrsw0l` FOREIGN KEY (`cParkings_dbId`) REFERENCES `Parking` (`dbId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ParkingManager_Parking`
--

LOCK TABLES `ParkingManager_Parking` WRITE;
/*!40000 ALTER TABLE `ParkingManager_Parking` DISABLE KEYS */;
/*!40000 ALTER TABLE `ParkingManager_Parking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Ranandeh`
--

DROP TABLE IF EXISTS `Ranandeh`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Ranandeh` (
  `dBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `family` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `idCode` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `isIranian` tinyint(1) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `sex` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`dBId`),
  UNIQUE KEY `UK_qnbbyh561opymb88f41kupswq` (`idCode`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Ranandeh`
--

LOCK TABLES `Ranandeh` WRITE;
/*!40000 ALTER TABLE `Ranandeh` DISABLE KEYS */;
INSERT INTO `Ranandeh` VALUES (1,'sdfa','0123456789',1,'jsj',1),(2,'تستی','0014472619',1,'تست',1),(3,'ع','3980205053',1,'ع',1),(4,'123','3380381187',1,'123',1),(5,'ريحانه','0017520495',1,'آرين',1),(6,'Bale','0014372619',0,'Garet',1),(7,'test','2740713381',1,'test',1),(8,'مردي','0075639173',1,'مرد',1);
/*!40000 ALTER TABLE `Ranandeh` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Rep_Temp_Template`
--

DROP TABLE IF EXISTS `Rep_Temp_Template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Rep_Temp_Template` (
  `dbId` bigint(20) NOT NULL AUTO_INCREMENT,
  `reportName` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `templateName` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `userId` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`dbId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Rep_Temp_Template`
--

LOCK TABLES `Rep_Temp_Template` WRITE;
/*!40000 ALTER TABLE `Rep_Temp_Template` DISABLE KEYS */;
/*!40000 ALTER TABLE `Rep_Temp_Template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Rep_Temp_TemplateParameter`
--

DROP TABLE IF EXISTS `Rep_Temp_TemplateParameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Rep_Temp_TemplateParameter` (
  `dbId` bigint(20) NOT NULL AUTO_INCREMENT,
  `enumClass` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `isPrimitive` tinyint(1) DEFAULT NULL,
  `paramName` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `paramValue` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  PRIMARY KEY (`dbId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Rep_Temp_TemplateParameter`
--

LOCK TABLES `Rep_Temp_TemplateParameter` WRITE;
/*!40000 ALTER TABLE `Rep_Temp_TemplateParameter` DISABLE KEYS */;
/*!40000 ALTER TABLE `Rep_Temp_TemplateParameter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Rep_Temp_Template_Rep_Temp_TemplateParameter`
--

DROP TABLE IF EXISTS `Rep_Temp_Template_Rep_Temp_TemplateParameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Rep_Temp_Template_Rep_Temp_TemplateParameter` (
  `Rep_Temp_Template_dbId` bigint(20) NOT NULL,
  `params_dbId` bigint(20) NOT NULL,
  UNIQUE KEY `UK_5bi3x9p2hnfmvlap0a3yxscav` (`params_dbId`),
  KEY `FK_5bi3x9p2hnfmvlap0a3yxscav` (`params_dbId`),
  KEY `FK_klsk3rk3ioeyuvmoam21cxd3` (`Rep_Temp_Template_dbId`),
  CONSTRAINT `FK_5bi3x9p2hnfmvlap0a3yxscav` FOREIGN KEY (`params_dbId`) REFERENCES `Rep_Temp_TemplateParameter` (`dbId`),
  CONSTRAINT `FK_klsk3rk3ioeyuvmoam21cxd3` FOREIGN KEY (`Rep_Temp_Template_dbId`) REFERENCES `Rep_Temp_Template` (`dbId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Rep_Temp_Template_Rep_Temp_TemplateParameter`
--

LOCK TABLES `Rep_Temp_Template_Rep_Temp_TemplateParameter` WRITE;
/*!40000 ALTER TABLE `Rep_Temp_Template_Rep_Temp_TemplateParameter` DISABLE KEYS */;
/*!40000 ALTER TABLE `Rep_Temp_Template_Rep_Temp_TemplateParameter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Responsibility`
--

DROP TABLE IF EXISTS `Responsibility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Responsibility` (
  `DTYPE` varchar(31) COLLATE utf8_bin NOT NULL,
  `ResponsibilityDBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `end_date` datetime DEFAULT NULL,
  `gomrok` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `empl_EmployeeDBId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ResponsibilityDBId`),
  KEY `FK_58tc688dc9cjmf5vq3418u47n` (`empl_EmployeeDBId`),
  CONSTRAINT `FK_58tc688dc9cjmf5vq3418u47n` FOREIGN KEY (`empl_EmployeeDBId`) REFERENCES `Employee` (`EmployeeDBId`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Responsibility`
--

LOCK TABLES `Responsibility` WRITE;
/*!40000 ALTER TABLE `Responsibility` DISABLE KEYS */;
INSERT INTO `Responsibility` VALUES ('SuperUser',1,NULL,'غرب تهران','2015-03-21 04:10:45',1),('AreaManagerResp',2,NULL,'غرب تهران','2015-03-21 04:12:29',1),('EbtalBadAz2SaatResp',3,NULL,'غرب تهران','2015-03-21 04:12:31',1),('EbtalBaskoolResp',4,NULL,'غرب تهران','2015-03-21 04:12:33',1),('EbtalBaskoolDovvomResp',5,NULL,'غرب تهران','2015-03-21 04:12:34',1),('EslaaheTozinResp',6,NULL,'غرب تهران','2015-03-21 04:12:36',1),('FaghatKhoroujSaderatResp',7,NULL,'غرب تهران','2015-03-21 04:12:38',1),('GaurdResp',8,NULL,'غرب تهران','2015-03-21 04:12:40',1),('Gozaresh2BaskooleResp',9,NULL,'غرب تهران','2015-03-21 04:12:42',1),('GozareshGuardResp',10,NULL,'غرب تهران','2015-03-21 04:12:45',1),('GozareshTedadBaskoolResp',11,NULL,'غرب تهران','2015-03-21 04:12:48',1),('HamleYeksareResp',12,NULL,'غرب تهران','2015-03-21 04:12:50',1),('MojavezKhoroujResc',13,NULL,'غرب تهران','2015-03-21 04:12:52',1),('NumberOfTozinsReportResp',14,NULL,'غرب تهران','2015-03-21 04:12:53',1),('MonitoringResp',15,NULL,'غرب تهران','2015-03-21 04:12:56',1),('PrintResp',16,NULL,'غرب تهران','2015-03-21 04:12:58',1),('SetHamlYekSareResp',17,NULL,'غرب تهران','2015-03-21 04:12:59',1),('TarifBaskoolResp',18,NULL,'غرب تهران','2015-03-21 04:13:01',1),('TozinBaSanadResp',19,NULL,'غرب تهران','2015-03-21 04:13:03',1),('ParkingResp',20,NULL,'غرب تهران','2015-03-21 04:13:05',1),('TozinBarTedadiResp',21,NULL,'غرب تهران','2015-03-21 04:13:07',1),('TozinBaSanadSubResp',22,NULL,'غرب تهران','2015-03-21 04:13:10',1),('VijeResp',23,NULL,'غرب تهران','2015-03-21 04:13:13',1),('TozinVijeResp',24,NULL,'غرب تهران','2015-03-21 04:13:16',1),('IjadMojavezVoroudResp',25,NULL,'غرب تهران','2015-03-21 04:13:21',1),('TozinDastiResp',26,NULL,'غرب تهران','2015-03-21 04:13:26',1),('TozinGheyreGomrokiResp',27,NULL,'غرب تهران','2015-03-22 01:59:33',1),('TozinVorudResp',28,NULL,'غرب تهران','2015-03-22 01:59:41',1),('NewEbtalResp',29,NULL,'غرب تهران','2015-08-31 16:57:38',1),('SafarManagerResp',30,NULL,'غرب تهران','2015-08-31 16:57:40',1),('EbtalBadAz2SaatResp',31,NULL,'غرب تهران','2015-09-07 15:27:11',1),('EbtalBaskoolResp',32,NULL,'غرب تهران','2015-09-07 15:27:12',1),('EbtalBaskoolDovvomResp',33,NULL,'غرب تهران','2015-09-07 15:27:15',1),('EslaaheTozinResp',34,NULL,'غرب تهران','2015-09-07 15:27:17',1),('FaghatKhoroujSaderatResp',35,NULL,'غرب تهران','2015-09-07 15:27:20',1),('GaurdResp',36,NULL,'غرب تهران','2015-09-07 15:27:23',1),('Gozaresh2BaskooleResp',37,NULL,'غرب تهران','2015-09-07 15:27:25',1),('GozareshTedadBaskoolResp',38,NULL,'غرب تهران','2015-09-07 15:27:33',1),('HamleYeksareResp',39,NULL,'غرب تهران','2015-09-07 15:27:36',1),('IjadMojavezVoroudResp',40,NULL,'غرب تهران','2015-09-07 15:27:38',1),('MojavezKhoroujResc',41,NULL,'غرب تهران','2015-09-07 15:27:41',1),('MonitoringResp',42,NULL,'غرب تهران','2015-09-07 15:27:43',1),('NewEbtalResp',43,NULL,'غرب تهران','2015-09-07 15:27:46',1),('NumberOfTozinsReportResp',44,NULL,'غرب تهران','2015-09-07 15:27:55',1),('ParkingResp',45,NULL,'غرب تهران','2015-09-07 15:27:57',1),('PrintResp',46,NULL,'غرب تهران','2015-09-07 15:27:59',1),('SafarManagerResp',47,NULL,'غرب تهران','2015-09-07 15:28:02',1),('SetHamlYekSareResp',48,NULL,'غرب تهران','2015-09-07 15:28:04',1),('TarifBaskoolResp',49,NULL,'غرب تهران','2015-09-07 15:28:07',1),('TozinBaSanadSubResp',50,NULL,'غرب تهران','2015-09-07 15:28:12',1),('TozinBarTedadiResp',51,NULL,'غرب تهران','2015-09-07 15:28:20',1),('TozinDastiResp',52,NULL,'غرب تهران','2015-09-07 15:28:23',1),('TozinGheyreGomrokiResp',53,NULL,'غرب تهران','2015-09-07 15:28:26',1),('TozinVijeResp',54,NULL,'غرب تهران','2015-09-07 15:28:28',1),('TozinVorudResp',55,NULL,'غرب تهران','2015-09-07 15:28:30',1),('VijeResp',56,NULL,'غرب تهران','2015-09-07 15:28:33',1),('SuperUser',57,NULL,NULL,'2015-09-07 15:28:35',1),('AreaManagerResp',58,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-03-30 09:02:11',1),('GozareshGuardResp',59,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-04-12 18:09:23',1),('EbtalBaskoolResp',60,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-04-20 11:00:34',1),('SuperUser',61,NULL,NULL,'2016-04-20 11:00:50',1),('SafarManagerResp',62,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-05-28 12:56:00',1),('MonitoringResp',63,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-05-28 12:56:58',1),('EbtalBadAz2SaatResp',64,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-05-28 12:57:17',1),('EbtalBaskoolResp',65,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-05-28 12:57:18',1),('EbtalBaskoolDovvomResp',66,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-05-28 12:57:20',1),('EslaaheTozinResp',67,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-05-28 12:57:21',1),('FaghatKhoroujSaderatResp',68,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-05-28 12:57:23',1),('GaurdResp',69,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-05-28 12:57:25',1),('GozareshTedadBaskoolResp',70,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-05-28 12:57:26',1),('Gozaresh2BaskooleResp',71,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-05-28 12:57:28',1),('HamleYeksareResp',72,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-05-28 12:57:29',1),('IjadMojavezVoroudResp',73,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-05-28 12:57:31',1),('MojavezKhoroujResc',74,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-05-28 12:57:32',1),('NewEbtalResp',75,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-05-28 12:57:34',1),('NumberOfTozinsReportResp',76,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-05-28 12:57:36',1),('ParkingResp',77,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-05-28 12:57:37',1),('PrintResp',78,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-05-28 12:57:39',1),('SetHamlYekSareResp',79,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-05-28 12:57:41',1),('TarifBaskoolResp',80,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-05-28 12:57:43',1),('TozinBaSanadResp',81,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-05-28 12:57:45',1),('TozinBarTedadiResp',82,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-05-28 12:57:46',1),('TozinBaSanadSubResp',83,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-05-28 12:57:49',1),('TozinDastiResp',84,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-05-28 12:57:51',1),('TozinVijeResp',85,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-05-28 12:57:54',1),('VijeResp',86,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-05-28 12:57:55',1),('TozinGheyreGomrokiResp',87,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-05-28 12:59:55',1),('TozinVorudResp',88,NULL,'منطقه ويژه اقتصادي شهيد رجايي','2016-05-28 12:59:59',1);
/*!40000 ALTER TABLE `Responsibility` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Rule`
--

DROP TABLE IF EXISTS `Rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Rule` (
  `dbId` bigint(20) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `ruleName` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ruleServiceName` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `src` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `synced` tinyint(1) DEFAULT NULL,
  `uniqueId` bigint(20) DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `targetClass_dbId` bigint(20) DEFAULT NULL,
  `ip` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`dbId`),
  KEY `uniqueId` (`uniqueId`),
  KEY `FK_gfr3hv915rvv1gmmnt4luk1bu` (`targetClass_dbId`),
  CONSTRAINT `FK_gfr3hv915rvv1gmmnt4luk1bu` FOREIGN KEY (`targetClass_dbId`) REFERENCES `RuleClass` (`dbId`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Rule`
--

LOCK TABLES `Rule` WRITE;
/*!40000 ALTER TABLE `Rule` DISABLE KEYS */;
INSERT INTO `Rule` VALUES (1,'2015-03-21 04:17:52',1,1,'قانون عمومی تست','ir.customs.baskool.rac.RoatSafarService',NULL,NULL,NULL,'2016-01-26 19:26:45',1,'172.16.111.178','مدیر (admin)'),(2,'2015-03-21 14:00:18',1,0,'قانون چند گارد','ir.customs.baskool.rac.RoatSafarService',NULL,NULL,NULL,'2015-03-21 17:09:03',2,NULL,NULL),(3,'2015-03-23 10:01:00',1,6,'تست کاستمز','ir.customs.baskool.rac.RoatSafarService',NULL,NULL,NULL,'2015-03-23 10:02:31',5,NULL,NULL),(4,'2015-09-07 17:58:20',1,10,'خالی','ir.customs.baskool.rac.RoatSafarService',NULL,NULL,NULL,'2015-09-08 09:41:26',8,'172.16.111.85','مدیر (admin)'),(5,'2015-09-08 09:50:14',1,10,'کانتینر خالی','ir.customs.baskool.rac.RoatSafarService',NULL,NULL,NULL,'2015-09-08 09:50:47',11,'172.16.111.85','تست تست (0014472619)'),(6,'2015-09-12 18:18:25',1,10,'kantiner','ir.customs.baskool.rac.RoatSafarService',NULL,NULL,NULL,'2015-09-12 18:20:55',14,'172.16.111.85','تست تست (0014472619)'),(7,'2016-01-26 19:32:21',0,1,'مسیر تستت','ir.customs.baskool.rac.RoatSafarService',NULL,NULL,NULL,'2016-01-26 19:59:18',17,'172.16.111.178','مدیر (admin)'),(8,'2016-03-24 18:18:34',0,0,'قانون عمومی  محمدزاده','ir.customs.baskool.rac.RoatSafarService',NULL,NULL,NULL,'2016-03-24 18:19:28',18,'172.17.193.186','مدیر (admin)'),(10,'2016-03-26 18:03:04',0,10,'واردات-حمل یکسره-مرزی','ir.customs.baskool.rac.RoatSafarService',NULL,NULL,NULL,'2016-03-26 18:45:12',22,'172.17.193.193','مدیر (admin)'),(11,'2016-03-30 09:23:25',0,2,'ورود موقت','ir.customs.baskool.rac.RoatSafarService',NULL,NULL,NULL,'2016-03-30 09:24:23',25,'172.16.111.85','مدیر (admin)'),(12,'2016-03-30 09:24:50',0,4,'ترانزیت مقصد گمرک جلفا','ir.customs.baskool.rac.RoatSafarService',NULL,NULL,NULL,'2016-03-30 09:27:04',28,'172.16.111.85','مدیر (admin)'),(13,'2016-03-30 09:27:15',0,3,'صادرات مبدا حمل یکسره','ir.customs.baskool.rac.RoatSafarService',NULL,NULL,NULL,'2016-03-30 09:28:55',31,'172.16.111.85','مدیر (admin)'),(14,'2016-03-30 09:29:12',0,9,'ترانزیت مبدا پل چوبی','ir.customs.baskool.rac.RoatSafarService',NULL,NULL,NULL,'2016-03-30 09:30:50',34,'172.16.111.85','مدیر (admin)'),(15,'2016-03-30 09:31:36',0,4,'کارنه تیر مقصد','ir.customs.baskool.rac.RoatSafarService',NULL,NULL,NULL,'2016-03-30 09:32:30',37,'172.16.111.85','مدیر (admin)'),(16,'2016-03-30 09:41:52',0,1,'حمل یکسره کارنه تیر','ir.customs.baskool.rac.RoatSafarService',NULL,NULL,NULL,'2016-03-30 09:42:38',40,'172.16.111.85','مدیر (admin)'),(17,'2016-03-30 09:42:48',0,10,'ترانزیت مبدا گمرک جلفا','ir.customs.baskool.rac.RoatSafarService',NULL,NULL,NULL,'2016-03-30 09:43:50',43,'172.16.111.85','مدیر (admin)'),(18,'2016-03-30 09:44:14',0,10,'صادرات مقصد پل چوبی','ir.customs.baskool.rac.RoatSafarService',NULL,NULL,NULL,'2016-03-30 09:45:35',46,'172.16.111.85','مدیر (admin)');
/*!40000 ALTER TABLE `Rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RuleClass`
--

DROP TABLE IF EXISTS `RuleClass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RuleClass` (
  `dbId` bigint(20) NOT NULL AUTO_INCREMENT,
  `beanClassName` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `ruleName` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `className` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `fieldName` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `hasValue` tinyint(1) DEFAULT NULL,
  `isCollection` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`dbId`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RuleClass`
--

LOCK TABLES `RuleClass` WRITE;
/*!40000 ALTER TABLE `RuleClass` DISABLE KEYS */;
INSERT INTO `RuleClass` VALUES (1,'RoatSafar',1,'RoatSafarService::قانون عمومی تست','RoatSafar','',0,NULL),(2,'RoatSafar',1,'RoatSafarService::قانون چند گارد','RoatSafar','',1,NULL),(3,'Safar',2,'RoatSafarService::قانون چند گارد','Safar','safar',1,NULL),(4,'EzharnameyeGomroki',3,'RoatSafarService::قانون چند گارد','EzharnameyeGomroki','ezharnameHayeGomroki',1,NULL),(5,'RoatSafar',1,'RoatSafarService::تست کاستمز','RoatSafar','',0,NULL),(6,'Safar',2,'RoatSafarService::قانون عمومی تست','Safar','safar',0,NULL),(7,'EzharnameyeGomroki',3,'RoatSafarService::قانون عمومی تست','EzharnameyeGomroki','ezharnameHayeGomroki',0,NULL),(8,'RoatSafar',1,'RoatSafarService::خالی','RoatSafar','',1,NULL),(9,'Safar',2,'RoatSafarService::خالی','Safar','safar',1,0),(10,'EzharnameyeGomroki',3,'RoatSafarService::خالی','EzharnameyeGomroki','ezharnameHayeGomroki',1,NULL),(11,'RoatSafar',1,'RoatSafarService::کانتینر خالی','RoatSafar','',1,NULL),(12,'Safar',2,'RoatSafarService::کانتینر خالی','Safar','safar',1,0),(13,'EzharnameyeGomroki',3,'RoatSafarService::کانتینر خالی','EzharnameyeGomroki','ezharnameHayeGomroki',1,NULL),(14,'RoatSafar',1,'RoatSafarService::kantiner','RoatSafar','',1,NULL),(15,'Safar',2,'RoatSafarService::kantiner','Safar','safar',1,0),(16,'EzharnameyeGomroki',3,'RoatSafarService::kantiner','EzharnameyeGomroki','ezharnameHayeGomroki',1,NULL),(17,'RoatSafar',1,'RoatSafarService::مسیر تستت','RoatSafar','',0,NULL),(18,'RoatSafar',1,'RoatSafarService::قانون عمومی  محمدزاده','RoatSafar','',0,NULL),(22,'RoatSafar',1,'RoatSafarService::واردات-حمل یکسره-مرزی','RoatSafar','',1,NULL),(23,'Safar',2,'RoatSafarService::واردات-حمل یکسره-مرزی','Safar','safar',1,0),(24,'EzharnameyeGomroki',3,'RoatSafarService::واردات-حمل یکسره-مرزی','EzharnameyeGomroki','ezharnameHayeGomroki',1,NULL),(25,'RoatSafar',1,'RoatSafarService::ورود موقت','RoatSafar','',1,NULL),(26,'Safar',2,'RoatSafarService::ورود موقت','Safar','safar',1,0),(27,'EzharnameyeGomroki',3,'RoatSafarService::ورود موقت','EzharnameyeGomroki','ezharnameHayeGomroki',1,NULL),(28,'RoatSafar',1,'RoatSafarService::ترانزیت مقصد گمرک جلفا','RoatSafar','',1,NULL),(29,'Safar',2,'RoatSafarService::ترانزیت مقصد گمرک جلفا','Safar','safar',1,0),(30,'EzharnameyeGomroki',3,'RoatSafarService::ترانزیت مقصد گمرک جلفا','EzharnameyeGomroki','ezharnameHayeGomroki',1,NULL),(31,'RoatSafar',1,'RoatSafarService::صادرات مبدا حمل یکسره','RoatSafar','',1,NULL),(32,'Safar',2,'RoatSafarService::صادرات مبدا حمل یکسره','Safar','safar',1,0),(33,'EzharnameyeGomroki',3,'RoatSafarService::صادرات مبدا حمل یکسره','EzharnameyeGomroki','ezharnameHayeGomroki',1,NULL),(34,'RoatSafar',1,'RoatSafarService::ترانزیت مبدا پل چوبی','RoatSafar','',1,NULL),(35,'Safar',2,'RoatSafarService::ترانزیت مبدا پل چوبی','Safar','safar',1,0),(36,'EzharnameyeGomroki',3,'RoatSafarService::ترانزیت مبدا پل چوبی','EzharnameyeGomroki','ezharnameHayeGomroki',1,NULL),(37,'RoatSafar',1,'RoatSafarService::کارنه تیر مقصد','RoatSafar','',1,NULL),(38,'Safar',2,'RoatSafarService::کارنه تیر مقصد','Safar','safar',1,0),(39,'EzharnameyeGomroki',3,'RoatSafarService::کارنه تیر مقصد','EzharnameyeGomroki','ezharnameHayeGomroki',1,NULL),(40,'RoatSafar',1,'RoatSafarService::حمل یکسره کارنه تیر','RoatSafar','',1,NULL),(41,'Safar',2,'RoatSafarService::حمل یکسره کارنه تیر','Safar','safar',1,0),(42,'EzharnameyeGomroki',3,'RoatSafarService::حمل یکسره کارنه تیر','EzharnameyeGomroki','ezharnameHayeGomroki',1,NULL),(43,'RoatSafar',1,'RoatSafarService::ترانزیت مبدا گمرک جلفا','RoatSafar','',1,NULL),(44,'Safar',2,'RoatSafarService::ترانزیت مبدا گمرک جلفا','Safar','safar',1,0),(45,'EzharnameyeGomroki',3,'RoatSafarService::ترانزیت مبدا گمرک جلفا','EzharnameyeGomroki','ezharnameHayeGomroki',1,NULL),(46,'RoatSafar',1,'RoatSafarService::صادرات مقصد پل چوبی','RoatSafar','',1,NULL),(47,'Safar',2,'RoatSafarService::صادرات مقصد پل چوبی','Safar','safar',1,0),(48,'EzharnameyeGomroki',3,'RoatSafarService::صادرات مقصد پل چوبی','EzharnameyeGomroki','ezharnameHayeGomroki',1,NULL);
/*!40000 ALTER TABLE `RuleClass` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RuleClass_FeatureData`
--

DROP TABLE IF EXISTS `RuleClass_FeatureData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RuleClass_FeatureData` (
  `RuleClass_dbId` bigint(20) NOT NULL,
  `featureDatas_dbId` bigint(20) NOT NULL,
  UNIQUE KEY `UK_iilksit2qp20uvlpnrl79clag` (`featureDatas_dbId`),
  KEY `FK_iilksit2qp20uvlpnrl79clag` (`featureDatas_dbId`),
  KEY `FK_jsthvdy3py49ep9hbhe73coj2` (`RuleClass_dbId`),
  CONSTRAINT `FK_iilksit2qp20uvlpnrl79clag` FOREIGN KEY (`featureDatas_dbId`) REFERENCES `FeatureData` (`dbId`),
  CONSTRAINT `FK_jsthvdy3py49ep9hbhe73coj2` FOREIGN KEY (`RuleClass_dbId`) REFERENCES `RuleClass` (`dbId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RuleClass_FeatureData`
--

LOCK TABLES `RuleClass_FeatureData` WRITE;
/*!40000 ALTER TABLE `RuleClass_FeatureData` DISABLE KEYS */;
INSERT INTO `RuleClass_FeatureData` VALUES (4,1),(4,2),(10,4),(10,5),(13,6),(13,7),(16,8),(24,11),(24,12),(24,13),(27,14),(27,15),(30,16),(30,17),(33,18),(33,19),(36,20),(36,21),(39,22),(39,23),(42,24),(42,25),(45,26),(45,27),(48,28),(48,29);
/*!40000 ALTER TABLE `RuleClass_FeatureData` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RuleClass_RuleClass`
--

DROP TABLE IF EXISTS `RuleClass_RuleClass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RuleClass_RuleClass` (
  `RuleClass_dbId` bigint(20) NOT NULL,
  `childs_dbId` bigint(20) NOT NULL,
  UNIQUE KEY `UK_cmlwcac0c25ldafn0g0txga9c` (`childs_dbId`),
  KEY `FK_cmlwcac0c25ldafn0g0txga9c` (`childs_dbId`),
  KEY `FK_gnjicetpjpd8sk7l0klg9miv7` (`RuleClass_dbId`),
  CONSTRAINT `FK_cmlwcac0c25ldafn0g0txga9c` FOREIGN KEY (`childs_dbId`) REFERENCES `RuleClass` (`dbId`),
  CONSTRAINT `FK_gnjicetpjpd8sk7l0klg9miv7` FOREIGN KEY (`RuleClass_dbId`) REFERENCES `RuleClass` (`dbId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RuleClass_RuleClass`
--

LOCK TABLES `RuleClass_RuleClass` WRITE;
/*!40000 ALTER TABLE `RuleClass_RuleClass` DISABLE KEYS */;
INSERT INTO `RuleClass_RuleClass` VALUES (2,3),(3,4),(8,9),(9,10),(11,12),(12,13),(14,15),(15,16),(22,23),(23,24),(25,26),(26,27),(28,29),(29,30),(31,32),(32,33),(34,35),(35,36),(37,38),(38,39),(40,41),(41,42),(43,44),(44,45),(46,47),(47,48);
/*!40000 ALTER TABLE `RuleClass_RuleClass` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Rule_ServiceResult`
--

DROP TABLE IF EXISTS `Rule_ServiceResult`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Rule_ServiceResult` (
  `Rule_dbId` bigint(20) NOT NULL,
  `results_dbId` bigint(20) NOT NULL,
  UNIQUE KEY `UK_5i88hyw81q0o7bsks5xyvcgjh` (`results_dbId`),
  KEY `FK_5i88hyw81q0o7bsks5xyvcgjh` (`results_dbId`),
  KEY `FK_dbxyabos92e7iha18o12e1s6t` (`Rule_dbId`),
  CONSTRAINT `FK_5i88hyw81q0o7bsks5xyvcgjh` FOREIGN KEY (`results_dbId`) REFERENCES `ServiceResult` (`dbId`),
  CONSTRAINT `FK_dbxyabos92e7iha18o12e1s6t` FOREIGN KEY (`Rule_dbId`) REFERENCES `Rule` (`dbId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Rule_ServiceResult`
--

LOCK TABLES `Rule_ServiceResult` WRITE;
/*!40000 ALTER TABLE `Rule_ServiceResult` DISABLE KEYS */;
INSERT INTO `Rule_ServiceResult` VALUES (1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7),(8,8),(10,10),(11,11),(12,12),(13,13),(14,14),(15,15),(16,16),(17,17),(18,18);
/*!40000 ALTER TABLE `Rule_ServiceResult` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Safar`
--

DROP TABLE IF EXISTS `Safar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Safar` (
  `dBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `cmr` varchar(255) DEFAULT NULL,
  `comment` text,
  `date` datetime DEFAULT NULL,
  `noeSafar` int(11) DEFAULT NULL,
  `parkingNo` varchar(255) DEFAULT NULL,
  `agent_dBId` bigint(20) DEFAULT NULL,
  `hamel_dBId` bigint(20) DEFAULT NULL,
  `ranandeh_dBId` bigint(20) DEFAULT NULL,
  `ravie_dBId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`dBId`),
  KEY `FK_o1b5vkku9x2nkhtm470t3b94k` (`agent_dBId`),
  KEY `FK_lof8haacw6k7aqna8pcnytmp` (`hamel_dBId`),
  KEY `FK_n68cjrf89wod3lvf7391im4gt` (`ranandeh_dBId`),
  KEY `FK_k6wp6fgeh8g4fbcqf6n87fhis` (`ravie_dBId`),
  CONSTRAINT `FK_k6wp6fgeh8g4fbcqf6n87fhis` FOREIGN KEY (`ravie_dBId`) REFERENCES `AccessPermission` (`dBId`),
  CONSTRAINT `FK_lof8haacw6k7aqna8pcnytmp` FOREIGN KEY (`hamel_dBId`) REFERENCES `Hamel` (`dBId`),
  CONSTRAINT `FK_n68cjrf89wod3lvf7391im4gt` FOREIGN KEY (`ranandeh_dBId`) REFERENCES `Ranandeh` (`dBId`),
  CONSTRAINT `FK_o1b5vkku9x2nkhtm470t3b94k` FOREIGN KEY (`agent_dBId`) REFERENCES `Agent` (`dBId`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Safar`
--

LOCK TABLES `Safar` WRITE;
/*!40000 ALTER TABLE `Safar` DISABLE KEYS */;
INSERT INTO `Safar` VALUES (1,NULL,'admin:توضيح-','2016-06-13 18:36:06',2,NULL,NULL,18,1,1),(2,NULL,'admin:توضيح-','2016-06-13 18:40:17',2,NULL,NULL,1,1,21),(3,NULL,'admin:توضيح-','2016-06-13 19:22:26',2,NULL,NULL,18,1,1),(4,NULL,'','2016-06-13 19:24:09',3,NULL,NULL,1,1,1),(5,NULL,'admin:توضيح-','2016-06-13 19:24:51',2,NULL,8,1,2,21),(6,NULL,'admin:توضيح-','2016-06-14 09:49:50',2,NULL,NULL,1,1,1),(7,NULL,'','2016-06-14 10:04:15',3,NULL,NULL,1,1,1),(8,NULL,'admin:توضيح-','2016-06-14 10:06:25',2,NULL,NULL,1,1,33),(9,NULL,'admin:توضيح-','2016-06-15 07:04:26',2,NULL,NULL,1,1,21),(10,NULL,'admin:توضيح-','2016-06-15 08:42:23',2,NULL,NULL,18,1,1),(11,NULL,'','2016-06-15 08:44:16',3,NULL,NULL,1,1,1),(12,NULL,'admin:توضيح-','2016-06-15 08:47:03',2,NULL,NULL,1,1,21),(13,NULL,'','2016-06-15 08:54:43',3,NULL,NULL,1,1,1),(14,NULL,'admin:توضيح-','2016-06-15 08:57:40',2,NULL,NULL,1,1,33),(15,NULL,'admin:توضيح-','2016-06-15 10:44:40',2,NULL,NULL,18,1,1),(16,NULL,'admin:توضيح-','2016-06-15 10:46:59',2,NULL,NULL,1,1,1),(17,NULL,'','2016-06-15 10:52:46',3,NULL,NULL,1,1,1),(18,NULL,'admin:توضيح-','2016-06-15 10:56:25',2,NULL,NULL,1,1,21),(19,NULL,'','2016-06-15 11:03:11',3,NULL,NULL,1,1,1),(20,NULL,'admin:توضيح-','2016-06-15 11:05:11',2,NULL,NULL,1,1,33),(21,NULL,'admin:توضيح-','2016-06-15 14:51:15',2,NULL,NULL,1,1,21),(22,NULL,'admin:توضيح-','2016-06-16 10:03:56',2,NULL,NULL,18,1,1),(23,NULL,'admin:توضيح-','2016-06-16 10:05:11',2,NULL,NULL,1,1,1),(24,NULL,'admin:توضيح-','2016-06-16 10:30:46',2,NULL,NULL,18,1,1),(25,NULL,'admin:توضيح-','2016-06-16 10:32:19',2,NULL,NULL,1,1,1),(26,NULL,'admin:توضيح-','2016-06-16 11:19:54',2,NULL,NULL,18,1,1),(27,NULL,'admin:توضيح-','2016-06-16 11:21:00',2,NULL,NULL,1,1,1),(28,NULL,'admin:توضيح-','2016-06-18 11:15:57',2,NULL,NULL,1,1,21),(29,NULL,'admin:توضيح-','2016-06-18 11:48:26',2,NULL,NULL,1,1,21),(30,NULL,'admin:توضيح-','2016-06-18 14:21:37',2,NULL,NULL,1,1,21),(31,NULL,'','2016-06-18 14:38:06',3,NULL,NULL,1,1,21),(32,NULL,'','2016-06-20 06:28:08',3,NULL,NULL,18,1,1);
/*!40000 ALTER TABLE `Safar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Safar_ezharnameyeGomroki`
--

DROP TABLE IF EXISTS `Safar_ezharnameyeGomroki`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Safar_ezharnameyeGomroki` (
  `Safar_dBId` bigint(20) NOT NULL,
  `ezharnameHayeGomroki_dBId` bigint(20) NOT NULL,
  KEY `FK_glamqp1k35u105w8aoglyt9ny` (`ezharnameHayeGomroki_dBId`),
  KEY `FK_pe66ltpcbpusqnoubf4octhal` (`Safar_dBId`),
  CONSTRAINT `FK_glamqp1k35u105w8aoglyt9ny` FOREIGN KEY (`ezharnameHayeGomroki_dBId`) REFERENCES `ezharnameyeGomroki` (`dBId`),
  CONSTRAINT `FK_pe66ltpcbpusqnoubf4octhal` FOREIGN KEY (`Safar_dBId`) REFERENCES `Safar` (`dBId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Safar_ezharnameyeGomroki`
--

LOCK TABLES `Safar_ezharnameyeGomroki` WRITE;
/*!40000 ALTER TABLE `Safar_ezharnameyeGomroki` DISABLE KEYS */;
INSERT INTO `Safar_ezharnameyeGomroki` VALUES (1,30),(2,33),(3,36),(4,37),(5,33),(6,39),(7,41),(8,42),(8,41),(9,43),(10,44),(11,46),(12,47),(13,50),(14,51),(14,50),(15,56),(16,58),(17,61),(18,62),(19,63),(20,64),(20,63),(21,73),(22,76),(23,79),(24,82),(25,83),(26,87),(27,88),(28,92),(29,93),(30,94),(31,95),(32,96);
/*!40000 ALTER TABLE `Safar_ezharnameyeGomroki` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Sample`
--

DROP TABLE IF EXISTS `Sample`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Sample` (
  `dbId` bigint(20) NOT NULL AUTO_INCREMENT,
  `isRegex` tinyint(1) DEFAULT NULL,
  `sample` varchar(255) DEFAULT NULL,
  `sampleSet_dbId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`dbId`),
  KEY `FK_k6iy1asngf7p9rqerrxdsn5ag` (`sampleSet_dbId`),
  CONSTRAINT `FK_k6iy1asngf7p9rqerrxdsn5ag` FOREIGN KEY (`sampleSet_dbId`) REFERENCES `SampleSet` (`dbId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Sample`
--

LOCK TABLES `Sample` WRITE;
/*!40000 ALTER TABLE `Sample` DISABLE KEYS */;
INSERT INTO `Sample` VALUES (1,0,'1',9),(2,0,'ترانزیت-حمل یکسره-مرزی',10),(3,0,'واردات-انباری-مرزی',11),(4,0,'واردات-حمل یکسره-انباری',12),(5,0,'مرزی',13),(6,0,'11ع11111',14);
/*!40000 ALTER TABLE `Sample` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SampleSet`
--

DROP TABLE IF EXISTS `SampleSet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SampleSet` (
  `dbId` bigint(20) NOT NULL AUTO_INCREMENT,
  `deleted` tinyint(1) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ruleServiceName` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `src` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `uniqueId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`dbId`),
  KEY `uniqueId` (`uniqueId`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SampleSet`
--

LOCK TABLES `SampleSet` WRITE;
/*!40000 ALTER TABLE `SampleSet` DISABLE KEYS */;
INSERT INTO `SampleSet` VALUES (1,0,'رویه های دریافتی چند گارد','ir.customs.baskool.rac.RoatSafarService',NULL,NULL),(2,0,'رویه های دریافتی صاردات','ir.customs.baskool.rac.RoatSafarService',NULL,NULL),(3,0,'واردات','ir.customs.baskool.rac.RoatSafarService',NULL,NULL),(4,0,'شماره مجوز بارگیری خاص','ir.customs.baskool.rac.RoatSafarService',NULL,NULL),(5,0,'حامل مخصوص باسکول','ir.customs.baskool.rac.RoatSafarService',NULL,NULL),(6,0,'اظهار خاص باسکول','ir.customs.baskool.rac.RoatSafarService',NULL,NULL),(7,0,'خالی','ir.customs.baskool.rac.RoatSafarService',NULL,NULL),(8,0,'پلاک حامد اینا','ir.customs.baskool.rac.RoatSafarService',NULL,NULL),(9,0,'ترانزیت','ir.customs.baskool.rac.RoatSafarService',NULL,NULL),(10,0,'مجموعه عمومی ترانزیت','ir.customs.baskool.rac.RoatSafarService',NULL,NULL),(11,0,'واردات-انباری-مرزی','ir.customs.baskool.rac.RoatSafarService',NULL,NULL),(12,0,'واردات-حمل یکسره-انباری','ir.customs.baskool.rac.RoatSafarService',NULL,NULL),(13,0,'مرزی','ir.customs.baskool.rac.RoatSafarService',NULL,NULL),(14,0,'11ع11111','ir.customs.baskool.rac.RoatSafarService',NULL,NULL);
/*!40000 ALTER TABLE `SampleSet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SampleSet_samplesSet`
--

DROP TABLE IF EXISTS `SampleSet_samplesSet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SampleSet_samplesSet` (
  `SampleSet_dbId` bigint(20) NOT NULL,
  `samplesSet` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  KEY `FK_nca4gh310ijteun34gcv4ewpr` (`SampleSet_dbId`),
  CONSTRAINT `FK_nca4gh310ijteun34gcv4ewpr` FOREIGN KEY (`SampleSet_dbId`) REFERENCES `SampleSet` (`dbId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SampleSet_samplesSet`
--

LOCK TABLES `SampleSet_samplesSet` WRITE;
/*!40000 ALTER TABLE `SampleSet_samplesSet` DISABLE KEYS */;
INSERT INTO `SampleSet_samplesSet` VALUES (1,'۲'),(2,'صادرات'),(3,'واردات'),(4,'4183186'),(5,'22ع22222'),(6,'4152340'),(6,'4183192'),(6,'50100-2817540'),(6,'91991'),(4,'91991'),(4,'4152340'),(4,'50100-2817540'),(7,'0012344321'),(8,'12ع12121'),(9,'1'),(10,'ترانزیت-حمل یکسره-مرزی'),(11,'واردات-انباری-مرزی'),(12,'واردات-حمل یکسره-انباری'),(13,'مرزی'),(14,'11ع11111');
/*!40000 ALTER TABLE `SampleSet_samplesSet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ServiceResult`
--

DROP TABLE IF EXISTS `ServiceResult`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ServiceResult` (
  `dbId` bigint(20) NOT NULL AUTO_INCREMENT,
  `percent` int(11) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `val` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`dbId`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ServiceResult`
--

LOCK TABLES `ServiceResult` WRITE;
/*!40000 ALTER TABLE `ServiceResult` DISABLE KEYS */;
INSERT INTO `ServiceResult` VALUES (1,NULL,1,'1'),(2,NULL,1,'2'),(3,NULL,1,'1'),(4,100,10,'8'),(5,100,10,'8'),(6,100,10,'8'),(7,NULL,1,'21'),(8,1,0,'26'),(10,100,10,'28'),(11,15,2,'36'),(12,75,6,'37'),(13,40,4,'32'),(14,85,8,'30'),(15,40,6,'35'),(16,100,8,'30'),(17,100,10,'33'),(18,100,10,'31');
/*!40000 ALTER TABLE `ServiceResult` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Setting`
--

DROP TABLE IF EXISTS `Setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Setting` (
  `dbid` int(11) NOT NULL AUTO_INCREMENT,
  `accessControl` tinyint(1) DEFAULT NULL,
  `guardKhoruj` tinyint(1) DEFAULT NULL,
  `guardVorud` tinyint(1) DEFAULT NULL,
  `parking` tinyint(1) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `rfid` tinyint(1) DEFAULT NULL,
  `voroudPrint` tinyint(1) DEFAULT NULL,
  `safarIdparity` tinyint(1) DEFAULT NULL,
  `customsGetExitPermit` tinyint(1) DEFAULT NULL,
  `licensePlateSize` int(11) DEFAULT NULL,
  `oneWayTransportationEnable` tinyint(1) DEFAULT NULL,
  `autoGuard` tinyint(1) DEFAULT NULL,
  `autoTozin` tinyint(1) DEFAULT NULL,
  `shenasePanelChoice` int(11) DEFAULT NULL,
  PRIMARY KEY (`dbid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Setting`
--

LOCK TABLES `Setting` WRITE;
/*!40000 ALTER TABLE `Setting` DISABLE KEYS */;
INSERT INTO `Setting` VALUES (1,1,0,1,0,10000,0,1,0,0,4,1,1,1,NULL);
/*!40000 ALTER TABLE `Setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TidManager`
--

DROP TABLE IF EXISTS `TidManager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TidManager` (
  `dbId` bigint(20) NOT NULL AUTO_INCREMENT,
  `tid` bigint(20) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  PRIMARY KEY (`dbId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TidManager`
--

LOCK TABLES `TidManager` WRITE;
/*!40000 ALTER TABLE `TidManager` DISABLE KEYS */;
/*!40000 ALTER TABLE `TidManager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TozinChandBaskoolePolicy`
--

DROP TABLE IF EXISTS `TozinChandBaskoolePolicy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TozinChandBaskoolePolicy` (
  `dBId` bigint(20) NOT NULL,
  PRIMARY KEY (`dBId`),
  KEY `FK_u785xj9dx8gdp6wpusbw1xbq` (`dBId`),
  CONSTRAINT `FK_u785xj9dx8gdp6wpusbw1xbq` FOREIGN KEY (`dBId`) REFERENCES `TozinGomrokiPolicy` (`dBId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TozinChandBaskoolePolicy`
--

LOCK TABLES `TozinChandBaskoolePolicy` WRITE;
/*!40000 ALTER TABLE `TozinChandBaskoolePolicy` DISABLE KEYS */;
/*!40000 ALTER TABLE `TozinChandBaskoolePolicy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TozinGomrokiDastiPolicy`
--

DROP TABLE IF EXISTS `TozinGomrokiDastiPolicy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TozinGomrokiDastiPolicy` (
  `dBId` bigint(20) NOT NULL,
  PRIMARY KEY (`dBId`),
  KEY `FK_w5ler06ovnxe4w17352ldj3g` (`dBId`),
  CONSTRAINT `FK_w5ler06ovnxe4w17352ldj3g` FOREIGN KEY (`dBId`) REFERENCES `TozinGomrokiPolicy` (`dBId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TozinGomrokiDastiPolicy`
--

LOCK TABLES `TozinGomrokiDastiPolicy` WRITE;
/*!40000 ALTER TABLE `TozinGomrokiDastiPolicy` DISABLE KEYS */;
/*!40000 ALTER TABLE `TozinGomrokiDastiPolicy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TozinGomrokiPolicy`
--

DROP TABLE IF EXISTS `TozinGomrokiPolicy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TozinGomrokiPolicy` (
  `dBId` bigint(20) NOT NULL,
  PRIMARY KEY (`dBId`),
  KEY `FK_ito3sgpordnvdyel37wtnhbu7` (`dBId`),
  CONSTRAINT `FK_ito3sgpordnvdyel37wtnhbu7` FOREIGN KEY (`dBId`) REFERENCES `TozinPolicy` (`dBId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TozinGomrokiPolicy`
--

LOCK TABLES `TozinGomrokiPolicy` WRITE;
/*!40000 ALTER TABLE `TozinGomrokiPolicy` DISABLE KEYS */;
/*!40000 ALTER TABLE `TozinGomrokiPolicy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TozinPolicy`
--

DROP TABLE IF EXISTS `TozinPolicy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TozinPolicy` (
  `dBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `policyDate` date DEFAULT NULL,
  PRIMARY KEY (`dBId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TozinPolicy`
--

LOCK TABLES `TozinPolicy` WRITE;
/*!40000 ALTER TABLE `TozinPolicy` DISABLE KEYS */;
/*!40000 ALTER TABLE `TozinPolicy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TozinPolicy_tozin`
--

DROP TABLE IF EXISTS `TozinPolicy_tozin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TozinPolicy_tozin` (
  `TozinPolicy_dBId` bigint(20) NOT NULL,
  `tozinha_dBId` bigint(20) NOT NULL,
  UNIQUE KEY `UK_7l9q246rqlhej6utu2pirndv2` (`tozinha_dBId`),
  KEY `FK_7l9q246rqlhej6utu2pirndv2` (`tozinha_dBId`),
  KEY `FK_na5myysfik5w8khone1ieva2o` (`TozinPolicy_dBId`),
  CONSTRAINT `FK_7l9q246rqlhej6utu2pirndv2` FOREIGN KEY (`tozinha_dBId`) REFERENCES `tozin` (`dBId`),
  CONSTRAINT `FK_na5myysfik5w8khone1ieva2o` FOREIGN KEY (`TozinPolicy_dBId`) REFERENCES `TozinPolicy` (`dBId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TozinPolicy_tozin`
--

LOCK TABLES `TozinPolicy_tozin` WRITE;
/*!40000 ALTER TABLE `TozinPolicy_tozin` DISABLE KEYS */;
/*!40000 ALTER TABLE `TozinPolicy_tozin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TozinWork`
--

DROP TABLE IF EXISTS `TozinWork`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TozinWork` (
  `noeBaskool` int(11) DEFAULT NULL,
  `tozihat` longtext,
  `vazn` double DEFAULT NULL,
  `dBId` bigint(20) NOT NULL,
  `ezharnameyeGomroki_dBId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`dBId`),
  KEY `FK_sl0ksfbdejao6dsul88tlnvu2` (`ezharnameyeGomroki_dBId`),
  KEY `FK_9n6huca3d1amwok72udgh0osk` (`dBId`),
  CONSTRAINT `FK_9n6huca3d1amwok72udgh0osk` FOREIGN KEY (`dBId`) REFERENCES `work` (`dBId`),
  CONSTRAINT `FK_sl0ksfbdejao6dsul88tlnvu2` FOREIGN KEY (`ezharnameyeGomroki_dBId`) REFERENCES `ezharnameyeGomroki` (`dBId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TozinWork`
--

LOCK TABLES `TozinWork` WRITE;
/*!40000 ALTER TABLE `TozinWork` DISABLE KEYS */;
INSERT INTO `TozinWork` VALUES (2,'',1,2,33),(2,'',1,3,36),(2,'',1,4,37),(2,'',1,5,39),(2,'',1,6,41),(2,'',1,7,43),(2,'',1,8,44),(2,'',1,9,46),(2,'',1,10,50),(2,'',1,11,56),(2,'',1,12,58),(2,'',1,13,61),(2,'',1,14,63),(2,'',1,15,73),(2,'',1,16,76),(2,'',1,17,79),(2,'',1,18,82),(2,'',1,19,83),(2,'',1,20,87),(2,'',1,21,88),(2,'',1,22,92),(2,'',1,23,93),(2,'',1,24,94),(2,'',1,25,95),(2,'',1,26,96);
/*!40000 ALTER TABLE `TozinWork` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TozinYekBaskoolePolicy`
--

DROP TABLE IF EXISTS `TozinYekBaskoolePolicy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TozinYekBaskoolePolicy` (
  `emptyWeight` double DEFAULT NULL,
  `hamelShenase` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `dBId` bigint(20) NOT NULL,
  PRIMARY KEY (`dBId`),
  KEY `FK_rhjjw3p8w7kvufs9ahx9cqwra` (`dBId`),
  CONSTRAINT `FK_rhjjw3p8w7kvufs9ahx9cqwra` FOREIGN KEY (`dBId`) REFERENCES `TozinGomrokiPolicy` (`dBId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TozinYekBaskoolePolicy`
--

LOCK TABLES `TozinYekBaskoolePolicy` WRITE;
/*!40000 ALTER TABLE `TozinYekBaskoolePolicy` DISABLE KEYS */;
/*!40000 ALTER TABLE `TozinYekBaskoolePolicy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `baskool`
--

DROP TABLE IF EXISTS `baskool`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `baskool` (
  `dBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `noeGate` int(11) DEFAULT NULL,
  `valid` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`dBId`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `baskool`
--

LOCK TABLES `baskool` WRITE;
/*!40000 ALTER TABLE `baskool` DISABLE KEYS */;
INSERT INTO `baskool` VALUES (1,'20.1.0.6','باسکول تست پژوهشکده',0,1),(2,'172.16.111.85','گارد لوکال',1,1),(3,'172.16.111.85','انبار لوکال',2,0),(4,'172.16.111.85','باسکول لوکال',0,1),(5,'194.225.0.50','تست',1,0),(6,'172.16.111.150','حامد',0,1),(7,'172.17.100.42','شايان',0,1),(8,'epl','epl',0,1),(10,'172.16.111.51','انبار سرور تست',2,1),(11,'192.168.0.21','انبار',2,1),(12,'system','system',0,1),(13,'172.16.111.170','حامد اينا',1,0),(14,'172.16.111.170','حامد اينا',0,1),(15,'192.168.0.21','حامد اينا',2,0),(16,'188.122.127.162','تست خانگي',0,1),(17,'172.17.193.210','test',0,1),(18,'127.0.0.1','گارد پويان',1,0),(19,'172.17.193.132','تست محمدزاده',0,1),(20,'172.1.1.5','انبار جلفا',2,1),(21,'172.1.1.54','گارد جلفا',1,1),(22,'172.1.1.55','باسکول جلفا',0,1),(23,'172.17.97.69','گارد آرين',1,1),(24,'172.17.193.223','ميلاد باسکول',0,1),(25,'172.17.193.223','ميلاد گارد',1,0),(26,'172.17.193.212','باسکول آرين',0,1),(27,'172.16.111.85','هلپ',0,1),(28,'194.225.0.50','هلپ',0,1),(29,'194.225.0.50','هلپ2',1,1),(30,'20.1.0.6','اپاچي',0,1);
/*!40000 ALTER TABLE `baskool` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezharnameyeGomroki`
--

DROP TABLE IF EXISTS `ezharnameyeGomroki`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezharnameyeGomroki` (
  `dBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `accessControlRavie` varchar(255) DEFAULT NULL,
  `codingEzharkonandeh` varchar(255) DEFAULT NULL,
  `codingSahebKala` varchar(255) DEFAULT NULL,
  `hamlKind` int(11) DEFAULT NULL,
  `issuance` datetime DEFAULT NULL,
  `kalahaStr` longtext,
  `mojavezBargiri` varchar(255) DEFAULT NULL,
  `nameEzharkonandeh` varchar(255) DEFAULT NULL,
  `nameSahebEzharname` varchar(255) DEFAULT NULL,
  `ravie` int(11) DEFAULT NULL,
  `shomareParvaneh` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`dBId`),
  UNIQUE KEY `UK_ey8d8bcdo35bmv49b3tb19o7e` (`mojavezBargiri`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezharnameyeGomroki`
--

LOCK TABLES `ezharnameyeGomroki` WRITE;
/*!40000 ALTER TABLE `ezharnameyeGomroki` DISABLE KEYS */;
INSERT INTO `ezharnameyeGomroki` VALUES (1,NULL,'2980360678','2980360678',0,'2016-06-11 11:00:39','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','6','وحید\\کامرانی ','وحید\\کامرانی',2,'14656383188560.978771684439906'),(2,NULL,'2980360678','2980360678',0,'2016-06-11 13:11:33','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','18','وحید\\کامرانی ','وحید\\کامرانی',2,'14656506649440.09196372074964676'),(3,NULL,'2980360678','2980360678',0,'2016-06-11 13:13:19','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','21','وحید\\کامرانی ','وحید\\کامرانی',2,'14656493719880.9557971795029687'),(4,NULL,'2980360678','2980360678',0,'2016-06-11 13:13:26','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','24','وحید\\کامرانی ','وحید\\کامرانی',2,'14656507665360.4938184069623134'),(5,NULL,'2980360678','2980360678',0,'2016-06-11 13:20:36','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','27','وحید\\کامرانی ','وحید\\کامرانی',2,'14656508409030.783104238573134'),(6,NULL,'2980360678','2980360678',0,'2016-06-13 18:34:06','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','30','وحید\\کامرانی ','وحید\\کامرانی',2,'14656527224820.9157560940345406'),(7,NULL,'0081674902','0081674902',1,'2016-06-11 13:56:24','mnfdj ـ ـ ـ ساير,','33','بروس\\وین ','بروس\\وین',2,'14656533429020.7070384766859487'),(8,NULL,'2980360678','2980360678',0,'2016-06-11 14:20:26','111 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",شرح تجاری کالا ندارد ـ ـ ساير,','42','وحید\\کامرانی ','وحید\\کامرانی',0,'a14656437692890.05557794761220969'),(9,NULL,'2980360678','2980360678',0,'2016-06-13 18:34:18','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','45','وحید\\کامرانی ','وحید\\کامرانی',2,'14656739070140.8031499014537332'),(10,NULL,'2980360678','2980360678',0,'2016-06-11 19:46:41','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','48','وحید\\کامرانی ','وحید\\کامرانی',2,'14656742653860.5343733884411338'),(11,NULL,'2980360678','2980360678',0,'2016-06-11 20:13:09','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','51','وحید\\کامرانی ','وحید\\کامرانی',2,'14656759072220.6824126171050048'),(12,NULL,'2980360678','2980360678',0,'2016-06-12 14:16:30','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','75','وحید\\کامرانی ','وحید\\کامرانی',2,'14657408533460.4761156495500971'),(13,NULL,'2980360678','2980360678',0,'2016-06-12 14:37:45','111 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",شرح تجاری کالا ندارد ـ ـ ساير,','78','وحید\\کامرانی ','وحید\\کامرانی',0,'a14657421952820.8873200871048408'),(14,NULL,'2980360678','2980360678',0,'2016-06-12 14:40:04','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','81','وحید\\کامرانی ','وحید\\کامرانی',2,'14657423314360.30216305596585413'),(15,NULL,'2980360678','2980360678',0,'2016-06-12 14:42:25','111 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",شرح تجاری کالا ندارد ـ ـ ساير,','84','وحید\\کامرانی ','وحید\\کامرانی',0,'a14657424685190.9675011721259766'),(16,NULL,'0081674902','0081674902',0,'2016-06-13 05:23:34','Fake Cargo Description 14 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 87 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 2 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','87','بروس\\وین ','بروس\\وین',0,'14657952478260.739494648004078'),(17,NULL,'2980360678','2980360678',0,'2016-06-13 06:12:37','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','93','وحید\\کامرانی ','وحید\\کامرانی',2,'14657982795220.719089018215008'),(18,NULL,'2980360678','2980360678',0,'2016-06-13 07:31:48','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','99','وحید\\کامرانی ','وحید\\کامرانی',2,'14658030485400.5929613666471032'),(19,NULL,'2980360678','2980360678',0,'2016-06-13 07:34:51','111 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','102','وحید\\کامرانی ','وحید\\کامرانی',0,'a14658032316750.6458711850765495'),(20,NULL,'2980360678','2980360678',0,'2016-06-13 08:49:34','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','105','وحید\\کامرانی ','وحید\\کامرانی',2,'14658077121120.9627764648286811'),(21,NULL,'2980360678','2980360678',0,'2016-06-13 08:52:22','111 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','108','وحید\\کامرانی ','وحید\\کامرانی',0,'a14658078856000.3461020185080732'),(22,NULL,'2980360678','2980360678',0,'2016-06-13 08:56:23','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','111','وحید\\کامرانی ','وحید\\کامرانی',2,'14658081239050.21147286944708032'),(23,NULL,'2980360678','2980360678',0,'2016-06-13 08:58:22','111 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','114','وحید\\کامرانی ','وحید\\کامرانی',0,'a14658082461610.9130525189829328'),(24,NULL,'2980360678','2980360678',0,'2016-06-13 12:10:03','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','117','وحید\\کامرانی ','وحید\\کامرانی',2,'14658196945070.4353160614523517'),(25,NULL,'2980360678','2980360678',0,'2016-06-13 13:42:00','111 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','120','وحید\\کامرانی ','وحید\\کامرانی',0,'a14658198734040.7383584603106088'),(26,NULL,'2980360678','2980360678',0,'2016-06-13 13:46:16','111 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','123','وحید\\کامرانی ','وحید\\کامرانی',0,'a14656273717080.09460585494072082'),(27,NULL,'2980360678','2980360678',0,'2016-06-13 16:18:38','111 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','126','وحید\\کامرانی ','وحید\\کامرانی',0,'a14654814956850.7432655474054748'),(28,NULL,'2980360678','2980360678',0,'2016-06-13 16:22:15','111 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','129','وحید\\کامرانی ','وحید\\کامرانی',0,'a14654541041630.660477083152449'),(29,NULL,'0081674902','0081674902',0,'2016-06-13 18:31:05','Fake Cargo Description 57 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 12 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 57 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','132','بروس\\وین ','بروس\\وین',0,'14658425593090.7673710330910967'),(30,NULL,'0014472619','0014472619',0,'2016-06-13 18:34:29','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','135','تست\\تست ','تست\\تست',2,'14658426891380.05713532921098208'),(31,NULL,'2980360678','2980360678',0,'2016-06-13 18:34:20','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','72','وحید\\کامرانی ','وحید\\کامرانی',2,'14657382285890.46499158406097163'),(32,NULL,'2980360678','2980360678',0,'2016-06-13 18:34:26','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','90','وحید\\کامرانی ','وحید\\کامرانی',2,'14657979546710.0024415150891082815'),(33,NULL,'0081674902','0081674902',0,'2016-06-13 18:39:26','Fake Cargo Description 88 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 47 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 20 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','150','بروس\\وین ','بروس\\وین',0,'14658428645310.9213547366924749'),(34,NULL,'--','12121212121',0,'2016-06-13 18:33:48','','141','-- --','شرکت حمل تست',1,''),(35,NULL,'--','12121212121',0,'2016-06-13 00:00:00','','50100-141','-- --','شرکت حمل تست',5,''),(36,NULL,'0014472619','0014472619',0,'2016-06-13 19:21:58','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','156','تست\\تست ','تست\\تست',2,'14658455605060.8799744236306567'),(37,NULL,'--','12121212121',0,'2016-06-13 19:22:13','','168','-- --','شرکت حمل تست',1,''),(38,NULL,'--','12121212121',0,'2016-06-13 00:00:00','','50100-168','-- --','شرکت حمل تست',5,''),(39,NULL,'0081674902','12121212121',0,'2016-06-14 09:49:07','Fake Cargo Description 44,Fake Cargo Description 98,Fake Cargo Description 97,','189','-- بروس\\وین','شرکت حمل تست',1,''),(40,NULL,'0081674902','12121212121',0,'2016-06-14 00:00:00','Fake Cargo Description 44,Fake Cargo Description 98,Fake Cargo Description 97,','50100-189','-- بروس\\وین','شرکت حمل تست',5,''),(41,NULL,'0081674902','12121212121',0,'2016-06-14 10:03:45','Fake Cargo Description 20,Fake Cargo Description 93,Fake Cargo Description 1,','200','-- بروس\\وین','شرکت حمل تست',1,''),(42,NULL,'0081674902','12121212121',0,'2016-06-14 00:00:00','Fake Cargo Description 20,Fake Cargo Description 93,Fake Cargo Description 1,','50100-200','-- بروس\\وین','شرکت حمل تست',5,''),(43,NULL,'0081674902','0081674902',0,'2016-06-15 07:03:20','Fake Cargo Description 82 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 13 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 16 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','226','بروس\\وین ','بروس\\وین',0,'14659734110910.6843865182043983'),(44,NULL,'0014472619','0014472619',0,'2016-06-15 08:41:55','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','232','تست\\تست ','تست\\تست',2,'14659799678830.8631355019437145'),(45,NULL,'0081674902','0081674902',0,'2016-06-15 08:42:54','Fake Cargo Description 91 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 93 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 9 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','247','بروس\\وین ','بروس\\وین',0,'14659800837880.5214086989373374'),(46,NULL,'0081674902','12121212121',0,'2016-06-15 08:43:42','Fake Cargo Description 36,Fake Cargo Description 16,Fake Cargo Description 80,','244','-- بروس\\وین','شرکت حمل تست',1,''),(47,NULL,'0081674902','12121212121',0,'2016-06-15 00:00:00','Fake Cargo Description 36,Fake Cargo Description 16,Fake Cargo Description 80,','50100-244','-- بروس\\وین','شرکت حمل تست',5,''),(48,NULL,'0081674902','0081674902',0,'2016-06-15 08:46:46','Fake Cargo Description 47 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 66 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 1 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','255','بروس\\وین ','بروس\\وین',0,'14659803148240.6911613743512652'),(49,NULL,'0081674902','0081674902',0,'2016-06-15 08:52:13','Fake Cargo Description 20 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 18 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 42 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','258','بروس\\وین ','بروس\\وین',0,'14659806394460.18419285963380738'),(50,NULL,'0081674902','12121212121',0,'2016-06-15 08:54:09','Fake Cargo Description 55,Fake Cargo Description 35,Fake Cargo Description 60,','264','-- بروس\\وین','شرکت حمل تست',1,''),(51,NULL,'0081674902','12121212121',0,'2016-06-15 00:00:00','Fake Cargo Description 55,Fake Cargo Description 35,Fake Cargo Description 60,','50100-264','-- بروس\\وین','شرکت حمل تست',5,''),(52,NULL,'0081674902','0081674902',0,'2016-06-15 09:09:18','Fake Cargo Description 8 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 8 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 34 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','272','بروس\\وین ','بروس\\وین',0,'14659816671600.8516677933468787'),(53,NULL,'0014472619','0014472619',0,'2016-06-15 09:33:41','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','275','تست\\تست ','تست\\تست',2,'14659832012780.757867420456686'),(54,NULL,'0081674902','0081674902',0,'2016-06-15 09:37:02','Fake Cargo Description 32 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 48 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 26 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','278','بروس\\وین ','بروس\\وین',0,'14659833427310.2715947950626997'),(55,NULL,'0081674902','0081674902',0,'2016-06-15 10:27:18','Fake Cargo Description 24 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 59 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 52 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','287','بروس\\وین ','بروس\\وین',0,'14659863488650.12277619566419662'),(56,NULL,'0014472619','0014472619',0,'2016-06-15 10:44:13','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','290','تست\\تست ','تست\\تست',2,'14659873392720.7293147529867212'),(57,NULL,'0081674902','0081674902',0,'2016-06-15 10:45:40','Fake Cargo Description 28 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 45 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 93 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','299','بروس\\وین ','بروس\\وین',0,'14659874552210.3363907895596412'),(58,NULL,'0081674902','12121212121',0,'2016-06-15 10:46:27','Fake Cargo Description 98,Fake Cargo Description 32,Fake Cargo Description 24,','305','-- بروس\\وین','شرکت حمل تست',1,''),(59,NULL,'0081674902','12121212121',0,'2016-06-15 00:00:00','Fake Cargo Description 98,Fake Cargo Description 32,Fake Cargo Description 24,','50100-305','-- بروس\\وین','شرکت حمل تست',5,''),(60,NULL,'0081674902','0081674902',0,'2016-06-15 10:51:56','Fake Cargo Description 75 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 58 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 26 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','319','بروس\\وین ','بروس\\وین',0,'14659878286690.4972994929233323'),(61,NULL,'0081674902','12121212121',0,'2016-06-15 10:52:12','Fake Cargo Description 77,Fake Cargo Description 70,Fake Cargo Description 74,','316','-- بروس\\وین','شرکت حمل تست',1,''),(62,NULL,'0081674902','12121212121',0,'2016-06-15 00:00:00','Fake Cargo Description 77,Fake Cargo Description 70,Fake Cargo Description 74,','50100-316','-- بروس\\وین','شرکت حمل تست',5,''),(63,NULL,'0081674902','12121212121',0,'2016-06-15 11:02:35','Fake Cargo Description 44,Fake Cargo Description 34,Fake Cargo Description 50,','330','-- بروس\\وین','شرکت حمل تست',1,''),(64,NULL,'0081674902','12121212121',0,'2016-06-15 00:00:00','Fake Cargo Description 44,Fake Cargo Description 34,Fake Cargo Description 50,','50100-330','-- بروس\\وین','شرکت حمل تست',5,''),(65,NULL,'0081674902','0081674902',0,'2016-06-15 11:04:55','Fake Cargo Description 70 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 30 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 56 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','338','بروس\\وین ','بروس\\وین',0,'14659886102610.04933772513018131'),(66,NULL,'0081674902','0081674902',0,'2016-06-15 11:12:28','Fake Cargo Description 67 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 23 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 84 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','341','بروس\\وین ','بروس\\وین',0,'14659890710440.3737193132216551'),(67,NULL,'0014472619','0014472619',0,'2016-06-15 11:44:01','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','344','تست\\تست ','تست\\تست',2,'14659910005040.6510327432963436'),(68,NULL,'0081674902','0081674902',0,'2016-06-15 11:46:43','Fake Cargo Description 15 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 75 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 85 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','347','بروس\\وین ','بروس\\وین',0,'14659911150170.059580085045841336'),(69,NULL,'0014472619','0014472619',0,'2016-06-15 11:47:33','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','356','تست\\تست ','تست\\تست',2,'14659912283450.9814761904835375'),(70,NULL,'0014472619','0014472619',0,'2016-06-15 11:49:07','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','359','تست\\تست ','تست\\تست',2,'14659913268230.7138146169923412'),(71,NULL,'0081674902','0081674902',0,'2016-06-15 11:51:31','Fake Cargo Description 8 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 57 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 40 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','362','بروس\\وین ','بروس\\وین',0,'14659914075680.35896741946365596'),(72,NULL,'0081674902','0081674902',0,'2016-06-15 11:58:45','Fake Cargo Description 88 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 69 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 37 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','371','بروس\\وین ','بروس\\وین',0,'14659918316220.695641355904774'),(73,NULL,'0081674902','0081674902',0,'2016-06-15 14:50:02','Fake Cargo Description 16 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 17 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 7 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','383','بروس\\وین ','بروس\\وین',0,'14660015125330.8969152588368119'),(74,NULL,'9876543210','9876543210',0,'2016-06-16 09:45:36','Fake Cargo Description 96 ـ ـ ـ اسب براي مسابقه,Fake Cargo Description 5 ـ سوزن‌هاي دوخت عرضه شده به صورت نوار,Fake Cargo Description 72 ـ ـ حيوانات مولد نژاد خالص,','427','test\\test ','test\\test',0,'14660702509790.2862928389386181'),(75,NULL,'9876543210','9876543210',0,'2016-06-16 09:52:23','Fake Cargo Description 27 ـ ـ ـ اسب براي مسابقه,Fake Cargo Description 48 ـ سوزن‌هاي دوخت عرضه شده به صورت نوار,Fake Cargo Description 16 ـ ـ حيوانات مولد نژاد خالص,','430','test\\test ','test\\test',0,'14660706674750.637469390114699'),(76,NULL,'0014472619','0014472619',0,'2016-06-16 10:02:52','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','433','تست\\تست ','تست\\تست',2,'14660712173390.23411739266450549'),(77,NULL,'9876543210','9876543210',0,'2016-06-16 10:02:02','Fake Cargo Description 92 ـ ـ ـ اسب براي مسابقه,Fake Cargo Description 82 ـ سوزن‌هاي دوخت عرضه شده به صورت نوار,Fake Cargo Description 78 ـ ـ حيوانات مولد نژاد خالص,','436','test\\test ','test\\test',0,'14660712341090.15069448533756225'),(78,NULL,'0081674902','0081674902',0,'2016-06-16 10:02:23','Fake Cargo Description 24 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 1 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 66 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','439','بروس\\وین ','بروس\\وین',0,'14660712629500.9932237969964843'),(79,NULL,'0081674902','12121212121',0,'2016-06-16 10:04:32','Fake Cargo Description 47,Fake Cargo Description 20,Fake Cargo Description 13,','451','-- بروس\\وین','شرکت حمل تست',1,''),(80,NULL,'0081674902','12121212121',0,'2016-06-16 00:00:00','Fake Cargo Description 47,Fake Cargo Description 20,Fake Cargo Description 13,','50100-451','-- بروس\\وین','شرکت حمل تست',5,''),(81,NULL,'9876543210','9876543210',0,'2016-06-16 10:16:42','Fake Cargo Description 39 ـ ـ ـ اسب براي مسابقه,Fake Cargo Description 53 ـ سوزن‌هاي دوخت عرضه شده به صورت نوار,Fake Cargo Description 93 ـ ـ حيوانات مولد نژاد خالص,','459','test\\test ','test\\test',0,'14660720998060.7233214455796184'),(82,NULL,'0014472619','0014472619',0,'2016-06-16 10:29:43','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','462','تست\\تست ','تست\\تست',2,'14660727340760.1443614220554329'),(83,NULL,'0081674902','12121212121',0,'2016-06-16 10:30:52','Fake Cargo Description 70,Fake Cargo Description 46,Fake Cargo Description 56,','474','-- بروس\\وین','شرکت حمل تست',1,''),(84,NULL,'0081674902','12121212121',0,'2016-06-16 00:00:00','Fake Cargo Description 70,Fake Cargo Description 46,Fake Cargo Description 56,','50100-474','-- بروس\\وین','شرکت حمل تست',5,''),(85,NULL,'0081674902','0081674902',0,'2016-06-16 10:36:52','Fake Cargo Description 6 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 91 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 64 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','482','بروس\\وین ','بروس\\وین',0,'14660733106520.8035577997973347'),(86,NULL,'0014472619','0014472619',0,'2016-06-16 10:37:12','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','485','تست\\تست ','تست\\تست',2,'14660733860550.4830992802689542'),(87,NULL,'0014472619','0014472619',0,'2016-06-16 11:18:09','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','494','تست\\تست ','تست\\تست',2,'14660756425390.38080567846621427'),(88,NULL,'0081674902','12121212121',0,'2016-06-16 11:18:54','Fake Cargo Description 11,Fake Cargo Description 30,Fake Cargo Description 18,','500','-- بروس\\وین','شرکت حمل تست',1,''),(89,NULL,'0081674902','12121212121',0,'2016-06-16 00:00:00','Fake Cargo Description 11,Fake Cargo Description 30,Fake Cargo Description 18,','50100-500','-- بروس\\وین','شرکت حمل تست',5,''),(90,NULL,'9876543210','9876543210',0,'2016-06-16 11:33:18','Fake Cargo Description 9 ـ ـ ـ اسب براي مسابقه,Fake Cargo Description 89 ـ سوزن‌هاي دوخت عرضه شده به صورت نوار,Fake Cargo Description 40 ـ ـ حيوانات مولد نژاد خالص,','517','test\\test ','test\\test',0,'14660766995530.006505746179961913'),(91,NULL,'0081674902','0081674902',0,'2016-06-18 10:46:06','Fake Cargo Description 36 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 10 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 96 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','535','بروس\\وین ','بروس\\وین',0,'14662466659330.8338769276864235'),(92,NULL,'0081674902','0081674902',0,'2016-06-18 11:14:33','Fake Cargo Description 78 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 58 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 97 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','538','بروس\\وین ','بروس\\وین',0,'14662481113240.4538925367061083'),(93,NULL,'0081674902','0081674902',0,'2016-06-18 11:48:26','Fake Cargo Description 29 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 62 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 77 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','541','بروس\\وین ','بروس\\وین',0,'14662500058320.6252627425752314'),(94,NULL,'0081674902','0081674902',0,'2016-06-18 14:20:32','Fake Cargo Description 72 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 31 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 45 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','562','بروس\\وین ','بروس\\وین',0,'14662592256280.8558750585023827'),(95,NULL,'0081674902','0081674902',0,'2016-06-18 14:37:42','Fake Cargo Description 28 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 82 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",Fake Cargo Description 90 \"بستني و ساير شربت‌هاي يخ‌زده (Edible ice)، حتي داراي كاكائو.\",','565','بروس\\وین ','بروس\\وین',0,'14662603563660.514373409970526'),(96,NULL,'0014472619','0014472619',0,'2016-06-20 06:27:35','ندادر ـ ـ ـ در بسته‌بندي آماده براي خرده فروشي به وزن پانصد گرم و کمتر,','571','تست\\تست ','تست\\تست',2,'14664039011830.3958425451547437');
/*!40000 ALTER TABLE `ezharnameyeGomroki` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `park`
--

DROP TABLE IF EXISTS `park`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `park` (
  `noeVorudKhoruj` int(11) DEFAULT NULL,
  `dBId` bigint(20) NOT NULL,
  PRIMARY KEY (`dBId`),
  KEY `FK_ahgdcv5bm9lqe4icakgrks62p` (`dBId`),
  CONSTRAINT `FK_ahgdcv5bm9lqe4icakgrks62p` FOREIGN KEY (`dBId`) REFERENCES `work` (`dBId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `park`
--

LOCK TABLES `park` WRITE;
/*!40000 ALTER TABLE `park` DISABLE KEYS */;
/*!40000 ALTER TABLE `park` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tozin`
--

DROP TABLE IF EXISTS `tozin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tozin` (
  `dBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `noe` int(11) DEFAULT NULL,
  `tozihat` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `tozinTime` datetime DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `baskool_dBId` bigint(20) DEFAULT NULL,
  `employee_EmployeeDBId` bigint(20) DEFAULT NULL,
  `hamel_dBId` bigint(20) DEFAULT NULL,
  `ranandeh_dBId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`dBId`),
  KEY `FK_n6uh5oq1d9w19qu7vbi3c22ay` (`baskool_dBId`),
  KEY `FK_pl2m0phkud2euckrl9frpq70k` (`employee_EmployeeDBId`),
  KEY `FK_rq5vv99olp6vis8e19ukp2bqt` (`hamel_dBId`),
  KEY `FK_sabnc4dkgw0mb7wphusncftyd` (`ranandeh_dBId`),
  CONSTRAINT `FK_n6uh5oq1d9w19qu7vbi3c22ay` FOREIGN KEY (`baskool_dBId`) REFERENCES `baskool` (`dBId`),
  CONSTRAINT `FK_pl2m0phkud2euckrl9frpq70k` FOREIGN KEY (`employee_EmployeeDBId`) REFERENCES `Employee` (`EmployeeDBId`),
  CONSTRAINT `FK_rq5vv99olp6vis8e19ukp2bqt` FOREIGN KEY (`hamel_dBId`) REFERENCES `Hamel` (`dBId`),
  CONSTRAINT `FK_sabnc4dkgw0mb7wphusncftyd` FOREIGN KEY (`ranandeh_dBId`) REFERENCES `Ranandeh` (`dBId`)
) ENGINE=InnoDB AUTO_INCREMENT=229 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tozin`
--

LOCK TABLES `tozin` WRITE;
/*!40000 ALTER TABLE `tozin` DISABLE KEYS */;
INSERT INTO `tozin` VALUES (1,4,'','2015-03-22 03:01:50',35560,1,1,1,1),(2,4,'','2015-03-22 16:19:33',35560,1,1,1,1),(3,4,'','2015-03-22 18:49:28',35560,1,1,1,1),(4,4,'','2015-03-29 18:14:15',35560,1,1,2,1),(5,4,'','2015-03-29 20:39:46',35560,1,1,2,1),(6,4,'','2015-03-29 21:36:48',35560,1,1,2,1),(7,4,'','2015-03-30 11:36:22',35560,1,1,2,1),(8,4,'','2015-03-30 12:32:26',35560,1,1,2,1),(9,4,'','2015-03-30 13:07:42',35560,1,1,2,1),(10,4,'','2015-03-30 13:40:48',35560,1,1,2,1),(11,4,'','2015-03-30 14:11:10',35560,1,1,2,1),(12,4,'','2015-03-30 14:56:54',35560,1,1,2,1),(13,4,'','2015-03-30 15:05:53',35560,1,1,2,1),(14,4,'','2015-03-30 15:51:24',35560,1,1,2,1),(15,4,'','2015-04-04 17:24:03',35560,1,1,2,1),(16,4,'','2015-04-05 16:23:56',35560,1,1,2,1),(17,4,'','2015-04-05 16:53:53',35560,1,1,2,1),(18,4,'','2015-04-05 17:07:11',35560,1,1,2,1),(19,4,'','2015-04-05 17:13:44',35560,1,1,2,1),(20,4,'','2015-04-05 17:53:41',35560,1,1,2,1),(21,4,'','2015-04-07 16:20:58',35560,1,1,2,1),(22,4,'','2015-04-07 16:27:43',35560,1,1,2,1),(23,4,'','2015-04-07 16:57:44',35560,1,1,2,1),(24,4,'','2015-04-07 17:28:59',35560,1,1,2,1),(25,4,'','2015-04-08 14:56:00',35560,1,1,2,1),(26,4,'','2015-04-08 15:00:17',35560,1,1,2,1),(27,4,'','2015-04-18 15:55:31',35560,1,1,2,1),(28,4,'','2015-04-18 16:08:02',35560,1,1,2,1),(29,4,'','2015-04-21 12:36:18',35560,1,1,2,1),(30,4,'','2015-04-21 12:41:00',35560,1,1,2,1),(31,4,'','2015-04-21 12:50:33',35560,1,1,2,1),(32,4,'','2015-04-21 12:58:55',35560,1,1,2,1),(33,4,'','2015-04-21 13:03:09',35560,1,1,2,1),(34,4,'','2015-04-21 13:48:52',35560,1,1,2,1),(35,4,'','2015-04-21 14:29:09',35560,1,1,2,1),(36,4,'','2015-04-21 14:43:24',35560,1,1,2,1),(37,4,'','2015-04-21 14:53:41',35560,1,1,2,1),(38,4,'','2015-08-30 15:16:03',35560,1,1,2,1),(39,4,'','2015-09-05 18:09:19',35560,1,1,2,1),(40,4,'','2015-09-05 18:26:04',35560,1,1,2,1),(41,4,'','2015-09-05 19:14:16',35560,1,1,2,1),(42,4,'','2015-09-05 20:01:11',35560,1,1,2,1),(43,4,'','2015-09-06 12:24:48',35560,1,1,2,1),(44,4,'','2015-09-06 17:32:36',35560,1,1,2,1),(45,4,'','2015-09-06 17:42:28',35560,1,1,2,1),(46,4,'','2015-09-06 17:50:16',35560,1,1,2,1),(47,4,'','2015-09-06 18:03:33',35560,1,1,2,1),(48,4,'','2015-09-06 18:07:24',35560,1,1,2,1),(49,4,'','2015-09-06 18:11:44',35560,1,1,2,1),(50,4,'','2015-09-06 18:21:49',35560,1,1,2,1),(51,4,'','2015-09-06 18:26:11',35560,1,1,2,1),(52,4,'','2015-09-07 11:31:32',35560,1,1,2,1),(53,4,'lln','2015-09-07 17:56:39',35560,1,1,2,1),(54,4,'','2015-09-07 18:04:24',35560,1,1,2,1),(55,4,'','2015-09-07 19:55:47',35560,1,1,2,1),(56,4,'','2015-09-07 20:03:19',35560,1,1,2,1),(57,4,'','2015-09-08 09:10:52',35560,1,1,2,1),(58,4,'','2015-09-08 09:16:37',35560,1,1,2,1),(59,4,'','2015-09-08 09:20:26',35560,1,1,2,1),(60,4,'','2015-09-08 09:27:08',35560,1,1,2,1),(61,4,'','2015-09-08 09:33:00',35560,1,1,2,1),(62,4,'','2015-09-08 09:40:48',35560,1,1,2,1),(63,4,'','2015-09-08 09:45:51',35560,1,1,2,1),(64,4,'','2015-09-08 11:23:14',35560,1,1,2,1),(65,4,'','2015-09-08 12:29:10',35560,1,1,2,1),(66,4,'','2015-09-08 14:47:54',35560,1,1,2,1),(67,4,'','2015-09-08 14:51:35',35560,1,1,2,1),(68,4,'','2015-09-08 16:11:16',35560,1,1,2,1),(69,4,'','2015-09-08 16:30:07',35560,1,1,2,1),(70,4,'','2015-09-08 16:36:53',35560,1,1,2,1),(71,4,'','2015-09-09 15:18:18',35560,1,1,2,1),(72,4,'','2015-09-09 15:24:15',35560,1,1,2,1),(73,4,'','2015-09-09 17:46:22',35560,1,1,2,1),(74,4,'','2015-09-09 18:45:58',35560,1,1,2,1),(75,4,'','2015-09-09 20:01:00',35560,1,1,2,1),(76,4,'','2015-09-10 11:50:28',35560,1,1,2,1),(77,4,'','2015-09-10 14:29:27',35560,1,1,2,1),(78,4,'','2015-09-10 16:21:46',35560,1,1,2,1),(79,4,'','2015-09-10 18:00:09',35560,1,1,2,1),(80,4,'','2015-09-10 19:51:11',35560,1,1,2,1),(81,4,'','2015-09-10 20:49:53',35560,1,1,2,1),(82,4,'','2015-09-10 21:10:55',35560,1,1,2,1),(83,4,'','2015-09-12 17:00:46',35560,1,1,2,1),(84,4,'','2015-09-12 17:52:51',35560,1,1,2,1),(85,4,'','2015-09-13 11:43:40',35560,1,1,2,1),(86,4,'','2015-09-13 16:45:58',35560,1,1,2,1),(87,4,'','2015-09-13 16:50:35',35560,1,1,2,1),(88,4,'','2015-09-14 13:35:12',35560,1,1,2,1),(89,4,'','2015-09-14 13:40:57',35560,1,1,2,1),(90,4,'','2015-09-15 16:44:28',35560,1,1,2,1),(91,4,'','2015-09-15 17:11:41',35560,1,1,2,1),(92,4,'','2015-09-19 11:34:19',35560,1,1,2,1),(93,4,'','2015-09-20 20:46:34',35560,1,1,2,1),(94,4,'','2015-09-20 20:52:06',35560,1,1,2,1),(95,4,'','2015-09-20 21:30:29',35560,1,1,2,1),(96,4,'','2015-09-20 21:45:53',35560,1,1,2,1),(97,4,'','2015-09-20 21:54:59',35560,1,1,2,1),(98,4,'','2015-09-20 23:02:33',35560,1,1,2,1),(99,4,'','2015-09-21 11:00:15',35560,1,1,2,1),(100,4,'','2015-09-21 13:01:42',35560,1,1,2,1),(101,4,'','2015-09-21 14:57:42',35560,1,1,2,1),(102,4,'','2015-09-22 11:19:53',35560,1,1,2,1),(103,4,'','2015-09-22 15:48:59',35560,1,1,2,1),(104,4,'','2015-09-22 17:30:41',35560,1,1,2,1),(105,4,'','2015-09-23 10:11:46',35560,1,1,2,1),(106,4,'','2015-09-23 11:16:52',35560,1,1,2,1),(107,4,'','2015-09-23 14:20:53',35560,1,1,2,1),(108,4,'','2015-09-24 18:15:10',35560,1,1,2,1),(109,4,'','2015-09-24 18:51:57',35560,1,1,2,1),(110,4,'','2015-09-24 22:09:58',35560,1,1,2,1),(111,4,'','2015-09-25 13:50:17',35560,1,1,2,1),(112,4,'','2015-09-26 12:31:13',35560,1,1,2,1),(113,4,'','2015-09-26 17:26:50',35560,1,1,2,1),(114,4,'','2015-09-26 17:59:23',35560,1,1,2,1),(115,4,'','2015-09-29 16:35:26',35560,1,1,2,1),(116,4,'','2015-09-29 16:41:10',35560,1,1,2,1),(117,4,'','2015-09-29 16:50:09',35560,1,1,2,1),(118,4,'','2015-09-29 16:56:30',35560,1,1,2,1),(119,4,'','2015-09-29 22:17:05',35560,1,1,2,1),(120,4,'','2015-09-30 14:38:27',35560,1,1,2,1),(121,4,'','2015-09-30 14:54:01',35560,1,1,2,1),(122,4,'','2015-09-30 15:02:46',35560,1,1,2,1),(123,4,'','2015-09-30 17:07:23',35560,1,1,2,1),(124,4,'','2015-09-30 17:16:25',35560,1,1,2,1),(125,4,'','2015-10-01 11:10:53',35560,1,1,2,1),(126,4,'','2015-10-01 11:20:06',35560,1,1,2,1),(127,4,'','2015-10-02 13:25:35',35560,1,1,2,1),(128,4,'','2015-10-02 13:31:07',35560,1,1,2,1),(129,4,'','2015-10-02 13:47:34',35560,1,1,2,1),(130,4,'','2015-10-02 14:13:09',35560,1,1,2,1),(131,4,'','2015-10-02 14:18:53',35560,1,1,2,1),(132,4,'','2015-10-02 15:04:58',35560,1,1,2,1),(133,4,'','2015-10-02 15:57:52',35560,1,1,2,1),(134,4,'','2015-10-02 23:08:21',35560,1,1,2,1),(135,4,'','2015-10-04 16:56:09',35560,1,1,2,1),(136,4,'','2015-10-04 17:06:22',35560,1,1,2,1),(137,4,'','2015-10-04 17:23:43',35560,1,1,2,1),(138,4,'','2015-10-04 17:33:49',35560,1,1,2,1),(139,4,'','2015-10-04 18:27:20',35560,1,1,2,1),(140,4,'','2015-10-04 19:02:32',35560,1,1,2,1),(141,4,'','2015-10-05 13:38:39',35560,1,1,2,1),(142,4,'','2015-10-05 17:28:23',35560,1,1,2,1),(143,4,'','2015-10-05 19:25:53',35560,1,1,2,1),(144,4,'','2015-10-06 14:01:14',35560,1,1,2,1),(145,4,'','2015-10-06 14:13:57',35560,1,1,2,1),(146,4,'','2015-10-06 19:45:56',35560,1,1,2,1),(147,4,'','2015-10-07 21:06:42',35560,1,1,2,1),(148,4,'','2015-10-07 22:45:17',35560,1,1,2,1),(149,4,'','2015-10-08 14:33:26',35560,1,1,2,1),(150,4,'','2015-10-08 15:35:07',35560,1,1,2,1),(151,4,'','2015-10-10 17:35:36',35560,1,1,2,1),(152,4,'','2015-10-12 17:17:53',35560,1,1,2,1),(153,4,'','2016-01-11 10:20:54',35560,1,1,2,1),(154,4,'','2016-01-11 10:43:23',35560,1,1,2,1),(155,4,'','2016-01-11 10:51:32',35560,1,1,2,1),(156,4,'','2016-01-12 16:03:27',35560,1,1,2,1),(157,4,'','2016-01-16 20:57:31',35560,1,1,2,1),(158,4,'','2016-01-19 15:06:45',35560,1,1,2,1),(159,4,'','2016-01-19 20:28:26',35560,1,1,2,1),(160,4,'','2016-01-26 15:28:24',35560,1,1,2,1),(161,4,'','2016-01-26 19:20:51',35560,1,1,2,1),(162,4,'','2016-01-29 00:21:45',35560,1,1,2,1),(163,4,'','2016-01-29 00:32:48',35560,1,1,2,1),(164,4,'','2016-01-29 13:56:44',35560,1,1,2,1),(165,4,'','2016-02-01 10:31:30',35560,1,1,2,1),(166,4,'','2016-02-01 17:28:43',35560,1,1,2,1),(167,4,'','2016-02-13 18:25:45',35560,1,1,2,1),(168,4,'','2016-02-15 19:36:13',35560,1,1,2,1),(169,4,'','2016-02-15 19:39:24',35560,1,1,2,1),(170,4,'','2016-02-15 19:43:23',35560,1,1,2,1),(171,4,'','2016-02-15 19:54:30',35560,1,1,2,1),(172,4,'','2016-02-16 19:22:29',35560,1,1,2,1),(173,4,'','2016-02-16 19:27:26',35560,1,1,2,1),(174,4,'','2016-02-16 19:31:27',35560,1,1,2,1),(175,4,'','2016-02-16 19:42:44',35560,1,1,2,1),(176,4,'','2016-02-17 09:34:40',35560,1,1,2,1),(177,4,'','2016-02-17 09:40:49',35560,1,1,2,1),(178,4,'','2016-02-17 11:55:55',35560,1,1,2,1),(179,4,'','2016-02-17 12:05:36',35560,1,1,2,1),(180,4,'','2016-02-17 12:29:21',35560,1,1,2,1),(181,4,'','2016-02-17 12:34:00',35560,1,1,2,1),(182,4,'','2016-02-17 12:37:42',35560,1,1,2,1),(183,4,'','2016-02-18 11:47:43',35560,1,1,2,1),(184,4,'','2016-02-18 11:50:36',35560,1,1,2,1),(185,4,'','2016-02-18 11:54:33',35560,1,1,2,1),(186,4,'','2016-02-18 12:01:06',35560,1,1,2,1),(187,4,'','2016-02-18 12:07:22',35560,1,1,2,1),(188,4,'','2016-02-18 12:30:53',35560,1,1,2,1),(189,4,'','2016-02-18 12:45:43',35560,1,1,2,1),(190,4,'','2016-02-18 23:01:32',35560,1,1,2,1),(191,4,'','2016-02-23 11:03:58',35560,1,1,2,1),(192,4,'','2016-02-24 09:28:25',35560,1,1,2,1),(193,4,'','2016-03-03 13:04:23',35560,1,1,2,1),(194,4,'','2016-03-03 15:34:48',35560,1,1,2,1),(195,4,'','2016-03-05 13:16:55',35560,1,1,2,1),(196,4,'','2016-03-05 14:10:35',35560,1,1,2,1),(197,4,'','2016-03-05 14:47:35',35560,1,1,2,1),(198,4,'','2016-03-05 16:07:51',35560,1,1,2,1),(199,4,'','2016-03-05 17:58:34',35560,1,1,2,1),(200,4,'','2016-03-05 18:06:22',35560,1,1,2,1),(201,4,'','2016-03-05 18:36:56',35560,1,1,2,1),(202,4,'','2016-03-05 18:45:23',35560,1,1,2,1),(203,4,'','2016-03-09 18:28:39',35560,1,1,2,1),(204,4,'','2016-03-24 17:00:17',35560,1,1,2,1),(205,4,'','2016-03-24 17:15:59',35560,1,1,2,1),(206,4,'','2016-03-24 19:02:49',35560,1,1,2,1),(207,4,'','2016-03-25 10:51:19',35560,1,1,2,1),(208,4,'','2016-03-25 12:52:19',35560,1,1,2,1),(209,4,'','2016-03-26 14:39:53',35560,1,1,2,1),(210,4,'','2016-03-26 16:39:03',35560,1,1,2,1),(211,4,'','2016-03-26 16:48:46',35560,1,1,2,1),(212,4,'','2016-03-26 17:22:01',35560,1,1,2,1),(213,4,'','2016-03-26 18:21:42',35560,1,1,2,1),(214,4,'','2016-03-26 19:01:02',35560,1,1,2,1),(215,4,'','2016-03-28 10:55:46',35560,1,1,2,1),(216,4,'','2016-03-29 11:50:00',35560,1,1,2,1),(217,4,'','2016-03-29 12:00:19',35560,1,1,2,1),(218,4,'','2016-03-29 12:31:49',35560,1,1,2,1),(219,4,'','2016-03-29 13:03:35',35560,1,1,2,1),(220,4,'','2016-03-29 20:09:14',35560,1,1,2,1),(221,4,'','2016-03-29 20:24:57',35560,1,1,2,1),(222,4,'','2016-03-29 20:33:01',35560,1,1,2,1),(223,4,'','2016-03-29 20:40:08',35560,1,1,2,1),(224,4,'','2016-03-29 20:48:48',35560,1,1,2,1),(225,4,'','2016-03-29 22:08:41',35560,1,1,2,1),(226,4,'','2016-03-29 22:41:05',35560,1,1,2,1),(227,4,'','2016-03-29 23:58:35',35560,1,1,2,1),(228,4,'','2016-03-30 00:14:17',35560,1,1,2,1);
/*!40000 ALTER TABLE `tozin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tozingomroki`
--

DROP TABLE IF EXISTS `tozingomroki`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tozingomroki` (
  `dBId` bigint(20) NOT NULL,
  `ezharnameyeGomroki_dBId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`dBId`),
  KEY `FK_5p0t2dojxadcfl02gmgqh0e8r` (`ezharnameyeGomroki_dBId`),
  KEY `FK_bvxrg3l472fqqkjljns7je4mr` (`dBId`),
  CONSTRAINT `FK_5p0t2dojxadcfl02gmgqh0e8r` FOREIGN KEY (`ezharnameyeGomroki_dBId`) REFERENCES `ezharnameyeGomroki` (`dBId`),
  CONSTRAINT `FK_bvxrg3l472fqqkjljns7je4mr` FOREIGN KEY (`dBId`) REFERENCES `tozin` (`dBId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tozingomroki`
--

LOCK TABLES `tozingomroki` WRITE;
/*!40000 ALTER TABLE `tozingomroki` DISABLE KEYS */;
/*!40000 ALTER TABLE `tozingomroki` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vorudKhoruj`
--

DROP TABLE IF EXISTS `vorudKhoruj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vorudKhoruj` (
  `noeVorudKhoruj` int(11) DEFAULT NULL,
  `dBId` bigint(20) NOT NULL,
  PRIMARY KEY (`dBId`),
  KEY `FK_jvi2f9r3unbob3x7gsi4hb0ju` (`dBId`),
  CONSTRAINT `FK_jvi2f9r3unbob3x7gsi4hb0ju` FOREIGN KEY (`dBId`) REFERENCES `work` (`dBId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vorudKhoruj`
--

LOCK TABLES `vorudKhoruj` WRITE;
/*!40000 ALTER TABLE `vorudKhoruj` DISABLE KEYS */;
INSERT INTO `vorudKhoruj` VALUES (0,40),(0,49),(0,53),(0,60),(0,69),(0,77),(0,85),(0,87),(0,91),(0,99),(0,103),(0,109),(0,116),(0,124),(0,131),(0,152),(0,153),(0,157),(0,163),(0,204),(0,217),(0,224),(0,229),(0,234),(0,241),(0,247),(0,253),(0,254),(0,262),(0,265),(0,268),(0,275),(0,280),(0,285),(0,290),(0,296),(0,301),(0,306),(0,312),(0,318),(0,323),(0,333),(0,389),(0,406),(0,409),(0,448),(0,454),(0,459),(0,465),(0,470),(0,476),(0,479),(0,522),(0,528),(0,534),(0,539),(0,548),(0,557),(0,563),(0,574),(0,688),(0,694),(0,700),(0,706),(0,712),(0,718),(0,724),(0,730),(0,736),(0,741),(0,747),(0,753),(0,765),(0,932),(0,934),(0,935),(0,936),(0,939),(0,941),(0,944),(0,946),(0,963),(0,964),(0,1160),(0,1166),(0,1176),(0,1190),(0,1201),(0,1233),(0,1239),(0,1245),(0,1249),(0,1255),(0,1260),(0,1266),(0,1272),(0,1307),(0,1320),(0,1321),(0,1328),(0,1329),(0,1332),(0,1333),(0,1334),(0,1335),(0,1336),(0,1340),(0,1345),(0,1365),(0,1370),(0,1378),(0,1383),(0,1388),(0,1394),(0,1400),(0,1406),(0,1408),(0,1412),(0,1421),(0,1428),(0,1431),(0,1433),(0,1441),(0,1451),(0,1454),(0,1459),(0,1467),(0,1469),(0,1473),(0,1476),(0,1481),(0,1527),(0,1534),(0,1542),(0,1550),(0,1557),(0,1589),(0,1638),(0,1663),(0,1682),(0,1698),(0,1710),(0,1720),(0,1817),(0,1848),(0,1897),(0,1910),(0,1917),(0,1956),(0,2019),(0,2030),(0,2069),(0,2072),(0,2085),(0,2090),(0,2091),(0,2096),(0,2111),(0,2115),(0,2116),(0,2119),(0,2122),(0,2127),(0,2133),(0,2146),(0,2149),(0,2159),(0,2162),(0,2228),(0,2290),(0,2294),(0,2303),(0,2306),(0,2366),(0,2371),(0,2375),(0,2380),(0,2383),(0,2385),(0,2387),(0,2389),(0,2393),(0,2402),(0,2414),(0,2422),(0,2432),(0,2442),(0,2452),(0,2488),(0,2504),(0,2528),(0,2555),(0,2585),(0,2646),(0,2675),(0,2717),(0,2833),(0,2838),(0,2839),(0,2844),(0,2861),(0,2863),(0,2864),(0,2871),(0,2879),(0,2889),(0,2921),(0,2930),(0,2940),(0,3265),(0,3274),(0,3280),(0,3291),(0,3339),(0,3400),(0,3406),(0,3414),(0,3429),(0,3439),(0,3478),(0,3488),(0,3494),(0,3538),(0,3547),(0,3553),(0,3554),(0,3560),(0,3561),(0,3562),(0,3563),(0,3564),(0,3573),(0,3579),(0,3580),(0,3586),(0,3587),(0,3588),(0,3596),(0,3599),(0,3606),(0,3612),(0,3702),(0,3711),(0,3713),(0,3719),(0,3729),(0,3730),(0,3736),(0,3737),(0,3743),(0,3744),(0,3754),(0,3831),(0,3840),(0,3875),(0,3883),(0,3901),(0,3908),(0,3948),(0,3954),(0,3955),(0,3961),(0,4007),(0,4015),(0,4049),(0,4057),(0,4066),(0,4073),(0,4106),(0,4114),(0,4123),(0,4131),(0,4255),(0,4264),(0,4394),(0,4415),(0,4417),(0,6448),(0,6455),(0,6457),(0,6464),(0,6470),(0,6529),(0,6536),(0,6810),(0,6817),(0,6909),(0,6915),(0,6946),(0,6953),(0,7152),(0,7159),(0,7184),(0,7288),(0,7292),(0,7297),(0,7368),(0,7388),(0,7616),(0,7617),(0,7815),(0,7819),(0,7821),(0,7897),(0,7899),(0,7903),(0,7907),(0,7913),(0,7976),(0,7981),(0,7985),(0,7991),(0,7993),(0,8009),(0,8014),(0,8032),(0,8039),(0,8045),(0,8048),(0,8054),(0,8060),(0,8118),(0,8122),(0,8128),(0,8134),(0,8140),(0,8146),(0,8152),(0,8153),(0,8159),(0,8171),(0,8177),(0,8197),(0,8280),(0,8294),(0,8301),(0,8322),(0,8328),(0,8393),(0,8399),(0,8407),(0,8414),(0,8425),(0,8431),(0,8433),(0,8440),(0,8444),(0,8451),(0,8456),(0,8463),(0,8465),(0,8472),(0,8478),(0,8482),(0,8489),(0,8490),(0,8497),(0,8614),(0,8620),(0,8729),(0,8735),(0,8737),(0,8743),(0,8745),(0,8751),(0,8758),(0,8762),(0,8768),(0,8777),(0,8783),(0,8784),(0,8790),(0,8791),(0,8797),(0,8798),(0,8804),(0,8805),(0,8811),(0,8812),(0,8818),(0,8819),(0,8822),(0,8825),(0,8828),(0,8829),(0,8832),(0,8835),(0,8838),(0,8841),(0,8844),(0,8850),(0,8861),(0,8864),(0,8876),(0,8880),(0,8881),(0,8889),(0,8891),(0,8892),(0,8901),(0,8902),(0,8903),(0,8912),(0,8913),(0,8916),(0,8922),(0,8924),(0,8929),(0,8930),(0,8939),(0,8941),(0,8942),(0,8951),(0,8952),(0,8953),(0,8962),(0,8963),(0,8964),(0,8973),(0,8974),(0,8977),(0,8984),(0,8985),(0,8986),(0,8995),(0,8996),(0,8997),(0,9006),(0,9008),(0,9011),(0,9018),(0,9020),(0,9023),(0,9029),(0,9031),(0,9036),(0,9039),(0,9042),(0,9045),(0,9048),(0,9066),(0,9070),(0,9074),(0,9078),(0,9082),(0,9084),(0,9087),(0,9092),(0,9097),(0,9100),(0,9105),(0,9112),(0,9116),(0,9120),(0,9124),(0,9128),(0,9132),(0,9136),(0,9140),(0,9144),(0,9148),(0,9152),(0,9158),(0,9162),(0,9168),(0,9173),(0,9174),(0,9175),(0,9178),(0,9182),(0,9183),(0,9185),(0,9186),(0,9187),(0,9190),(0,9191),(0,9193),(0,9195),(0,9198),(0,9199),(0,9201),(0,9203),(0,9206),(0,9207),(0,9209),(0,9212),(0,9213),(0,9215),(0,9216),(0,9218),(0,9220),(0,9221),(0,9222),(0,9223),(0,9224),(0,9225),(0,9227),(0,9228),(0,9231),(0,9232),(0,9233),(0,9235),(0,9237),(0,9240),(0,9241),(0,9242),(0,9244),(0,9246),(0,9251),(0,9255),(0,9258),(0,9259),(0,9260),(0,9262),(0,9264),(0,9267),(0,9268),(0,9269),(0,9271),(0,9273),(0,9277),(0,9280),(0,9281),(0,9282),(0,9284),(0,9286),(0,9288),(0,9289),(0,9290),(0,9292),(0,9293),(0,9294),(0,9295),(0,9297),(0,9301),(0,9304),(0,9307),(0,9310),(0,9311),(0,9312),(0,9314),(0,9322),(0,9332),(0,9337),(0,9340),(0,9341),(0,9342),(0,9344),(0,9346),(0,9349),(0,9350),(0,9351),(0,9353),(0,9387),(0,9388),(0,9389),(0,9390),(0,9391),(0,9392),(0,9393),(0,9398),(0,9403),(0,9407),(0,9412),(0,9415),(0,9416),(0,9425),(0,9426),(0,9432),(0,9433),(0,9435),(0,9436),(0,9442),(0,9443),(0,9450),(0,9451),(0,9458),(0,9465),(0,9466),(0,9468),(0,9469),(0,9473),(0,9483),(0,9484),(0,9485),(0,9486),(0,9487),(0,9488),(0,9489),(0,9492),(0,9493),(0,9494),(0,9497),(0,9498),(0,9499),(0,9502),(0,9505),(0,9506),(0,9507),(0,9508),(0,9510),(0,9512),(0,9513),(0,9518),(0,9522),(0,9525),(0,9526),(0,9527),(0,9529),(0,9535),(0,9540),(0,9544),(0,9549),(0,9552),(0,9553),(0,9554),(0,9556),(0,9558),(0,9566),(0,9567),(0,9568),(0,9570),(0,9603),(0,9606),(0,9607),(0,9608),(0,9610),(0,9613),(0,9617),(0,9628),(0,9629),(0,9656),(0,9657),(0,9661),(0,9662),(0,9667),(0,9668),(0,9699),(0,9700),(0,9702),(0,9703),(0,9705),(0,9706),(0,9708),(0,9709),(0,9713),(0,9714),(0,9716),(0,9717),(0,9732),(0,9733),(0,9737),(0,9738),(0,9746),(0,9747),(0,9791),(0,9792),(0,9795),(0,9825),(0,9826),(0,9832),(0,9836),(0,9837),(0,9839),(0,9840),(0,9842),(0,9843),(0,9844),(0,9845),(0,9848),(0,9849),(0,9852),(0,9853),(0,9854),(0,9855),(0,9857),(0,9858),(0,9861),(0,9862),(0,9864),(0,9865),(0,9867),(0,9868),(0,9870),(0,9871),(0,9872),(0,9873),(0,9875),(0,9876),(0,9878),(0,9879),(0,9881),(0,9882),(0,9884),(0,9885),(0,9886),(0,9887),(0,9892),(0,9893),(0,9894),(0,9895),(0,9899),(0,9900),(0,9904),(0,9905),(0,9907),(0,9908),(0,9917),(0,9918),(0,9925),(0,9926),(0,9927),(0,9929),(0,9931),(0,9932),(0,9938),(0,9939),(0,9940),(0,9941),(0,9945),(0,9946),(0,9948),(0,9949),(0,9984),(0,9985),(0,10007),(0,10008),(0,10011),(0,10012),(0,10014),(0,10015),(0,10018),(0,10019),(0,10022),(0,10023),(0,10025),(0,10026),(0,10029),(0,10030),(0,10034),(0,10035),(0,10036),(0,10037),(0,10054),(0,10055),(0,10059),(0,10060),(0,10061),(0,10062),(0,10065),(0,10066),(0,10070),(0,10071),(0,10077),(0,10078),(0,10081),(0,10082),(0,10102),(0,10103),(0,10105),(0,10106),(0,10115),(0,10116),(0,10117),(0,10118),(0,10121),(0,10122),(0,10125),(0,10126),(0,10129),(0,10130),(0,10137),(0,10138),(0,10140),(0,10141),(0,10144),(0,10145),(0,10147),(0,10148),(0,10150),(0,10151),(0,10154),(0,10155),(0,10170),(0,10171),(0,10172),(0,10173),(0,10174),(0,10175),(0,10176),(0,10177),(0,10179),(0,10180),(0,10181),(0,10182),(0,10183),(0,10184),(0,10187),(0,10188),(0,10193),(0,10194),(0,10195),(0,10196),(0,10199),(0,10200),(0,10203),(0,10204),(0,10206),(0,10207),(0,10210),(0,10211),(0,10214),(0,10215),(0,10217),(0,10218),(0,10228),(0,10229),(0,10233),(0,10234),(0,10237),(0,10238),(0,10240),(0,10241),(0,10242),(0,10243),(0,10244),(0,10245),(0,10246),(0,10247),(0,10250),(0,10251),(0,10252),(0,10253),(0,10256),(0,10257),(0,10262),(0,10263),(0,10265),(0,10266),(0,10272),(0,10273),(0,10274),(0,10275),(0,10277),(0,10278),(0,10281),(0,10282),(0,10285),(0,10286),(0,10293),(0,10294),(0,10296),(0,10297),(0,10299),(0,10300),(0,10301),(0,10302),(0,10306),(0,10307),(0,10311),(0,10312),(0,10319),(0,10320),(0,10326),(0,10327),(0,10339),(0,10340),(0,10347),(0,10348),(0,10354),(0,10355),(0,10358),(0,10359),(0,10367),(0,10368),(0,10371),(0,10372),(0,10379),(0,10380),(0,10386),(0,10387),(0,10391),(0,10392),(0,10398),(0,10399),(0,10402),(0,10403),(0,10407),(0,10408),(0,10410),(0,10411),(0,10412),(0,10413),(0,10418),(0,10419),(0,10421),(0,10422),(0,10424),(0,10425),(0,10426),(0,10427),(0,10438),(0,10439),(0,10441),(0,10442),(0,10443),(0,10444),(0,10446),(0,10447),(0,10451),(0,10452),(0,10455),(0,10456),(0,10458),(0,10459),(0,10461),(0,10462),(0,10466),(0,10467),(0,10471),(0,10472),(0,10475),(0,10476),(0,10480),(0,10481),(0,10485),(0,10486),(0,10489),(0,10490),(0,10502),(0,10503),(0,10504),(0,10505),(0,10506),(0,10507),(0,10508),(0,10509),(0,10510),(0,10511),(0,10512),(0,10513),(0,10514),(0,10515),(0,10516),(0,10517),(0,10518),(0,10519),(0,10520),(0,10521),(0,10522),(0,10523),(0,10524),(0,10525),(0,10526),(0,10527),(0,10528),(0,10529),(0,10530),(0,10531),(0,10532),(0,10533),(0,10551),(0,10552),(0,10556),(0,10557),(0,10558),(0,10559),(0,10562),(0,10563),(0,10565),(0,10566),(0,10571),(0,10572),(0,10575),(0,10576),(0,10579),(0,10580),(0,10582),(0,10583),(0,10587),(0,10588),(0,10589),(0,10590),(0,10593),(0,10594),(0,10596),(0,10597),(0,10599),(0,10600),(0,10603),(0,10604),(0,10607),(0,10608),(0,10609),(0,10610),(0,10612),(0,10613),(0,10615),(0,10616),(0,10618),(0,10619),(0,10621),(0,10622),(0,10624),(0,10625),(0,10627),(0,10628),(0,10631),(0,10632),(0,10633),(0,10634),(0,10636),(0,10637),(0,10638),(0,10639),(0,10641),(0,10642),(0,10644),(0,10645),(0,10647),(0,10648),(0,10650),(0,10651),(0,10654),(0,10655),(0,10658),(0,10659),(0,10660),(0,10661),(0,10664),(0,10665),(0,10666),(0,10667),(0,10669),(0,10670),(0,10671),(0,10672),(0,10674),(0,10675),(0,10677),(0,10678),(0,10680),(0,10681),(0,10683),(0,10684),(0,10686),(0,10687),(0,10689),(0,10690),(0,10692),(0,10693),(0,10695),(0,10696),(0,10698),(0,10699),(0,10700),(0,10701),(0,10703),(0,10704),(0,10706),(0,10707),(0,10709),(0,10710),(0,10712),(0,10713),(0,10716),(0,10717),(0,10719),(0,10720),(0,10722),(0,10723),(0,10725),(0,10726),(0,10728),(0,10729),(0,10731),(0,10732),(0,10734),(0,10735),(0,10737),(0,10738),(0,10740),(0,10741),(0,10743),(0,10744),(0,10746),(0,10747),(0,10749),(0,10750),(0,10752),(0,10753),(0,10754),(0,10755),(0,10757),(0,10758),(0,10759),(0,10760),(0,10762),(0,10763),(0,10765),(0,10766),(0,10768),(0,10769),(0,10771),(0,10772),(0,10774),(0,10775),(0,10777),(0,10778),(0,10780),(0,10781),(0,10783),(0,10784),(0,10786),(0,10787),(0,10789),(0,10790),(0,10792),(0,10793),(0,10795),(0,10796),(0,10798),(0,10799),(0,10801),(0,10802),(0,10803),(0,10804),(0,10806),(0,10807),(0,10809),(0,10810),(0,10812),(0,10813),(0,10815),(0,10816),(0,10818),(0,10819),(0,10821),(0,10822),(0,10824),(0,10825),(0,10827),(0,10828),(0,10830),(0,10831),(0,10832),(0,10833),(0,10835),(0,10836),(0,10838),(0,10839),(0,10841),(0,10842),(0,10844),(0,10846),(0,10847),(0,10848),(0,10850),(0,10851),(0,10853),(0,10854),(0,10856),(0,10857),(0,10859),(0,10860),(0,10862),(0,10863),(0,10865),(0,10866),(0,10868),(0,10869),(0,10871),(0,10872),(0,10874),(0,10875),(0,10877),(0,10878),(0,10880),(0,10881),(0,10884),(0,10885),(0,10888),(0,10889),(0,10891),(0,10892),(0,10894),(0,10895),(0,10897),(0,10898),(0,10900),(0,10901),(0,10903),(0,10904),(0,10907),(0,10908),(0,10911),(0,10912),(0,10915),(0,10916),(0,10919),(0,10920),(0,10922),(0,10923),(0,10926),(0,10927),(0,10930),(0,10931),(0,10934),(0,10935),(0,10937),(0,10938),(0,10940),(0,10941),(0,10942),(0,10943),(0,10944),(0,10945),(0,10946),(0,10947),(0,10948),(0,10949),(0,10950),(0,10951),(0,10952),(0,10953),(0,10954),(0,10955),(0,10957),(0,10960),(0,10964),(0,10965),(0,10967),(0,10968),(0,10969),(0,10970),(0,10972),(0,10973),(0,10976),(0,10977),(0,10978),(0,10979),(0,10981),(0,10982),(0,10985),(0,10986),(0,10988),(0,10989),(0,10991),(0,10992),(0,10994),(0,10995),(0,10997),(0,10998),(0,11000),(0,11001),(0,11003),(0,11004),(0,11006),(0,11007),(0,11008),(0,11009),(0,11011),(0,11012),(0,11014),(0,11015),(0,11017),(0,11018),(0,11021),(0,11022),(0,11024),(0,11025),(0,11027),(0,11028),(0,11030),(0,11031),(0,11035),(0,11036),(0,11038),(0,11039),(0,11041),(0,11042),(0,11044),(0,11045),(0,11047),(0,11048),(0,11050),(0,11051),(0,11053),(0,11054),(0,11056),(0,11057),(0,11059),(0,11060),(0,11062),(0,11063),(0,11074),(0,11075),(0,11081),(0,11082),(0,11086),(0,11087),(0,11089),(0,11090),(0,11092),(0,11093),(0,11095),(0,11096),(0,11098),(0,11099),(0,11101),(0,11102),(0,11104),(0,11105),(0,11107),(0,11108),(0,11111),(0,11112),(0,11113),(0,11114),(0,11115),(0,11116),(0,11117),(0,11118),(0,11119),(0,11120),(0,11121),(0,11122),(0,11123),(0,11124),(0,11125),(0,11126),(0,11128),(0,11129),(0,11131),(0,11132),(0,11134),(0,11135),(0,11136),(0,11137),(0,11139),(0,11140),(0,11142),(0,11143),(0,11145),(0,11146),(0,11147),(0,11148),(0,11149),(0,11150),(0,11151),(0,11152),(0,11154),(0,11155),(0,11156),(0,11157),(0,11191),(0,11192),(0,11196),(0,11197),(0,11199),(0,11200),(0,11206),(0,11207),(0,11211),(0,11212),(0,11215),(0,11216),(0,11218),(0,11219),(0,11229),(0,11230),(0,11238),(0,11239),(0,11244),(0,11245),(0,11247),(0,11248),(0,11249),(0,11250),(0,11255),(0,11256),(0,11259),(0,11260),(0,11264),(0,11265),(0,11268),(0,11269),(0,11272),(0,11273),(0,11277),(0,11278),(0,11281),(0,11282),(0,11285),(0,11286),(0,11289),(0,11290),(0,11292),(0,11293),(0,11297),(0,11298),(0,11302),(0,11303),(0,11305),(0,11306),(0,11310),(0,11311),(0,11314),(0,11315),(0,11318),(0,11319),(0,11323),(0,11324),(0,11332),(0,11333),(0,11334),(0,11335),(0,11339),(0,11340),(0,11343),(0,11344),(0,11347),(0,11348),(0,11351),(0,11352),(0,11356),(0,11357),(0,11360),(0,11361),(0,11364),(0,11365),(0,11370),(0,11371),(0,11373),(0,11374),(0,11377),(0,11378),(0,11382),(0,11383),(0,11385),(0,11386),(0,11390),(0,11391),(0,11394),(0,11395),(0,11397),(0,11398),(0,11402),(0,11403),(0,11407),(0,11408),(0,11409),(0,11410),(0,11413),(0,11414),(0,11417),(0,11418),(0,11420),(0,11421),(0,11424),(0,11425),(0,11430),(0,11431),(0,11436),(0,11437),(0,11439),(0,11440),(0,11442),(0,11443),(0,11445),(0,11446),(0,11450),(0,11451),(0,11452),(0,11453),(0,11454),(0,11455),(0,11456),(0,11457),(0,11458),(0,11459),(0,11460),(0,11461),(0,11462),(0,11463),(0,11464),(0,11465),(0,11466),(0,11467),(0,11468),(0,11469),(0,11470),(0,11471),(0,11472),(0,11473),(0,11474),(0,11475),(0,11476),(0,11477),(0,11478),(0,11479),(0,11480),(0,11481),(0,11482),(0,11483),(0,11484),(0,11486),(0,11487),(0,11490),(0,11491),(0,11493),(0,11494),(0,11495),(0,11496),(0,11498),(0,11499),(0,11501),(0,11502),(0,11503),(0,11504),(0,11506),(0,11507),(0,11509),(0,11510),(0,11512),(0,11513),(0,11516),(0,11517),(0,11520),(0,11521),(0,11530),(0,11531),(0,11534),(0,11535),(0,11537),(0,11538),(0,11540),(0,11541),(0,11543),(0,11544),(0,11546),(0,11547),(0,11551),(0,11552),(0,11557),(0,11558),(0,11568),(0,11569),(0,11571),(0,11572),(0,11573),(0,11574),(0,11578),(0,11579),(0,11582),(0,11583);
/*!40000 ALTER TABLE `vorudKhoruj` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `work`
--

DROP TABLE IF EXISTS `work`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `work` (
  `dBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `valid` tinyint(1) DEFAULT NULL,
  `emp_EmployeeDBId` bigint(20) DEFAULT NULL,
  `gate_dBId` bigint(20) DEFAULT NULL,
  `safar_dBId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`dBId`),
  KEY `FK_kojravofimgei6exh228y6con` (`emp_EmployeeDBId`),
  KEY `FK_cnuycgfx6w4ba7ys4vadd4jif` (`gate_dBId`),
  KEY `FK_tebro2x7vn31xf6meh0wl2j9q` (`safar_dBId`),
  CONSTRAINT `FK_cnuycgfx6w4ba7ys4vadd4jif` FOREIGN KEY (`gate_dBId`) REFERENCES `baskool` (`dBId`),
  CONSTRAINT `FK_kojravofimgei6exh228y6con` FOREIGN KEY (`emp_EmployeeDBId`) REFERENCES `Employee` (`EmployeeDBId`),
  CONSTRAINT `FK_tebro2x7vn31xf6meh0wl2j9q` FOREIGN KEY (`safar_dBId`) REFERENCES `Safar` (`dBId`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `work`
--

LOCK TABLES `work` WRITE;
/*!40000 ALTER TABLE `work` DISABLE KEYS */;
INSERT INTO `work` VALUES (2,'2016-06-13 19:20:11',1,1,1,2),(3,'2016-06-13 19:22:38',1,1,1,3),(4,'2016-06-13 19:24:20',1,1,1,4),(5,'2016-06-14 09:50:04',1,1,1,6),(6,'2016-06-14 10:04:27',1,1,1,7),(7,'2016-06-15 07:04:34',1,1,1,9),(8,'2016-06-15 08:42:35',1,1,1,10),(9,'2016-06-15 08:44:30',1,1,1,11),(10,'2016-06-15 08:54:55',1,1,1,13),(11,'2016-06-15 10:44:52',1,1,1,15),(12,'2016-06-15 10:47:11',1,1,1,16),(13,'2016-06-15 10:52:57',1,1,1,17),(14,'2016-06-15 11:03:24',1,1,1,19),(15,'2016-06-15 14:51:25',1,1,1,21),(16,'2016-06-16 10:04:30',1,1,1,22),(17,'2016-06-16 10:05:34',1,1,1,23),(18,'2016-06-16 10:31:28',1,1,1,24),(19,'2016-06-16 10:32:56',1,1,1,25),(20,'2016-06-16 11:20:57',1,1,1,26),(21,'2016-06-16 11:22:06',1,1,1,27),(22,'2016-06-18 11:29:40',1,1,1,28),(23,'2016-06-18 11:48:34',1,1,1,29),(24,'2016-06-18 14:21:46',1,1,1,30),(25,'2016-06-18 14:38:13',1,1,1,31),(26,'2016-06-20 06:28:20',1,1,1,32);
/*!40000 ALTER TABLE `work` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-22  6:39:40
