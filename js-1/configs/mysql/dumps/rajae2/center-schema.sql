/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.12 : Database - center
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`center` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `center`;

/*Table structure for table `Employee` */

DROP TABLE IF EXISTS `Employee`;

CREATE TABLE `Employee` (
  `EmployeeDBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `currentGomrok` varchar(255) DEFAULT NULL,
  `gomrokZone` varchar(255) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  `melliCode` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phoneNumber` varchar(255) DEFAULT NULL,
  `secretKey` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`EmployeeDBId`),
  UNIQUE KEY `UK_1ndh1xg7odg4cvrlny2fv7q6o` (`melliCode`),
  KEY `currentGomrok` (`currentGomrok`),
  KEY `isActive` (`isActive`),
  KEY `melliCode` (`melliCode`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `Employee` */

LOCK TABLES `Employee` WRITE;

insert  into `Employee`(`EmployeeDBId`,`currentGomrok`,`gomrokZone`,`isActive`,`melliCode`,`name`,`password`,`phoneNumber`,`secretKey`) values (1,'center',NULL,NULL,'admin','ظ…ط¯غŒط±','123456Aa',NULL,NULL);

UNLOCK TABLES;

/*Table structure for table `Employee_Responsibility` */

DROP TABLE IF EXISTS `Employee_Responsibility`;

CREATE TABLE `Employee_Responsibility` (
  `Employee_EmployeeDBId` bigint(20) NOT NULL,
  `responsiblities_ResponsibilityDBId` bigint(20) NOT NULL,
  UNIQUE KEY `UK_ma1k6yhpqr7mqoocf7o2wq1vw` (`responsiblities_ResponsibilityDBId`),
  KEY `FK_ma1k6yhpqr7mqoocf7o2wq1vw` (`responsiblities_ResponsibilityDBId`),
  KEY `FK_b68sgw1qdl7159tvyb63gktwg` (`Employee_EmployeeDBId`),
  CONSTRAINT `FK_b68sgw1qdl7159tvyb63gktwg` FOREIGN KEY (`Employee_EmployeeDBId`) REFERENCES `Employee` (`EmployeeDBId`),
  CONSTRAINT `FK_ma1k6yhpqr7mqoocf7o2wq1vw` FOREIGN KEY (`responsiblities_ResponsibilityDBId`) REFERENCES `Responsibility` (`ResponsibilityDBId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `Employee_Responsibility` */

LOCK TABLES `Employee_Responsibility` WRITE;

insert  into `Employee_Responsibility`(`Employee_EmployeeDBId`,`responsiblities_ResponsibilityDBId`) values (1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(1,8);

UNLOCK TABLES;

/*Table structure for table `GomrokProperties` */

DROP TABLE IF EXISTS `GomrokProperties`;

CREATE TABLE `GomrokProperties` (
  `DBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`DBId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `GomrokProperties` */

LOCK TABLES `GomrokProperties` WRITE;

insert  into `GomrokProperties`(`DBId`,`id`,`ip`,`name`,`url`) values (1,'000000','127.0.0.1','ﺱﻭییچ ﺲﻨﺗﺭ','http://center-swich-rajae:8080/Center-Swich-Rajae/'),(2,'50100','20.1.1.100','ﻢﻨﻄﻘﻫ ﻭیژﻩ ﺎﻘﺘﺻﺍﺩی ﺶﻫیﺩ ﺮﺟﺍیی','http://swich-rajae:8080/Swich-Rajae/'),(3,'30100','20.1.1.101','ﺏﺍﺯﺭگﺎﻧ','http://swich-bazargan:8080/Swich-Bazargan/'),(4,'10300','20.1.1.102','ﺖﻫﺭﺎﻧ','http://swich-tehran:8080/Swich-Tehran/'),(6,'50100','20.2.1.100','ﻢﻨﻄﻘﻫ ﻭیژﻩ ﺎﻘﺘﺻﺍﺩی ﺶﻫیﺩ ﺮﺟﺍیی','http://swich-rajae:8080/Swich-Rajae/'),(7,'30100','20.2.1.101','ﺏﺍﺯﺭگﺎﻧ','http://swich-bazargan:8080/Swich-Bazargan/'),(8,'10300','20.2.1.102','ﺖﻫﺭﺎﻧ','http://swich-tehran:8080/Swich-Tehran/'),(9,'50100','20.3.1.100','ﻭیژﻩ ﺎﻘﺘﺻﺍﺩی ﺶﻫیﺩ ﺮﺟﺍیی','http://swich-rajae:8080/Swich-Rajae/'),(10,'30100','20.3.1.101','ﺏﺍﺯﺭگﺎﻧ','http://swich-bazargan:8080/Swich-Bazargan/'),(11,'10300','20.3.1.102','ﺖﻫﺭﺎﻧ','http://swich-tehran:8080/Swich-Tehran/');

UNLOCK TABLES;

/*Table structure for table `PollerProperties` */

DROP TABLE IF EXISTS `PollerProperties`;

CREATE TABLE `PollerProperties` (
  `DBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `dst` varchar(255) DEFAULT NULL,
  `num` int(11) NOT NULL,
  `period` int(11) DEFAULT NULL,
  `sid` int(11) DEFAULT NULL,
  PRIMARY KEY (`DBId`),
  UNIQUE KEY `UK_n06d12cnf9qq50byjeeta8790` (`dst`,`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `PollerProperties` */

LOCK TABLES `PollerProperties` WRITE;

UNLOCK TABLES;

/*Table structure for table `Responsibility` */

DROP TABLE IF EXISTS `Responsibility`;

CREATE TABLE `Responsibility` (
  `DTYPE` varchar(31) NOT NULL,
  `ResponsibilityDBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `end_date` datetime DEFAULT NULL,
  `gomrok` varchar(255) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `empl_EmployeeDBId` bigint(20) DEFAULT NULL,
  `hazfKonande_EmployeeDBId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ResponsibilityDBId`),
  KEY `FK_58tc688dc9cjmf5vq3418u47n` (`empl_EmployeeDBId`),
  KEY `FK_4jqoupsl0b9oj3gwb6vou9nek` (`hazfKonande_EmployeeDBId`),
  CONSTRAINT `FK_4jqoupsl0b9oj3gwb6vou9nek` FOREIGN KEY (`hazfKonande_EmployeeDBId`) REFERENCES `Employee` (`EmployeeDBId`),
  CONSTRAINT `FK_58tc688dc9cjmf5vq3418u47n` FOREIGN KEY (`empl_EmployeeDBId`) REFERENCES `Employee` (`EmployeeDBId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `Responsibility` */

LOCK TABLES `Responsibility` WRITE;

insert  into `Responsibility`(`DTYPE`,`ResponsibilityDBId`,`end_date`,`gomrok`,`start_date`,`type`,`empl_EmployeeDBId`,`hazfKonande_EmployeeDBId`) values ('SuperUser',1,NULL,'center','2016-06-07 14:38:39',NULL,1,NULL),('DynamicResp',2,NULL,'center','2016-06-07 14:39:27','selectivityBaznini',1,NULL),('DynamicResp',3,NULL,'center','2016-06-07 14:42:13','addTsc',1,NULL),('DynamicResp',4,NULL,'center','2016-06-07 14:42:14','arzNerkh',1,NULL),('DynamicResp',5,NULL,'center','2016-06-07 14:42:15','bimeDaran',1,NULL),('DynamicResp',6,NULL,'center','2016-06-07 14:42:17','blackList',1,NULL),('DynamicResp',7,NULL,'center','2016-06-07 14:42:18','daftarVaredat',1,NULL),('DynamicResp',8,NULL,'center','2016-06-07 14:42:19','selectivitySalon',1,NULL);

UNLOCK TABLES;

/*Table structure for table `Rule` */

DROP TABLE IF EXISTS `Rule`;

CREATE TABLE `Rule` (
  `dbId` bigint(20) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `ruleName` varchar(255) DEFAULT NULL,
  `ruleServiceName` varchar(255) DEFAULT NULL,
  `src` varchar(255) DEFAULT NULL,
  `synced` tinyint(1) DEFAULT NULL,
  `uniqueId` bigint(20) DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `targetClass_dbId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`dbId`),
  KEY `uniqueId` (`uniqueId`),
  KEY `FK_gfr3hv915rvv1gmmnt4luk1bu` (`targetClass_dbId`),
  CONSTRAINT `FK_gfr3hv915rvv1gmmnt4luk1bu` FOREIGN KEY (`targetClass_dbId`) REFERENCES `RuleClass` (`dbId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `Rule` */

LOCK TABLES `Rule` WRITE;

insert  into `Rule`(`dbId`,`createDate`,`deleted`,`ip`,`priority`,`ruleName`,`ruleServiceName`,`src`,`synced`,`uniqueId`,`updateDate`,`username`,`targetClass_dbId`) values (1,'2016-06-07 14:39:51',0,'172.17.193.185',1,'tesr','ir.customs.ruleManager.SelectivityBazbini',NULL,NULL,NULL,'2016-06-07 14:40:19','ظ…ط¯غŒط± (admin)',1);

UNLOCK TABLES;

/*Table structure for table `RuleClass` */

DROP TABLE IF EXISTS `RuleClass`;

CREATE TABLE `RuleClass` (
  `dbId` bigint(20) NOT NULL AUTO_INCREMENT,
  `beanClassName` varchar(255) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `ruleName` varchar(255) DEFAULT NULL,
  `className` varchar(255) DEFAULT NULL,
  `fieldName` varchar(255) DEFAULT NULL,
  `hasValue` tinyint(1) DEFAULT NULL,
  `isCollection` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`dbId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `RuleClass` */

LOCK TABLES `RuleClass` WRITE;

insert  into `RuleClass`(`dbId`,`beanClassName`,`depth`,`ruleName`,`className`,`fieldName`,`hasValue`,`isCollection`) values (1,'ParvanehVaredati',1,'SelectivityBazbini::tesr','ParvanehVaredati','',0,NULL);

UNLOCK TABLES;

/*Table structure for table `Rule_ServiceResult` */

DROP TABLE IF EXISTS `Rule_ServiceResult`;

CREATE TABLE `Rule_ServiceResult` (
  `Rule_dbId` bigint(20) NOT NULL,
  `results_dbId` bigint(20) NOT NULL,
  UNIQUE KEY `UK_5i88hyw81q0o7bsks5xyvcgjh` (`results_dbId`),
  KEY `FK_5i88hyw81q0o7bsks5xyvcgjh` (`results_dbId`),
  KEY `FK_dbxyabos92e7iha18o12e1s6t` (`Rule_dbId`),
  CONSTRAINT `FK_5i88hyw81q0o7bsks5xyvcgjh` FOREIGN KEY (`results_dbId`) REFERENCES `ServiceResult` (`dbId`),
  CONSTRAINT `FK_dbxyabos92e7iha18o12e1s6t` FOREIGN KEY (`Rule_dbId`) REFERENCES `Rule` (`dbId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `Rule_ServiceResult` */

LOCK TABLES `Rule_ServiceResult` WRITE;

insert  into `Rule_ServiceResult`(`Rule_dbId`,`results_dbId`) values (1,1),(1,2);

UNLOCK TABLES;

/*Table structure for table `ServiceResult` */

DROP TABLE IF EXISTS `ServiceResult`;

CREATE TABLE `ServiceResult` (
  `dbId` bigint(20) NOT NULL AUTO_INCREMENT,
  `percent` int(11) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `val` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`dbId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `ServiceResult` */

LOCK TABLES `ServiceResult` WRITE;

insert  into `ServiceResult`(`dbId`,`percent`,`priority`,`val`) values (1,98,1,'Green'),(2,2,1,'Red');

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
