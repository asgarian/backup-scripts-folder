-- MySQL dump 10.13  Distrib 5.5.47, for debian-linux-gnu (x86_64)
--
-- Host: 172.16.111.12    Database: center-swich
-- ------------------------------------------------------
-- Server version	5.7.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `center-swich`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `center-swich` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `center-swich`;

--
-- Table structure for table `AccessDate`
--

DROP TABLE IF EXISTS `AccessDate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AccessDate` (
  `dbId` bigint(20) NOT NULL AUTO_INCREMENT,
  `accessDate` datetime DEFAULT NULL,
  `resultValue` varchar(255) DEFAULT NULL,
  `routedObj` varchar(255) DEFAULT NULL,
  `rule_dbId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`dbId`),
  KEY `FK_2yyxn4hex6nhl3p1dmytvolp0` (`rule_dbId`),
  CONSTRAINT `FK_2yyxn4hex6nhl3p1dmytvolp0` FOREIGN KEY (`rule_dbId`) REFERENCES `Rule` (`dbId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AccessDate`
--

LOCK TABLES `AccessDate` WRITE;
/*!40000 ALTER TABLE `AccessDate` DISABLE KEYS */;
/*!40000 ALTER TABLE `AccessDate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CalculationCurrency`
--

DROP TABLE IF EXISTS `CalculationCurrency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CalculationCurrency` (
  `dbId` bigint(20) NOT NULL AUTO_INCREMENT,
  `arzName` varchar(255) DEFAULT NULL,
  `arzValue` double DEFAULT NULL,
  PRIMARY KEY (`dbId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CalculationCurrency`
--

LOCK TABLES `CalculationCurrency` WRITE;
/*!40000 ALTER TABLE `CalculationCurrency` DISABLE KEYS */;
/*!40000 ALTER TABLE `CalculationCurrency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CalculationPolicy`
--

DROP TABLE IF EXISTS `CalculationPolicy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CalculationPolicy` (
  `DBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `endDate` datetime DEFAULT NULL,
  `nerkheArz` double DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  `creator_EmployeeDBId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`DBId`),
  KEY `FK_cdwirs1oonimt5udemajqbmx4` (`creator_EmployeeDBId`),
  CONSTRAINT `FK_cdwirs1oonimt5udemajqbmx4` FOREIGN KEY (`creator_EmployeeDBId`) REFERENCES `Employee` (`EmployeeDBId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CalculationPolicy`
--

LOCK TABLES `CalculationPolicy` WRITE;
/*!40000 ALTER TABLE `CalculationPolicy` DISABLE KEYS */;
/*!40000 ALTER TABLE `CalculationPolicy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CalculationPolicy_CalculationCurrency`
--

DROP TABLE IF EXISTS `CalculationPolicy_CalculationCurrency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CalculationPolicy_CalculationCurrency` (
  `CalculationPolicy_DBId` bigint(20) NOT NULL,
  `currencies_dbId` bigint(20) NOT NULL,
  PRIMARY KEY (`CalculationPolicy_DBId`,`currencies_dbId`),
  UNIQUE KEY `UK_lq2569xarxw9lmxqs5tfymmec` (`currencies_dbId`),
  KEY `FK_lq2569xarxw9lmxqs5tfymmec` (`currencies_dbId`),
  KEY `FK_mt99qrtcgh3oi7xhm0k4xv048` (`CalculationPolicy_DBId`),
  CONSTRAINT `FK_lq2569xarxw9lmxqs5tfymmec` FOREIGN KEY (`currencies_dbId`) REFERENCES `CalculationCurrency` (`dbId`),
  CONSTRAINT `FK_mt99qrtcgh3oi7xhm0k4xv048` FOREIGN KEY (`CalculationPolicy_DBId`) REFERENCES `CalculationPolicy` (`DBId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CalculationPolicy_CalculationCurrency`
--

LOCK TABLES `CalculationPolicy_CalculationCurrency` WRITE;
/*!40000 ALTER TABLE `CalculationPolicy_CalculationCurrency` DISABLE KEYS */;
/*!40000 ALTER TABLE `CalculationPolicy_CalculationCurrency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ChangePassLog`
--

DROP TABLE IF EXISTS `ChangePassLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ChangePassLog` (
  `DBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `changeDate` datetime DEFAULT NULL,
  `changedField` varchar(255) DEFAULT NULL,
  `employee_EmployeeDBId` bigint(20) DEFAULT NULL,
  `modifier_EmployeeDBId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`DBId`),
  KEY `FK_fqfs499tfrh9dsq219vy6jgee` (`employee_EmployeeDBId`),
  KEY `FK_9ykmr0fkxop8k1q0jdf77mov0` (`modifier_EmployeeDBId`),
  CONSTRAINT `FK_9ykmr0fkxop8k1q0jdf77mov0` FOREIGN KEY (`modifier_EmployeeDBId`) REFERENCES `Employee` (`EmployeeDBId`),
  CONSTRAINT `FK_fqfs499tfrh9dsq219vy6jgee` FOREIGN KEY (`employee_EmployeeDBId`) REFERENCES `Employee` (`EmployeeDBId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ChangePassLog`
--

LOCK TABLES `ChangePassLog` WRITE;
/*!40000 ALTER TABLE `ChangePassLog` DISABLE KEYS */;
/*!40000 ALTER TABLE `ChangePassLog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ConnectionInfo`
--

DROP TABLE IF EXISTS `ConnectionInfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ConnectionInfo` (
  `DBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `dst` varchar(255) DEFAULT NULL,
  `failureCount` int(11) NOT NULL,
  `sid` int(11) DEFAULT NULL,
  `src` varchar(255) DEFAULT NULL,
  `successCount` int(11) NOT NULL,
  PRIMARY KEY (`DBId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ConnectionInfo`
--

LOCK TABLES `ConnectionInfo` WRITE;
/*!40000 ALTER TABLE `ConnectionInfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `ConnectionInfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Employee`
--

DROP TABLE IF EXISTS `Employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Employee` (
  `EmployeeDBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `currentGomrok` varchar(255) DEFAULT NULL,
  `gomrokZone` varchar(255) DEFAULT NULL,
  `melliCode` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  `phoneNumber` varchar(255) DEFAULT NULL,
  `secretKey` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`EmployeeDBId`),
  UNIQUE KEY `UK_1ndh1xg7odg4cvrlny2fv7q6o` (`melliCode`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Employee`
--

LOCK TABLES `Employee` WRITE;
/*!40000 ALTER TABLE `Employee` DISABLE KEYS */;
INSERT INTO `Employee` VALUES (1,'منطقه ويژه اقتصادي شهيد رجايي',NULL,'admin','مدیر','bahmanz',NULL,NULL,NULL);
/*!40000 ALTER TABLE `Employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Employee_Responsibility`
--

DROP TABLE IF EXISTS `Employee_Responsibility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Employee_Responsibility` (
  `Employee_EmployeeDBId` bigint(20) NOT NULL,
  `responsiblities_ResponsibilityDBId` bigint(20) NOT NULL,
  UNIQUE KEY `UK_ma1k6yhpqr7mqoocf7o2wq1vw` (`responsiblities_ResponsibilityDBId`),
  KEY `FK_ma1k6yhpqr7mqoocf7o2wq1vw` (`responsiblities_ResponsibilityDBId`),
  KEY `FK_b68sgw1qdl7159tvyb63gktwg` (`Employee_EmployeeDBId`),
  CONSTRAINT `FK_b68sgw1qdl7159tvyb63gktwg` FOREIGN KEY (`Employee_EmployeeDBId`) REFERENCES `Employee` (`EmployeeDBId`),
  CONSTRAINT `FK_ma1k6yhpqr7mqoocf7o2wq1vw` FOREIGN KEY (`responsiblities_ResponsibilityDBId`) REFERENCES `Responsibility` (`ResponsibilityDBId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Employee_Responsibility`
--

LOCK TABLES `Employee_Responsibility` WRITE;
/*!40000 ALTER TABLE `Employee_Responsibility` DISABLE KEYS */;
INSERT INTO `Employee_Responsibility` VALUES (1,1);
/*!40000 ALTER TABLE `Employee_Responsibility` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FeatureData`
--

DROP TABLE IF EXISTS `FeatureData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FeatureData` (
  `dbId` bigint(20) NOT NULL AUTO_INCREMENT,
  `beanClassName` varchar(255) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `ruleName` varchar(255) DEFAULT NULL,
  `andOr` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `secondFieldPath` longtext,
  PRIMARY KEY (`dbId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FeatureData`
--

LOCK TABLES `FeatureData` WRITE;
/*!40000 ALTER TABLE `FeatureData` DISABLE KEYS */;
/*!40000 ALTER TABLE `FeatureData` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FeatureData_FeatureValue`
--

DROP TABLE IF EXISTS `FeatureData_FeatureValue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FeatureData_FeatureValue` (
  `FeatureData_dbId` bigint(20) NOT NULL,
  `featureValues_dbId` bigint(20) NOT NULL,
  UNIQUE KEY `UK_7n4s575waxm7qax5u8rru9udj` (`featureValues_dbId`),
  KEY `FK_7n4s575waxm7qax5u8rru9udj` (`featureValues_dbId`),
  KEY `FK_1qcby7mln08agqfghwmdqstjw` (`FeatureData_dbId`),
  CONSTRAINT `FK_1qcby7mln08agqfghwmdqstjw` FOREIGN KEY (`FeatureData_dbId`) REFERENCES `FeatureData` (`dbId`),
  CONSTRAINT `FK_7n4s575waxm7qax5u8rru9udj` FOREIGN KEY (`featureValues_dbId`) REFERENCES `FeatureValue` (`dbId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FeatureData_FeatureValue`
--

LOCK TABLES `FeatureData_FeatureValue` WRITE;
/*!40000 ALTER TABLE `FeatureData_FeatureValue` DISABLE KEYS */;
/*!40000 ALTER TABLE `FeatureData_FeatureValue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FeatureValue`
--

DROP TABLE IF EXISTS `FeatureValue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FeatureValue` (
  `dbId` bigint(20) NOT NULL AUTO_INCREMENT,
  `beanClassName` varchar(255) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `ruleName` varchar(255) DEFAULT NULL,
  `comparator` int(11) DEFAULT NULL,
  `val` varchar(255) DEFAULT NULL,
  `sampleSet_dbId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`dbId`),
  KEY `FK_rxtu9mjcpn0gtm53q5g6l9cfg` (`sampleSet_dbId`),
  CONSTRAINT `FK_rxtu9mjcpn0gtm53q5g6l9cfg` FOREIGN KEY (`sampleSet_dbId`) REFERENCES `SampleSet` (`dbId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FeatureValue`
--

LOCK TABLES `FeatureValue` WRITE;
/*!40000 ALTER TABLE `FeatureValue` DISABLE KEYS */;
/*!40000 ALTER TABLE `FeatureValue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GomrokEvent`
--

DROP TABLE IF EXISTS `GomrokEvent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GomrokEvent` (
  `DBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `eventId` int(11) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `userId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`DBId`),
  KEY `IDX_date` (`date`),
  KEY `IDX_eventId` (`eventId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GomrokEvent`
--

LOCK TABLES `GomrokEvent` WRITE;
/*!40000 ALTER TABLE `GomrokEvent` DISABLE KEYS */;
/*!40000 ALTER TABLE `GomrokEvent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GomrokEvent_Parameter`
--

DROP TABLE IF EXISTS `GomrokEvent_Parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GomrokEvent_Parameter` (
  `GomrokEvent_DBId` bigint(20) NOT NULL,
  `Ids_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_c7lxdec3v77gqhoen9p8n9ktn` (`Ids_id`),
  KEY `FK_c7lxdec3v77gqhoen9p8n9ktn` (`Ids_id`),
  KEY `FK_7tacv7gxkvk9b8ti8po4a611g` (`GomrokEvent_DBId`),
  CONSTRAINT `FK_7tacv7gxkvk9b8ti8po4a611g` FOREIGN KEY (`GomrokEvent_DBId`) REFERENCES `GomrokEvent` (`DBId`),
  CONSTRAINT `FK_c7lxdec3v77gqhoen9p8n9ktn` FOREIGN KEY (`Ids_id`) REFERENCES `Parameter` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GomrokEvent_Parameter`
--

LOCK TABLES `GomrokEvent_Parameter` WRITE;
/*!40000 ALTER TABLE `GomrokEvent_Parameter` DISABLE KEYS */;
/*!40000 ALTER TABLE `GomrokEvent_Parameter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GomrokProperties`
--

DROP TABLE IF EXISTS `GomrokProperties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GomrokProperties` (
  `DBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`DBId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GomrokProperties`
--

LOCK TABLES `GomrokProperties` WRITE;
/*!40000 ALTER TABLE `GomrokProperties` DISABLE KEYS */;
INSERT INTO `GomrokProperties` VALUES (1,'000000','127.0.0.1','ﺱﻭییچ ﺲﻨﺗﺭ','http://center-swich-rajae:8080/Center-Swich-Rajae/'),(2,'50100','20.1.1.100','ﻢﻨﻄﻘﻫ ﻭیژﻩ ﺎﻘﺘﺻﺍﺩی ﺶﻫیﺩ ﺮﺟﺍیی','http://swich-rajae:8080/Swich-Rajae/'),(3,'30100','20.1.1.101','ﺏﺍﺯﺭگﺎﻧ','http://swich-bazargan:8080/Swich-Bazargan/'),(4,'10300','20.1.1.102','ﺖﻫﺭﺎﻧ','http://swich-tehran:8080/Swich-Tehran/'),(6,'50100','20.2.1.100','ﻢﻨﻄﻘﻫ ﻭیژﻩ ﺎﻘﺘﺻﺍﺩی ﺶﻫیﺩ ﺮﺟﺍیی','http://swich-rajae:8080/Swich-Rajae/'),(7,'30100','20.2.1.101','ﺏﺍﺯﺭگﺎﻧ','http://swich-bazargan:8080/Swich-Bazargan/'),(8,'10300','20.2.1.102','ﺖﻫﺭﺎﻧ','http://swich-tehran:8080/Swich-Tehran/'),(9,'50100','20.3.1.100','ﻭیژﻩ ﺎﻘﺘﺻﺍﺩی ﺶﻫیﺩ ﺮﺟﺍیی','http://swich-rajae:8080/Swich-Rajae/'),(10,'30100','20.3.1.101','ﺏﺍﺯﺭگﺎﻧ','http://swich-bazargan:8080/Swich-Bazargan/'),(11,'10300','20.3.1.102','ﺖﻫﺭﺎﻧ','http://swich-tehran:8080/Swich-Tehran/');
/*!40000 ALTER TABLE `GomrokProperties` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Parameter`
--

DROP TABLE IF EXISTS `Parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Parameter` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Parameter`
--

LOCK TABLES `Parameter` WRITE;
/*!40000 ALTER TABLE `Parameter` DISABLE KEYS */;
/*!40000 ALTER TABLE `Parameter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Responsibility`
--

DROP TABLE IF EXISTS `Responsibility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Responsibility` (
  `DTYPE` varchar(31) NOT NULL,
  `ResponsibilityDBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `end_date` datetime DEFAULT NULL,
  `gomrok` varchar(255) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `empl_EmployeeDBId` bigint(20) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `hazfKonande_EmployeeDBId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ResponsibilityDBId`),
  KEY `FK_58tc688dc9cjmf5vq3418u47n` (`empl_EmployeeDBId`),
  KEY `FK_4jqoupsl0b9oj3gwb6vou9nek` (`hazfKonande_EmployeeDBId`),
  CONSTRAINT `FK_4jqoupsl0b9oj3gwb6vou9nek` FOREIGN KEY (`hazfKonande_EmployeeDBId`) REFERENCES `Employee` (`EmployeeDBId`),
  CONSTRAINT `FK_58tc688dc9cjmf5vq3418u47n` FOREIGN KEY (`empl_EmployeeDBId`) REFERENCES `Employee` (`EmployeeDBId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Responsibility`
--

LOCK TABLES `Responsibility` WRITE;
/*!40000 ALTER TABLE `Responsibility` DISABLE KEYS */;
INSERT INTO `Responsibility` VALUES ('SuperUser',1,NULL,NULL,'2015-11-14 19:09:25',1,NULL,NULL);
/*!40000 ALTER TABLE `Responsibility` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Rule`
--

DROP TABLE IF EXISTS `Rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Rule` (
  `dbId` bigint(20) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `ruleName` varchar(255) DEFAULT NULL,
  `ruleServiceName` varchar(255) DEFAULT NULL,
  `src` varchar(255) DEFAULT NULL,
  `synced` tinyint(1) DEFAULT NULL,
  `uniqueId` bigint(20) DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `targetClass_dbId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`dbId`),
  KEY `uniqueId` (`uniqueId`),
  KEY `FK_gfr3hv915rvv1gmmnt4luk1bu` (`targetClass_dbId`),
  CONSTRAINT `FK_gfr3hv915rvv1gmmnt4luk1bu` FOREIGN KEY (`targetClass_dbId`) REFERENCES `RuleClass` (`dbId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Rule`
--

LOCK TABLES `Rule` WRITE;
/*!40000 ALTER TABLE `Rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `Rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RuleClass`
--

DROP TABLE IF EXISTS `RuleClass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RuleClass` (
  `dbId` bigint(20) NOT NULL AUTO_INCREMENT,
  `beanClassName` varchar(255) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `ruleName` varchar(255) DEFAULT NULL,
  `className` varchar(255) DEFAULT NULL,
  `fieldName` varchar(255) DEFAULT NULL,
  `hasValue` tinyint(1) DEFAULT NULL,
  `isCollection` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`dbId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RuleClass`
--

LOCK TABLES `RuleClass` WRITE;
/*!40000 ALTER TABLE `RuleClass` DISABLE KEYS */;
/*!40000 ALTER TABLE `RuleClass` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RuleClass_FeatureData`
--

DROP TABLE IF EXISTS `RuleClass_FeatureData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RuleClass_FeatureData` (
  `RuleClass_dbId` bigint(20) NOT NULL,
  `featureDatas_dbId` bigint(20) NOT NULL,
  UNIQUE KEY `UK_iilksit2qp20uvlpnrl79clag` (`featureDatas_dbId`),
  KEY `FK_iilksit2qp20uvlpnrl79clag` (`featureDatas_dbId`),
  KEY `FK_jsthvdy3py49ep9hbhe73coj2` (`RuleClass_dbId`),
  CONSTRAINT `FK_iilksit2qp20uvlpnrl79clag` FOREIGN KEY (`featureDatas_dbId`) REFERENCES `FeatureData` (`dbId`),
  CONSTRAINT `FK_jsthvdy3py49ep9hbhe73coj2` FOREIGN KEY (`RuleClass_dbId`) REFERENCES `RuleClass` (`dbId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RuleClass_FeatureData`
--

LOCK TABLES `RuleClass_FeatureData` WRITE;
/*!40000 ALTER TABLE `RuleClass_FeatureData` DISABLE KEYS */;
/*!40000 ALTER TABLE `RuleClass_FeatureData` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RuleClass_RuleClass`
--

DROP TABLE IF EXISTS `RuleClass_RuleClass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RuleClass_RuleClass` (
  `RuleClass_dbId` bigint(20) NOT NULL,
  `childs_dbId` bigint(20) NOT NULL,
  UNIQUE KEY `UK_cmlwcac0c25ldafn0g0txga9c` (`childs_dbId`),
  KEY `FK_cmlwcac0c25ldafn0g0txga9c` (`childs_dbId`),
  KEY `FK_gnjicetpjpd8sk7l0klg9miv7` (`RuleClass_dbId`),
  CONSTRAINT `FK_cmlwcac0c25ldafn0g0txga9c` FOREIGN KEY (`childs_dbId`) REFERENCES `RuleClass` (`dbId`),
  CONSTRAINT `FK_gnjicetpjpd8sk7l0klg9miv7` FOREIGN KEY (`RuleClass_dbId`) REFERENCES `RuleClass` (`dbId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RuleClass_RuleClass`
--

LOCK TABLES `RuleClass_RuleClass` WRITE;
/*!40000 ALTER TABLE `RuleClass_RuleClass` DISABLE KEYS */;
/*!40000 ALTER TABLE `RuleClass_RuleClass` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Rule_ServiceResult`
--

DROP TABLE IF EXISTS `Rule_ServiceResult`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Rule_ServiceResult` (
  `Rule_dbId` bigint(20) NOT NULL,
  `results_dbId` bigint(20) NOT NULL,
  UNIQUE KEY `UK_5i88hyw81q0o7bsks5xyvcgjh` (`results_dbId`),
  KEY `FK_5i88hyw81q0o7bsks5xyvcgjh` (`results_dbId`),
  KEY `FK_dbxyabos92e7iha18o12e1s6t` (`Rule_dbId`),
  CONSTRAINT `FK_5i88hyw81q0o7bsks5xyvcgjh` FOREIGN KEY (`results_dbId`) REFERENCES `ServiceResult` (`dbId`),
  CONSTRAINT `FK_dbxyabos92e7iha18o12e1s6t` FOREIGN KEY (`Rule_dbId`) REFERENCES `Rule` (`dbId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Rule_ServiceResult`
--

LOCK TABLES `Rule_ServiceResult` WRITE;
/*!40000 ALTER TABLE `Rule_ServiceResult` DISABLE KEYS */;
/*!40000 ALTER TABLE `Rule_ServiceResult` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Sample`
--

DROP TABLE IF EXISTS `Sample`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Sample` (
  `dbId` bigint(20) NOT NULL AUTO_INCREMENT,
  `isRegex` tinyint(1) DEFAULT NULL,
  `sample` varchar(255) DEFAULT NULL,
  `sampleSet_dbId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`dbId`),
  KEY `FK_k6iy1asngf7p9rqerrxdsn5ag` (`sampleSet_dbId`),
  CONSTRAINT `FK_k6iy1asngf7p9rqerrxdsn5ag` FOREIGN KEY (`sampleSet_dbId`) REFERENCES `SampleSet` (`dbId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Sample`
--

LOCK TABLES `Sample` WRITE;
/*!40000 ALTER TABLE `Sample` DISABLE KEYS */;
/*!40000 ALTER TABLE `Sample` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SampleSet`
--

DROP TABLE IF EXISTS `SampleSet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SampleSet` (
  `dbId` bigint(20) NOT NULL AUTO_INCREMENT,
  `deleted` tinyint(1) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `ruleServiceName` varchar(255) DEFAULT NULL,
  `src` varchar(255) DEFAULT NULL,
  `uniqueId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`dbId`),
  KEY `uniqueId` (`uniqueId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SampleSet`
--

LOCK TABLES `SampleSet` WRITE;
/*!40000 ALTER TABLE `SampleSet` DISABLE KEYS */;
/*!40000 ALTER TABLE `SampleSet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SampleSet_samplesSet`
--

DROP TABLE IF EXISTS `SampleSet_samplesSet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SampleSet_samplesSet` (
  `SampleSet_dbId` bigint(20) NOT NULL,
  `samplesSet` varchar(255) DEFAULT NULL,
  KEY `FK_nca4gh310ijteun34gcv4ewpr` (`SampleSet_dbId`),
  CONSTRAINT `FK_nca4gh310ijteun34gcv4ewpr` FOREIGN KEY (`SampleSet_dbId`) REFERENCES `SampleSet` (`dbId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SampleSet_samplesSet`
--

LOCK TABLES `SampleSet_samplesSet` WRITE;
/*!40000 ALTER TABLE `SampleSet_samplesSet` DISABLE KEYS */;
/*!40000 ALTER TABLE `SampleSet_samplesSet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ServiceResult`
--

DROP TABLE IF EXISTS `ServiceResult`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ServiceResult` (
  `dbId` bigint(20) NOT NULL AUTO_INCREMENT,
  `percent` int(11) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `val` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`dbId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ServiceResult`
--

LOCK TABLES `ServiceResult` WRITE;
/*!40000 ALTER TABLE `ServiceResult` DISABLE KEYS */;
/*!40000 ALTER TABLE `ServiceResult` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TidManager`
--

DROP TABLE IF EXISTS `TidManager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TidManager` (
  `dbId` bigint(20) NOT NULL AUTO_INCREMENT,
  `tid` bigint(20) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  PRIMARY KEY (`dbId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TidManager`
--

LOCK TABLES `TidManager` WRITE;
/*!40000 ALTER TABLE `TidManager` DISABLE KEYS */;
/*!40000 ALTER TABLE `TidManager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TransObj`
--

DROP TABLE IF EXISTS `TransObj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TransObj` (
  `DBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `direction` int(11) NOT NULL,
  `obj` longtext,
  `rcvTime` datetime DEFAULT NULL,
  `sid` int(11) DEFAULT NULL,
  `sndTime` datetime DEFAULT NULL,
  `tid` bigint(20) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`DBId`),
  KEY `TransObj_tid_Index` (`tid`),
  KEY `IDX_sid_direction_tid` (`sid`,`direction`,`tid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TransObj`
--

LOCK TABLES `TransObj` WRITE;
/*!40000 ALTER TABLE `TransObj` DISABLE KEYS */;
/*!40000 ALTER TABLE `TransObj` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `swichLib_Employee`
--

DROP TABLE IF EXISTS `swichLib_Employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `swichLib_Employee` (
  `EmployeeDBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `currentGomrok` varchar(255) DEFAULT NULL,
  `gomrokZone` varchar(255) DEFAULT NULL,
  `melliCode` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phoneNumber` varchar(255) DEFAULT NULL,
  `secretKey` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`EmployeeDBId`),
  UNIQUE KEY `UK_d2k9fr6uk19d0uv3422ssmlud` (`melliCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `swichLib_Employee`
--

LOCK TABLES `swichLib_Employee` WRITE;
/*!40000 ALTER TABLE `swichLib_Employee` DISABLE KEYS */;
/*!40000 ALTER TABLE `swichLib_Employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `swichLib_Employee_swichLib_Responsibility`
--

DROP TABLE IF EXISTS `swichLib_Employee_swichLib_Responsibility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `swichLib_Employee_swichLib_Responsibility` (
  `swichLib_Employee_EmployeeDBId` bigint(20) NOT NULL,
  `responsiblities_ResponsibilityDBId` bigint(20) NOT NULL,
  UNIQUE KEY `UK_7a112slt147eo39dy6cx8u9p6` (`responsiblities_ResponsibilityDBId`),
  KEY `FK_7a112slt147eo39dy6cx8u9p6` (`responsiblities_ResponsibilityDBId`),
  KEY `FK_asb1ijadrx7dn2p8wxg5xjj2b` (`swichLib_Employee_EmployeeDBId`),
  CONSTRAINT `FK_7a112slt147eo39dy6cx8u9p6` FOREIGN KEY (`responsiblities_ResponsibilityDBId`) REFERENCES `swichLib_Responsibility` (`ResponsibilityDBId`),
  CONSTRAINT `FK_asb1ijadrx7dn2p8wxg5xjj2b` FOREIGN KEY (`swichLib_Employee_EmployeeDBId`) REFERENCES `swichLib_Employee` (`EmployeeDBId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `swichLib_Employee_swichLib_Responsibility`
--

LOCK TABLES `swichLib_Employee_swichLib_Responsibility` WRITE;
/*!40000 ALTER TABLE `swichLib_Employee_swichLib_Responsibility` DISABLE KEYS */;
/*!40000 ALTER TABLE `swichLib_Employee_swichLib_Responsibility` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `swichLib_Responsibility`
--

DROP TABLE IF EXISTS `swichLib_Responsibility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `swichLib_Responsibility` (
  `DTYPE` varchar(31) NOT NULL,
  `ResponsibilityDBId` bigint(20) NOT NULL AUTO_INCREMENT,
  `end_date` datetime DEFAULT NULL,
  `gomrok` varchar(255) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `empl_EmployeeDBId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ResponsibilityDBId`),
  KEY `FK_k7mhp4qatif8mv564la2mf028` (`empl_EmployeeDBId`),
  CONSTRAINT `FK_k7mhp4qatif8mv564la2mf028` FOREIGN KEY (`empl_EmployeeDBId`) REFERENCES `swichLib_Employee` (`EmployeeDBId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `swichLib_Responsibility`
--

LOCK TABLES `swichLib_Responsibility` WRITE;
/*!40000 ALTER TABLE `swichLib_Responsibility` DISABLE KEYS */;
/*!40000 ALTER TABLE `swichLib_Responsibility` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-22  6:40:03
