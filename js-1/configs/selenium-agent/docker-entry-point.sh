#!/bin/bash
vncserver -httpport 6112  "$@"

# setup dummy printer
/usr/sbin/cupsd
lpadmin -p dummy -E -v file:///dev/null

firefox -marionette
