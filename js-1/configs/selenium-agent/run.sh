#!/bin/bash

name=test-agent0

docker rm -f $name
docker run -dit --name $name -p 9001:5901 -v $(pwd)/mozilla:/root/.mozilla/   test-agent
docker cp /js/0/seleniums/ test-agent0:/

docker exec -it test-agent0 python2 /run_selenium.py '/seleniums/Test_suits/varedat/suit'
