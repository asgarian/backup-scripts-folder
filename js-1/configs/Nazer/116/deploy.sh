#!/bin/bash
database_name=nazer
database_url=127.0.0.1
database_user=root
database_pass=mysql123sjb
#
cache_size="maxElementsInMemory=\"20000\""
cache_time="timeToIdleSeconds=\"50000\""
#
configurations[0]="swich_url=http://127.0.0.1:8080/Swich/"
configurations[1]="current_customs_code=30100"
configurations[2]="data_distribution_service_poller_time_interval=10"
#
#

sed -i 's/<property name="hibernate.connection.url">.*<\/property>/<property name="hibernate.connection.url">jdbc:mysql:\/\/'$database_url':3306\/'$database_name'?autoReconnect=true\&amp;useUnicode=true\&amp;characterEncoding=UTF-8\&amp;tcpKeepAlive=true<\/property>/g' WEB-INF/classes/hibernate.cfg.xml
sed -i 's/<property name="hibernate.connection.username">.*<\/property>/<property name="hibernate.connection.username">'$database_user'<\/property>/g' WEB-INF/classes/hibernate.cfg.xml
sed -i 's/<property name="hibernate.connection.password">.*<\/property>/<property name="hibernate.connection.password">'$database_pass'<\/property>/g' WEB-INF/classes/hibernate.cfg.xml
#
sed -i 's/maxElementsInMemory="20000"/'$cache_size'/g' WEB-INF/classes/ehcache.xml
sed -i 's/timeToIdleSeconds="50000"/'$cache_time'/g' WEB-INF/classes/ehcache.xml
#
printf "%s\n" "${configurations[@]}" > WEB-INF/classes/configurations.properties

. copy-to-webapps.sh $1

