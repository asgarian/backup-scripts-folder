docker run -d -p 5000:5000 --restart=always --name docker-registery -v /docker-registry:/var/lib/registry \
  -v `pwd`/certs:/certs \
  -e REGISTRY_HTTP_TLS_CERTIFICATE=certs/domain.crt \
  -e REGISTRY_HTTP_TLS_KEY=certs/domain.key \
  registry:2
