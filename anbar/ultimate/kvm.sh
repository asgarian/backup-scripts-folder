#!/bin/bash


#apt-get -y install qemu-kvm
#apt-get -y install libvirt-bin
#apt-get -y install ubuntu-vm-builder
#apt-get -y install bridge-utils
#apt-get -y install virtualbox

ok="KVM acceleration can be used"
a=$(kvm-ok | sed -n 2p)

if [[ $a != $ok ]] ; then
        echo "kvm not install"
        exit 1 
fi

echo -e "\t\t\t\t\t*******       kvm is ok        *******"
