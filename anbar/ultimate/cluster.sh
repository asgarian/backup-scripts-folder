#!/bin/bash

if [ -z "$3" ]; then
    echo "correct use: cluster.sh [cluster_name] [cluster_index] [command]"
    exit 1
fi

#
#define vars
#
export cluster_name="c$2"

export docker_hub_url="hub.docker.iais.co:5000"


export police_rajae_image="$docker_hub_url/police-rajae-$1-$2"
export anbar_rajae_image="$docker_hub_url/anbar-rajae-$1-$2"
export anbar_bazargan_image="$docker_hub_url/anbar-bazargan-$1-$2"
export anbar_tehran_image="$docker_hub_url/anbar-tehran-$1-$2"
export customs_rajae_image="$docker_hub_url/customs-rajae-$1-$2"
export baskool_rajae_image="$docker_hub_url/baskool-rajae-$1-$2"
export swich_rajae_image="$docker_hub_url/swich-rajae-$1-$2"
export acc_rajae_image="$docker_hub_url/acc-dockertest-$1-$2"
export center_rajae_image="$docker_hub_url/center-rajae-$1-$2"
export center_swich_rajae_image="$docker_hub_url/center-swich-rajae-$1-$2"
export center_apache_rajae_image="$docker_hub_url/center-apache-rajae"
export packinglist_rajae_image="$docker_hub_url/packinglist-rajae-$1-$2"
export nezarat_rajae_image="$docker_hub_url/nezarat-rajae-$1-$2"
export apache_rajae_image="$docker_hub_url/apache-rajae-$1-$2"
export mysql_rajae_image="$docker_hub_url/mysql-rajae"
export postgres_rajae_image="$docker_hub_url/postgres-rajae"
export tir_rajae_image="$docker_hub_url/tir-rajae-$1-$2"
export jbazbini_rajae_image="$docker_hub_url/jbazbini-rajae-$1-$2"
export acc_tax_image="$docker_hub_url/acc-taxtest-$1-$2"

export customs_bazargan_image="$docker_hub_url/customs-bazargan-$1-$2"
export baskool_bazargan_image="$docker_hub_url/baskool-bazargan-$1-$2"
export swich_bazargan_image="$docker_hub_url/swich-bazargan-$1-$2"
export mysql_bazargan_image="$docker_hub_url/mysql-bazargan"

export customs_tehran_image="$docker_hub_url/customs-tehran-$1-$2"
export baskool_tehran_image="$docker_hub_url/baskool-tehran-$1-$2"
export swich_tehran_image="$docker_hub_url/swich-tehran-$1-$2"
export mysql_tehran_image="$docker_hub_url/mysql-tehran"

export cassandra0_image="$docker_hub_url/cassandra0"
export cassandra1_image="$docker_hub_url/cassandra1"
export cassandra2_image="$docker_hub_url/cassandra2"

export anbar_rajae_port=$(( 8010  ))
export anbar_bazargan_port=$(( 8020  ))
export anbar_tehran_port=$(( 8030  ))
export customs_rajae_port=$(( 8000 + $2*100  ))
export baskool_rajae_port=$(( 8001 + $2*100  ))
export swich_rajae_port=$(( 8002 + $2*100  ))
export acc_rajae_port=$(( 8003 + $2*100  ))
export center_rajae_port=$(( 8004 + $2*100  ))
export center_swich_rajae_port=$(( 8005 + $2*100  ))
export center_apache_rajae_port=$(( 8006 + $2*100  ))
export apache_rajae_port=$(( 8080 + $2*100  ))
export tir_rajae_port=$(( 8007 + $2*100  ))
export jbazbini_rajae_port=$(( 8009 + $2*100  ))
export acc_tax_port=$(( 8040 + $2*100  ))

export customs_bazargan_port=$(( 8010 + $2*100  ))
export baskool_bazargan_port=$(( 8011 + $2*100  ))
export swich_bazargan_port=$(( 8012 + $2*100  ))

export customs_tehran_port=$(( 8020 + $2*100  ))
export baskool_tehran_port=$(( 8021 + $2*100  ))
export swich_tehran_port=$(( 8022 + $2*100  ))

export packinglist_rajae_port=$(( 8030 + $2*100  ))
export nezarat_rajae_port=$(( 8008 + $2*100  ))

export mysql_rajae_port=$(( 3000 + $2*100  ))
export mysql_bazargan_port=$(( 3010 + $2*100  ))
export mysql_tehran_port=$(( 3020 + $2*100  ))

export cassandra_thrift_port=$(( 8031 + $2*100  ))
export cassandra_cql_port=$(( 8032 + $2*100  ))


export postgres_rajae_port=$(( 5032 + $2*100  ))

export cluster_dir="/docker/cluster$2"
export mysql_rajae_data_dir="$cluster_dir/mysql-rajae"
export mysql_bazargan_data_dir="$cluster_dir/mysql-bazargan"
export mysql_tehran_data_dir="$cluster_dir/mysql-tehran"


export cluster_mysql_password="mysql123sjb"
export cluster_postgres_password="bahmanz"

export default_network_subnet="20.$(( $2+1  )).0.0/16"
export swich_rajae_internal_ip="20.$(( $2+1 )).1.100"
export swich_bazargan_internal_ip="20.$(( $2+1 )).1.101"
export swich_tehran_internal_ip="20.$(( $2+1 )).1.102"

export cassandra0_internal_ip="20.$(( $2+1 )).1.200"
export cassandra1_internal_ip="20.$(( $2+1 )).1.201"
export cassandra2_internal_ip="20.$(( $2+1 )).1.202"
export anbar_rajae_internal_ip="20.$(( $2+1 )).1.203"

export anbar_host_name="c-${1}.anbar.test.iais.co"


echo ""
echo ""
echo "customs_rajae_port        = $customs_rajae_port"
echo "baskool_rajae_port        = $baskool_rajae_port"
echo "swich_rajae_port          = $swich_rajae_port"
echo "acc_rajae_port            = $acc_rajae_port"
echo "center_rajae_port         = $center_rajae_port"
echo "center_swich_rajae_port   = $center_swich_rajae_port"
echo "center_apache_rajae__port = $center_apache_rajae_port"
echo "apache_rajae_port         = $apache_rajae_port"
echo ""
echo "customs_bazargan_port     = $customs_bazargan_port"
echo "baskool_bazargan_port     = $baskool_bazargan_port"
echo "swich_bazargan_port       = $swich_bazargan_port"
echo ""
echo "customs_tehran_port       = $customs_tehran_port"
echo "baskool_tehran_port       = $baskool_tehran_port"
echo "swich_tehran_port         = $swich_tehran_port"
echo ""
echo "packinglist_rajae_port    = $packinglist_rajae_port"
echo "nezarat_rajae_port        = $nezarat_rajae_port"
echo ""
echo "mysql_rajae_port          = $mysql_rajae_port"
echo "mysql_bazargan_port       = $mysql_bazargan_port"
echo "mysql_tehran_port         = $mysql_tehran_port"
echo ""
echo "postgres_rajae_port       = $postgres_rajae_port"
echo ""
echo "tir_rajae_port      	= $tir_rajae_port"
echo ""
echo "jbazbini_rajae_port       = $jbazbini_rajae_port"
echo ""
echo "acc-tax_port              = $acc_tax_port"
echo ""
echo "anbar_rajae_port          = $anbar_rajae_port"
echo ""
echo "anbar_bazargan_port       = $anbar_bazargan_port"
echo ""
echo "anbar_tehran_port         = $anbar_tehran_port"
echo ""


if [ $3 = "up" ]; then
    #
    #set anbar host name
    #
    ip="$(ifconfig | perl -nle 's/dr:(\S+)/print $1/e' | grep -E '172.16.111.*|172.17.193.*')"
    echo $ip
    curl -X POST host-name-setter:be84ccb3323e063e47632142e1a27e29@172.16.111.8:8080/job/Set-Host-Name/build --data-urlencode json='{"parameter":[{"name":"IP", "value":"'${ip}'"},{"name":"HOST_NAME", "value":"c-'$1'.test.iais.co"}]}'


    #
    #prepare mysql data
    #
    mkdir -p $cluster_dir
    if [ "$?" -ne 0 ]; then
        echo "Error: could not create $cluster_dir"
        exit 1
    fi

    if [[ $* == *-revert* ]]; then
        rm -rf $mysql_rajae_data_dir
        cp -r mysql-rajae $mysql_rajae_data_dir

        rm -rf $mysql_bazargan_data_dir
        cp -r mysql-bazargan $mysql_bazargan_data_dir

        rm -rf $mysql_tehran_data_dir
        cp -r mysql-tehran $mysql_tehran_data_dir

        docker-compose -p $cluster_name stop mysql-rajae
        docker-compose -p $cluster_name stop mysql-bazargan
        docker-compose -p $cluster_name stop mysql-tehran
    fi


    #
    #docker-compose up
    #
    docker-compose -p $cluster_name pull
    docker-compose -p $cluster_name up -d
    docker-compose -p $cluster_name restart center-apache-rajae
    docker-compose -p $cluster_name restart apache-rajae
    

    rm -rf /kvm/0/*
    docker cp c0_anbar-rajae_1:/kvm/Anbar-Rajae /kvm/0/Anbar-Rajae
    docker cp c0_anbar-bazargan_1:/kvm/Anbar-Bazargan /kvm/0/Anbar-Bazargan
    docker cp c0_anbar-tehran_1:/kvm/Anbar-Tehran /kvm/0/Anbar-Tehran
    docker cp c0_police-rajae_1:/kvm/Police-Rajae /kvm/0/Police-Rajae
    chmod 777 /kvm -R
    bash /kvm/0/Anbar-Rajae/kvm.sh
    bash /kvm/0/Anbar-Rajae/samba.sh
    bash /kvm/0/Anbar-Rajae/restore-snapshot.sh


elif [ $3 = "clear-logs"  ]; then
    truncate -s 0 /var/lib/docker/containers/*/*-json.log

elif [ $3 = "clear-containers" ]; then
    docker rm -f $(docker ps -a -q)
    docker network rm $(docker network ls -q)

elif [ $3 = "clear-images" ]; then
    docker rmi -f $(docker images -a -q)
elif [ $3 = "mysqls-up" ]; then
    docker run -dit -p $mysql_rajae_port:3306 -e MYSQL_ROOT_PASSWORD=$cluster_mysql_password --name mysql-rajae $mysql_rajae_image
    docker run -dit -p $mysql_bazargan_port:3306 -e MYSQL_ROOT_PASSWORD=$cluster_mysql_password --name mysql-bazargan $mysql_bazargan_image
    docker run -dit -p $mysql_tehran_port:3306 -e MYSQL_ROOT_PASSWORD=$cluster_mysql_password --name mysql-tehran $mysql_tehran_image



else
    docker-compose -p $cluster_name "${@:3}"
fi


