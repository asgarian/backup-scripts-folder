#!/bin/bash

VBoxManage controlvm anbar poweroff
VBoxManage snapshot anbar restore anb-snp
VBoxManage startvm anbar --type headless
sleep 20s
