#!/bin/bash

VBoxManage controlvm anbar1 poweroff
VBoxManage snapshot anbar1 restore anb-snp
VBoxManage startvm anbar1 --type headless
