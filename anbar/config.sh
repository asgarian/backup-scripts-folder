#!/bin/bash

args=("$@")
if [[ $# -le 1 ]] ; then
    echo -e 'follow the example:\nEx:  bash config.sh [web.config] [deploy-config] '
    exit 0
fi

##########################################################################################

as=$(awk '/<appSettings>/,/<\/appSettings>/' ${args[1]})
cs=$(awk '/<connectionStrings>/,/<\/connectionStrings>/' ${args[1]})
perl -p0e 's#\<appSettings\>.*?\<\/appSettings\>#'"$as"'#s' ${args[0]} > temp1
dos2unix temp1
perl -p0e 's#\<connectionStrings\>.*?\<\/connectionStrings\>#'"$cs"'#s' temp1 > ${args[0]}
dos2unix Web.config
rm temp1
