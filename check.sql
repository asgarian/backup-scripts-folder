#!/bin/bash

args=("$@")
if [[ $# -le 1 ]] ; then
    echo -e 'follow the example:\nEx:  bash checkbak.sh [path-to-file] [customs-ip] '
    exit 0
fi
#############      clear mysql   ######################
mysql -u root -pmysql123sjb -e "show databases">tem1
tr ' ' '\n' < tem1  > tem2
sed -i '/^\s*$/d' tem2
sed -i '/Database\|information_schema\|mysql\|performance_schema/d' tem2
chnull=$(cat tem2)
if [ -n "$chnull" ]; then
        for VAR in $(cat tem2)
        do
                mysql -u root -pmysql123sjb -e "drop database $VAR"
        done
fi
#######################################################
gunzip < /home/ut/check2013/ | mysql -u root -pmysql123sjb
exbak="/home/ut/chbak${args[1]}"
if [ -e "$exbak" ]
then
        tem3=$(cat /home/ut/chbak${args[1]})
        mysql -u root -pmysql123sjb -e "SELECT COUNT(*) FROM customs.Comment" |sed -n 2p > tem4
        if [[ $tem4 -le $tem3 ]] ; then
                echo "File is corrupt! "
				echo ${args[1]} >> failedlist
                rm tem*
				rm -rf /home/ut/check2013/*
                exit 1
        fi
fi
mysql -u root -pmysql123sjb -e "SELECT COUNT(*) FROM customs.Comment" |sed -n 2p > /home/ut/chbak${args[1]}
#mysql -u root -pmysql123sjb -e "SELECT COUNT(*) FROM tranzit.Comment" |sed -n 2p > /home/ut/chbak${args[1]}
echo "Test backup successfully executed"
echo ${args[1]} >> successlist
rm tem*
rm -rf /home/ut/check2013/*