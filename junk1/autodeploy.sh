        port=2022
        password="mkt123sjb"
fi
name=$4;
i=0
while read softwareValues
do
        softwares[$i]="$softwareValues"
        ((i++))
done < softwares
flag=0
for j in "${softwares[@]}"
do
        if [[ $1 == $j ]] ; then
                flag=1
                continue;
        fi
done
if [ $flag -eq 0 ] ; then
        echo "software name not accepted."
        printf "currect software names: "
        printf "%s " "${softwares[@]}"
        printf "\n\nFAILED\n\n"
        exit
fi
cat $1/customsnameswithurls | grep $3 > all_tmp/tmp_tmp_tmp_
echo "$1"
pwd
tmp_tmp_tmp_=""
read tmp_tmp_tmp_ < all_tmp/tmp_tmp_tmp_
rm all_tmp/tmp_tmp_tmp_
if [[ $tmp_tmp_tmp_ == "" ]] ; then
        cat customsnameswithurls | grep $3 > all_tmp/tmp_tmp_tmp_
        read tmp_tmp_tmp_ < all_tmp/tmp_tmp_tmp_
        rm all_tmp/tmp_tmp_tmp_
        if [[ $tmp_tmp_tmp_ == "" ]] ; then
                printf "no url matched for $3. please refer to customsnameswithurls."
                printf "\n\nFAILED\n\n"
                exit
        fi
fi
urlname=${tmp_tmp_tmp_#*=}
printf "\nplease wait...\n\n"

if [ "$name" == "default" ] ; then
        name=${urlname#*=}
        echo $name;
fi
customsurl=${urlname%%=*}
echo "url matched: $customsurl"
cd $1
if [ ! -f wars/$1$2.war ]; then
        printf "$1$2.war does not exist\n"
        printf "there is no $1 with the version specified."
        printf "\n\nFAILED\n\n"
        exit
fi
if [ ! -f configurations/$3.sh ]; then
        printf "there is no config file for $1 of $3."
        printf "\n\nFAILED\n\n"
        exit
fi
folder=wars/tmp_$RANDOM
if [ -d $folder ]; then
        printf "temporary folder is already in use."
        printf "\n\nFAILED\n\n"
        exit
else
        mkdir $folder
fi
cp configurations/$3.sh $folder/
cp prepare.sh $folder/
cp ../deploy.sh $folder/
cp wars/$1$2.war $folder/$name.war
cd $folder
sed '1a\oldIFS="$IFS"\nIFS="="\nwhile read name value\ndo\n\teval $name="$value"\ndone < '$3'.sh\nIFS="$oldIFS"' prepare.sh > prepared.sh
/bin/bash prepared.sh $name
sed -i 's/filename/'$name'/g' deploy.sh
echo "do you confirm? (Y/n)"
#read -s -n 1 yn
if [[ $yn == "n" ]] ; then
        cd ../../
        rm -r $1/$folder
        exit
fi
rsync  $name  $customsurl: -r -avz --delete
scp deploy.sh $customsurl:
if [[ $customsurl == tomcat* ]] ; then
        ssh  -n $customsurl "bash deploy.sh"
else
        ssh -n $customsurl "echo $password|sudo -S bash deploy.sh"
fi
cd ../../../
rm -r $1/$folder
printf "\n\nSCRIPT SUCCESSFULLY EXECUTED\n\n"