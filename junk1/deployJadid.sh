#!/bin/bash
/etc/init.d/tomcat stop
RA=`date +%s`
cp -r /tomcat/webapps/ROOT /tomcat/ROOT_$RA
rsync -avz deploy/ /tomcat/webapps/ROOT/ --delete
cd /tomcat/webapps/
rm -r  ../work/Catalina ../conf/Catalina
chown tomcat:tomcat /tomcat/* -R
/etc/init.d/tomcat start
