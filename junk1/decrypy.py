__author__ = 'aboozar'
from Crypto import Random
from Crypto.Cipher import AES
import base64
import argparse
import re
import getpass

PASSWORD='A13U8712VC%xcmnz'
INPUT='/home/mkt/abz/secrets.txt'
BAZBIN='/home/mkt/abz/bazbinisecrets.txt'
BAZUSER='/home/mkt/abz/morteza'
BS = 16
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
unpad = lambda s : s[0:-ord(s[-1])]

class AESCipher:
    def __init__( self, key ):
        self.key = key

    def encrypt( self, raw ):
        raw = pad(raw)
        iv = Random.new().read( AES.block_size )
        cipher = AES.new( self.key, AES.MODE_CBC, iv )
        return base64.b64encode( iv + cipher.encrypt( raw ) )

    def decrypt( self, enc ):
        enc = base64.b64decode(enc)
        iv = enc[:16]
        cipher = AES.new(self.key, AES.MODE_CBC, iv )
        return unpad(cipher.decrypt( enc[16:] ))

def main():
    try:
        f=open(INPUT,'r')
        lines=f.readlines()
        new_lines=[]
        for line in lines:
            if len(str(line).strip()) >2:
                new_lines.append(str(line).strip())
        f=open(INPUT,'w')
        for line in new_lines:
            f.write(line)
            if line!=new_lines[-1]:
                f.write("\n")
        f.close()
    except IOError:
        pass
    parser = argparse.ArgumentParser(description='get password for given host.')
    parser.add_argument('-s','--server', metavar='HOST',dest='host',help='the name of host')
    parser.add_argument('-m','--mode',metavar='MODE',dest='mode',default='dec',choices=['enc','dec'],help='mode of operation')
    parser.add_argument('-v','--verbose',action='count')
    parser.add_argument('-b','--bazbini',metavar='BAZBINI',dest='bazbini',help='used for morteza')
    args = parser.parse_args()
    if args.mode == 'dec':
        try:
            if args.host != None:
                if args.verbose != None:
                    print '*'*60
                    print "Entering Decryption mode for host : "+str(args.host)
                    print '*'*60
                host_found=False
                if args.bazbini == None:
                    f=open(INPUT,'r')
                    lines=f.readlines()
                    for line in lines:
                        if str(line).startswith(str(args.host).strip()):
                            host_found=True
                            aes=AESCipher(PASSWORD[:-len(str(args.host))]+str(args.host).strip())
                            data = aes.decrypt(str(line).split("====")[1].strip())
                            print data
                    if not host_found:
                        print "mkt123sjb"
                        if args.verbose != None:
                            print "sorry the required host does not exist!"
                else:
                    f=open(BAZBIN,'r')
                    lines=f.readlines()
                    for line in lines:
                        if str(line).startswith(str(args.host).strip()):
                            host_found=True
                            aes=AESCipher(PASSWORD[:-len(str(args.host))]+str(args.host).strip())
                            data = aes.decrypt(str(line).split("====")[1].strip())
                            fil=file(BAZUSER,'r')
                            lins=fil.readlines()
                            darichert='ai'
                            for li in lins:
                                if li.startswith(str(args.host).strip()):
                                    darichert=str(li.split("=")[1]).strip()
                            if darichert != 'ai':
                                print str(darichert)+"@"+str(data)
                            else:
                                print "ut@"+str(data)
                    if not host_found:
                        print "ut@mkt123sjb"

            else:
                print "In decryption mode -s is required!\nuser python decrypt.py -h for better understanding"
        except IOError:
            print "there is no host, first you should run this program in enc mode"
    elif args.mode == 'enc':
        if args.verbose != None:
            print '*'*60
            print "Entering Encryption mode"
            print '*'*60
        #host_code=raw_input("please enter codename for host: ")
        host_code=args.host
        comp=re.compile('^\d+$')
        if comp.match(host_code):
            aes=AESCipher(PASSWORD[:-len(str(host_code))]+str(host_code).strip())
            pw = getpass.unix_getpass("please enter password: ")
            encData=aes.encrypt(pw)
            try:
                if args.bazbini:
                    f=file(BAZBIN,'r')
                else:
                    f=file(INPUT,'r')
                host_found=False
                lines=f.readlines()
                new_lines=[]
                for line in lines:
                    if line.startswith((host_code).strip()):
                        host_found=True
                        new_line=str(host_code)+"===="+encData
                    else:
                        new_line=line
                    new_lines.append(new_line)
                if not host_found:
                    new_line=str(host_code)+"===="+encData
                    new_lines.append(new_line)
            except IOError:
                print "there is no host, first you should run this program in enc mode"
                new_lines=[]
            if args.bazbini:
                f=file(BAZBIN,'w')
            else:
                f=file(INPUT,'w')
            for line in new_lines:
                f.write(line+"\n")
            f.close()
        else:
            print 'wrong_host quitting...'

if __name__=="__main__":
    main()