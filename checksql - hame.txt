#!/bin/bash
cd /home/ut
args=("$@")
if [[ $# -le 1 ]] ; then
    echo -e 'follow the example:\nEx:  bash checkbak.sh [path-to-file] [customs-ip] '
    exit 0
fi
#############      clear mysql   ######################
mysql -u root -pmysql123sjb -e "show databases">tem1
tr ' ' '\n' < tem1  > tem2
sed -i '/^\s*$/d' tem2
sed -i '/Database\|information_schema\|mysql\|performance_schema/d' tem2
chnull=$(cat tem2)
if [ -n "$chnull" ]; then
        for VAR in $(cat tem2)
        do
                echo \`$VAR\`>tem40
                spchar=$(cat tem40)
                mysql -u root -pmysql123sjb -e "drop database $spchar"
        done
fi
#######################################################
gunzip < /mnt/Seagate/${args[0]} | mysql -u root -pmysql123sjb
exbak="/home/ut/chbak/${args[1]}"
if [ -e "$exbak" ]
then
        tem3=$(cat /home/ut/chbak/${args[1]})
        mysql -u root -pmysql123sjb -e "SELECT COUNT(*) FROM customs.Comment" || mysql -u root -pmysql123sjb -e "SELECT COUNT(*) FROM tranzit.Comment">tem49
		sed -n 2p tem49 > tem4
        if [[ $tem4 -le $tem3 ]] ; then
                echo "File is corrupt! "
                echo ${args[1]} >> failedlist
                rm tem*
                rm -rf /mnt/Seagate/*
                exit 0
        fi
fi
mysql -u root -pmysql123sjb -e "SELECT COUNT(*) FROM customs.Comment" || mysql -u root -pmysql123sjb -e "SELECT COUNT(*) FROM tranzit.Comment">tem44
sed -n 2p tem44 >/home/ut/chbak/${args[1]}
echo "Test backup successfully executed"
echo ${args[1]} >> successlist
rm tem*
rm -rf /mnt/Seagate/*
-----------------------------------------------------------------------------------------


#!/bin/bash
date -d "yesterday 13:00 " '+%Y-%m-%d' >tem10
yesterday=$(cat tem10)
cut -d '-' -f 1-100 --output-delimiter='/' tem10 >tem11
nyesterday=$(cat tem11)
#find /mnt/Seagate/*/*/$nyesterday -type f > tem12
find /mnt/Seagate/*/*/2016/05/18 -type f > tem12
sed '/diff\|inc\|postgres\|Warehouse\|bz2/d' tem12 > yesterday-files
cut -d '/' -f 4 yesterday-files > receivedlist
grep -v -f receivedlist defaultlist > notreceive
#############################
cut -d '/' -f 9 yesterday-files >tem30
i=1
for var in $(cat tem30)
        do
                file[$i]=$var
                i=$((i+1))
        done
#############################
cut -d '/' -f 4-5 yesterday-files >tem31
cut -d '/' -f 1-100 --output-delimiter='-' tem31 >tem32
k=1
for q in $(cat tem32)
        do
                name[$k]=$q
                k=$((k+1))
        done
####################     check .sql files    ################
ssh ut@172.17.0.98 rm successlist && rm failedlist
j=1
p=1
for f in $(cat yesterday-files)
    do
                scp $f ut@172.17.0.98:/mnt/Seagate/
                ssh ut@172.17.0.98 bash checkbak.sh ${file[$j]} ${name[$p]}
                j=$((j+1))
                p=$((p+1))
        done
############################################################
echo "#------------------------------------------------------------------------------#"
echo "#                             BACKUP REPORT                                    #"
echo "#------------------------------------------------------------------------------#"
echo "not receive :"
cat notreceive
#echo "successlist :"
#ssh ut@172.17.0.98 cat successlist
#echo "failedlist :"
#ssh ut@172.17.0.98 cat failedlist
rm tem*